<?

set_time_limit(0);
error_reporting(E_ALL);
ini_set("display_errors", 1);
error_reporting( E_ERROR );

# Константа для Include
define("CONST_RUFUS", true);
# Автоподгрузка классов
function __autoload($name)
{
	include("classes/_class233r." . $name . ".php");
}

# Класс конфига
$config = new config;
# Подключение к базе данных
$db = new db($config->HostDB, $config->UserDB, $config->PassDB, $config->BaseDB);

$db->Query("SELECT * FROM db_config WHERE id = 1 LIMIT 1");
$sonfig_site = $db->FetchArray();

$time = time();
if(date("H:i",$time) == "23:59"){
	# Обнуляем визиты
	$db->Query("TRUNCATE db_visits");
	$db->Query("UPDATE db_users_c SET refvisit_today = 0 WHERE refvisit_today > 0");
	# Наказываем неактивных
	$days = 45;
	$lost_time = time() - ($days * 24 * 60 * 60);
	$db->Query("UPDATE db_users_b SET a_t = 0, b_t = 0, c_t = 0, d_t = 0, e_t = 0, money_p = 0, money_b = 0, money_r = 0 WHERE id IN (SELECT id FROM db_users_a WHERE fake = 0 AND date_login <= {$lost_time})");
	# удаляем ненужные записи о просмотре рекламмы в серфинге
	$db->Query("DELETE FROM db_serfing_view WHERE time_add < DATE_SUB(NOW(), INTERVAL 2 DAY)");
}

# Обновление статистики сайта 
$c_date = date("Ymd", $time);
$c_date_begin = strtotime($c_date . " 00:00:00");
$online = $time - 60 * 60 * 5;
$tfstats = $time - 60 * 60 * 24;
$db->Query("
UPDATE db_stats SET
	all_users 		= (SELECT COUNT(*) FROM db_users_a),
	all_payments 	= (SELECT SUM(payment_sum) + (SELECT fake_payment FROM db_config WHERE id = 1) FROM db_users_b),
	all_insert 		= (SELECT SUM(insert_sum) + (SELECT fake_insert FROM db_config WHERE id = 1) FROM db_users_b),
	new_users		= (SELECT COUNT(*) FROM db_users_a WHERE date_reg > {$tfstats}),
	today_users		= (SELECT COUNT(*) FROM db_users_a WHERE date_reg > {$c_date_begin}),
	online_users	= (SELECT COUNT(*) FROM db_users_a WHERE date_login > {$online})
WHERE id = 1");

$cron_time = 60;
$rand = 50;

if($sonfig_site['fakecontrol_payment']) $script[] = "http://{$_SERVER['HTTP_HOST']}/__fake_payment.php?min={$sonfig_site['fc_payment_count_min']}&max={$sonfig_site['fc_payment_count_max']}";
if($sonfig_site['fakecontrol_users']) $script[] = "http://{$_SERVER['HTTP_HOST']}/__fake_user.php?min={$sonfig_site['fc_users_count_min']}&max={$sonfig_site['fc_users_count_max']}";
if($sonfig_site['fakecontrol_insert']) $script[] = "http://{$_SERVER['HTTP_HOST']}/__fake_insert.php?count={$sonfig_site['fc_insert_count']}&max={$sonfig_site['fc_insert_max']}";

foreach($script as $uri){
	$result = file_get_contents($uri);
	echo date("d.m.Y H:i:s",time()).": {$uri} :: {$result}<br/>";
	sleep(rand(1,floor($cron_time / count($script))));	
}

?>