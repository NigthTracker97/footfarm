<?php
define('TIME', time());
define('BASE_DIR', $_SERVER['DOCUMENT_ROOT']);
session_start();
if (!isset($_SESSION['user_id'])) {
    exit();
}

if (isset($_GET['cnt']) && isset($_SESSION['view']['id']) && isset($_SESSION['view']['timer']) && $_GET['cnt'] == $_SESSION['view']['cnt']) {
?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <link rel="stylesheet" href="/style/style-serf.css" type="text/css"/>
        <script type="text/javascript" src="/js/serfing.js?1"></script>
        <script type="text/javascript" language="JavaScript">
            var vtime = stattime = <?php echo $_SESSION['view']['timer']; ?>;
            var cnt = '<?php echo $_SESSION['view']['cnt']; ?>';
        </script>
    </head>
    <body>
    <script>
        function getHTTPRequest() {
            var req = false;
            try {
                req = new XMLHttpRequest();
            } catch (err) {
                try {
                    req = new ActiveXObject("MsXML2.XMLHTTP");
                } catch (err) {
                    try {
                        req = new ActiveXObject("Microsoft.XMLHTTP");
                    } catch (err) {
                        req = false;
                    }
                }
            }
            return req;
        }

        var defsummin = 1;
        function advevent(badv, buse) {
            document.getElementById("moder_button").innerHTML = '<img width="25" src="/img/loading2.gif"/>';
            var postc = '<?php echo $_SESSION['cnt']; ?>';
            var issend = true;
            if (buse == 3) issend = confirm("Обнулить счётчик просмотров ссылки №" + badv + "?");
            if (buse == 4) issend = confirm("Вы уверены что хотите удалить ссылку №" + badv + "?");
            if (issend)
                senddata(badv, buse, postc, 1);
            return true;
        }


        function senddata(radv, ruse, rpostc, rmode) {
            var myReq = getHTTPRequest();
            var params = "use=" + ruse + "&mode=" + rmode + "&adv=" + radv + "&cnt=" + rpostc;

            function setstate() {
                if ((myReq.readyState == 4) && (myReq.status == 200)) {
                    var resvalue = parseInt(myReq.responseText);
                    if (resvalue > 0) {
                        if (ruse == 1) {
                            document.getElementById("advimg" + radv).innerHTML = "<span class='serfcontrol-pause' title='Остановить показ рекламной площадки' onclick='javascript:advevent(" + radv + ",2);'></span>";
                        } else if (ruse == 2) {
                            document.getElementById("advimg" + radv).innerHTML = "<span class='serfcontrol-play' title='Запустить показ рекламной площадки' onclick='javascript:advevent(" + radv + ",1);'></span>";
                        } else if (ruse == 3) {
                            document.getElementById("erase" + radv).innerHTML = "0";
                        } else if (ruse == 4) {
                            $('#adv' + radv).fadeOut('def');
                        } else if (ruse == 5) {
                            if ((resvalue > 0) && (resvalue < 8))
                                document.getElementById("int" + radv).className = 'scon-speed-' + resvalue;
                        } else if (ruse == 6) {
                            document.getElementById("status" + radv).innerHTML = "<span class='desctext' style='text-decoration: blink;'>Ожидает<br />проверки</span>";
                            document.getElementById("advimg" + radv).innerHTML = "<span class='serfcontrol-postmoder'></span>";
                        } else if (ruse == 7) {
                            window.location.reload(true);
                        }
                        else if (ruse == 10) {
                            document.getElementById("moder_button").innerHTML = '<font color="green"><b>Ссылка одобрена!</b></font>';
                        }
                        else if (ruse == 11) {
                            document.getElementById("moder_button").innerHTML = '<font color="red"><b>Ссылка удалена!</b></font>';
                        }
                        else if (ruse >= 110) {
                            document.getElementById("moder_button").innerHTML = '<font color="red"><b>Ссылка отклонена!</b></font>';
                        }
                    }
                }
            }

            myReq.open("POST", "/ajax/us-advservice.php", true);
            myReq.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            myReq.setRequestHeader("Content-lenght", params.length);
            myReq.onreadystatechange = setstate;
            myReq.send(params);
            return false;
        }


    </script>

    <style>
        button{
            background: #efefef;
            border: 1px solid #707070;
            border-radius: 2px;
            color: #222;
            margin: 8px 0px 5px 0px;
            cursor: pointer;
            padding: 5px 10px;
            font-size: 7pt;
        }
        button[name=yes]{
            background: #589816;
            color: white;
        }
        button[name=no]{
            background: #e44c4c;
            color: white;
        }
        button[name=wrong]{
            background: #e87b1d;
            color: white;
        }
        button:hover{
            background: #009297;
            color: white;
        }
        img{
            border: none;
        }
    </style>

    <table class="serfframe" id="moder_form" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td>
                <div id="blockverify">
                    <div id="blockwait">
                        Подождите, сайт загружается...
                    </div>
                    <div id="blocktimer" style="display: none;">
                        <form class="clockalert" name="frm" method="post" action="" onsubmit="return false;">
                            <input name="clock" size="3" readonly="readonly" type="text" value=""/>
                            <br/>
                            <span>Дождитесь окончания таймера</span>
                        </form>
                    </div>
                </div>
            </td>
            <td align="right" class="footer" width="800">
                <div><?=$_SESSION['view']['url'];?></div>
                <div id="moder_button">
                    <button name="yes" onclick="javascript:advevent(<?php echo $_SESSION['view']['id']; ?>,10);">ОДОБРИТЬ</button>
                    <button name="wrong" onclick="javascript:advevent(<?php echo $_SESSION['view']['id']; ?>,111);">НЕ КОРРЕКТНАЯ ССЫЛКА</button>
                    <button name="wrong" onclick="javascript:advevent(<?php echo $_SESSION['view']['id']; ?>,112);">ЛОМАЕТ ТАЙМЕР</button>
                    <button name="wrong" onclick="javascript:advevent(<?php echo $_SESSION['view']['id']; ?>,113);">НРУШЕНИЕ ПРАВИЛ</button>
                    <button name="wrong" onclick="javascript:advevent(<?php echo $_SESSION['view']['id']; ?>,114);">НЕ ОТОБРАЖАЕТСЯ</button>
                    <button name="wrong" onclick="javascript:advevent(<?php echo $_SESSION['view']['id']; ?>,115);">ТЕКСТ ГАВНО</button>
                    <button name="no" onclick="javascript:advevent(<?php echo $_SESSION['view']['id']; ?>,11);">УДАЛИТЬ</button>
                </div>
            </td>
        </tr>
    </table>
    </body>
    </html>
    <?php
}
?>