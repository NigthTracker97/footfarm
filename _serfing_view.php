<?php
ini_set( 'session.cookie_domain', '.footfarm.com' );
session_set_cookie_params (0, '/', '.footfarm.com');

session_start();


if (empty($_SESSION['user_id'])) {
    exit('need auth');
}

# Автоподгрузка классов
function __autoload($name)
{
    include("classes/_class233r." . $name . ".php");
}

# Класс конфига
$config = new config;

# Функции
$func = new func;

# База данных
$db = new db($config->HostDB, $config->UserDB, $config->PassDB, $config->BaseDB);

# Настройки
$sonfig_site = $func->config_site($db);

$paymeny_money_type = array(
    '0' => 'сер.',
    '1' => 'сер.',
    '2' => 'руб.',
    '3' => 'CP'
);

define('BASE_DIR', $_SERVER['DOCUMENT_ROOT']);
define('TIME', time());
define('VALUTA', $paymeny_money_type[$sonfig_site['serf_money_type']]);

$db->Query('SET CHARACTER SET utf8');
	$db->Query("set names utf8");

$id = intval($_GET['view']);

if($sonfig_site['serf_moder'] == 1 || isset($_SESSION['admin'])){
    $db->query("SELECT * FROM db_serfing WHERE id = {$id} LIMIT 1");
    $moder = true;
}else{
    $time = time();
    $db->query("SELECT * FROM db_serfing WHERE id = '" . $id . "' and (money >= price OR unlimend > {$time}) and status = '2' LIMIT 1");
    $moder = false;
}

if ($db->NumRows()) {
    $result = $db->FetchAssoc();

	//$_SESSION['view']['cnt'] = md5($_COOKIE['__cfduid'] . $_SESSION['user_id']);
    $_SESSION['view']['cnt'] = md5(session_id() . $_SESSION['user_id'] . $result['id']);
    $_SESSION['view']['id'] = $result['id'];
	$_SESSION['view']['timer'] = $result['timer'];
	
	if(empty($result['timer'])) $_SESSION['view']['timer'] = 20;
    
    $_SESSION['view']['timestart'] = TIME;
    if($moder){
        $_SESSION['view']['timer'] = 3;
        $_SESSION['view']['url'] = $result['url'];
        $url = "/_moder_serf-fblock.php?cnt={$_SESSION['view']['cnt']}";
    }else{
        $url = "/_serf-fblock.php?cnt={$_SESSION['view']['cnt']}";
    }

    ?>
   
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html"/>
        <title>Просмотр сайтов</title>
        <meta name="robots" content="none"/>
		<link rel="stylesheet" href="/style/style-serf.css?<?=time();?>" type="text/css" />
		<script type="text/javascript" src="/js/serfing.js?14"></script>
	   <script type="text/javascript" language="JavaScript">
		   var vtime = stattime = <?php if(isset($_SESSION['view']['timer'])){echo $_SESSION['view']['timer'];}else{echo 30;} ?>;
		   var cnt = '<?php echo $_SESSION['view']['cnt']; ?>';
	   </script>
    </head>
	<iframe style="padding-bottom: 100px;" onLoad="startClock();"  src="<?php echo $result['url']; ?>" width="100%" height="100%" id="framesite" frameborder="0" scrolling="yes"></iframe>

	<?
		if($moder){
	?>
	<script>
        function getHTTPRequest() {
            var req = false;
            try {
                req = new XMLHttpRequest();
            } catch (err) {
                try {
                    req = new ActiveXObject("MsXML2.XMLHTTP");
                } catch (err) {
                    try {
                        req = new ActiveXObject("Microsoft.XMLHTTP");
                    } catch (err) {
                        req = false;
                    }
                }
            }
            return req;
        }

        var defsummin = 1;
        function advevent(badv, buse) {
            document.getElementById("moder_button").innerHTML = '<img width="25" src="/img/loading2.gif"/>';
            var postc = '<?php echo $_SESSION['cnt']; ?>';
            var issend = true;
            if (buse == 3) issend = confirm("Обнулить счётчик просмотров ссылки №" + badv + "?");
            if (buse == 4) issend = confirm("Вы уверены что хотите удалить ссылку №" + badv + "?");
            if (issend)
                senddata(badv, buse, postc, 1);
            return true;
        }


        function senddata(radv, ruse, rpostc, rmode) {
            var myReq = getHTTPRequest();
            var params = "use=" + ruse + "&mode=" + rmode + "&adv=" + radv + "&cnt=" + rpostc;

            function setstate() {
                if ((myReq.readyState == 4) && (myReq.status == 200)) {
                    var resvalue = parseInt(myReq.responseText);
                    if (resvalue > 0) {
                        if (ruse == 1) {
                            document.getElementById("advimg" + radv).innerHTML = "<span class='serfcontrol-pause' title='Остановить показ рекламной площадки' onclick='javascript:advevent(" + radv + ",2);'></span>";
                        } else if (ruse == 2) {
                            document.getElementById("advimg" + radv).innerHTML = "<span class='serfcontrol-play' title='Запустить показ рекламной площадки' onclick='javascript:advevent(" + radv + ",1);'></span>";
                        } else if (ruse == 3) {
                            document.getElementById("erase" + radv).innerHTML = "0";
                        } else if (ruse == 4) {
                            $('#adv' + radv).fadeOut('def');
                        } else if (ruse == 5) {
                            if ((resvalue > 0) && (resvalue < 8))
                                document.getElementById("int" + radv).className = 'scon-speed-' + resvalue;
                        } else if (ruse == 6) {
                            document.getElementById("status" + radv).innerHTML = "<span class='desctext' style='text-decoration: blink;'>Ожидает<br />проверки</span>";
                            document.getElementById("advimg" + radv).innerHTML = "<span class='serfcontrol-postmoder'></span>";
                        } else if (ruse == 7) {
                            window.location.reload(true);
                        }
                        else if (ruse == 10) {
                            document.getElementById("moder_button").innerHTML = '<font color="green"><b>Ссылка одобрена!</b></font>';
                        }
                        else if (ruse == 11) {
                            document.getElementById("moder_button").innerHTML = '<font color="red"><b>Ссылка удалена!</b></font>';
                        }
                        else if (ruse >= 110) {
                            document.getElementById("moder_button").innerHTML = '<font color="red"><b>Ссылка отклонена!</b></font>';
                        }
                    }
                }
            }

            myReq.open("POST", "/ajax/us-advservice.php", true);
            myReq.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            myReq.setRequestHeader("Content-lenght", params.length);
            myReq.onreadystatechange = setstate;
            myReq.send(params);
            return false;
        }


    </script>
	<style>
        button{
            background: #efefef;
            border: 1px solid #707070;
            border-radius: 2px;
            color: #222;
            margin: 8px 0px 5px 0px;
            cursor: pointer;
            padding: 5px 10px;
            font-size: 7pt;
        }
        button[name=yes]{
            background: #589816;
            color: white;
        }
        button[name=no]{
            background: #e44c4c;
            color: white;
        }
        button[name=wrong]{
            background: #e87b1d;
            color: white;
        }
        button:hover{
            background: #009297;
            color: white;
        }
        img{
            border: none;
        }
    </style>

    <table class="serfframe" style="position: absolute; bottom:0; height: 80px;" id="moder_form" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td>
                <div id="blockverify">
                    <div id="blockwait">
                        Подождите, сайт загружается...
                    </div>
                    <div id="blocktimer" style="display: none;">
                        <form class="clockalert" name="frm" method="post" action="" onsubmit="return false;">
                            <input name="clock" size="3" readonly="readonly" type="text" value=""/>
                            <br/>
                            <span>Дождитесь окончания таймера</span>
                        </form>
                    </div>
                </div>
            </td>
            <td align="right" class="footer" width="800">
                <div><?=$_SESSION['view']['url'];?></div>
                <div id="moder_button">
                    <button name="yes" onclick="javascript:advevent(<?php echo $_SESSION['view']['id']; ?>,10);">ОДОБРИТЬ</button>
                    <button name="wrong" onclick="javascript:advevent(<?php echo $_SESSION['view']['id']; ?>,111);">НЕ КОРРЕКТНАЯ ССЫЛКА</button>
                    <button name="wrong" onclick="javascript:advevent(<?php echo $_SESSION['view']['id']; ?>,112);">ЛОМАЕТ ТАЙМЕР</button>
                    <button name="wrong" onclick="javascript:advevent(<?php echo $_SESSION['view']['id']; ?>,113);">НРУШЕНИЕ ПРАВИЛ</button>
                    <button name="wrong" onclick="javascript:advevent(<?php echo $_SESSION['view']['id']; ?>,114);">НЕ ОТОБРАЖАЕТСЯ</button>
                    <button name="wrong" onclick="javascript:advevent(<?php echo $_SESSION['view']['id']; ?>,115);">ТЕКСТ ГАВНО</button>
                    <button name="no" onclick="javascript:advevent(<?php echo $_SESSION['view']['id']; ?>,11);">УДАЛИТЬ</button>
                </div>
            </td>
        </tr>
    </table>
	<?
		}else{
	?>
	<table class="serfframe" style="position: absolute; bottom:0; height: 80px;" border="0" cellspacing="0" cellpadding="0">
    <tr>
     <td>
	<div class="logo">
	 FootFarm
	</div>
      <div id="blockverify" style="width: 350px;margin: 0 auto;">
       <div id="blockwait">
        Подождите, сайт загружается...
       </div>
       <div id="blocktimer" style="display: none;">
        <form class="clockalert" name="frm" method="post" action="" onsubmit="return false;">
         <span>Дождитесь окончания таймера</span>
		 <input name="clock" size="3" readonly="readonly" type="text" value=""/>
        </form>
       </div>
      </div>
     </td>
    </tr>
   </table>
   <?
		}
	?>
    </html>
    <?php
} else {
    exit('Не существует или закончились просмотры');
}
?>

