<?
# јвтоподгрузка классов
function __autoload($name)
{
    include("../classes/_class233r." . $name . ".php");
}

#  ласс конфига 
$config = new config;

# ‘ункции
$func = new func;

# Ѕаза данных
$db = new db($config->HostDB, $config->UserDB, $config->PassDB, $config->BaseDB);

# Настройки
$sonfig_site = $func->config_site($db);

$db->Query("TRUNCATE TABLE `db_race_leader2`");
$db->Query("UPDATE db_users_b SET bonus_race_perc = 0 WHERE 1");

$bonus2 = array();
$bonus2[0] = 5;
$bonus2[1] = 4;
$bonus2[2] = 3;
$bonus2[3] = 2;
$bonus2[4] = 1;

$day = time() - 60 * 60 * 60 * 1;
$day7 = time() - 60 * 60 * 60 * 7;
$mounth = time() - 60 * 60 * 60 * 30;
$year = time() - 60 * 60 * 60 * 365;


$arr_1 = array();
$arr_2 = array();
$arr_3 = array();
$arr_4 = array();

function cmp($a, $b)
{
    return -($a["money"] - $b["money"]);
}

$bonus = array();

echo '-------------------- <br>';
echo 'Топ за 1 день <br>';
echo '-------------------- <br>';

$db->Query("SELECT user, user_id, money FROM db_insert_money WHERE date_add > $day");
while($data = $db->FetchArray()){
	$user_id = $data['user_id'];
	if(array_key_exists($user_id, $arr_1)){
		$arr_1[$user_id]['money'] += $data['money']; 
	}else{
		$arr_1[$user_id]['user_id'] = $data['user_id'];
		$arr_1[$user_id]['username'] = $data['user'];
		$arr_1[$user_id]['money'] = $data['money'];
	}
}

uasort($arr_1, "cmp");

$cnt = 0;
foreach($arr_1 as $user_id => $arr) {
	if($cnt < 5){
		$usname = $arr['username'];
		if(isset($bonus[$usname]['bonus'])){
			$bonus[$usname]['bonus'] += $bonus2[$cnt];
		}else{
			$bonus[$usname]['bonus'] = $bonus2[$cnt];
		}
		$db->Query("INSERT INTO db_race_leader2 (user, user_id, sum, perc, block) VALUES ('".$arr['username']."', ".$arr['user_id'].", '".$arr['money']."', ".$bonus2[$cnt].", 1)");
		$db->Query("UPDATE db_users_b SET bonus_race_perc = bonus_race_perc + ".$bonus2[$cnt]." WHERE id = ".$arr['user_id']);
		echo $user_id . ' - '. $arr['username'] . ' - ' . $arr['money'] . ' - ' . $bonus[$usname]['bonus'] . '<br>';
	}else{
		if($cnt > 9){
			break;
		}
		$db->Query("INSERT INTO db_race_leader2 (user, user_id, sum, perc, block) VALUES ('".$arr['username']."', ".$arr['user_id'].", '".$arr['money']."', 0, 1)");
	}
	$cnt++;
}

echo '<br>';
echo '-------------------- <br>';
echo 'Топ за 7 дней <br>';
echo '-------------------- <br>';

$db->Query("SELECT user, user_id, money FROM db_insert_money WHERE date_add > $day7");
while($data = $db->FetchArray()){
	$user_id = $data['user_id'];
	if(array_key_exists($user_id, $arr_2)){
		$arr_2[$user_id]['money'] += $data['money']; 
	}else{
		$arr_2[$user_id]['user_id'] = $data['user_id'];
		$arr_2[$user_id]['username'] = $data['user'];
		$arr_2[$user_id]['money'] = $data['money'];
	}
}

uasort($arr_2, "cmp");

$cnt = 0;
foreach($arr_2 as $user_id => $arr) {
	if($cnt < 5){
		$usname = $arr['username'];
		if(isset($bonus[$usname]['bonus'])){
			$bonus[$usname]['bonus'] += $bonus2[$cnt];
		}else{
			$bonus[$usname]['bonus'] = $bonus2[$cnt];
		}
		$db->Query("INSERT INTO db_race_leader2 (user, user_id, sum, perc, block) VALUES ('".$arr['username']."', ".$arr['user_id'].", '".$arr['money']."', ".$bonus2[$cnt].", 2)");
		$db->Query("UPDATE db_users_b SET bonus_race_perc = bonus_race_perc + ".$bonus2[$cnt]." WHERE id = ".$arr['user_id']);
		echo $user_id . ' - '. $arr['username'] . ' - ' . $arr['money'] . ' - ' . $bonus[$usname]['bonus'] . '<br>';
	}else{
		if($cnt > 9){
			break;
		}
		$db->Query("INSERT INTO db_race_leader2 (user, user_id, sum, perc, block) VALUES ('".$arr['username']."', ".$arr['user_id'].", '".$arr['money']."', 0, 2)");
	}
	$cnt++;
}

echo '<br>';
echo '-------------------- <br>';
echo 'Топ за месяц <br>';
echo '-------------------- <br>';

$db->Query("SELECT user, user_id, money FROM db_insert_money WHERE date_add > $mounth");
while($data = $db->FetchArray()){
	$user_id = $data['user_id'];
	if(array_key_exists($user_id, $arr_3)){
		$arr_3[$user_id]['money'] += $data['money']; 
	}else{
		$arr_3[$user_id]['user_id'] = $data['user_id'];
		$arr_3[$user_id]['username'] = $data['user'];
		$arr_3[$user_id]['money'] = $data['money'];
	}
}

uasort($arr_3, "cmp");

$cnt = 0;
foreach($arr_3 as $user_id => $arr) {
	if($cnt < 5){
		$usname = $arr['username'];
		if(isset($bonus[$usname]['bonus'])){
			$bonus[$usname]['bonus'] += $bonus2[$cnt];
		}else{
			$bonus[$usname]['bonus'] = $bonus2[$cnt];
		}
		$db->Query("INSERT INTO db_race_leader2 (user, user_id, sum, perc, block) VALUES ('".$arr['username']."', ".$arr['user_id'].", '".$arr['money']."', ".$bonus2[$cnt].", 3)");
		$db->Query("UPDATE db_users_b SET bonus_race_perc = bonus_race_perc + ".$bonus2[$cnt]." WHERE id = ".$arr['user_id']);
		echo $user_id . ' - '. $arr['username'] . ' - ' . $arr['money'] . ' - ' . $bonus[$usname]['bonus'] . '<br>';
	}else{
		if($cnt > 9){
			break;
		}
		$db->Query("INSERT INTO db_race_leader2 (user, user_id, sum, perc, block) VALUES ('".$arr['username']."', ".$arr['user_id'].", '".$arr['money']."', 0, 3)");
	}
	$cnt++;
}

echo '<br>';
echo '-------------------- <br>';
echo 'Топ за 365 дней <br>';
echo '-------------------- <br>';

$db->Query("SELECT user, user_id, money FROM db_insert_money WHERE date_add > $year");
while($data = $db->FetchArray()){
	$user_id = $data['user_id'];
	if(array_key_exists($user_id, $arr_4)){
		$arr_4[$user_id]['money'] += $data['money']; 
	}else{
		$arr_4[$user_id]['user_id'] = $data['user_id'];
		$arr_4[$user_id]['username'] = $data['user'];
		$arr_4[$user_id]['money'] = $data['money'];
	}
}

uasort($arr_4, "cmp");

$cnt = 0;
foreach($arr_4 as $user_id => $arr) {
	if($cnt < 5){
		$usname = $arr['username'];
		if(isset($bonus[$usname]['bonus'])){
			$bonus[$usname]['bonus'] += $bonus2[$cnt];
		}else{
			$bonus[$usname]['bonus'] = $bonus2[$cnt];
		}
		$db->Query("INSERT INTO db_race_leader2 (user, user_id, sum, perc, block) VALUES ('".$arr['username']."', ".$arr['user_id'].", '".$arr['money']."', ".$bonus2[$cnt].", 4)");
		$db->Query("UPDATE db_users_b SET bonus_race_perc = bonus_race_perc + ".$bonus2[$cnt]." WHERE id = ".$arr['user_id']);
		echo $user_id . ' - '. $arr['username'] . ' - ' . $arr['money'] . ' - ' . $bonus[$usname]['bonus'] . '<br>';
	}else{
		if($cnt > 9){
			break;
		}
		$db->Query("INSERT INTO db_race_leader2 (user, user_id, sum, perc, block) VALUES ('".$arr['username']."', ".$arr['user_id'].", '".$arr['money']."', 0, 4)");
	}
	$cnt++;
}

echo '<br>';
echo '-------------------- <br>';
exit();