<?php
session_start();

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

# јвтоподгрузка классов
function __autoload($name)
{
    include("../classes/_class233r." . $name . ".php");
}

#  ласс конфига
$config = new config;

# ‘ункции
$func = new func;

# Ѕаза данных
$db = new db($config->HostDB, $config->UserDB, $config->PassDB, $config->BaseDB);

exit($func->genPin($db));

