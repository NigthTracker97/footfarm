<?php
define('TIME', time());
define('BASE_DIR', $_SERVER['DOCUMENT_ROOT']);

//header("Content-type: text/html; charset=utf-8");

session_start();

$admin = false;
$moder = false;
$admin = $_SESSION['admin'];

if (!isset($_SESSION['user_id'])) { exit(); }

function __autoload($name){ include(BASE_DIR."/classes/_class233r.".$name.".php");}

$config = new config;

$db = new db($config->HostDB, $config->UserDB, $config->PassDB, $config->BaseDB);
$db->Query('SET CHARACTER SET utf8');
	$db->Query("set names utf8");
	
if (isset($_POST['cnt']) && $_POST['cnt'] == $_SESSION['cnt']){
  $user_name = $_SESSION['user'];
  $adv = isset($_POST['adv']) ? intval($_POST['adv']) : 0;
  $mode = isset($_POST['mode']) ? intval($_POST['mode']) : 0;
  $use = isset($_POST['use']) ? intval($_POST['use']) : 0;
  $type = strval($_POST['type']);
  $money = floatval($_POST['price']);

  if (!$adv && !$mode && !$use) exit('no1');

	$mcheck = false;
	if($use == 10) $mcheck = true;
	if($use == 11) $mcheck = true;
	if($use > 100 && $use < 200) $mcheck = true;
	if($mcheck){
		# Функции
		$func = new func;
		$conf = $func->config_site($db);
		if($conf['serf_moder'] == 1){
			$moder = true;
		}
	}

  if ($admin || $moder){
    $db->query("SELECT * FROM db_serfing WHERE id = '".$adv."'");
  }else{
    $db->query("SELECT * FROM db_serfing WHERE user_name = '".$user_name."' and id = '".$adv."'");
  }

  if (!$db->NumRows()) exit('no2');

  $result = $db->FetchAssoc();

  switch ($use){
    //запуск
    case 1:
    	if ($result['status'] == 3 && ($result['money'] >= $result['price'] || $result['unlimend'] > time())){
	      $db->query("UPDATE db_serfing SET status = '2' WHERE id = '".$adv."'");
	      exit('1');
	    }
	    break;

    //пауза
    case 2:
		if ($result['status'] == 2){
      		$db->query("UPDATE db_serfing SET status = '3' WHERE id = '".$adv."'");
  			exit('1');
    	}
     	break;

    //очистка просмотров
    case 3:
		if ($result['view'] > 0){
      		$db->query("UPDATE db_serfing SET view = '0' WHERE id = '".$adv."'");
        	exit('1');
    	}
    	break;

    //удаление
    case 4:
		if ($result['money'] > $result['price'] && !isset($_SESSION['admin'])) exit('no3');
		if ($mode == 2) exit();
		$db->query("DELETE FROM db_serfing WHERE id = '".$adv."'");
		$db->query("DELETE FROM db_serfing_view WHERE ident = '".$adv."'");
		exit('1');
		break;

    //скорость просмотров
    case 5:
		$speed = ($result['speed'] + 1) <= 7 ? $result['speed'] + 1 : 1;
		$db->query("UPDATE db_serfing SET speed = '".$speed."' WHERE id = '".$adv."'");
		exit(''.$speed.'');
		break;

    //отправка на модерацию
    case 6:

    	if ($result['status'] == 0){
      		$db->query("UPDATE db_serfing SET status = '1', error = 0 WHERE id = '".$adv."'");
			exit('1');
    	}
		break;

    //одобрение модером
    case 10:
		if ($result['status'] == 1){
      		$db->query("UPDATE db_serfing SET status = '2', error = 0 WHERE id = '".$adv."'"); //Было 3 хз зачем
			exit('1');
    	}
		break;

    //удаление модером
    case 11:
		$db->query("DELETE FROM db_serfing WHERE id = '".$adv."'");
    	$db->query("DELETE FROM db_serfing_view WHERE ident = '".$adv."'");
		exit('1');
        break;

    //пополнение баланса
    case 12:

		if($type == 'money'){
			if ($admin){
				$db->query("UPDATE db_serfing SET `money` = `money` + '".$money."' WHERE id = '".$adv."'");
				exit('OK');
			}else{
				$db->Query("
				INSERT INTO db_orders
						SET
							`user_id` 	= {$_SESSION['user_id']},
							`title` 	= 'Серфинг {$adv}',
							`money` 	= {$money},
							`desc` 		= 'Оплата серфинга пользователем {$_SESSION['user']} на сумму {$money} руб.',
							`date` 		= ".time().",
							`data`		= 'SERFING;{$adv}',
							`status` 	= 0
				");
				$order_id = $db->LastInsert();
				echo($order_id);
				exit;
			}
		}else{
			$func = new func;
			$sonfig_site = $func->config_site($db);
			switch($type){
				case 'unlim1d': $days = 1; $serftype = 'SERFUNLIM1D'; $money = $result['price'] * $sonfig_site['serf_unlim1d']; break;
				case 'unlim1w': $days = 7; $serftype = 'SERFUNLIM1W'; $money = $result['price'] * $sonfig_site['serf_unlim1w']; break;
				case 'unlim1m': $days = 30; $serftype = 'SERFUNLIM1M'; $money = $result['price'] * $sonfig_site['serf_unlim1m']; break;
				default: exit('Ошибка заказа счета');
			}
			$ttime = time();
			if($result['unlimend'] >= $ttime) $ttime = $result['unlimend'];
			$sunlimarr = array(
				'SERFUNLIM1D' => $ttime + (24 * 60 * 60),
				'SERFUNLIM1W' => $ttime + (24 * 60 * 60 * 7),
				'SERFUNLIM1M' => $ttime + (24 * 60 * 60 * 30)
			);
			if ($admin){
				$db->query("UPDATE db_serfing SET unlimend = {$sunlimarr[$serftype]} WHERE id = '".$adv."'");
				exit('OK');
			}else{
				$db->Query("
				INSERT INTO db_orders
						SET
							`user_id` 	= {$_SESSION['user_id']},
							`title` 	= 'Серфинг(Б) {$adv}',
							`money` 	= {$money},
							`desc` 		= 'Оплата серфинга (безлимит на {$days} д.) пользователем {$_SESSION['user']} на сумму {$money} руб.',
							`date` 		= ".time().",
							`data`		= '{$serftype};{$adv};',
							`status` 	= 0
				");
				$order_id = $db->LastInsert();
				echo($order_id);
				exit;
			}
			exit('no5');
		}
        break;

	  case 111:
		  $db->query("UPDATE db_serfing SET status = 0, error = 1 WHERE id = '".$adv."'");
		  exit('1');
		  break;//удаление модером

	  case 112:
		  $db->query("UPDATE db_serfing SET status = 0, error = 2 WHERE id = '".$adv."'");
		  exit('1');
		  break;
	  
	  case 113:
		  $db->query("UPDATE db_serfing SET status = 0, error = 3 WHERE id = '".$adv."'");
		  exit('1');
		  break;
	  
	  case 114:
		  $db->query("UPDATE db_serfing SET status = 0, error = 4 WHERE id = '".$adv."'");
		  exit('1');
		  break;

	  case 115:
		  $db->query("UPDATE db_serfing SET status = 0, error = 4 WHERE id = '".$adv."'");
		  exit('1');
		  break;

    default:
    	break;
  }
}

exit('Ошибка обработки данных');
?>