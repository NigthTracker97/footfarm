<?php
session_start();

if (!isset($_SESSION['user_id'])) { exit(); }

# Автоподгрузка классов
function __autoload($name){ include("../classes/_class233r.".$name.".php");}

# Класс конфига
$config = new config;

# Функции
$func = new func;

# База данных
$db = new db($config->HostDB, $config->UserDB, $config->PassDB, $config->BaseDB);

# Настройки
$sonfig_site = $func->config_site($db);

$db->Query('SET CHARACTER SET utf8');
	$db->Query("set names utf8");

$paymeny_money_type = array(
	'0' => 'сер.',
	'1' => 'сер.',
	'2' => 'руб.',
	'3' => 'CP'
);
$paymeny_money_type_purse = array(
	'0' => 'money_b',
	'1' => 'money_p',
	'2' => 'money_r',
	'3' => 'cash_points'
);

define('TIME', time());
define('VALUTA', $paymeny_money_type[$sonfig_site['serf_money_type']]);

function get_codek_ckick($dek){
	$codek[1] = array(
   		1 => '2',
	    2 => '6',
		3 => '3',
		4 => '4',
		5 => '5',
	    6 => '1',
		7 => '7',
		8 => '8'
	);

   	$codek[2] = array(
   		1 => '3',
	    2 => '2',
		3 => '8',
		4 => '4',
		5 => '5',
	    6 => '7',
		7 => '6',
		8 => '1'
	);

   	$codek[3] = array(
   		1 => '8',
	    2 => '2',
		3 => '4',
		4 => '7',
		5 => '5',
	    6 => '6',
		7 => '3',
		8 => '1'
	);
 	if (isset($codek[$dek])) return $codek[$dek];
  	return false;
}

if (isset($_POST['cnt']) && isset($_POST['num']) && isset($_SESSION['view']) && $_POST['cnt'] == $_SESSION['view']['cnt']){
  	$num = (int)$_POST['num'];
   	if ($num){
    	if (TIME - $_SESSION['view']['timestart'] < $_SESSION['view']['timer']) exit('err4');
     	$codek = get_codek_ckick($_SESSION['view']['codek_click']);
      	foreach ($codek as $k => $v){
      		if ($v == $num){
        		$num = $k;
        		break;
      		}
    	}
     	if ($num == $_SESSION['view']['captcha']){
			$time = time();
        	$db->query("SELECT id, move, url, price, unlimend FROM db_serfing WHERE id = {$_SESSION['view']['id']} AND (money >= price OR unlimend > {$time}) LIMIT 1");
         	if ($db->NumRows()){
        		$result = $db->FetchAssoc();
                $db->query("SELECT id FROM db_serfing_view WHERE ident = '".$_SESSION['view']['id']."' and user_id = '".$_SESSION['user_id']."' and time_add + INTERVAL 24*60*60 SECOND > NOW() LIMIT 1");
          		if ($db->NumRows()) exit('<div class="blockerror">ERROR!<br /><span>Not link 24 hours</span></div>');
                $move = ($result['move'] == 1) ? $result['url'] : 0;
                $id = $result['id'];
                if ($id != $_SESSION['view']['id']) exit('<div class="blockerror">ERROR!!!</div>');
                $price = $result['price'];

				$price = $price + ($price * ($sonfig_site['serf_bonus'] / 100));

				switch ($sonfig_site['serf_money_type']){
					case 0: $pay_user = number_format($price * $sonfig_site['ser_per_wmr'], 0); break;
					case 1: $pay_user = number_format($price * $sonfig_site['ser_per_wmr'], 0); ;break;
					case 2: $pay_user = number_format($price, 3); break;
					case 3: $pay_user = number_format($price, 3); break;
				}

				unset($_SESSION['view']);
				echo 'OK;'.$pay_user.';'.$move;

				# выплачиваем рефералу первого уровня
				//$referer1_money = $pay_user * ($sonfig_site['serf_ref1_persent'] / 100);

				# получает ид реферера 1го уровня
				//$db->Query("SELECT referer_id FROM db_users_a WHERE id = {$_SESSION['user_id']}");
				//$referer1_id = $db->FetchRow();

				# Записываем статистику пользователя (без учета реферальных рефереру)
				$db->Query("UPDATE db_users_c SET serf_money_p{$sonfig_site['serf_money_type']} = serf_money_p{$sonfig_site['serf_money_type']} + {$pay_user}, serf_count = serf_count + 1 WHERE id = {$_SESSION['user_id']}");

				# Записываем статистику пользователя
				//$db->Query("UPDATE db_users_c SET serf_money_p{$sonfig_site['serf_money_type']} = serf_money_p{$sonfig_site['serf_money_type']} + {$pay_user}, serf_to_ref1_p{$sonfig_site['serf_money_type']} = serf_to_ref1_p{$sonfig_site['serf_money_type']} + {$referer1_money}, serf_count = serf_count + 1 WHERE id = {$_SESSION['user_id']}");

				# Записываем статистику рефереру 1го уровня
				//$db->query("UPDATE db_users_c SET serf_from_ref1_p{$sonfig_site['serf_money_type']} = serf_from_ref1_p{$sonfig_site['serf_money_type']} + {$referer1_money} WHERE id = {$referer1_id}");

				# выплачиваем самому пользователю денюжку
				$db->Query("UPDATE db_users_b SET {$paymeny_money_type_purse[$sonfig_site['serf_money_type']]} = {$paymeny_money_type_purse[$sonfig_site['serf_money_type']]} + {$pay_user}, insert_sum = insert_sum + {$price} WHERE id = {$_SESSION['user_id']}");

				# выпачиваем рефререру 1го уровня
				//$db->query("UPDATE db_users_b SET {$paymeny_money_type_purse[$sonfig_site['serf_money_type']]} = {$paymeny_money_type_purse[$sonfig_site['serf_money_type']]} + {$referer1_money} WHERE id = {$referer1_id}");

				# записываем просмотр списываем бабло
				$db->Query("UPDATE db_serfing SET `view` = `view` + '1', `money` = `money` - '".$price."' WHERE id = '".$id."'");

				# записываем что просмотрена ссылка
				$db->Query("SELECT id, ident FROM db_serfing_view WHERE user_id = '".$_SESSION['user_id']."' and ident = '".$id."' LIMIT 1");
				if ($db->NumRows()){
					$result_view = $db->FetchAssoc();
						$db->Query("UPDATE db_serfing_view SET time_add = NOW() WHERE id = '".$result_view['id']."'");
				}else{
					$db->query("INSERT INTO db_serfing_view (`ident`, `time_add`, `user_id`) VALUES ('".$id."', NOW(), '".$_SESSION['user_id']."')");
				}

				exit();

      	}else{
        	exit('err1');
      	}
    }else{
      	exit('<div class="blockerror">ERROR!<br /><span>Неверный ответ</span></div>');
    }
}else if ($num == 0){

	if($sonfig_site['serf_moder'] == 1){
		exit("<b>Проверка ссылки прошла успешно!</b>");
	}
	if(isset($_SESSION['admin'])){
		exit("<b>Проверка ссылки прошла успешно!</b>");
	}

	$codek_new = rand(1, 3);
	$_SESSION['view']['codek_click'] = $codek_new;
    $codek = get_codek_ckick($codek_new);
    $rand = rand(1000000, 9999999);
    ?>
    <table class="clocktable">
     <tr>
      <td><img src="/captcha/captcha-st/captcha.php?sid=<?php echo $rand; ?>" alt="Проверка" style="margin: 0 10px 0 0;" /></td>
      <td nowrap="nowrap">
       <?php
       for($n = 1; $n<=8; $n++){
         if ($n == 5) echo '<br />';
         ?>
         <span class="serfnum" onclick="vernum(<?php echo $codek[$n] ?>);"><?php echo $n; ?></span>
         <?php
       }
       ?>
      </td>
     </tr>
    </table>
    <?php
}else{
    exit('err3');
}
}
else
{
  exit('err2');
}
?>