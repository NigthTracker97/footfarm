<?PHP
class config{
	# Настройки проекта проекта
	public $site_name = 'FootFarm.Org'; // Название проекта
	public $VAL = "Руб."; // Валюта
	public $adminKey = 484343523232; // ключ для разблокировки доступа к админке

	# База данных
	public $HostDB = "localhost"; // Хост БД
	public $UserDB = "root"; // логин от БД
	public $PassDB = "Ak2tXDPgSh"; // пароль от БД
	public $BaseDB = "foot"; // название БД
	
	# PAYEER настройки выплаты
	public $AccountNumber = 'P2068630'; // Аккаунт пайера
	public $apiId = '496372156'; // АПИ ИД для выплат
	public $apiKey = ''; // Секретный ключ, для выплат пайера
	
	# PAYEER настройки пополнения
	public $shopID = 496351173; // ИД магазина, для пополнений с пайера
	public $secretW = ""; // Секретное слово, для пополнений с пайера

	# FREEKASSA настройка пополнения
	public $fk_merchant_id = 68775;
	public $fk_merchant_sword = '';
	public $fk_merchant_sword2 = '';
	
	# FREE-KASSA WALLET
	public $fw_apiId = 'F103346828';
	public $fw_apiKey = '';
	
	# WEBMONEY настройка пополнения
	public $wm_wmid = 00000000;
	public $wm_purse_wmr = '00000000';
	public $wm_secret = '00000000';
	
	# YANDEX.деньги
	public $ya_purse = '00000000';
	public $ya_secret = '00000000';
	
	# Интеркасса (прием платежей)
	public $ik_shopId = '00000000';
	public $ik_shopKey = '00000000'; // тестовый KN3SX6gNDfqU5K1e
	
	# Интеркасса (API)
	public $ik_apiId = '00000000';
	public $ik_apiKey = '00000000';
	
	# Настройки ВКонтакте
	public $vk_group_id = 162192672; // ИД группы вконтакте
	public $vk_id = 6376914; // АПИ ИД приложения вконтакте
	public $vk_key = ''; // Ключ для приложения вконтакте
	public $vk_redirect_uri = "https://footfarm.com/vkauth.php"; // Страница для обработки данных вконатакте
	
	# reCaptcha
	public $re_site_key = '00000000-I7'; // РеКапча - ключ сайта
	public $re_secret_key = '00000000'; // Рекапча - секретный ключ
	
}
?>