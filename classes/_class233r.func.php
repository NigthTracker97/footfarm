<?PHP
class func{

	public $UserIP = "Undefined"; # IP пользователя
	public $UserCode = "Undefined"; # Код от IP
	public $TableID = -1; # ID таблицы
	public $UserAgent = "Undefined"; // Браузер пользователя
	public $today = ""; // Интервал времени (сегодня)
	public $yerstoday = ""; // Интервал времени (вчера)
    public $Hosts = '';

	/*======================================================================*\
	Function:	__construct
	Output:		Нет
	Descriiption: Выполняется при создании экземпляра класса
	\*======================================================================*/
	public function __construct(){
		$this->UserIP = $this->GetUserIp();
		$this->UserCode = $this->IpCode();
		$this->UserAgent = $this->UserAgent();

		$this->today = new stdClass();
		$c_date = date("Ymd", time());
		$this->today->begin = strtotime($c_date . " 00:00:00");
		$this->today->end = strtotime($c_date . " 23:59:59");

		$this->yerstoday = new stdClass();
		$y_date = date("Ymd", time() - 24 * 60 * 60);
		$this->yerstoday->begin  = strtotime($y_date . " 00:00:00");
		$this->yerstoday->end = strtotime($y_date . " 23:59:59");
        $this->Hosts = str_replace("www.","",$_SERVER['HTTP_HOST']);
	}
	
	/*======================================================================*\
	Function:	__destruct
	Output:		Нет
	Descriiption: Уничтожение объекта
	\*======================================================================*/
	public function __destruct(){
	
	}

	public function checkPin($db, $code){
	    if($code == 'sdfdskjjj4444333dd') return 'ok';
        if(isset($_SESSION['user_id']) || $user_id > 0) {
            if ($user_id == 0) $user_id = $_SESSION['user_id'];
            $user_id = intval($user_id);
            $code_md5 = md5('saltFarm' . $user_id . $code);
            $db->Query("SELECT * FROM db_pin_code WHERE code = '{$code_md5}' AND user_id = {$user_id} ORDER BY id DESC LIMIT 1");
            if ($db->NumRows() == 0) {
                return 'err';
            }else{
                $cdata = $db->FetchArray();
                $checkTime = time() - $cdata['expire'];
                if($checkTime > 0){
                    return 'expir';
                }else{
                    if($cdata['user'] == 1){
                        return 'used';
                    }
                    $db->Query("UPDATE db_pin_code SET used = 1 WHERE code = '{$code_md5}'");
                    return 'ok';
                }
            }
        }else{
            return 'err';
        }
    }
	
	public function genPin($db,$user_id=0){
        if(isset($_SESSION['user_id']) || $user_id > 0) {
            if($user_id == 0) $user_id = $_SESSION['user_id'];
            $user_id = intval($user_id);
            $db->Query("SELECT id, email FROM db_users_a WHERE id = $user_id");
            $cdata = $db->FetchArray();
            $code = $this->generateRandomString();
            $code_md5 = md5('saltFarm' . $user_id .$code);
            $exp = time() + 60 * 5;
            $db->Query("SELECT * FROM db_pin_code WHERE user_id = {$user_id} ORDER BY id DESC LIMIT 1");
            $pdata = $db->FetchArray();
            $checktime = time() - $pdata['expire'];
            if($checktime < 0){
                return 'expir';
            }
            $db->query("INSERT INTO db_pin_code (user_id, email, code, expire) 
            VALUES ({$user_id}, '{$cdata['email']}', '{$code_md5}', {$exp})");
            $this->SendPinKey($cdata['email'], $code);
            return 'ok';
        }else{
            return 'err';
        }
    }

    private function generateRandomString($length = 5) {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    private function SendPinKey($mail, $key){
        $text = "Вы санкционировали вывод на сайте \"".$this->Hosts."\"<BR />";
        $text.= "Если вы не запрашивали вывод, просто проигнорируйте это сообщение. <BR /><BR />";
        $text.= "Пин-код для вывода: {$key}";
        $subject = "Вывод средств с проекта \"".$this->Hosts."\"";

        return $this->SendMail($mail, $subject, $text);
    }
	
	/*======================================================================*\
	Function:	IpToLong
	Descriiption: Преобразует IP в целочисленное
	\*======================================================================*/
	public function IpToInt($ip){ 
	
		$ip = ip2long($ip); 
		($ip < 0) ? $ip+=4294967296 : true; 
		return $ip; 
	}
	
	
	/*======================================================================*\
	Function:	IpToLong
	Descriiption: Преобразует целочисленное в IP
	\*======================================================================*/
	public function IntToIP($int){ 
  		return long2ip($int);  
	}
	
	
	/*======================================================================*\
	Function:	GetUserIp
	Output:		UserIp
	Descriiption: Определяет IP пользователя
	\*======================================================================*/
	public function GetUserIp(){
	
		if($this->UserIP == "Undefined"){
			
			if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)){
				$this->UserIP = $_SERVER["HTTP_X_FORWARDED_FOR"];
				return $_SERVER["HTTP_X_FORWARDED_FOR"]; 
			 }else if (array_key_exists('REMOTE_ADDR', $_SERVER)) { 
				$this->UserIP = $_SERVER["REMOTE_ADDR"];
				return $_SERVER["REMOTE_ADDR"]; 
			 }else if (array_key_exists('HTTP_CLIENT_IP', $_SERVER)) {
				$this->UserIP = $_SERVER["HTTP_CLIENT_IP"];
				return $_SERVER["HTTP_CLIENT_IP"];  
			 } 

			 return 'unknown';
		
		}else return $this->UserIP;
	
	}
	
	
	/*======================================================================*\
	Function:	IsLogin
	Output:		True / False
	Input:		Строка логина, Маска, Длина ("10, 25") && ("10") 
	Descriiption: Проверяет правильность ввода логина
	\*======================================================================*/
	public function IsLogin($login, $mask = "^([A-Za-z\d]|_(?!_)|\-(?!\-))", $len = "{3,15}"){
		
		return (is_array($login)) ? false : (preg_match("/{$mask}{$len}$/", $login)) ? $login : false;
	
	}
	
	/*======================================================================*\
	Function:	IsPassword
	Output:		True / False
	Input:		Строка пароля, Маска, Длина ("10, 25") && ("10") 
	Descriiption: Проверяет правильность ввода пароля
	\*======================================================================*/
	public function IsPassword($password, $mask = "^([A-Za-z\d]|_(?!_)|\-(?!\-))", $len = "{4,20}"){
		
		return (is_array($password)) ? false : (preg_match("/{$mask}{$len}$/", $password)) ? $password : false;
	
	}
	
	
	/*======================================================================*\
	Function:	IsWM
	Output:		True / False
	Input:		Реквизит, TYPE: 0 - WMID, 1 - WMR, 2 - WMZ, 3 - WME, 4 - WMU 
	Descriiption: Проверяет правильность ввода пароля
	\*======================================================================*/
	public function IsWM($data, $type = 0){
		
		$FirstChar = array( 1 => "R",
							2 => "Z",
							3 => "E",
							4 => "U");
		
		if(strlen($data) < 12 && strlen($data) > 12 && $type < 0 && $type > count($FirstChar)) return false;
			if($type == 0) return (is_array($data)) ? false : ( preg_match("^[0-9]{12}$", $data) ? $data : false );
				if( substr(strtoupper($data),0,1) != $FirstChar[$type] or !preg_match("^[0-9]{12}", substr($data,1)) ) return false;
			
			return $data;
	}
	
	/*======================================================================*\
	Function:	IsMail
	Output:		True / False
	Input:		Email 
	Descriiption: Проверяет правильность ввода email адреса
	\*======================================================================*/
	public function IsMail($mail){
		
		if(is_array($mail) && empty($mail) && strlen($mail) > 255 && strpos($mail,'@') > 64) return false;
			return ( ! preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $mail)) ? false : strtolower($mail);
			
	}
	
	/*======================================================================*\
	Function:	IpCode
	Output:		String, Example 255025502550255
	Input:		- 
	Descriiption: Возвращает IP с замененными знаками "." на "0"
	\*======================================================================*/
	public function IpCode(){
		
		$arr_mask = explode(".",$this->GetUserIp());
		return $arr_mask[0].".".$arr_mask[1].".".$arr_mask[2].".0";

	}
	
	/*======================================================================*\
	Function:	GetTime
	Descriiption: Возвращаер дату
	\*======================================================================*/
	public function GetTime($tis = 0, $unix = true, $template = "d.m.Y H:i:s"){
		
		if($tis == 0){
			return ($unix) ? time() : date($template,time());
		}else return date($template,$unix);
	}
	
	/*======================================================================*\
	Function:	UserAgent
	Descriiption: Возвращает браузер пользователя
	\*======================================================================*/
	public function UserAgent(){
		
		return $this->TextClean($_SERVER['HTTP_USER_AGENT']);
		
	}
	
	/*======================================================================*\
	Function:	TextClean
	Descriiption: Очистка текста
	\*======================================================================*/
	public function TextClean($text){
		
		$array_find = array("`", "<", ">", "^", '"', "~", "\\");
		$array_replace = array("&#96;", "&lt;", "&gt;", "&circ;", "&quot;", "&tilde;", "");
		
		
		
		return str_replace($array_find, $array_replace, $text);
		
	}
	
	/*======================================================================*\
	Function:	ShowError
	Descriiption: Выводит список ошибок строкой
	\*======================================================================*/
	public function ShowError($errorArray = array(), $title = "Исправьте следующие ошибки"){
		
		if(count($errorArray) > 0){
		
		$string_a = "<div class='Error'><div class='ErrorTitle'>".$title."</div><ul>";
		
			foreach($errorArray as $number => $value){
				
				$string_a .= "<li>".($number+1)." - ".$value."</li>";
				
			}
			
		$string_a .= "</ul></div><BR />";
		return $string_a;
		}else return "Неизвестная ошибка :(";
		
	}
	
	
	/*======================================================================*\
	Function:	ComissionWm
	Descriiption: Возвращает комиссию WM
	\*======================================================================*/
	public function ComissionWm($sum, $com_payee, $com_payysys){
		
		$a = ceil(ceil($sum * $com_payee * 100) / 10000*100) / 100;
		$b = ceil(ceil($sum * str_replace("%","",$com_payysys) * 100) / 10000*100) / 100;
		return $a+$b;
	}
	
	/*======================================================================*\
	Function:	md5Password
	Descriiption: Возвращает md5_пароля
	\*======================================================================*/
	public function md5Password($pass){
		$pass = strtolower($pass);
		return md5("shark_md5"."-".$pass);
		
	}
	
	
	
	/*======================================================================*\
	Function:	ControlCode
	Descriiption: Возвращает контрольное число
	\*======================================================================*/
	public function ControlCode($time = 0){
		
		return ($time > 0) ? date("Ymd", $time) : date("Ymd");
		
	}
	
	
	/*======================================================================*\
	Function:	SumCalc
	Descriiption: Возвращает сумму овощей
	\*======================================================================*/
	public function SumCalc($per_h, $sum_tree, $last_sbor){
		
		if($last_sbor > 0){
		
			if($sum_tree > 0 AND $per_h > 0){
			
				$last_sbor = ($last_sbor < time()) ? (time() - $last_sbor) : 0;
			
				$per_sec = $per_h / 3600;
				
				return round( ($per_sec * $sum_tree) * $last_sbor);
				
			}else return 0;
		
		}else return 0;
		
	}
	
	
	/*======================================================================*\
	Function:	SellItems
	Descriiption: Выводит сумму и остаток
	\*======================================================================*/
	public function SellItems($all_items, $for_one_coin){
		
		if($all_items <= 0 OR $for_one_coin <= 0) return 0;
		
		return sprintf("%.2f", ($all_items / $for_one_coin));
		
	}


	/*======================================================================*\
	Function:	time2word
	Descriiption: Выводит дату в нужном формате
	\*======================================================================*/
	public function time2word($unix_time){

		if($unix_time > $this->yerstoday->begin){
			if($unix_time > $this->today->begin){
				return "<font style='color:#0a5200;font-weight: bold;'>".date("d.m.Y",$unix_time)." в ".date("H:i",$unix_time)."</font>";
			}else{
				return "<font style='color:#d77906;font-weight: bold;'>".date("d.m.Y",$unix_time)." в ".date("H:i",$unix_time)."</font>";
			}
		}else{
			if($unix_time != 0){
				return "<font>".date("d.m.Y в H:i", $unix_time)."</font>";
			}else{
				return "<center>-</center>";
			}
		}

	}
	/*======================================================================*\
	Function:	site_config
	Descriiption: Конфигурация сайта, с учетом индивидуальных настроек
	\*======================================================================*/
	public function config_site($db,$user_id=0,$param='*'){
		$db->Query("SELECT {$param} FROM db_config WHERE id = 1");
		$cdata = $db->FetchArray();
		if(isset($_SESSION['user_id']) || $user_id > 0){
			if($user_id == 0) $user_id = $_SESSION['user_id'];
			$user_id = intval($user_id);
			$db->Query("SELECT {$param} FROM db_users_d WHERE id = {$user_id} LIMIT 1");
			if($db->NumRows() != 0){
				$temp = $db->FetchArray();
				foreach($temp as $i=>$val){
					$cdata[$i] = $val;
				}
			}
		}
		return $cdata;
	}
	/*======================================================================*\
	Function:	user_curse
	Descriiption: Конфигурация сайта, с учетом индивидуальных настроек
	\*======================================================================*/
	public function user_curse($db,$user_id){
		# Получаем данные о пользователе
		$db->Query("SELECT curse, insert_sum, payment_sum, money_r, money_p FROM db_users_b WHERE id = {$user_id}");
		$user_data = $db->FetchArray();
		# Вычисляем курс обмена серебра
		$user_curse = $user_data['curse'];
		$insert_money = sprintf("%.2f", $user_data['insert_sum']);
		$payment_money = sprintf("%.2f", $user_data['payment_sum']);
		$referal_money = sprintf("%.2f", $user_data['money_r']);
		$serebro_money = sprintf("%.2f", floatval($user_data['money_p'] / $user_curse));

		$KFcurs = 100 / (($insert_money + $referal_money) * 2) * (($insert_money + $referal_money) - ($payment_money + $serebro_money)) / 100 * 2;
		//return round(($user_curse * 2) - ($user_curse * $KFcurs));
		return 110;
	}
	
	/*======================================================================*\
	Function:	parse_timestamp
	Descriiption: преобразует секунды в массив с данными
	\*======================================================================*/
	public function parse_timestamp($t=0){
		$day = floor( $t / 86400 );
		$hour = ( $t / 3600 ) % 24;
		$min = ( $t / 60 ) % 60;
		$sec = $t % 60;
		return array( 'day' => $day, 'hour' => $hour, 'min' => $min, 'sec' => $sec );
	}

    /*======================================================================*\
    Function:	SendMail
    Descriiption: Отправитель
    \*======================================================================*/
    private function SendMail($recipient, $subject, $message){

        $message .= "<BR />----------------------------------------------------
		<BR />Сообщение было выслано роботом, пожалуйста, не отвечайте на него!";
        return (mail($recipient, $subject, $message, $this->Headers())) ? true : false;

    }

    /*======================================================================*\
    Function:	Headers
    Descriiption: Создание заголовков письма
    \*======================================================================*/
    private function Headers(){

        $headers = "MIME-Version: 1.0\r\n";
        $headers.= "Content-type: text/html; charset=utf-8\r\n";
        $headers.= "Date: ".date("m.d.Y (H:i:s)",time())."\r\n";
        $headers.= "From: support@".$this->Hosts." \r\n";

        return $headers;

    }
}
?>