<?PHP

class insert{

    public $db;
    public $insert_data;

    public $user_id;
    public $user;
    public $user_curse;
    public $payment_sum;
    public $payment_bonus;

    public $ps_name;

    public $order_data;

    private $func;

    private $to_referer_all = 0;

    public function __construct($db){
        $this->db = $db;
        $this->func = new func;
    }

    public function go($insert_data, $ps_name, $bonus_persent=10){

        if(empty($insert_data)) return false;

        $this->insert_data = $insert_data;
        $this->ps_name = strval($ps_name);

        $user_id = intval($this->insert_data['user_id']);

        # Получаем данные о пользователе
        $this->db->Query("SELECT `id`, `user`, `curse`, `payment_sum` FROM db_users_b WHERE id = {$user_id} LIMIT 1");

        if($this->db->NumRows() == 0) return false;

        $user_ardata = $this->db->FetchArray();
        $this->user_id       = $user_ardata['id'];
        $this->user          = $user_ardata['user'];
        $this->user_curse    = $user_ardata['curse'];
        $this->payment_sum   = $user_ardata['payment_sum'];
		
        $this->payment_bonus = floatval($bonus_persent);
		
		$this->db->Query("SELECT `referer_id` FROM db_users_a WHERE id = {$this->user_id} LIMIT 1");

        $user_brdata = $this->db->FetchArray();
		$this->user_ref 	 = $user_brdata['referer_id'];
		

        # Забераем комиссию администрации
        $this->admin_comission();

        if($this->insert_data['order_id'] > 0){
            # Производим оплату выставленного счета
            $this->payment_order();
        }else{
            # Производим пополнение баланса
			if($this->insert_data['adv'] == 1){
				$this->insert_money($this->insert_data['sum'], true);
			}else{
				$this->insert_money($this->insert_data['sum']);
			}
        }

        return true;

    }

    private function order_data($order_id){
        if($this->user_id <= 0) return false;
        if($order_id <= 0) return false;
        $this->db->Query("SELECT * FROM db_orders WHERE id = {$order_id} AND user_id = {$this->user_id} AND status = 0 LIMIT 1");
        if($this->db->NumRows() == 1) {
            return $this->db->FetchArray();
        }else {
            return array();
        }
    }

    private function cloase_order(){
        if(empty($this->order_data)) return false;
        if($this->user_id <= 0) return false;
        $this->db->Query("UPDATE db_orders SET status = 1 WHERE id = {$this->order_data['id']} AND user_id = {$this->user_id} AND status = 0 LIMIT 1");
    }

    public function payment_order_ser($order_id,$user_id){
        # Получаем данные о счете
        $this->user_id = intval($user_id);
        $this->order_data = $this->order_data(intval($order_id));
        $type = explode(';',$this->order_data['data']);
        switch ($type[0]){
            # Оплата серфинга
            case 'SERFING':
                $this->db->query("UPDATE db_serfing SET money = money + {$this->order_data['money']} WHERE id = {$type[1]}");
                $this->cloase_order();
                return true;
            # По умолчанию
            default: return false;
        }
    }

    private function payment_order(){
        # Получаем данные о счете
        $this->order_data = $this->order_data($this->insert_data['order_id']);
        $type = explode(';',$this->order_data['data']);

        switch ($type[0]){
            # Оплата серфинга
            case 'SERFING':
                $this->payment_serfing($type[1]);
                $this->cloase_order();
                break;
            case 'SERFUNLIM1D':
                $this->payment_serfing_unlim(1,$type[1]);
                $this->cloase_order();
                break;
            case 'SERFUNLIM1W':
                $this->payment_serfing_unlim(7,$type[1]);
                $this->cloase_order();
                break;
            case 'SERFUNLIM1M':
                $this->payment_serfing_unlim(30,$type[1]);
                $this->cloase_order();
                break;
            # По умолчанию выполняем как обычное пополнение баланса
            default: insert_money($this->insert_data['sum']); break;
        }
    }

    private function payment_serfing($serf_id){
        $this->db->query("UPDATE db_serfing SET money = money + {$this->order_data['money']} WHERE id = {$serf_id}");
        $this->add_insert($this->order_data['money'],0,$this->order_data['title']);
        $this->add_stats($this->order_data['money']);
        return true;
    }

    private function payment_serfing_unlim($hour,$serf_id){
        # получаем данные о серфинге
        $this->db->query("SELECT unlimend FROM db_serfing WHERE id = {$serf_id} LIMIT 1");
        if($this->db->NumRows() == 0) return false;
        $serfdata = $this->db->FetchAssoc();
        $ttime = time();
        if($serfdata['unlimend'] >= $ttime) $ttime = $serfdata['unlimend'];
        $ttime = $ttime + 24 * 60 * 60 * intval($hour);
        $this->db->query("UPDATE db_serfing SET unlimend = {$ttime} WHERE id = {$serf_id}");
        $this->add_insert($this->order_data['money'],0,$this->order_data['title']);
        $this->add_stats($this->order_data['money']);
        return true;
    }

    private function add_insert($money,$serebro,$desc){
        $da = time();
        $dd = $da + 60 * 60 * 24 * 15;
        $this->db->Query("
              INSERT INTO db_insert_money 
              (`user`, `user_id`, `ps` , `money`, `serebro`, `date_add`, `date_del`, `desc`) 
              VALUES ('{$this->user}','{$this->user_id}','{$this->ps_name}','{$money}','{$serebro}','{$da}','{$dd}','{$desc}')
        ");
        return true;
    }

    private function add_stats($money){
        $this->db->Query("UPDATE db_stats SET all_insert = all_insert + {$money} WHERE id = 1");
    }

    private function competition($money){
        $competition = new competition($this->db);
        $competition->UpdatePoints($this->user_id, $money);
    }

    private function admin_comission(){
        $this->db->Query("SELECT * FROM db_admins");
        if($this->db->NumRows() > 0){
            $adminKomis = 0;
            $admins = array();
            while($adm = $this->db->FetchArray()){
                $admins[] = $adm;
            }
            foreach($admins as $data){
                $adminKomisOne = round($this->insert_data['sum'] * ($data['persent'] / 100), 2);
                $this->db->Query("UPDATE db_admins SET money = money + {$adminKomisOne}, payment_sum = payment_sum + {$adminKomisOne} WHERE id = {$data['id']}");
                $adminKomis += $adminKomisOne;
            }
            $this->db->Query("UPDATE db_stats SET admin_money = admin_money + {$adminKomis}, all_admin = all_admin + {$adminKomis} WHERE id = 1");
        }
    }

    private function wmset($sonfig_site,$money){
        $wmset = new wmset($sonfig_site);
        $marray = $wmset->GetSet($money);
        $birds_array = array(
            't_e'   => 'e_t',
            't_d'   => 'd_t',
            't_c'   => 'c_t',
            't_b'   => 'b_t',
            't_a'   => 'a_t',
            'wheel' => 'wheel'
        );
        # Зачисляем бонусы сета при пополнении
        foreach ($marray AS $birds => $val) {
            $this->db->Query("UPDATE db_users_b SET {$birds_array[$birds]} = {$birds_array[$birds]} + {$val} WHERE id = {$this->user_id}");
        }
    }

    private function referals_money($money){
        # Зачисляем денюжки рефералам
        $this->db->Query("SELECT * FROM db_users_c WHERE id = {$this->user_id} LIMIT 1");
        $user_referals = $this->db->FetchArray();

        $to_referer = array();

        $sonfig_site = $this->func->config_site($this->db);

        for ($i = 1; $i <= 2; $i++) {
            $ref_id = $user_referals['referer' . $i . '_id'];
            if ($ref_id > 0) {
                $psonfig_site = $this->func->config_site($this->db, $ref_id);
                $ref_money = $money * ($psonfig_site['ref' . $i . '_pers'] / 100);
				//$ref_money = 0;
                $ref_serebro = $ref_money * $psonfig_site["ser_per_wmr"];
                $cash_points = $money * ($psonfig_site['ref' . $i . '_cp'] / 100);
                $this->to_referer_all += $ref_money;

                //if($psonfig_site['payment_limit'] == 2){
                //    switch ($psonfig_site['ref_mtype']){
                //        case 0; $ref_serebro = $ref_serebro / 2; $ref_money = $ref_money / 2; break;
                //        case 1; $ref_money = 0; break;
                //        case 2; $ref_serebro = 0; break;
                //    }
                //}
				
				//$ref_money = 0;

                $to_referer[$i] = $ref_money;
                $to_referer_ser[$i] = $ref_serebro;

                # Зачисление вознаграждения рефералу
                $this->db->Query("UPDATE db_users_b SET money_p = money_p + {$ref_serebro}, cash_points = cash_points + {$cash_points}, from_referals = from_referals + {$ref_money} WHERE id = {$ref_id}");
                # Таблица статистики пользвоателя, чьим рефералом является пополнитель
                $this->db->Query("UPDATE db_users_c SET from_referals{$i} = from_referals{$i} + {$ref_money}, from_referals{$i}_ser = from_referals{$i}_ser + {$ref_serebro} WHERE id = {$ref_id}");
            }
        }
        $sql_string = '';
        foreach ($to_referer as $level => $money) {
            $sql_string .= "to_referer{$level} = to_referer{$level} + {$money},";
        }
        foreach ($to_referer_ser as $level => $money) {
            $sql_string .= "to_referer{$level}_ser = to_referer{$level}_ser + {$money},";
        }
        if ($sql_string != '') $this->db->Query("UPDATE db_users_c SET " . substr($sql_string, 0, -1) . " WHERE id = {$this->user_id}");
    }

    private function cp_from_active($sonfig_site,$money){
        $dateactive = time() - ($sonfig_site['time_cp2active'] * 60 * 60);
        $this->db->Query("SELECT COUNT(id) FROM db_users_a WHERE date_login > {$dateactive} AND id != {$this->user_id} AND `fake` = 0");
        $count2active = $this->db->FetchRow();
        if($count2active > 0){
            $money2acive = floor((($money * ($sonfig_site['persent_cp2active'] / 100)) / $count2active) * 100000) / 100000;
            $this->db->Query("UPDATE db_users_b SET cash_points = cash_points + {$money2acive} WHERE id IN(SELECT id FROM db_users_a WHERE date_login > {$dateactive} AND id != {$this->user_id} AND fake = 0)");
        }
    }

    private function insert_money($money, $adv = false){
        # Получаем настройки системы
        $sonfig_site = $this->func->config_site($this->db);

        # Серебро пользователя
        $serebro = sprintf("%.4f", floatval($sonfig_site["ser_per_wmr"] * $money));

        # Зачисляем положенные доп бунсы при пополнении
        //$this->wmset($sonfig_site,$money);

        # Зачисляем реферальные
        $this->referals_money($money);

        # Вычисляем полагающиеся пользвоателю платежные баллы
        $psonfig_site = $this->func->config_site($this->db, $this->user_id);
        $cash_points = $money * ($psonfig_site['user_cash_points'] / 100);

        # Проверяем первое ли пополнение
        /*$this->db->Query("SELECT COUNT(id) FROM db_insert_money WHERE user_id = {$this->user_id}");
        if($this->db->FetchRow() == 0){
            $user_serebro = $serebro + ($serebro * ($sonfig_site['first_payment_pers'] / 100));
        }else{
            $user_serebro = $serebro;
        }*/

        if($this->payment_bonus > 0){
            $user_serebro = $serebro + ($serebro * ($this->payment_bonus / 100));
		}else{
			$user_serebro = $serebro;
		}

        # Обнуляем курс, если выбрана такая опция
        switch ($sonfig_site['null_curse']){
            case 1:     $curse = $sonfig_site['ser_per_wmr']; break;
            case 2:     $curse = $this->user_curse / 2; break;
            case 3:     $curse = $this->user_curse / 4; break;
            case 4:     $curse = $this->user_curse / 8; break;
            case 5:     $curse = $this->user_curse - floor(abs($money / ($this->payment_sum + 1) * $curse = $this->user_curse)); break;
            default:    $curse = $this->user_curse;
        }
        if($curse < $sonfig_site['ser_per_wmr']){
            $sqlcurse = "curse = {$sonfig_site['ser_per_wmr']},";
        }else{
            $sqlcurse = "curse = {$curse},";
        }

        # Обновляем данные пользователя
        $lsb = time();
		if($adv){
			$this->db->Query("UPDATE db_users_b SET {$sqlcurse} money_r = money_r + '{$money}', to_referer = to_referer + {$this->to_referer_all} WHERE id = '{$this->user_id}'");
		}else{
			$this->db->Query("UPDATE db_users_b SET {$sqlcurse} money_b = money_b + '{$user_serebro}', to_referer = to_referer + {$this->to_referer_all}, last_sbor = '{$lsb}', insert_sum = insert_sum + '{$money}', cash_points = cash_points + {$cash_points} WHERE id = '{$this->user_id}'");
		}
		
		if($this->user_ref > 1){
			$ref = $this->user_ref;
			$this->db->Query("
				  INSERT INTO db_pay_ref
				  (`user`, `user_id`, `sum` , `ref`, `data`) 
				  VALUES ('{$this->user}','{$this->user_id}','{$money}','{$ref}','{$lsb}')
			");
		}
		
        # Добавляем платежные балы активным пользователям
        $this->cp_from_active($sonfig_site,$money);

        # Записываем данные о пополнении
        $this->add_insert($this->insert_data['sum'],$user_serebro,"Пополнение баланса");

        # Обновляем статистику в конкурсе рефералов
        $this->competition($money);

        # Обновляем статистику
        $this->add_stats($money);

    }

}


?>