<?php

class rfs_fkassa
{
	private $url = 'http://www.free-kassa.ru/api.php';

	private $apiId;
	private $sWord;
	private $hash;

	private $output;

	public $errors = false;

	public $auth = false;
	
	public function __construct($apiId,$sWord){

		if($apiId == ''){
			$this->errors = 'error apiId';
			return false;
		}
		if($sWord == ''){
			$this->errors = 'error secret Word';
			return false;
		}

		$this->apiId = $apiId;
		$this->sWord = $sWord;

		$this->hash = $this->getHash();

		$this->auth = array(
			'merchant_id' 	=> $this->apiId,
			's' 			=> $this->hash
		);

	}

	private function getHash(){
		return md5($this->apiId.$this->sWord);
	}

	private function getResponce($arGet){
		$arGet = array_merge($arGet, $this->auth);
		$data = @file_get_contents($this->url.'?'.http_build_query($arGet));
		return $this->parsXML($data);

	}

	private function parsXML($xmlstring){
		$xml = simplexml_load_string($xmlstring, "SimpleXMLElement", LIBXML_NOCDATA);
		$json = json_encode($xml);
		return $this->objectToArray(json_decode($json,TRUE));
	}

	public function getBalance(){
		$arGet = array(
			'action' 		=> 'get_balance'
		);
		$result = $this->getResponce($arGet);
		if($result['answer'] == 'error'){
			$this->errors = $result['desc'];
			return 0;
		}
		return $result['balance'];
	}

	public function orders(){
		$arGet = array(
			'action' 		=> 'get_orders'
		);
		$result = $this->getResponce($arGet);
		if($result['answer'] == 'error'){
			$this->errors = $result['desc'];
			return false;
		}
		return $result;
	}

	public function check_order_fk($order_id){
		$arGet = array(
			'intid' 		=> intval($order_id),
			'action' 		=> 'check_order_status',
		);
		return $this->getResponce($arGet);
	}

	public function check_order_shop($order_id){
		$arGet = array(
			'order_id' 		=> intval($order_id),
			'action' 		=> 'check_order_status',
		);
		return $this->getResponce($arGet);
	}

	private function objectToArray($ob){
		$arr = array();
		foreach ($ob as $k => $v){
			if (is_object($v)){
				$arr[$k] = $this->objectToArray($v);
			}else{
				$arr[$k] = iconv("UTF-8", "windows-1251", $v);
			}
		}
		return $arr;
	}

	public function payment($currency,$sum){
		$arGet = array(
			'currency' 		=> $currency,
			'amount' 		=> round(floatval($sum),2),
			'action' 		=> 'payment',
		);
		return $this->getResponce($arGet);
	}

	private function cp1251_Array($in)
	{
		$arr = array();
		foreach ($in as $k => $v)
		{
			$arr[$k] = iconv("UTF-8", "windows-1251", $v);
		}
		return $arr;
	}
	
}
?>