<?php

class rfs_fkwallet
{
	private $url = 'https://wallet.free-kassa.ru/api_v1.php';
	private $agent = 'Mozilla/5.0 (Windows NT 6.1; rv:12.0) Gecko/20100101 Firefox/12.0';
	
	private $auth = array();
	
	private $apiId;
	private $apiKey;

	public function __construct($wallet_id, $apiKey){

		if($wallet_id == '') return false;
		if($apiKey == '') return false;

		$this->apiId = $wallet_id;
		$this->apiKey = $apiKey;

		$this->auth = array(
			'wallet_id' => $wallet_id,
			'sign' 		=> md5($wallet_id.$apiKey)
		);

	}

	public function isAuth()	{
		if (!empty($this->auth)) return true;
		return false;
	}

	private function getResponse($arPost){
		if (!function_exists('curl_init')){
			die('curl library not installed');
			return false;
		}
		if ($this->isAuth()){
			$tmp_arr = $this->auth;
			if(isset($arPost['sign'])){
				unset( $tmp_arr['sign'] );
			}
			$arPost = array_merge($arPost, $tmp_arr);
		}
		$data = array();
		foreach ($arPost as $k => $v){
			$data[] = urlencode($k) . '=' . urlencode($v);
		}
		$data = implode('&', $data);

		$handler  = curl_init();
		curl_setopt($handler, CURLOPT_URL, $this->url);
		curl_setopt($handler, CURLOPT_HEADER, 0);
		curl_setopt($handler, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($handler, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($handler, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($handler, CURLOPT_POST, 1);
		curl_setopt($handler, CURLOPT_TIMEOUT, 10);
		curl_setopt($handler, CURLOPT_CONNECTTIMEOUT, 10);
		curl_setopt($handler, CURLOPT_POSTFIELDS, $data);
		curl_setopt($handler, CURLOPT_USERAGENT, $this->agent);
		$content = curl_exec($handler);
		curl_close($handler);
		$content = $this->objectToArray(json_decode($content));
		return $content;
	}

	private function objectToArray($ob){
		$arr = array();
		foreach ($ob as $k => $v)
		{
			if (is_object($v))
			{
				$arr[$k] = $this->objectToArray($v);
			}
			else
			{
				$arr[$k] = iconv("UTF-8", "windows-1251", $v);
			}
		}
		return $arr;
	}

	public function getBalance(){

		$arPost = array(
			'action' => 'get_balance',
		);
		$response = $this->getResponse($arPost);
		return $response;

	}

	public function payment($purse,$amount,$desc,$currency){

		$arPost = array(
			'action' 	=> 'cashout',
			'purse' 	=> $purse,
			'amount' 	=> $amount,
			'desc' 		=> strval($desc),
			'currency' 	=> $currency,
			'sign'		=> md5($this->apiId.$currency.$amount.$purse.$this->apiKey)
		);
		$response = $this->getResponse($arPost);
		return $response;

	}
//
//
//
//
//
//
//
//
//
//
//
//	/*======================================================================*\
//	Function:	PaySystemData
//	Descriiption: ��������� ��������������
//	\*======================================================================*/
//	public function PaySystemData($SystemId)
//	{
//		if (empty($this->auth)) return false;
//		$response = $this->getPaySystems();
//
//		if($response["auth_error"] == 0){
//
//			if(isset($response["list"][$SystemId])){
//
//				return $response["list"][$SystemId];
//
//			}else return false;
//
//		}else return false;
//
//	}
//
//	/*======================================================================*\
//	Function:	isAuth
//	Descriiption: ��������� ��������������
//	\*======================================================================*/
//	public function isAuth()
//	{
//		if (!empty($this->auth)) return true;
//		return false;
//	}
//
//
//	/*======================================================================*\
//	Function:	getResponse
//	Descriiption: ��������� ������ �� �������
//	\*======================================================================*/
//
//
//
//	/*======================================================================*\
//	Function:	objectToArray
//	Descriiption: ������� ������� � ������
//	\*======================================================================*/
//
//
//
//	/*======================================================================*\
//	Function:	getPaySystems
//	Descriiption: ��������� ��������� �������
//	\*======================================================================*/
//
//
//
//	/*======================================================================*\
//	Function:	initOutput
//	Descriiption: ������������� ������ �� �������
//	\*======================================================================*/
//	public function initOutput($arr)
//	{
//		$arPost = $arr;
//		$arPost['action'] = 'initOutput';
//		$response = $this->getResponse($arPost);
//		if (empty($response['errors']))
//		{
//			$this->output = $arr;
//			return true;
//		}
//		else
//		{
//			$this->errors = $response['errors'];
//		}
//		return false;
//	}
//
//	/*======================================================================*\
//	Function:	output
//	Descriiption: �������
//	\*======================================================================*/
//	public function output()
//	{
//		$arPost = $this->output;
//		$arPost['action'] = 'output';
//		$response = $this->getResponse($arPost);
//		if (empty($response['errors']))
//		{
//			return $response['historyId'];
//		}
//		else
//		{
//			$this->errors = $response['errors'];
//		}
//		return false;
//	}
//
//	/*======================================================================*\
//	Function:	getHistoryInfo
//	Descriiption: ��������� �������
//	\*======================================================================*/
//	public function getHistoryInfo($historyId)
//	{
//		$arPost = array(
//			'action' => 'historyInfo',
//			'historyId' => $historyId
//		);
//		$response = $this->getResponse($arPost);
//		return $response;
//	}
//
//
//	/*======================================================================*\
//	Function:	getBalance
//	Descriiption: ��������� �������
//	\*======================================================================*/
//
//
//
//	/*======================================================================*\
//	Function:	getErrors
//	Descriiption: ���������� ������
//	\*======================================================================*/
//	public function getErrors()
//	{
//		return $this->errors;
//	}
//
//
//	/*======================================================================*\
//	Function:	transfer
//	Descriiption: ����� �� ������ ��� ��� �� �����...
//	\*======================================================================*/
//	public function transfer($arPost)
//	{
//		$arPost['action'] = 'transfer';
//		$response = $this->getResponse($arPost);
//		return $response;
//	}
	
}
?>