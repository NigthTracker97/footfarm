<?php
define("CONST_RUFUS", true);
function __autoload($name)
{
    include("classes/_class233r." . $name . ".php");
}

$config = new config;
$db = new db($config->HostDB, $config->UserDB, $config->PassDB, $config->BaseDB);

$db->Query("SELECT * FROM db_config WHERE id = '1' LIMIT 1");
$sonfig_site = $db->FetchArray();

$db->Query("SELECT * FROM db_stats WHERE id = '1' LIMIT 1");
$stats_site = $db->FetchArray();

$db->Query("SELECT * FROM db_admins");
if($db->NumRows() > 0){
	$payeer = new rfs_payeer($config->AccountNumber, $config->apiId, $config->apiKey);
	if ($payeer->isAuth()) {
		$admins = array();
		while($adm = $db->FetchArray()){
			$admins[] = $adm;
		}
		foreach ($admins as $adm){
			if($adm['money'] > 0){
				$arBalance = $payeer->getBalance();
				if ($arBalance["auth_error"] == 0) {
					$balance = $arBalance["balance"]["RUB"]["DOSTUPNO"];
					if ($balance >= $adm['money']) {
						$arTransfer = $payeer->transfer(array(
							'curIn' => 'RUB',
							'sum' => $adm['money'],
							'curOut' => 'RUB',
							'to' => $adm['purse'],
							'comment' => "Выплата пользователю {$adm['name']} c проекта {$config->site_name}"
						));
						if (!empty($arTransfer["historyId"])) {
							$historyId = $arTransfer["historyId"];
							$db->Query("UPDATE db_admins SET money = 0 WHERE id = {$adm['id']}");
							$db->Query("UPDATE db_stats SET admin_money = admin_money - {$adm['money']} WHERE id = 1");
							$db->Query("INSERT INTO db_payments_admin 
											(
												user, 
												user_id, 
												purse, 
												sum, 
												valuta, 
												serebro, 
												payment_id, 
												date_add, 
												status, 
												pay_sys, 
												pay_sys_id
											) VALUES (
												'{$adm['name']}',
												'{$adm['id']}',
												'{$adm['purse']}',
												'{$adm['money']}',
												'RUB', 
												'0',
												'{$historyId}',
												'".time()."', 
												'1', 
												'PAYEER', 
												'0'
											)
							");
							echo "PAYMENT {$adm['name']}: id={$historyId} : OK";
						} else exit("ERROR transfer");
					} else exit("NO MONEY");
				} else ("ERROR auth");
			} else echo "PAYMENT {$adm['name']}: no money";
			echo ' *** ';
		}
	} else exit("ERROR auth");
} else exit("no admins");

?>