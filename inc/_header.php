<!DOCTYPE html>
<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<title>{!TITLE!}</title>
		<meta name="description" content="FootFarm.Org - экономическая игра с выводом денег">
		<meta name="keywords" content="Заработок на персонажах, вложения, заработать, ферма, денежная ферма, заработать на игре">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/css/bootstrap.min.css" type="text/css">
		<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,700italic,400italic,300,300italic&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
		<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css" rel="stylesheet">
		<link rel="stylesheet" href="/style/style.css?89" type="text/css" />
		<link rel="stylesheet" href="/style/media.css?2" type="text/css" />

		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="/js/functions.js"></script>
		<script type="text/javascript" src="/js/jquery.noty.packaged.js"></script>
        <meta name="megakassa" content="ed7f8b65161" />		
		<script charset="UTF-8" src="//cdn.sendpulse.com/9dae6d62c816560a842268bde2cd317d/js/push/84d11b95bae2d42c5fd1c8d9cac11a28_1.js" async></script>
	</head>	
	   
	<body class="main">
        <div class="main__container">

<nav class="navbar navbar-expand-lg">

  
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample07" aria-controls="navbarsExample07" aria-expanded="false" aria-label="Toggle navigation">
	  <i class="fa fa-bars"></i>
	</button>

	<div class="collapse navbar-collapse" id="navbarsExample07">
	  <ul class="navbar-nav mr-auto">
		<li class="nav-item active">
				<a class="nav-link" href="/">Главная</a>
		</li>
		<li class="nav-item">
		  <a class="nav-link" href="/news">Новости</a>
		</li>
		<li class="nav-item">
        <?php 
		$db->Query("SELECT count(*) FROM db_serfing WHERE money >= price and status = '2'"); 
		$num = $db->FetchRow(); 
		?>
		  <a class="nav-link" href="/buks/serfing">Серфинг <font color="green">+<?=$num?></font> </a>
		</li>
		<li class="nav-item">
		  <a class="nav-link" href="/stats">Статистика</a>
		</li>
		<li class="nav-item">
		  <a class="nav-link" href="/raceleader">Лидеры</a>
		</li>
		<li class="nav-item">
		  <a class="nav-link" href="/rules">Правила</a>
		</li>
		<li class="nav-item">
		  <a class="nav-link" href="/contacts">Контакты</a>
		</li>
		<li class="nav-item">
		  <a class="nav-link" href="/help">FAQ</a>
		</li>
	  </ul>
	  
	</div>
	<div class="form-inline nav-btns">
		<ul>
			<li style="margin-top: 17px;float: left;margin-right: 10px;"><div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'ru', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script></li>
			<?PHP
			if(isset($_SESSION["user"])){ 
			?>
			<li style="float:right;" class="btn btn-8 btn-8d"><a href="/account/exit"><i class="fa fa-times" aria-hidden="true"></i> ВЫХОД</a></li>  
			<li style="float:right;" class="btn btn-8 btn-8d"><a href="/account"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> В АККАУНТ</a></li>
			<?PHP
			} else { 
			?>
			<li style="float:right;" class="btn btn-8 btn-8d"><a href="/signup"><i class="fa fa-user" aria-hidden="true"></i> РЕГИСТРАЦИЯ</a></li>	
			<li style="float:right;" class="btn btn-8 btn-8d"><a href="/login"><i class="fa fa-unlock-alt" aria-hidden="true"></i> ВОЙТИ</a></li>  
			<?PHP
			}
			?>
		</ul>
	  </div>

</nav>


            <?PHP if(isset($_GET["menu"])) include("inc/_menu_left.php"); ?>

       

