<?php
    if(empty($_SESSION['bonus'])){
        $confdata = $func->config_site($db);
        $temp = array();
        if($confdata['regbonus'] > 0 && !isset($_SESSION['user_id']))   $temp[0] = $confdata['regbonus'];
        if($confdata['first_payment_pers'] > 0)                         $temp[1] = $confdata['first_payment_pers'];
        if($confdata['refsale_enable'] > 0)                             $temp[2] = $confdata['refsale_enable'];
        $_SESSION['bonus'] = $temp;
    }
?>
<? if(!empty($_SESSION['bonus'])){ ?>

    <div class="leftbbmenu newsinlmenu">

        <h5>Акции и Бонусы</h5>
        <? if(!empty($_SESSION['bonus'][0])){ ?>
            <div>
                <a href="/signup">Бонус при регистрации</a>
                <text>Получайте в подарок <b><?=$_SESSION['bonus'][0]?></b> серебра при регистрации на проекте!</text>
            </div>
        <? } ?>
        <? if(!empty($_SESSION['bonus'][1])){ ?>
            <div>
                <a href="/account/insert">+<?=$_SESSION['bonus'][1]?>% к первому пополнению</a>
                <text>Пополняя баланс в первый раз получайте в подарок <b><?=$_SESSION['bonus'][1]?>%</b> от суммы пополнения!</text>
            </div>
        <? } ?>

        <? if(!empty($_SESSION['bonus'][2])){ ?>
            <div>
                <a href="/account/refsale">Продажа рефералов</a>
                <text>Получайте за каждого приглашенного партнера вознаграждение!</text>
            </div>
        <? } ?>

    </div>

<? } ?>
