<?PHP
# Настройки
$sonfig_site = $func->config_site($db);

$db->Query("SELECT * FROM db_users_a, db_users_b, db_users_c WHERE db_users_a.id = db_users_b.id AND db_users_a.id = db_users_c.id AND db_users_a.id = {$_SESSION["user_id"]}");
$prof_data = $db->FetchArray();



?>

<div class="container-fluid">
<div class="row">
<aside class="leftmenu">
	<h3 style="text-align: center;text-transform: uppercase;font-size: 15px;">
		<center><a href="/account" title="Ваш аккаунт" style="text-align: center; text-transform: uppercase;font-size: 15px;"><?= $_SESSION["user"]; ?></a><a href="/account" style="font-size: 18px;margin-left: 5px;" title="Ваш аккаунт"><i class="fa fa-cog" aria-hidden="true" style="vertical-align: middle; top: -0.5px; position: relative;"></i></a></center>
	</h3>
	<div class="panel-leftbar balance">
		<div class="ul-box">
			<ul class="nav nav-pills nav-stacked nav-leftbar" data-box="2">
				<li><a>Для покупок: <span class="balance-menu">{!BALANCE_B!}</span></a></li>
				<li><a>Для вывода: <span class="balance-menu">{!BALANCE_P!}</span></a></li>
				<li><a>На рекламу: <span class="balance-menu">{!BALANCE_R!} руб.</span></a></li>
			</ul>
		</div>
	</div>
	<div class="menu-heading">Операции с балансом</div>
	<div class="links">
		<div class="ul-box">
			<ul class="nav nav-pills nav-stacked nav-leftbar" data-box="2">
				<li><a href="/finance/insert">Пополнить баланс <i aria-hidden="true" class="fa fa-chevron-right" style="margin: 12px 5px 0 10px;float: right;"></i> <span style="float: right;color: #ffffff;background: #4bc05e;height: 24px;line-height: 24px;padding-left: 5px;padding-right: 5px;border-radius: 4px;margin-right: 5px;margin-top: 5px;">+10%</span></a></li>
				<li><a href="/finance/payments">Заказать выплату <i aria-hidden="true" class="fa fa-chevron-right" style="margin: 12px 5px 0 10px;float: right;"></i></a></li>
				<li><a href="/finance/exchange">Обмен баланса <i aria-hidden="true" class="fa fa-chevron-right" style="margin: 12px 5px 0 10px;float: right;"></i> <span style="float: right;color: #ffffff;background: #4bc05e;height: 24px;line-height: 24px;padding-left: 5px;padding-right: 5px;border-radius: 4px;margin-right: 5px;margin-top: 5px;">+10%</span></a></li>
			</ul>
		</div>
	</div>
	<div class="menu-heading">Основное меню</div>
	<div class="links">
		<div class="ul-box">
			<ul class="nav nav-pills nav-stacked nav-leftbar" data-box="2">
				<li><a href="/farm/myfarm">Купить клуб <i aria-hidden="true" class="fa fa-chevron-right" style="margin: 12px 5px 0 10px;float: right;"></i></a></li>
				<li><a href="/farm/store">Собрать прибыль <i aria-hidden="true" class="fa fa-chevron-right" style="margin: 12px 5px 0 10px;float: right;"></i></a></li>
				<li><a href="/raceleader">Гонка лидеров <i aria-hidden="true" class="fa fa-chevron-right" style="margin: 12px 5px 0 10px;float: right;"></i></a></li>
				<li><a href="/account/config">Настройки профиля <i aria-hidden="true" class="fa fa-chevron-right" style="margin: 12px 5px 0 10px;float: right;"></i></a></li>
				<li><a href="/account">Мой профиль <i aria-hidden="true" class="fa fa-chevron-right" style="margin: 12px 5px 0 10px;float: right;"></i></a></li>
				<li><a href="/account/chat" id="chat-menu">Чат <i aria-hidden="true" class="fa fa-chevron-right" style="margin: 12px 5px 0 10px;float: right;"></i></a></li>
				<!--<li><a href="/support">Служба поддержки <i aria-hidden="true" class="fa fa-chevron-right" style="margin: 12px 5px 0 10px;float: right;"></i></a></li>-->
			</ul>
		</div>
	</div>
	<div class="menu-heading">Серфинг</div>
	<div class="links">
		<div class="ul-box">
			<ul class="nav nav-pills nav-stacked nav-leftbar" data-box="2">
				<li><a href="/buks/serfing"> Серфинг сайтов <i aria-hidden="true" class="fa fa-chevron-right" style="margin: 12px 5px 0 10px;float: right;"></i></a></li>
				<li><a href="/buks/serfing/cabinet"> Добавить сайт в серфинг <i aria-hidden="true" class="fa fa-chevron-right" style="margin: 12px 5px 0 10px;float: right;"></i></a></li>
				<li><a href="/buks/refill">Рекламный баланс <i aria-hidden="true" class="fa fa-chevron-right" style="margin: 12px 5px 0 10px;float: right;"></i></a></li>
			</ul>
		</div>
	</div>
	<div class="menu-heading">Реферальная система</div>
	<div class="links">
		<div class="ul-box">
			<ul class="nav nav-pills nav-stacked nav-leftbar" data-box="2">
				<li><a href="/referals/myreferals"> Мои рефералы <i aria-hidden="true" class="fa fa-chevron-right" style="margin: 12px 5px 0 10px;float: right;"></i> <span style="float: right;color: #ffffff;background: #4bc05e;height: 24px;line-height: 24px;padding-left: 5px;padding-right: 5px;border-radius: 4px;margin-right: 5px;margin-top: 5px;"><?= $prof_data["referals"]; ?> чел.</span></a></li>
				<li><a href="/referals/promo"> Рекламные материалы <i aria-hidden="true" class="fa fa-chevron-right" style="margin: 12px 5px 0 10px;float: right;"></i></a></li>
				<li> <font style="border-bottom: 2px solid #e4a014;position: absolute;font-size: 13px;line-height: 11px;margin-top: 9px;     margin-left: 162px;color: #ffffff;border: 1px solid #ffb51c;border-radius: 2px;background: #ffb51c;font-weight: bold;padding: 2px 5px 2px 5px;z-index: 10;font-family: 'Open Sans', sans-serif; border-bottom: 2px solid #e4a014;">NEW</font><a href="/competition">Конкурс рефералов <i aria-hidden="true" class="fa fa-chevron-right" style="margin: 12px 5px 0 10px;float: right;"></i> </a></li>
				<li><a href="/referals/refleader">Автореферал <i aria-hidden="true" class="fa fa-chevron-right" style="margin: 12px 5px 0 10px;float: right;"></i></a></li>
				<li><a href="/referals/visits">Статистика переходов<i aria-hidden="true" class="fa fa-chevron-right" style="margin: 12px 5px 0 10px;float: right;"></i></a></li>
			</ul>
		</div>
	</div>
	<?php 
		$db->Query("SELECT count(*) FROM db_serfing WHERE money >= price and status = '2'"); 
		$num = $db->FetchRow(); 
		?>
	<div class="menu-heading">Дополнительно</div>
	<div class="links">
		<div class="ul-box">
			<ul class="nav nav-pills nav-stacked nav-leftbar" data-box="2">
				<li> <font style="border-bottom: 2px solid #e4a014;position: absolute;font-size: 13px;line-height: 11px;margin-top: 9px;     margin-left: 162px;color: #ffffff;border: 1px solid #ffb51c;border-radius: 2px;background: #ffb51c;font-weight: bold;padding: 2px 5px 2px 5px;z-index: 10;font-family: 'Open Sans', sans-serif; border-bottom: 2px solid #e4a014;">NEW</font><a href="/farm/vkbonus">Серебро в подарок <i aria-hidden="true" class="fa fa-chevron-right" style="margin: 12px 5px 0 10px;float: right;"></i> </a></li>
				<li><a href="/farm/bonus">Бонусный баланс <i aria-hidden="true" class="fa fa-chevron-right" style="margin: 12px 5px 0 10px;float: right;"></i></a></li>
				<li><a href="/account/calc">Калькулятор доходов <i aria-hidden="true" class="fa fa-chevron-right" style="margin: 12px 5px 0 10px;float: right;"></i></a></li>
			</ul>
		</div>
	</div>
	<!--<div class="menu-heading">Игровая зона</div>
	<div class="links">
		<div class="ul-box">
			<ul class="nav nav-pills nav-stacked nav-leftbar" data-box="2">
				<li><a href="/auc">Аукцион <i aria-hidden="true" class="fa fa-chevron-right" style="margin: 12px 5px 0 10px;float: right;"></i> </a></li>
				<li><a href="/games/lottery">Лотерея <i aria-hidden="true" class="fa fa-chevron-right" style="margin: 12px 5px 0 10px;float: right;"></i></a></li>
				<li><a href="/games/fortuna_wheel">Колесо фортуны <i aria-hidden="true" class="fa fa-chevron-right" style="margin: 12px 5px 0 10px;float: right;"></i></a></li>
			</ul>
		</div>
	</div>-->
</aside>