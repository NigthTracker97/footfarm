<?PHP
error_reporting(0); // вывод ошибок

# Счетчик
function TimerSet(){
	list($seconds, $microSeconds) = explode(' ', microtime());
	return $seconds + (float) $microSeconds;
}

$_timer_a = TimerSet();

# Старт сессии
@session_start();

if (isset($_SESSION['HTTP_USER_AGENT']))
{
    if ($_SESSION['HTTP_USER_AGENT'] != md5($_SERVER['HTTP_USER_AGENT']))
    {
        @session_destroy();
		Header("Location: /");
		exit();
    }
}
else
{
    $_SESSION['HTTP_USER_AGENT'] = md5($_SERVER['HTTP_USER_AGENT']);
}

# Старт буфера
@ob_start();

# Константа для Include
define("CONST_RUFUS", true);

# Автоподгрузка классов
function __autoload($name){ include("classes/_class233r.".$name.".php");}

# Определение откуда пришел пользвоатель
function getdomain(){
	$parseUrl = parse_url(trim($_SERVER['HTTP_REFERER']));
	return trim($parseUrl['host'] ? $parseUrl['host'] : array_shift(explode('/', $parseUrl['path'], 2)));
}

# Mobile Detect
include("classes/Mobile_Detect.php");
$detect = new Mobile_Detect();

# Класс конфига 
$config = new config;

# Функции
$func = new func;

foreach($_POST as $i => $value){$_POST[$i]=stripslashes($_POST[$i]);}
foreach($_GET as $i => $value){$_GET[$i]=stripslashes($_GET[$i]);}
foreach($_COOKIE as $i => $value){$_COOKIE[$i]=stripslashes($_COOKIE[$i]);}

function anti_sql()
{
$check = html_entity_decode( urldecode( $_SERVER['REQUEST_URI'] ) );
$check = str_replace( "", "/", $check );

$check2 = trim($check);
$check = array("AND","UNION","SELECT","WHERE","INSERT","UPDATE","DELETE","OUTFILE","FROM","OR","SHUTDOWN","CHANGE","MODIFY","RENAME","RELOAD","ALTER","GRANT","DROP","CONCAT","cmd","exec");
$check = str_replace($check,"",$check2);

if( $check )
{
if((strpos($check, '<')!==false) || (strpos($check, '>')!==false) || (strpos($check, '"')!==false) || (strpos($check,"'")!==false) || (strpos($check, '*')!==false) || (strpos($check, '(')!==false) || (strpos($check, ')')!==false) || (strpos($check, ' ')!==false) || (strpos($check, ' ')!==false) || (strpos($check, ' ')!==false) )
{
$prover = true;
}

if((strpos($check, 'src')!==false) || (strpos($check, 'img')!==false) || (strpos($check, 'OR')!==false) || (strpos($check, 'Image')!==false) || (strpos($check, 'script')!==false) || (strpos($check, 'javascript')!==false) || (strpos($check, 'language')!==false) || (strpos($check, 'document')!==false) || (strpos($check, 'cookie')!==false) || (strpos($check, 'gif')!==false) || (strpos($check, 'png')!==false) || (strpos($check, 'jpg')!==false) || (strpos($check, 'js')!==false) )
{
$prover = true;
}

}

if (isset($prover))
{
die( "Попытка атаки на сайт или введены запрещённые символы!" );
return false;
exit;
}
}
anti_sql();


# Защита админки
/*if(isset($_GET['admin']) && $_GET['admin'] == $config->adminKey){
	$_SESSION['admin4ik'] = true;
	header("Location: /");
}*/

//if(!$_SESSION['admin4ik']) exit('Настройка параметров...'); 

# Записываем откуда пришел пользователь
if(empty($_SESSION['come_url'])){
	$_SESSION['come_url'] = getdomain();
	if($_SESSION['come_url'] == '') $_SESSION['come_url'] = '-';
}

# База данных
$db = new db($config->HostDB, $config->UserDB, $config->PassDB, $config->BaseDB);

if(isset($_SESSION['user_id']) && empty($_SESSION['admin4ik'])){
	# Защита от мультисессий
	$db->Query("SELECT session_id FROM db_users_a WHERE id = {$_SESSION['user_id']}");
	if(session_id() != $db->FetchRow()){
		@session_destroy();
		Header("Location: /");
		exit();
	}
}

function geodataISO($ip){
	global $SxGeo;
	$geo_date = $SxGeo->getCityFull($ip);
	$country = $geo_date['country']['iso'];
	if($country == '') $country = 'none';
	return $country;
}

# Default
$_OPTIMIZATION = array();
$_OPTIMIZATION["title"] = "{$config->site_name} - экономическая онлайн игра с выводом реальных денег. Моментальные выплаты на популярные платежные системы";
$_OPTIMIZATION["description"] = "Заработок в интернете, деньги, доход каждый день, стабильный заработок, стабильный доход";
$_OPTIMIZATION["keywords"] = "Заработок на яицах, вложения, заработать, заработать на яицах";

if(isset($_GET['user'])){
	$us = strval($_GET['user']);
	switch($us){
		case 'login': include("./_geolocation/SxGeo.php"); $SxGeo = new SxGeo('./_geolocation/SxGeoCity.dat'); include("user/login/login.php"); exit(); break;
		case 'reg': include("user/login/reg.php"); exit(); break;
		case 'recovery': include("user/login/recovery.php"); exit(); break;
		default: include("pages/_index.php"); break;
	}
}

if(isset($_GET['menu'])){
	if($_GET['menu'] != 'admin4ik'){
		# Установка REFERER
		include("inc/_set_referer.php");
		# Шапка
		@include("inc/_header.php");
	}else{
		# Шапка
		@include("inc/_header_admin.php");
	}
}else{
	# Установка REFERER
	include("inc/_set_referer.php");
	@include("inc/_header.php");
}

		if(isset($_GET["menu"])){
			
		
			$menu = strval($_GET["menu"]);
			
			switch($menu){
			
				case "404": include("pages/_404.php"); break; // Страница ошибки
				case "help": include("pages/_help.php"); break; // помощь
				case "rules": include("pages/_rules.php"); break; // Правила проекта
				case "about": include("pages/_about.php"); break; // О проекте
				case "contacts": include("pages/_contacts.php"); break; // Контакты
				case "news": include("pages/_news.php"); break; // Новости
				case "signup": include("pages/_signup.php"); break; // Регистрация
				case "recovery": include("pages/_recovery.php"); break; // Восстановление пароля
				case "competition": include("pages/_competition.php"); break; // Конкурсы
				case "users": include("pages/_users_list.php"); break; // Пользователи
				case "payments": include("pages/_payments_list.php"); break; // Выплаты
				case "login": include("pages/_login.php"); break; // Пользователи
				# подразделы
				case "account": include("pages/_account.php"); break; // Аккаунт
				case "buks": include("pages/_buks.php"); break; // Букс
				case "farm": include("pages/_farm.php"); break; // Ферма
				case "games": include("pages/_games.php"); break; // Игры и развлечеия
				case "referals": include("pages/_referals.php"); break; // Партнерская программа
				case "refleader": include("pages/_refleader.php"); break; // Партнерская программа
				case "finance": include("pages/_finance.php"); break; // Финансовые операции
				case "auc": include("pages/_auc.php"); break; // Ауэкцион
				case "chat": include("pages/_chat.php"); break; // Чат
				case "contest": include("pages/_contest.php"); break; // Конкурсы
				case "race": include("pages/_race.php"); break; // Конкурсы
				/*case "admin4ik":
					if($_SESSION['admin4ik']) include("pages/_admin.php"); else @include("pages/_404.php");
					break; // Админка*/
				
			# Страница ошибки
			default: @include("pages/_404.php"); break;
			
			}
		}else @include("pages/_index.php");

if(isset($_GET['menu'])){
	if($_GET['menu'] != 'admin4ik'){
		# Подвал
		@include("inc/_footer.php");
	}else{
		# Подвал
		@include("inc/_footer_admin.php");
	}
}else{
	@include("inc/_footer.php");
}

# Заносим контент в переменную
$content = ob_get_contents();

# Очищаем буфер
ob_end_clean();
	
	# Заменяем данные
	$content = str_replace("{!TITLE!}",$_OPTIMIZATION["title"],$content);
	$content = str_replace('{!DESCRIPTION!}',$_OPTIMIZATION["description"],$content);
	$content = str_replace('{!KEYWORDS!}',$_OPTIMIZATION["keywords"],$content);
	$content = str_replace('{!GEN_PAGE!}', sprintf("%.5f", (TimerSet() - $_timer_a)) ,$content);
	$content = str_replace('{!SITENAME!}', $config->site_name ,$content);

	# Вывод баланса
	if(isset($_SESSION["user_id"])){
		$db->Query("SELECT money_b, money_p, money_r, cash_points, from_referals, (SELECT payment_limit FROM db_config WHERE id = 1) payment_limit  FROM db_users_b WHERE id = {$_SESSION["user_id"]}");
		$balance = $db->FetchArray();

		$content = str_replace('{!BALANCE_B!}', sprintf("%.2f", $balance["money_b"]) ,$content);
		$content = str_replace('{!BALANCE_P!}', sprintf("%.2f", $balance["money_p"]) ,$content);
		$content = str_replace('{!BALANCE_R!}', sprintf("%.2f", $balance["money_r"]) ,$content);
		
		/*if($balance['payment_limit'] == 1){
			if($balance['from_referals'] > 0){
				$content = str_replace('{!BALANCE_R!}', sprintf("%.2f", $balance["cash_points"]) ,$content);
			}else{
				$content = str_replace('{!BALANCE_R!}', sprintf("%.2f", 0) ,$content);
			}
		}else{
			$content = str_replace('{!BALANCE_R!}', sprintf("%.2f", $balance["money_r"]) ,$content);
		}*/
	}
	
// Выводим контент
echo $content;
?>







