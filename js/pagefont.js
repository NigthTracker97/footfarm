$(document).ready(function () {
	// $('.newsBlock').newsBlock();
	$('.carsBlock').carsBlock();
});

$.fn.carsBlock = function () {
	return this.each(function () {
		var component = $(this);
		var carsArea = component.find('.carsArea');
		var left = component.find('.aLeft');
		var right = component.find('.aRight');
		var plane = component.find('.carsPlane');
		var carMargin = 50;
		var carWidth = carMargin + 240;
		var planeLeft = 0;
		var visibleCars = 4;

		right.click(function () {
			plane.find('.car').finish();
			plane.finish();

			for (var n = 1; n <= visibleCars; n++) {
				if (n == visibleCars) {
					plane.find('.car:eq(' + n + ')').css('opacity', 0).animate({
						opacity: 1
					}, 500);
				} else {
					plane.find('.car:eq(' + n + ')').css('opacity', 1);
				}
			}
			plane.find('.car:first').animate({
				opacity: 0
			}, 500);

			plane.animate({
				left: planeLeft - carWidth
			}, 500, '', function () {
				var firstCar = plane.find('.car:first');

				firstCar.remove().appendTo(plane);
				$(window).resize();
			});
		});
		left.click(function () {
			plane.find('.car').finish();
			plane.finish();
			var lastCar = plane.find('.car:last');

			plane.find('.car:eq(' + (visibleCars - 1) + ')').animate({
				opacity: 0
			}, 500);
			lastCar.remove().prependTo(plane).css('opacity', 0).animate({
				opacity: 1
			}, 500);
			plane.css('left', planeLeft - carWidth).animate({
				left: planeLeft
			}, 500, '', function () {
				$(window).resize();
			});
		});

		$(window).resize(function () {
			if (carsArea.width() < carWidth * 2 - carMargin) {
				planeLeft = carsArea.width() / 2 - carWidth / 2 + carMargin / 2;
				visibleCars = 1;
			}	else if (carsArea.width() < carWidth * 3 - carMargin) {
				planeLeft = carsArea.width() / 2 - carWidth * 2 / 2;
				visibleCars = 2;
			} else if (carsArea.width() < carWidth * 4 - carMargin) {
				planeLeft = carsArea.width() / 2 - carWidth * 3 / 2;
				visibleCars = 3;
			} else {
				planeLeft = 0;
				visibleCars = 4;
			}
			if (planeLeft < 0) {
				planeLeft = 0;
			}
			plane.find('.car').css('opacity', 0);
			for (var n = 0; n < visibleCars; n++) {
				plane.find('.car:eq(' + n + ')').css('opacity', 1);
			}
			plane.css('left', planeLeft);
		}).resize();
	});
};

$.fn.newsBlock = function () {
	return this.each(function () {
		var component = $(this);
		var news = component.find('.news');
		var left = component.find('.aLeft');
		var right = component.find('.aRight');
		var pages = component.find('.pages span');
		var container = component.find('.newsContainer');
		var currentIndex = 0;
		var animating = false;

		pages.each(function (nextIndex) {
			var page = $(this);

			page.click(function () {
				if (page.hasClass('active')) {
					return false;
				}
				if (animating) {
					return false;
				}
				animating = true;
				var current = news.eq(currentIndex);
				var next = news.eq(nextIndex).show();

				container.css('height', Math.max(current.outerHeight(), next.outerHeight()));
				current.css({
					marginTop: currentIndex > nextIndex ? -next.outerHeight() : 0
				}).animate({
					opacity: 0
				}, 500, '', function () {
					news.removeClass('active');
					current.hide();
					next.addClass('active').css('marginTop', 0);
					animating = false;
					container.css('height', 'auto');
				});
				next.css({
					opacity: 0,
					marginTop: currentIndex > nextIndex ? 0 : -current.outerHeight()
				}).animate({
					opacity: 1
				}, 500, '');

				pages.removeClass('active');
				page.addClass('active');
				currentIndex = nextIndex;

			});
		});

		left.click(function () {
			pages.eq(currentIndex == 0 ? pages.size() - 1 : currentIndex - 1).click();
		});
		right.click(function () {
			pages.eq(currentIndex + 1 >= pages.size() ? 0 : currentIndex + 1).click();
		});

		setInterval(function () {
			right.click();
		}, 10000);
	});
};