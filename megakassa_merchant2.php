<?php

# јвтоподгрузка классов
function __autoload($name)
{
    include("classes/_class233r." . $name . ".php");
}

#  ласс конфига 
$config = new config;

# ‘ункции
$func = new func;

# Ѕаза данных
$db = new db($config->HostDB, $config->UserDB, $config->PassDB, $config->BaseDB);

// проверка IP-адреса
$ip_checked = false;

foreach(array(
	'HTTP_X_CLUSTER_CLIENT_IP',
	'HTTP_X_FORWARDED_FOR',
	'HTTP_X_FORWARDED',
	'HTTP_FORWARDED_FOR',
	'HTTP_FORWARDED',
	'HTTP_CLIENT_IP',
	'REMOTE_ADDR'
) as $param) {
	if(!empty($_SERVER[$param]) && $_SERVER[$param] === '5.196.121.217') {
		$ip_checked = true;
		break;
	}
}
if(!$ip_checked) {
	die('error');
}

// проверка на наличие обязательных полей
// поля $payment_time и $debug могут дать true для empty() поэтому их нет в проверке
foreach(array(
	'uid',
	'amount',
	'amount_shop',
	'amount_client',
	'currency',
	'order_id',
	'payment_method_title',
	'creation_time',
	'client_email',
	'status',
	'signature'
) as $field) {
	if(empty($_REQUEST[$field])) {
		die('error');
	}
}

// ваш секретный ключ
$secret_key	= '199433146a562ea9';

// нормализация данных
$uid					= (int)$_REQUEST["uid"];
$amount					= (double)$_REQUEST["amount"];
$amount_shop			= (double)$_REQUEST["amount_shop"];
$amount_client			= (double)$_REQUEST["amount_client"];
$currency				= $_REQUEST["currency"];
$order_id				= $_REQUEST["order_id"];
$payment_method_id		= (int)$_REQUEST["payment_method_id"];
$payment_method_title	= $_REQUEST["payment_method_title"];
$creation_time			= $_REQUEST["creation_time"];
$payment_time			= $_REQUEST["payment_time"];
$client_email			= $_REQUEST["client_email"];
$status					= $_REQUEST["status"];
$debug					= (!empty($_REQUEST["debug"])) ? '1' : '0';
$signature				= $_REQUEST["signature"];

// проверка валюты
if(!in_array($currency, array('RUB', 'USD', 'EUR'), true)) {
	die('error');
}

// проверка статуса платежа
if(!in_array($status, array('success', 'fail'), true)) {
	die('error');
}

// проверка формата сигнатуры
if(!preg_match('/^[0-9a-f]{32}$/', $signature)) {
	die('error');
}

// проверка значения сигнатуры
$signature_calc = md5(join(':', array(
	$uid, $amount, $amount_shop, $amount_client, $currency, $order_id,
	$payment_method_id, $payment_method_title, $creation_time, $payment_time,
	$client_email, $status, $debug, $secret_key
)));
if($signature_calc !== $signature) {
	die('error');
}

// далее ваши проверки:
// - на наличие платежа в вашей БД по $order_id
// - на соответствии суммы и валюты платежа в вашей БД
// ...

// обработка платежа
switch($status) {
	case 'success':
	
		// время соверешния платежа в Unix timestamp (если нужно)
		$payment_time_ts = strtotime($payment_time);
	
		$db->Query("SELECT * FROM db_money_insert WHERE id = {$order_id}");
        $insert_row = $db->FetchArray();

        if ($insert_row["status"] > 0) {
            echo "ERROR1";
            exit;
        }

        if($insert_row['payment_system'] != 'MEGAKASSA'){
            echo "ERROR2";
            exit;
        }
		
		$db->Query("UPDATE db_money_insert SET status = '1' WHERE id = {$order_id}");

        $insert = new insert($db);

        $res = $insert->go($insert_row, $insert_row['payment_system']);
		
		if(!$res){
			die('ERROR3');
		}
		
		break;
	case 'fail':
		// ваш код при отмене или истечении платежа
		// ...
		break;
}

// успешный ответ для Мегакассы и завершение скрипта
die('ok');
?>