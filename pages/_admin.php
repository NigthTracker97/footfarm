<?PHP
$_OPTIMIZATION["title"] = "Административная панель";
$_OPTIMIZATION["description"] = "Аккаунт пользователя";
$_OPTIMIZATION["keywords"] = "Аккаунт, личный кабинет, пользователь";
$not_counters = true;

# Блокировка сессии
if(!isset($_SESSION["admin"])){ include("pages/admin/_login.php"); return; }

if(isset($_GET["sel"])){
		
	$smenu = strval($_GET["sel"]);
			
	switch($smenu){
		
		case "404": include("pages/_404.php"); break; // Страница ошибки
		case "control": include("pages/admin/_control.php"); break; // Контроль
		case "stats": include("pages/admin/_stats.php"); break; // Статистика
		case "visits": include("pages/admin/_visits.php"); break; // Статистика
		case "financestats": include("pages/admin/_financestats.php"); break; // Статистика
		case "config": include("pages/admin/_config.php"); break; // Настройки
		case "story_buy": include("pages/admin/_story_buy.php"); break; // История покупок деревьев
		case "story_swap": include("pages/admin/_story_swap.php"); break; // История обмена в обменнике
		case "compconfig": include("pages/admin/_compconfig.php"); break; // Управление конкурсами
		case "story_insert": include("pages/admin/_story_insert.php"); break; // История пополнений баланса
		case "story_sell": include("pages/admin/_story_sell.php"); break; // История рынка
		case "news": include("pages/admin/_news_a.php"); break; // Новости
		case "users": include("pages/admin/_users.php"); break; // Список пользователей
		case "payments": include("pages/admin/_payments.php"); break; // выплаты
		case "payments_ext": include("pages/admin/_payments_ext.php"); break; // выплаты2
		case "sender": include("pages/admin/_sender.php"); break; // Рассылка пользователям
		case "while": include("pages/admin/_while.php"); break; // Тикеты
		case "payment_system": include("pages/admin/_payment_system.php"); break; // payment_system
		case "errors": include("pages/admin/_errors.php"); break; // оишбки

	# Страница ошибки
	default: @include("pages/_404.php"); break;
			
	}
			
}else @include("pages/admin/_financestats.php");

?>