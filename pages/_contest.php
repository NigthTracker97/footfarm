<?
$_OPTIMIZATION["title"] = "Конкурсы";
$_OPTIMIZATION["description"] = "Конкурсы";
$_OPTIMIZATION["keywords"] = "Конкурсы";

$sonfig_site = $func->config_site($db);

$db->Query("SELECT * FROM db_contest WHERE id = 1");
$conc1 = $db->FetchArray();

$db->Query("SELECT * FROM db_contest WHERE id = 2");
$conc2 = $db->FetchArray();

?>

<section class="container-fluid">
	<div class="row" style="margin-top: 20px;">
		<div class="col-md-6">
			<div class="panel2 panel-default thumbnail">
				<div class="panel-heading" style="text-align: center;">
					<b style="font-size: 22px;text-transform: uppercase;">Конкурс инвесторов <b style="color: #f73454;">№<?=$conc1['num'];?></b></b><br>
					<b style="font-size: 18px;text-transform: uppercase;color: #485;">Завершение конкурса: <?=$func->time2word($conc1['end']);?></b>
					<img src="/images/prize1.png">
					<div style="font-size: 24px;margin-top: -35px;text-transform: uppercase;color: #8c0036;font-weight: bold;">Фонд: <?=$conc1['prize1'] + $conc1['prize2'] + $conc1['prize3'] + $conc1['prize4'] + $conc1['prize5'];?> Руб.</div>
					<hr style="margin-bottom: 10px;">
					<p style="text-align:center;font-size: 16px;">
						<?=$conc1['desc'];?>
					</p>
				</div>
				<table width="99%" class="table table-bordered" align="center">
					<tbody>
						<tr bgcolor="#efefef">
							<td align="center" colspan="4"><b>Таблица 10 лидеров</b></td>
						</tr>
						<tr bgcolor="#efefef">
							<td align="center" width="75" class="m-tb">Позиция</td>
							<td align="center" class="m-tb">Пользователь</td>
							<td align="center" class="m-tb">Сумма</td>
							<td align="center" class="m-tb">Приз</td>
						</tr>
						<?
						$db->Query("SELECT * FROM db_contest_users WHERE contest_id = 1");
						$i = 1;
						while($data = $db->FetchArray()){
							if(isset($conc1["prize{$i}"])){
								$prize = $conc1["prize{$i}"].' руб.';
							}else{
								$prize = '---';
							}
						?>
							<tr class="htt">
								<td align="center" width="75"><?=$i;?></td>
								<td align="center"><?=$data['user'];?></td>
								<td align="center"><?=$data['sum'];?> руб.</td>
								<td align="center"><?=$prize;?></td>
							</tr>
						<?
							$i++;
						}
						?>
					</tbody>
				</table>
			</div>
		</div>
		<div class="col-md-6">
			<div class="panel2 panel-default thumbnail">
				<div class="panel-heading" style="text-align: center;">
					<b style="font-size: 22px;text-transform: uppercase;">Конкурс рефералов <b style="color: #f73454;">№<?=$conc2['num'];?></b></b><br>
					<b style="font-size: 18px;text-transform: uppercase;color: #485;">Завершение конкурса: <?=$func->time2word($conc2['end']);?></b>
					<img src="/images/prize2.png">
					<div style="font-size: 24px;margin-top: -35px;text-transform: uppercase;color: #1a3884;font-weight: bold;">ФОНД: <?=$conc2['prize1'] + $conc2['prize2'] + $conc2['prize3'] + $conc2['prize4'] + $conc2['prize5'];?> Руб.</div>
					<hr style="margin-bottom: 10px;">
					<p style="text-align:center;font-size: 16px;">
						<?=$conc2['desc'];?>
					</p>
				</div>
				<table width="99%" class="table table-bordered" align="center">
					<tbody>
						<tr bgcolor="#efefef">
							<td align="center" colspan="4"><b>Таблица 10 лидеров</b></td>
						</tr>
						<tr bgcolor="#efefef">
							<td align="center" width="75" class="m-tb">Позиция</td>
							<td align="center" class="m-tb">Пользователь</td>
							<td align="center" class="m-tb">Сумма</td>
							<td align="center" class="m-tb">Приз</td>
						</tr>
						<?
						$db->Query("SELECT * FROM db_contest_users WHERE contest_id = 2");
						$i = 1;
						while($data = $db->FetchArray()){
							if(isset($conc2["prize{$i}"])){
								$prize = $conc2["prize{$i}"].' руб.';
							}else{
								$prize = '---';
							}
						?>
							<tr class="htt">
								<td align="center" width="75"><?=$i;?></td>
									<td align="center"><?=$data['user'];?></td>
									<td align="center"><?=$data['sum'];?> руб.</td>
									<td align="center"><?=$prize;?></td>
							</tr>
						<?
							$i++;
						}
						?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</section>