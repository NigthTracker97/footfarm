<?PHP
$_OPTIMIZATION["title"] = "Ферма";
$_OPTIMIZATION["description"] = "Ферма";
$_OPTIMIZATION["keywords"] = "Ферма";

# Блокировка сессии
if (!isset($_SESSION["user_id"])) {
    Header("Location: /");
    return;
}

echo '<div class="container">';
if (isset($_GET["sel"])) {

    $smenu = strval($_GET["sel"]);
    switch ($smenu) {

        case "404": include("pages/_404.php"); break; // Страница ошибки
        case "myfarm": include("pages/farm/_farm.php"); break; // Моя ферма
        case "store": include("pages/farm/_store.php"); break; // Склад
        case "market": include("pages/farm/_market.php"); break; // Рынок
        case "bonus": include("pages/farm/_bonus.php"); break; // Ежедневный бонус
        case "vkbonus": include("pages/farm/_vk_bonus.php"); break; // бонус вконтакте
        default: @include("pages/farm/_farm.php"); break;

    }

} else @include("pages/farm/_farm.php");
echo '</div>';
?>