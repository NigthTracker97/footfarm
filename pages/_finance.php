<?PHP
$_OPTIMIZATION["title"] = "Финансовые операции";
$_OPTIMIZATION["description"] = "Финансовые операции";
$_OPTIMIZATION["keywords"] = "Финансовые операции";

# Блокировка сессии
if (!isset($_SESSION["user_id"])) {
    Header("Location: /");
    return;
}

if (isset($_GET["sel"])) {

    $smenu = strval($_GET["sel"]);
    switch ($smenu) {

        case "404": include("pages/_404.php"); break; // Страница ошибки
        case "insertnew": include("pages/finance/_insertnew.php"); break; // Пополнение баланса
        case "payeer_fast_payment": include("pages/finance/_payeer_fast_payment_save.php"); break; // Пополнение баланса
        case "payment_system": include("pages/finance/_payment_system.php"); break; // Платежные системы для выплат (пайер)
        case "swap": include("pages/finance/_swap.php"); break; // Реинвест - обмен серебра
		case "exchange": include("pages/finance/_exchange.php"); break; // Реинвест - обмен серебра
        case "set": include("pages/finance/_set.php"); break; // Бонусы при пополнении
        case "cash_points": include("pages/finance/_cash_points.php"); break; // Платежные баллы
        case "payeer_payment": include("pages/finance/_payeer_payment_save.php"); break; // Вывод средств через ПС Пайер

        default: @include("pages/finance/_insertnew.php"); break;

    }

} else @include("pages/finance/_insertnew.php");
?>