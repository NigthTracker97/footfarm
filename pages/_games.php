<?PHP
$_OPTIMIZATION["title"] = "Игры и развлечения";
$_OPTIMIZATION["description"] = "Игры и развлечения";
$_OPTIMIZATION["keywords"] = "Игры и развлечения";

# Блокировка сессии
if (!isset($_SESSION["user_id"])) {
    Header("Location: /");
    return;
}

if (isset($_GET["sel"])) {

    $smenu = strval($_GET["sel"]);
    switch ($smenu) {

        case "404": include("pages/_404.php"); break; // Страница ошибки
        case "wheel": include("pages/games/_wheel.php"); break; // Рулетка
        case "fortuna_wheel": include("pages/games/_fortuna_wheel.php"); break; // Колесо фортуны
        case "lottery": include("pages/games/_lottery.php"); break; // Лотерея
        default: @include("pages/games/_index.php"); break;

    }

} else @include("pages/games/_index.php");
?>