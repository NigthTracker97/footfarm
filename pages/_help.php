<?PHP
$_OPTIMIZATION["title"] = "ЧАСТО ЗАДАВАЕМЫЕ ВОПРОСЫ И ОТВЕТЫ НА НИХ";
$_OPTIMIZATION["description"] = "Помощь, Связь с администрацией";
$_OPTIMIZATION["keywords"] = "Помощь, Связь с администрацией проекта";

$db->Query("SELECT payment_limit, referals_level FROM db_config WHERE id = 1");
$syscfg = $db->FetchArray();

?>
<?PHP

if(isset($_SESSION["user"])){ 

?>

<div class="container">

<?PHP

} else { 

?>



<div class="s-bk-lf">
    <div class="acc-title">ЧАСТО ЗАДАВАЕМЫЕ ВОПРОСЫ И ОТВЕТЫ НА НИХ</div>
</div>
<div class="container">
<?PHP

} 

?>
<div class="panel panel-default">
    <div class="panel-heading" role="tab" id="heading1">
        <h4 class="panel-title">
            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="false" aria-controls="collapse1">
                Как начать играть?
            </a>
        </h4>
    </div>
    <div id="collapse1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading1">
        <div class="panel-body">
            <p>После регистрации в проекте, переходите в раздел "Покупка персонажей", где покупаете одного из персонажей,
                далее получаете каждый час опыт, затем продаете опыт в складе опыта за серебро и выводите серебро в реальные денежные средства!</p>
        </div>
    </div>
</div>
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="heading2">
            <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="false" aria-controls="collapse2">
                    У вас есть платежные баллы: PAY POINTS, CASH POINTS, и др?
                </a>
            </h4>
        </div>
        <div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2">
            <div class="panel-body">
                <p>В отличии от других подобных проектов, на нашем проекте НЕТ платежных баллов и нет лимитов на выплаты. Всё что пользователь зарабатывает, он может выводить в полном объеме в любое время. </p>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="heading3">
            <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="false" aria-controls="collapse3">
                    Если я не пополнил баланс, могу ли я начать играть?
                </a>
            </h4>
        </div>
        <div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3">
            <div class="panel-body">
                <p>Да, можете! Ежедневный бонусы позволят Вам собрать серебра для покупок, но для того чтобы купить персонажей, нужно заходить каждый день и получать бонусы! </p>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="heading4">
            <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="false" aria-controls="collapse4">
                    Как пополнить баланс?
                </a>
            </h4>
        </div>
        <div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4">
            <div class="panel-body">
                <p>Баланс Вы можете пополнить в разделе "Пополнить баланс".</p>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="heading5">
            <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse5" aria-expanded="false" aria-controls="collapse5">
                    Как мне вывести деньги из проекта?
                </a>
            </h4>
        </div>
        <div id="collapse5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading5">
            <div class="panel-body">
                <p>Вывести заработанное Вы можете в разделе "Заказать выплату". Там присутствуют специальные формы для выплат.</p>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="heading6">
            <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse6" aria-expanded="false" aria-controls="collapse6">
                    Как скоро я смогу начинать выводить деньги?
                </a>
            </h4>
        </div>
        <div id="collapse6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading6">
            <div class="panel-body">
                <p>Сразу после пополнения и покупки персонажей, в разделе "Заказать выплату". Перед выводом средств Вам нужно собрать опыт и продать в "Складе опыта"! </p>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="heading7">
            <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse7" aria-expanded="false" aria-controls="collapse7">
                    Как можно еще зарабатывать серебро в проекте, кроме пополнения?
                </a>
            </h4>
        </div>
        <div id="collapse7" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading7">
            <div class="panel-body">
                <p>Игровую валюту Вы можете заработать несколькими способами:<br/>
                    1. Получать ежедневный бонус.<br/>
                    2. Приглашать друзей в игру и получать свой процент от их пополнений.<br/>
                    3. Участвуя в конкурсе рефералов<br/>
                    4. Привлекать новых пользователей - рефералов.<br/>
                    В каждом соответствующем разделе есть подробное описание</p>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="heading8">
            <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse8" aria-expanded="false" aria-controls="collapse8">
                    Сколько уровней в вашей реферальной системе?
                </a>
            </h4>
        </div>
        <div id="collapse8" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading8">
            <div class="panel-body">
                <p>На нашем проекте действует реферальная система с глубиной до <?=$syscfg['referals_level']?> уровней. Более подробную информацию вы можете узнать в разделе "<a
                            href="/referals">Мои партнеры</a>".</p>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="heading9">
            <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse9" aria-expanded="false" aria-controls="collapse9">
                    Как мне привлечь рефералов?
                </a>
            </h4>
        </div>
        <div id="collapse9" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading9">
            <div class="panel-body">
                <p>Вам нужно отправить свою реферальную ссылку, которую вы можете найти в разделе <a href="/referals">Ваши
                        партнеры</a>, своим друзьям, коллегам, или использовать промо-площадки.</p>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="heading10">
            <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse10" aria-expanded="false" aria-controls="collapse10">
                    Какие преимущества у пользователей, которые привлекают много рефералов?
                </a>
            </h4>
        </div>
        <div id="collapse10" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading10">
            <div class="panel-body">
                <p>Заработанные на Ваших рефералах средства поступают на отдельный счет и могут быть выведенны в любое время. Более подробную информацию можно найти в разделе "Заказать выплату" -> "Экстренная выплата". </p>
            </div>
        </div>
    </div>

    <b>Если у Вас еще остались вопросы, обратитесь в нашу техническую поддержку!</b>

    <br><br>

    <div class="clr"></div>
</div></div>


