
<?PHP

$c_date = date("Ymd", time());
$c_date_begin = strtotime($c_date . " 00:00:00");
$online = time() - 60 * 60 * 5;

$tfstats = time() - 60 * 60 * 24;
$db->Query("SELECT * FROM db_stats WHERE id = 1");
$stats_data = $db->FetchArray();

$kfsf = 24.1;
$_SESSION['bbn2'] = $stats_data['new_users']*$kfsf;
$_SESSION['bbn1'] = $_SESSION['bbn2']*14.5;
$_SESSION['bbn3'] = $stats_data['today_users']*$kfsf; 

?>
<div style="background: white">
	<section class="header-vector">
		<div class="header-stores">
			<div class="layout">
			<ul>
			<li><h4><span>Всего</span> <span>Пользователей</span></h4><div class="clr"></div><span class="include-icon"><?= $stats_data["all_users"]; ?> ЧЕЛ.</span></li>
			<li><h4><span>Резерв проекта </span></h4><div class="clr"></div><span class="include-icon"><?= sprintf("%.0f", $stats_data["all_insert"]); ?> РУБ.</span></li>
			<li><h4><span>ВСЕГО Выплачено</span></h4><div class="clr"></div><span class="include-icon"><?= sprintf("%.0f", $stats_data["all_payments"]); ?> Руб. </span></li>
			</ul>
			</div>
		</div>
		<div class="carsBlock">
        	<i class="fa fa-arrow-left aLeft" aria-hidden="true"></i>
        	<div class="carsArea">
        		<div class="carsPlane">
        			<div class="car">
        				<div class="cImage img-vertical">
        					<img src="images/v2/rueaca0887773.jpeg" class="img-responsive">

        				</div>
        				<div class="cLevel"><b>50</b>РУБЛЕЙ</div>
        				<div class="cLeve2">ТОТТЕНХЕМ</div>
        				<div class="cInfo">
        					<div class="cPrice">Доходность <font style="font-size: 22px;color: #13adc0;">15%</font> в месяц</div>
        				</div>
        				<div class="cBuy"></div>
        			</div>
        			<div class="car">
        				<div class="cImage img-vertical">
        					<img src="images/v2/rue4a417205ae.jpeg" class="img-responsive">
        				</div>
        				<div class="cLevel"><b>150</b>РУБЛЕЙ</div>
        				<div class="cLeve2">БАВАРИЯ</div>
        				<div class="cInfo">
        					<div class="cPrice">Доходность <font style="font-size: 22px;color: #13adc0;">20%</font> в месяц</div>
        				</div>
        				<div class="cBuy"></div>
        			</div>
        			<div class="car">
        				<div class="cImage img-vertical">
        					<img src="images/v2/rue2f5f4a4fd1.jpeg" class="img-responsive">
        				</div>
        				<div class="cLevel"><b>450</b>РУБЛЕЙ</div>
        				<div class="cLeve2">ЛИВЕРПУЛЬ</div>
        				<div class="cInfo">
        					<div class="cPrice">Доходность <font style="font-size: 22px;color: #13adc0;">25%</font> в месяц</div>
        				</div>
        				<div class="cBuy"></div>
        			</div>
        			<div class="car">
        				<div class="cImage img-vertical">
        					<img src="images/v2/rue5281d41089.jpeg" class="img-responsive">
        				</div>
        				<div class="cLevel"><b>1250</b>РУБЛЕЙ</div>
        				<div class="cLeve2">ЧЕЛСИ</div>
        				<div class="cInfo">
        					<div class="cPrice">Доходность <font style="font-size: 22px;color: #13adc0;">27%</font> в месяц</div>
        				</div>
        				<div class="cBuy"></div>
        			</div>
        			<div class="car">
        				<div class="cImage img-vertical">
        					<img src="images/v2/rue80ff95a4b4.jpeg" class="img-responsive">
        				</div>
        				<div class="cLevel"><b>2500</b>РУБЛЕЙ</div>
        				<div class="cLeve2">ЦСКА</div>
        				<div class="cInfo">
        					<div class="cPrice">Доходность <font style="font-size: 22px;color: #13adc0;">29%</font> в месяц</div>
        				</div>
        				<div class="cBuy"></div>
        			</div>
        			<div class="car">
        				<div class="cImage img-vertical">
        					<img src="images/v2/rueb389d6bfbe.jpg" class="img-responsive">
        				</div>
        				<div class="cLevel"><b>5000</b>РУБЛЕЙ</div>
        				<div class="cLeve2">ЗЕНИТ</div>
        				<div class="cInfo">
        					<div class="cPrice">Доходность <font style="font-size: 22px;color: #13adc0;">30%</font> в месяц</div>
        				</div>
        				<div class="cBuy"></div>
        			</div>
        			<div class="car">
                        <div class="cImage img-vertical">
                            <img src="images/v2/rue00d7d50127.jpeg" class="img-responsive">
                        </div>
                        <div class="cLevel"><b>15000</b>РУБЛЕЙ</div>
                        <div class="cLeve2">РЕАЛ МАДРИД</div>
                        <div class="cInfo">
                            <div class="cPrice">Доходность <font style="font-size: 22px;color: #13adc0;">32%</font> в месяц</div>
                        </div>
                        <div class="cBuy"></div>
                    </div>
                    <div class="car">
                        <div class="cImage img-vertical">
                            <img src="images/v2/rue6d982f73e3.jpeg" class="img-responsive">
                        </div>
                        <div class="cLevel"><b>25000</b>РУБЛЕЙ</div>
                        <div class="cLeve2">МАНЧЕСТЕР ЮНАЙТЕД</div>
                        <div class="cInfo">
                            <div class="cPrice">Доходность <font style="font-size: 22px;color: #13adc0;">34%</font> в месяц</div>
                        </div>
                        <div class="cBuy"></div>
                    </div>
                    <div class="car">
                        <div class="cImage img-vertical">
                            <img src="images/v2/rue2997571681.jpeg" class="img-responsive">
                        </div>
                        <div class="cLevel"><b>50000</b>РУБЛЕЙ</div>
                        <div class="cLeve2">СПАРТАК</div>
                        <div class="cInfo">
                            <div class="cPrice">Доходность <font style="font-size: 22px;color: #13adc0;">36%</font> в месяц</div>
                        </div>
                        <div class="cBuy"></div>
                    </div>
                    <div class="car">
                        <div class="cImage img-vertical">
                            <img src="images/v2/rue1f07b4ae80.jpeg" class="img-responsive">
                        </div>
                        <div class="cLevel"><b>75000</b>РУБЛЕЙ</div>
                        <div class="cLeve2">БАРСЕЛОНА</div>
                        <div class="cInfo">
                            <div class="cPrice">Доходность <font style="font-size: 22px;color: #13adc0;">38%</font> в месяц</div>
                        </div>
                        <div class="cBuy"></div>
                    </div>
        		</div>
        	</div>
        	<i class="fa fa-arrow-right aRight" aria-hidden="true"></i>
        </div>
        <div class="header-vector__mark">100% на вывод</div>
		<div class="countdown-wrapper">
			<div id="defaultCountdown"></div>
		</div>
	</section>

<section class="container-fluid  info" style="padding-bottom: 30px;">

	
	<div class="container-fluid__main">
		<h3 class="h2">Онлайн игра с выводом средств «<?=$config->site_name?>»</h3>
		<p>
			Добро пожаловать в увлекательную онлайн игру с выводом денежных средств «<?=$config->site_name?>». Все что от вас требуется это зарегистрироваться на нашем проекте и продумать свою стратегию игры для извлечения максимальной прибыли. <center>При регистрации в игре, <b>50 РУБ</b> в подарок!</center>
		</p>
	</div>

	<a href="/signup" class="signup-link">Зарегистрироваться прямо сейчас и получить 50 рублей!</a>

</section>

<span class="divider-2 index-s-divider"></span>


<section class="container-fluid" style="padding-bottom: 30px; padding-top: 30px;">

	<div class="<?=$detect->isMobile() ? '' : 'row'; ?>"> 
		<div class="col-12 col-sm-12 title-wrap">
			<h2 class="section-title" <?=$detect->isMobile() ? 'style="font-size: 30px;"' : ''; ?>> <span class="light-font">наши </span> <strong> преимущества</strong> </h2>
			<h4 class="sub-title"> основные качества </h4>
		</div>
		<div class="container-fluid__container">
			<div class="col-12 col-md-4 pt-50 container-fluid__container__f">
				<div class="left"> 
					<div class="quality-img"><div class="im1"></div></div>
					<div class="quality-caption"> 
						<h2 class="title-1 index-f-title">Уникальный скрипт</h2>
						<span class="divider-2 index-f-divider"></span>
						<p class="index-f-info">Мы используем уникальный скрипт собственной разработки, сайт создавался с нуля лично нами</p>
					</div>                               
				</div>
				<div class="left"> 
					<div class="quality-img"><div class="im2"></div></div>
					<div class="quality-caption"> 
						<h2 class="title-1 index-f-title">Открытая статистика</h2>
						<span class="divider-2 index-f-divider"></span>
						<p class="index-f-info">Статистика проекта всегда открыта и доступна для просмотра любому пользователю проекта</p>
					</div>                               
				</div>
				<div class="left"> 
					<div class="quality-img"><div class="im3"></div></div>
					<div class="quality-caption"> 
						<h2 class="title-1 index-f-title">Регулярные конкурсы</h2>
						<span class="divider-2 index-f-divider"></span>
						<p class="index-f-info">Каждый месяц мы проводим конкурсы активности, участие в которых может принять любой участник проекта</p>
					</div>                               
				</div>
			</div>
			<div class="col-md-4 text-center index-992-none"> 
				<img class="tower-img" src="/img/icons/tower.png">
			</div>
			<div class="col-12 col-md-4 pt-50 hidden-phone"> 
				<div class="right"> 
					<div class="quality-img"><div class="im4"></div></div>
					<div class="quality-caption"> 
						<h2 class="title-1 index-f-title">Выделенный сервер</h2>
						<span class="divider-2 index-f-divider"></span>
						<p class="index-f-info">Проект размещен на выделенном, защищенном сервере, что обеспечит максимальную доступность</p>
					</div> 
				</div>
				<div class="right"> 
					<div class="quality-img"><div class="im5"></div></div>
					<div class="quality-caption"> 
						<h2 class="title-1 index-f-title">Мобильная версия</h2>
						<span class="divider-2 index-f-divider"></span>
						<p class="index-f-info">Сайт доступен с любых мобильных устройств, и будет масштабироваться под Ваше разрешение экрана</p>
					</div> 
				</div>
				<div class="right"> 
					<div class="quality-img"><div class="im6"></div></div>
					<div class="quality-caption"> 
						<h2 class="title-1 index-f-title">Плавный маркетинг</h2>
						<span class="divider-2 index-f-divider"></span>
						<p class="index-f-info">Благодаря плавному и продуманному маркетингу FootFarm.Org обещает стать настоящим долгожителем</p>
					</div> 
				</div>
			</div>
		</div>                    
	</div>
</section>

<span class="divider-2 index-s-divider"></span>

<section class="container-fluid info" style="padding-bottom: 30px;">
	<div style="padding-top: 10px; font-family: 'Open Sans', sans-serif;">
		<h3 class="h2">ИГРА «<?=$config->site_name?>», КАК ПАССИВНЫЙ ИСТОЧНИК ДОХОДА</h3>
		<p style="text-align: center;">
		Для всех пользователей проекта также доступна партнерская программа. Мы выплачиваем <b>10%</b> от суммы каждого пополнения баланса приглашенных Вами игроков (рефералов), а так же <b>5%</b> от рефералов 2 уровня. В нашей игре «<?=$config->site_name?>» вас ждут постоянные конкурсы, акции и бонусы!
		</p>
	</div>
</section>

<span class="divider-2 index-s-divider"></span>

<?PHP
$_OPTIMIZATION["title"] = "О проекте";
$_OPTIMIZATION["description"] = "О нашем проекте";
$_OPTIMIZATION["keywords"] = "Немного о нас и о нашем проекте";
?>
<style>
.none {
    color: #343636;
    font-family: 'Open Sans', sans-serif;
    font-weight: 600;
    margin-top: 20px;
    font-size: 14px;
}
</style>




<section class="container-fluid info" style="padding-top: 10px; font-family: 'Open Sans', sans-serif;">
<h3 class="h2">О НАШЕМ ПРОЕКТЕ</h3>
<div class="container">
<p><span>Стоимость клубов всего от 50 рублей (5000 серебра).&nbsp;</span><br /><br /><span>Автоматическая система накопления прибыли! Сбор прибыли без потерь, без ограничений по срокам! Собирайте прибыль, так как удобно именно Вам!&nbsp;</span><br /><br /><span>Хоть раз в час, хоть раз в день, хоть раз в месяц! &nbsp;</span><br /><br /><span>Системный рынок позволит мгновенно обменять монеты на Серебро!&nbsp;</span><br /><br /><span>Максимально быстрые выплаты денег на Ваш кошелек!&nbsp;</span><br /><br /><span>В процессе игры дополнительно будут добавлены новые функции и возможности!&nbsp;</span></p>
</div>
</section>

<div class="op" style="text-align: center; color: white;">.</div>
<div class="op2 hide">
<p>FootFarm.Com &ndash; это новая экономическая игра с выводом реальных денег. На данный момент на сайте зарегистрировано около 5000 человек, но, не смотря на это, проект является достаточно интересным, учитывая его недавнее создание, ведь только две недели прошло с тех пор, как он был создан.</p>
<p>На сайте выказывается резерв проекта, который составляет более 100 тысяч рублей, на сегодня уже выплачено более 6 тысяч рублей, что впечатляет, учитывая пока что недолгое время, которое прошло с момента создания проекта.</p>
<p>Сайт доступен на разных языках, которыми пользователь может воспользоваться.</p>
<p>Прямо на сайте указываются преимущества именно этого проекта по сравнению с другими, среди которых:</p>
<ol>
<li>Уникальный скрипт указывает на тот факт, что сайт не был одолжен с чьей-то идеи, а был создан практически с нуля модераторами сайта;</li>
<li>Открытость статистики сайта. Этот факт уже был наведен свыше, где было указано примерное количество пользователей, а также резерв проекта. На сайте все указано с точностью к единицам, и это говорит о прозрачности данного проекта по сравнению с другими проектами данного рода;</li>
<li>Проведение различных конкурсов активности. Они проводятся раз в месяц, и, как уверяет нас администрация, участия в них сможет без труда взять любой желающий. Проводятся также кроме конкурсов и различные акции, некоторые игроки награждаются разного рода бонусами;</li>
<li>Доступность сервера благодаря эго выделению на отдельном сервере, что будет гарантировать некую защиту данного сервера;</li>
<li>Доступность для разных гаджетов в связи с существованием мобильной версии сайта, которая будет оптимизироваться под любое разрешение вашего экрана;</li>
<li>Как уже можно сделать выводы, сайт действительно обещает существовать долго, ведь этому способствуют большой бюджет, а также, здравый подход модераторов к постройке данного проекта, что действительно гарантирует хорошие варианты легкого заработка на данном ресурсе.</li>
</ol>
<p>На сайте также прямо указывается на существование партнерской программы, что является очень хорошим стимулом для людей, которые участвуют в данном проекте, ведь в таком случае можно побольше заработать. Естественно, этот вид заработка относиться к пассивным, но кто же не хочет получать пассивный доход в наше время, когда деньги нужны всем! И сайт, как уже говорилось выше, гарантирует легальность этого заработка. Суть партнерской программы базируется на том, что модераторами проекта выплачивается <strong>10%</strong>&nbsp;от суммы каждого пополнения баланса приглашенных Вами игроков (рефералов), а так же&nbsp;<strong>5%</strong>&nbsp;от рефералов, которые достигли второго уровня. На заметку, рефералы &ndash; это те люди, которые зарегистрировались на сайте при помощи вашей реферальной ссылки, которая у каждого участника проекта разная.</p>
<p>Теперь перейдем непосредственно к структуре сайта. И так, в верхнем углу расположено поле, где каждый может авторизоваться, а те, кто еще не зарегистрирован на проекте, может зарегистрироваться. Также там расположены разделы;</p>
<ul>
<li>&laquo;Новости&raquo;. Все свежие новости будут размещаться именно там. Также будет указываться там о проведение конкурсов, все анонсы об изменениях в структуре сайта, либо что то еще. Одним словом, в этом разделе каждый сможет узнать, чем живёт сайт на данный момент, и какие изменения будут происходить в недалеком будущем;</li>
<li>&laquo;Статистика&raquo;. Данный раздел указывает точную статистику проекта, например, количество участников проекта, а также, выплаты участникам проекта. Обновляется в данном разделе рейтинг участников по доходу с рефералов, а также рейтинг участников по выводу средств, существуют также и другие рейтинги;</li>
<li>Раздел &laquo;Контакты&raquo;. Указывается обратная связь с администраторами. Также с последними можно связаться и без регистрации, в таком случае следует только отправить письмо при помощи электронной почты на адрес <strong>support@footfarm.com</strong> ;</li>
<li>&laquo;Серфинг&raquo;. Здесь участники могут выполнять различные задания и получать за это доход, который приравнивается к пополнению баланса. Нужно сказать, что этот раздел доступен только для авторизованных пользователей</li>
<li>&laquo;Правила&raquo;. Размещены правила, права та обязанности сторон.</li>
</ul>
<p>Теперь перейдем непосредственно к проекту, на чем он базируется, какие правила. И так, необходимо купить для начала клуб, который регулярно будет приносить нам доход. Минимальная стоимость такого здания составляет 50 рублей, или на валюту игры &ndash; это 5000 серебра. Есть также и другие клубы, которые и стоят больше, но зато приносят намного больший доход. И так, откуда же взять эти первоначальные 50 рублей? Здесь существуют два варианты. Первый из них &ndash; это попросту пополнить свой баланс. Второй способ более экономный, но с другой стороны требует больше времени для накопления первоначального капитала. Речь идет о серфинге, где выполняя задания можно заработать тот первоначальный капитал, с помощью которого можно приобрести здание.</p>
<p>У каждого возникнет вопрос, а что же дальше делать после того, как мы приобретем первый футбольный клуб. А дальше необходимо просто собирать прибыль. Когда накопим достаточно прибыли, можем ее вывести на кошелек, либо же за нее купить еще клубы и таким образом зарабатывать еще больше. Кто как захочет, так и сделает.</p>
<p>Немного о прибыли. Накапливается она автоматически, то есть вам ничего для этого не нужно делать. Только в определенный срок необходимо ее собрать себе в копилку. Сбор осуществляется в любое удобное для вас время, без каких либо потерь прибыли и потом при помощи системы, которая работает на сайте, обменивайте эти получение монеты на серебро.</p>
<p>Что касается вывода средств, то они зачисляются максимально быстро на ваш кошелек.</p>
<p>Виды клубов, которые присутствуют в игре, следующие:</p>
<ul>
<li>Клуб ТОТТЕНХЕМ. Самый дешевый клуб, и, естественно, с него пользователь получает самую низкую прибыль. Так, его цена составляет всего лишь 50 рублей и доходность в месяц 15%;</li>
<li>Клуб БАВАРИЯ, стоимость которого 150 рублей и доходность в месяц 20%;</li>
<li>Клуб ЛИВЕРПУЛЬ - это следующий клуб, которое стоит 450 рублей, но и доходность соответственно составляет 25% в месяц;</li>
<li>Клуб ЧЕЛСИ. Цена 1250 рублей, доходность 27% в месяц;</li>
<li>Клуб ЦСКА &ndash; его цена составляет 2500 рублей, доходность его 29% в месяц;</li>
<li>Клуб ЗЕНИТ &ndash; его цена составляет 5000 рублей. Доходность этого клуба &ndash; 30% в месяц.</li>
<li>Клуб РЕАЛ МАДРИД - его цена которая составляет 5000 рублей. Доходность этого клуба &ndash; 32% в месяц.</li>
<li>Клуб МАНЧЕСТЕР ЮНАЙТЕД - его цена составляет 5000 рублей. Доходность этого клуба &ndash; 34% в месяц.</li>
<li>Клуб СПАРТАК - его цена составляет 5000 рублей. Доходность этого клуба &ndash; 36% в месяц.</li>
<li>Клуб БАРСЕЛОНА - его цена составляет 5000 рублей. Доходность этого клуба &ndash; 38% в месяц.</li>
</ul>
<p>Отдельно также хочется сказать о правах и обязанностях участников. Так, принимать участие в проекте имеют право только лица, которые обладают дееспособностью. Участник несет ответственность за несоблюдение этого пункта. Каждый участник обязан сообщать достоверные ведомости о своей личности.</p>
<p>Запрещается использовать любые программы, которые дают, какие либо преимущества в игре. Аккаунты таких участников будут блокироваться администрацией. Также запрещается использование ненормативной лексики и оскорблений в любой форме, не можно обманывать участников и организаторов проекта.</p>
<p>Если возникнут какие то разногласия, то решить их обязан организатор, и, именно решение, которое примет организатор будет окончательным. Переговоры &ndash; единственный путь решения споров и разногласий.</p>
<p>Исходя из того, что проект полностью легальный, на информацию, полученную администрацией от участника проекта, распространяются условия конфиденциальности. Организатор даже, исходя из правил проекта, гарантирует что данные, сообщенные участником при регистрации в Игре, будут использоваться Организатором только внутри Игры. Персональная информация об участнике может быть передана третьим лицам только в случае, если сам же участник изъявил такое желание раскрыть данную информацию.</p>
<p>По этому, учитывая все выше сказанное, можно с уверенностью сказать, что этот проект является уникальным, ведь здесь существует реальная возможность большого заработка, при малых усильях со стороны участников проекта. Как уже говорилось, любому участнику следуют только лишь зарегистрироваться и вложить первые 50 рублей (получить которые можно не только вкладывая свои деньги, но и выполняя на сайте различные задания) и потом следует только собирать прибыль и менять ее на реальные деньги. Все очень просто ведь.</p>
</div>



<script src="/js/pagefont.js"></script>
<script src="/js/jquery.plugin.min.js"></script>
<script src="/js/jquery.countdown.min.js?1"></script>
<style>
.hide {
	display: none;
}
</style>
<script>
	if($().countdown){
		var newYear=new Date();
		newYear=new Date(newYear.getFullYear()+ 1,1- 1,1);
        $('#defaultCountdown').countdown({since:new Date(2018,4,26,21)});
	}
	
	$(".op").click(function(){
		$(".op2").toggleClass("hide");
	});
</script>
</div>
