<div class="s-bk-lf">
    <div class="acc-title">ВХОД В ЛИЧНЫЙ КАБИНЕТ</div>
</div>
<?
if ($detect->isMobile()) {
?> 
	<section class="container-fluid">
<?
}else{
?>
	<section class="container">
		<div class="row">
<?
}

# Настройки
$db->Query("SELECT captcha FROM db_config WHERE id = '1' LIMIT 1");
$captcha_check = $db->FetchRow();

include("./_geolocation/SxGeo.php");
$SxGeo = new SxGeo('./_geolocation/SxGeoCity.dat');

function geodataISO($ip){
    global $SxGeo;
    $geo_date = $SxGeo->getCityFull($ip);
    $country = iconv("UTF-8","Windows-1251",$geo_date['country']['iso']);
    if($country == '') $country = 'none';
    return $country;
}

if (isset($_POST["log_email"])) {

    if (!$_SESSION['admin4ik'] && $captcha_check == 1){

        if (empty($_POST['g-recaptcha-response'])) {
            $recaptcha = false;
        }

        $url = 'https://www.google.com/recaptcha/api/siteverify';

        $secret = $config->re_secret_key;
        $recaptcha = $_POST['g-recaptcha-response'];
        $ip = $_SERVER['REMOTE_ADDR'];

        $url_data = $url . '?secret=' . $secret . '&response=' . $recaptcha . '&remoteip=' . $ip;
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, $url_data);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $res = curl_exec($curl);
        curl_close($curl);

        $res = json_decode($res);

        if ($res->success) {
            $recaptcha = true;
        } else {
            $recaptcha = false;
        }
    } else {
        $recaptcha = true;
    }

    $lmail = $func->IsMail($_POST["log_email"]);

    if ($recaptcha) {

        if ($lmail !== false) {

            $db->Query("SELECT id, user, pass, referer_id, banned FROM db_users_a WHERE email = '$lmail'");
            if ($db->NumRows() == 1) {

                $log_data = $db->FetchArray();

                if (strtolower($log_data["pass"]) == strtolower($_POST["pass"])) {

                    if ($log_data["banned"] == 0) {

                        # Настройки
                        $sonfig_site = $func->config_site($db, $log_data["id"]);

                        $sqlquery = "SELECT (SELECT COUNT(id) FROM db_users_c WHERE referer1_id = {$log_data["id"]}) level_1";
                        for($i=1;$i<=$sonfig_site["referals_level"];$i++){
                            $sqlquery .= ", (SELECT COUNT(id) FROM db_users_c WHERE referer{$i}_id = {$log_data["id"]}) level_{$i}";
                        }

                        $db->Query($sqlquery);
                        $refs = $db->FetchArray();

                        for($i=1;$i<=$sonfig_site["referals_level"];$i++){
                            $all_referals += $refs['level_'.$i];
                        }
                        $user_ip = $func->UserIP;
                        $time = time();
                        $session_id = session_id();
                        $db->Query("UPDATE db_users_a SET date_login = '{$time}', referals = {$all_referals}, session_id = '{$session_id}' WHERE id = {$log_data["id"]}");

                        if(!isset($_SESSION['refsale_enable'])){
                            $sonfig_site = $func->config_site($db,$log_data["id"]);
                            $_SESSION['refsale_enable'] = $sonfig_site['refsale_enable'];
                        }

                        # Записываем историю входов в аккаунт
                        $db->Query("INSERT INTO db_login SET user_id = {$log_data["id"]}, ip = INET_ATON('{$user_ip}'), date = {$time}");
                        # Количество строк, которое нужно оставить
                        $num_row = 10;
                        # Удаляем все кроме $num_row строк из таблицы
                        $db->Query("DELETE FROM db_login WHERE user_id = {$log_data["id"]} AND id NOT IN(SELECT id FROM (SELECT id FROM db_login WHERE user_id = {$log_data["id"]} ORDER BY id DESC LIMIT {$num_row}) AS t)");

                        $_SESSION['user_ip'] = $user_ip;
                        $_SESSION['country'] = geodataISO($user_ip);
                        $_SESSION["user_id"] = $log_data["id"];
                        $_SESSION["user"] = $log_data["user"];
                        $_SESSION["referer_id"] = $log_data["referer_id"];
                        Header("Location: /account");
                        //header( "Location: http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] );

                    } else echo "<div class='col-12 col-sm-12 sign-up' style='text-align: center;'><b>Аккаунт заблокирован</b></div>";

                } else echo "<div class='col-12 col-sm-12 sign-up' style='text-align: center;'><b>Email и/или Пароль указан неверно</b></div>";

            } else echo "<div class='col-12 col-sm-12 sign-up' style='text-align: center;'><b>Указанный Email не зарегистрирован в системе</b></div>";

        } else echo "<div class='col-12 col-sm-12 sign-up' style='text-align: center;'><b>Email указан неверно</b></div>";
    } else echo "<div class='col-12 col-sm-12 sign-up' style='text-align: center;'><b>Каптча указана не верно</b></div>";

}

if (!$_SESSION['admin4ik'] && $captcha_check == 1) {?>
	<script src="https://www.google.com/recaptcha/api.js?onload=loadCaptcha&render=explicit" async defer></script>
	<script type='text/javascript'>
		var captchaContainer = null;
		var loadCaptcha = function () {
			captchaContainer = grecaptcha.render('captcha_container', {
				'sitekey': '<?=$config->re_site_key?>',
				'bind': 'recaptcha-submit',
				'badge': 'inline',
				'size': 'compact',
				'callback': function (response) {
					//console.log(response); 
					$("#loginste").submit();
				}
			});
		};
	</script>
<? } ?>

<div class="col-12 col-sm-5 sign-up">
<form action="" method="post">
    <h1 class="sign-up-title">АВТОРИЗАЦИЯ</h1>
    <input type="email" class="sign-up-input" placeholder="Введите ваш e-mail" name="log_email" required size="23" maxlength="35" onfocus="if (this.value == 'Email') this.value = '';" onblur="if (this.value == '') this.value = 'Email';" value="Email">
	<input type="password" class="sign-up-input" placeholder="Введите пароль" required size="23" maxlength="35" onfocus="if (this.value == 'Пароль') this.value = '';" onblur="if (this.value == '') this.value = 'Пароль';" value="Пароль" name="pass">

	
	 
	<input type="submit" value="ПРОДОЛЖИТЬ" id="recaptcha-submit" class="sign-up-button" style="margin-top: 10px;">
	  <div style="display:none;" id="captcha_container"></div>
	<label>
	 
	 <font style="font-weight: 600;font-size: 11px;margin-top: 10px;">
	  <a href="/recovery" target="_blank" class="stn" style="border-bottom: none;">Восстановить пароль</a>  
	 </font>
	 </label>
  </form>
</div>

<div class="col-12 col-sm-5 offset-md-1 sign-up" >
<h1 class="sign-up-title" >ВОПРОС-ОТВЕТ</h1>
<div class="layer" style="height: 229px;">
<b>Вопрос:</b> Как начать играть? <br>
    <b>Ответ:</b> После регистрации в проекте, переходите в раздел "Покупка персонажей", где покупаете одного из персонажей,
    далее получаете каждый час опыт, затем продаете опыт в торговой лавке за серебро и выводите серебро в реальные денежные средства!
    <br><br>

        <b>Вопрос:</b> У вас есть платежные баллы: PAY POINTS, CASH POINTS, и др? <br>
    <b>Ответ:</b> В отличии от других подобных проектов, на нашем проекте НЕТ платежных баллов и нет лимитов на выплаты. Всё что пользователь зарабатывает, он может выводить в полном объеме в любое время.
    <br><br>
    
    <b>Вопрос:</b> Если я не пополнил баланс, могу ли я начать играть?<br>
    <b>Ответ:</b> Да, можете! Ежедневный бонусы позволят Вам собрать серебра для покупок, но для того чтобы купить
    персонажей, нужно заходить каждый день и получать бонусы! Есть разные бонусы, в том числе "При регистрации Персонаж в подарок".<br><br>

    <b>Вопрос:</b> Как пополнить баланс?<br>
    <b>Ответ:</b> Баланс Вы можете пополнить в разделе "Пополнить баланс".<br><br>

    <b>Вопрос</b>: Как мне вывести деньги из проекта?<br>
    <b>Ответ</b>: Вывести заработанное Вы можете в разделе "Заказать выплату". Там присутствуют специальные формы для
    выплат.<br><br>

    <b>Вопрос</b>: Как скоро я смогу начинать выводить деньги?<br>
    <b>Ответ</b>: Сразу после пополнения и покупки персонажей, в разделе "Заказать выплату". Перед выводом средств Вам нужно
    собрать опыт и продать в "торговой лавке"!
    <br><br>


    <b>Вопрос</b>: Как можно еще зарабатывать серебро в проекте, кроме пополнения?<br>
    <b>Ответ</b>:
    Игровую валюту Вы можете заработать несколькими способами:<br>
    1. Получать ежедневный бонус.<br>
    2. Приглашать друзей в игру и получать свой процент от их пополнений.<br>
    3. Участвуя в конкурсе рефералов<br>
    4. Получать разнообразные бонусы в социальных сетях.<br>
    5. Привлекать новых пользователей - рефералов.<br>

    В каждом соответствующем разделе есть подробное описание
    <br><br>

    <b>Вопрос</b>: Сколько уровней в вашей реферальной системе?<br>
    <b>Ответ</b>:
    На нашем проекте действует реферальная система с глубиной до 2 уровней. Более подробную информацию вы можете узнать в разделе "<a href="/account/referals">Мои партнеры</a>".
    <br><br>

    <b>Вопрос</b>: Как мне привлечь рефералов?<br>
    <b>Ответ</b>:
    Вам нужно отправить свою реферальную ссылку, которую вы можете найти в разделе <a href="/account/referals">Ваши
        партнеры</a>, своим друзьям, коллегам, или использовать промо-площадки.
    <br><br>
 
    <b>Вопрос</b>: Какие преимущества у пользователей, которые привлекают много рефералов?<br>
    <b>Ответ</b>:
    Заработанные на Ваших рефералах средства поступают на отдельный счет и могут быть выведенны в любое время. Более подробную информацию можно найти в разделе "Заказать выплату" -&gt; "Экстренная выплата".
    <br><br>

    <b>Если у Вас еще остались вопросы, обратитесь в нашу техническую поддержку!</b>

    <br>
</div></div>





	</div>
</section>



