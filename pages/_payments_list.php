<?PHP

//if (!isset($_SESSION['admin'])) return;

$_OPTIMIZATION["title"] = "Статистика проекта";
$_OPTIMIZATION["description"] = "Статистика проекта";
$_OPTIMIZATION["keywords"] = "Статистика проекта";

$hour = 12;

$sonfig_site = $func->config_site($db);

$db->Query("SELECT * FROM db_stats WHERE id = 1");
$stats_data = $db->FetchArray();

?>
<style>
.course__price {
    font-size: 14px;
    font-weight: 400;
    padding: 24px 23px 22px;
    color: black;
    border: 1px solid;
    box-shadow: inset 0 1px 3px rgba(0,0,0,.33);
	background: none;
}
.popup_protect__def__img {
    display: inline-block;
    margin-right: 19px;
    width: 50px;
    margin-bottom: 20px;
    padding-bottom: 10px;
    vertical-align: middle;
}
.card__head {
	display: inline-block;
    position: relative;
    padding: 25px;
}
.card__name, .card__period {
    font-weight: 700;
    text-transform: uppercase;
}
.card__name {
    font-size: 15px;
    margin-top: 4px;
    margin-bottom: 8px;
}
.card__period {
    font-size: 13px;
    color: #98b5c7;
}
.card__period:before {
    content: '';
    position: relative;
    display: inline-block;
    background-image: url(/images/stats/clock.png);
    width: 16px;
    height: 16px;
    top: 3px;
    margin-right: 7px;
}
.bordered {
    -moz-border-radius: 6px;
    -webkit-border-radius: 6px;
    border-radius: 6px;
    -webkit-box-shadow: 0 1px 1px #ccc;
    -moz-box-shadow: 0 1px 1px #ccc;
    box-shadow: 0 1px 1px #ccc;
}
.bordered td, .bordered th {
    border-left: 1px solid #f7f7f7;
    border-top: 1px solid #f7f7f7;
    padding: 15px;
    text-align: center;
}
.bordered th {
    text-transform: uppercase;
    font-weight: bold;
    letter-spacing: 1px;
    background-color: #dce9f9;
    background-image: -webkit-gradient(linear,left top,left bottom,from(#ebf3fc),to(#dce9f9));
    background-image: -webkit-linear-gradient(top,#ebf3fc,#dce9f9);
    background-image: -moz-linear-gradient(top,#ebf3fc,#dce9f9);
    background-image: -ms-linear-gradient(top,#ebf3fc,#dce9f9);
    background-image: -o-linear-gradient(top,#ebf3fc,#dce9f9);
    background-image: linear-gradient(top,#ebf3fc,#dce9f9);
    -webkit-box-shadow: 0 1px 0 rgba(255,255,255,.8) inset;
    -moz-box-shadow: 0 1px 0 rgba(255,255,255,.8) inset;
    box-shadow: 0 1px 0 rgba(255,255,255,.8) inset;
    border-top: none;
    text-shadow: 0 1px 0 rgba(255,255,255,.5);
}

@media screen and (max-width:767px){
	.bordered td, .bordered th {
		padding: 0!important;
		padding-top: 10px;
		padding-bottom: 10px;
	}
	.popup_protect__def__img {
		display: none;
	}
}
</style>
<?PHP

if(isset($_SESSION["user"])){ 

?>

<div class="container">

<?PHP

} else { 

?>



<div class="s-bk-lf">
    <div class="acc-title">Статистика проекта</div>
</div>
<div class="container">
<?PHP

} 

?>

<div class="row">

<div class="col-md-6 col-12">
<div class="course__price" style="margin-bottom:15px;">
<div class="course__price-item">
<div class="course__price-kind">Всего пополнено:</div>
<div class="course__price-value"><?=round($stats_data['all_insert'], 2);?> руб.</div>
</div>
<div class="course__price-item">
<div class="course__price-kind">Всего выплачено:</div>
<div class="course__price-value"><?=round($stats_data['all_payments'], 2);?> руб.</div>
</div>
</div>
</div>
<div class="col-md-6 col-12">
<div class="course__price" style="margin-bottom:15px;">
<div class="course__price-item">
<div class="course__price-kind">Всего участников:</div>
<div class="course__price-value"><?=$stats_data['all_users'];?> чел.</div>
</div>
<div class="course__price-item">
<div class="course__price-kind">Новых за 24 часа:</div>
<div class="course__price-value"><?=$stats_data['today_users'];?> чел.</div>
</div>
</div>
</div>

<div class="col-md-6 col-12">
<div class="popup_protect__def__img">
<img src="/images/stats/stats_1.png">
</div>
<div class="card__head">
<h3 class="card__name">Рейтинг участников по доходу с рефералов</h3>
<div class="card__period">Обновление раз в 60 минут</div>
</div>
<table class="bordered" style="width:100%;margin-bottom:15px;">
<thead>
<tr>
<th>Логин</th>
<th>Дата регистрации</th>
<th>Реф. доход</th>
</tr>
</thead>
<tbody>
<?
	$db->Query("SELECT t1.user, t1.from_referals, t2.date_reg 
	FROM db_users_b t1 
	LEFT OUTER JOIN db_users_a t2
	ON t2.id = t1.id WHERE t2.user <> 'system' ORDER BY t1.from_referals DESC LIMIT 5");
	while($ref = $db->FetchArray()){
?>
<tr>
<td><?=$ref['user'];?></td>
<td><?=$func->time2word($ref['date_reg']);?></td>
<td><?=$ref['from_referals'];?> руб.</td>
</tr>
<?
	}
?>
</tbody></table>
</div>

<div class="col-md-6 col-12">
<div class="popup_protect__def__img">
<img src="/images/stats/stats_2.png">
</div>
<div class="card__head">
<h3 class="card__name">Рейтинг участников по сумме вывода средств</h3>
<div class="card__period">Обновление раз в 10 минут</div>
</div>
<table class="bordered" style="width:100%;margin-bottom:15px;">
<thead>
<tr>
<th>Логин</th>
<th>Дата регистрации</th>
<th>Выведено</th>
</tr>
</thead>
<tbody>
<?
	/*$db->Query("SELECT t1.user, t2.date_reg, t1.all_time_a, t1.all_time_b, t1.all_time_c, t1.all_time_d, t1.all_time_e, t1.all_time_f 
	FROM db_users_b t1 
	LEFT OUTER JOIN db_users_a t2
	ON t2.id = t1.id WHERE t2.user <> 'system' ORDER BY t1.all_time_a + t1.all_time_b + t1.all_time_c + t1.all_time_d + t1.all_time_e + t1.all_time_f DESC LIMIT 5");*/
	$db->Query("SELECT t1.user, t1.payment_sum, t2.date_reg
	FROM db_users_b t1
	LEFT OUTER JOIN db_users_a t2
	ON t2.id = t1.id ORDER BY t1.payment_sum DESC LIMIT 5");
	while($data = $db->FetchArray()){
		//$sum = $data['all_time_a'] + $data['all_time_b'] + $data['all_time_c'] + $data['all_time_d'] + $data['all_time_e'] + $data['all_time_f'];
?>
<tr>
<td><?=$data['user'];?></td>
<td><?=$func->time2word($data['date_reg']);?></td>
<td><?=$data['payment_sum'];?> руб.</td>
</tr>
<?
	}
?>
</tbody></table>
</div>
<div class="col-md-6 col-12">
<div class="popup_protect__def__img">
<img src="/images/stats/stats_3.png">
</div>
<div class="card__head">
<h3 class="card__name">Рейтинг участников по кол-ву рефералов</h3>
<div class="card__period">Обновление раз в 10 минут</div>
</div>
<table class="bordered" style="width:100%;margin-bottom:15px;">
<thead>
<tr>
<th>Логин</th>
<th>Дата регистрации</th>
<th>Рефералы</th>
</tr>
</thead>
<tbody>
<?
	$db->Query("SELECT user, referals, date_reg 
	FROM db_users_a WHERE user <> 'system' ORDER BY referals DESC LIMIT 20");
	while($ref = $db->FetchArray()){
?>
<tr>
<td><?=$ref['user'];?></td>
<td><?=$func->time2word($ref['date_reg']);?></td>
<td><?=$ref['referals'];?> чел.</td>
</tr>
<?
	}
?>
</tbody></table>
</div>
<div class="col-md-6 col-12">
<div class="popup_protect__def__img">
<img src="/images/stats/stats_4.png">
</div>
<div class="card__head">
<h3 class="card__name">Последние 20 выплат участникам</h3>
<div class="card__period">Обновление раз в минуту</div>
</div>
<table class="bordered" style="width:100%;">
<thead>
<tr>
<th>Логин</th>
<th>Платежная система</th>
<th>Сумма</th>
</tr>
</thead>
<tbody>
<?
	$db->Query("SELECT user, pay_sys, sum 
	FROM db_payments ORDER BY id DESC LIMIT 20");
	while($ref = $db->FetchArray()){
?>
<tr>
<td><?=$ref['user'];?></td>
<td><?=$ref['pay_sys'];?></td>
<td><?=$ref['sum'];?> руб.</td>
</tr>
<?
	}
?>

</tbody></table>
</div>

</div>
</div>
