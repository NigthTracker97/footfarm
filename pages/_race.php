<?

$_OPTIMIZATION["title"] = "Гонка лидеров";

$usid = $_SESSION["user_id"];

$guest = false;
if(empty($usid)) $guest = true;

if(!$guest){
	$db->Query("SELECT user, bonus_race_perc FROM db_users_b WHERE id = '$usid' LIMIT 1");
	$user_data = $db->FetchArray();
}

$day = time() - 60 * 60 * 60 * 1;
$day7 = time() - 60 * 60 * 60 * 7;
$mounth = time() - 60 * 60 * 60 * 30;
$year = time() - 60 * 60 * 60 * 365;

if(!$guest){
	$style_8 = 'col-lg-8';
	$arr_1 = 0;
	$arr_2 = 0;
	$arr_3 = 0;
	$arr_4 = 0;

	$db->Query("SELECT user, user_id, money FROM db_insert_money WHERE date_add > $day AND user_id = '$usid'");
	while($data = $db->FetchArray()){
		$arr_1 += $data['money'];
	}

	$db->Query("SELECT user, user_id, money FROM db_insert_money WHERE date_add > $day7 AND user_id = '$usid'");
	while($data = $db->FetchArray()){
		$arr_2 += $data['money'];
	}

	$db->Query("SELECT user, user_id, money FROM db_insert_money WHERE date_add > $mounth AND user_id = '$usid'");
	while($data = $db->FetchArray()){
		$arr_3 += $data['money'];
	}

	$db->Query("SELECT user, user_id, money FROM db_insert_money WHERE date_add > $year AND user_id = '$usid'");
	while($data = $db->FetchArray()){
		$arr_4 += $data['money'];
	}
}else{
	$style_8 = 'col-12';
}

if($guest){
?>
<style>
	.mybonus{
		display: none;
	}
</style>
<?
}
?>
<div class="s-bk-lf" style="background: rgba(220, 53, 53, 0.47);">
<div class="acc-title"></div>
<center><h5>АКЦИЯ НА ПОПОЛНЕНИЕ БАЛАНСА:</h5>

<h6>Пополнение на любую сумму: +10% от суммы пополнения!</h6>
</center>
</div>

<div class="container-fluid">
<div class="row">
<div class="col-lg-4 mybonus">
<div class="panel panel-default racerespan1">
<div class="panel-body text-center">
<p class="text-muted m-b-10 m-t-0"><code class="profilemsd">Обновление данных происходит раз в час</code></p>
<h2 class="m-t-0 m-b-0 profilemst"><i class="mdi mdi-gift text-danger m-r-10"></i><b>Ваш бонус: <?=$user_data['bonus_race_perc'];?>%</b></h2> </div>
</div>
</div>
<div class="<?=$style_8;?>" >
<div class="panel panel-default">
<div class="panel-body">
<p class="raceinfotext m-b-0"> Гонка лидеров - это дополнительный способ увеличить доход до 20%, при условии что Вы являетесь лидером по пополнениям в проекте за 24 часа, неделю, месяц или год. Все бонусы суммируются, Вы можете одновременно быть лидером за 24 часа, неделю, месяц, год и получать постоянный бонус к скорости заработка до 20%.</p>
</div>
</div>
</div>
</div>
<div class="row">
<div class="col-lg-3 col-md-6 col-12">
<div class="panel2 panel-default">
<div class="panel-heading">
<h3 class="panel-title racetabletitle"><i class="fa fa-motorcycle"></i> Лидеры за 24 часа</h3>
</div>
<div class="panel-body">
<div class="row">
<div class="col-xs-12">
<table class="table table-striped">
<tbody>
<? 
$db->Query("SELECT user, sum, perc FROM db_race_leader2 WHERE block = 1");
while($bl = $db->FetchArray()){ ?>
<tr class="raceusertr">
<td><?=$bl['user'];?></td>
<td><div><?=$bl['sum'];?><span class="racerub"> <i class="fa fa-rub" aria-hidden="true"></i></span> 
<? if($bl['perc'] > 0){ ?>
<sup>+<?=$bl['perc'];?>%</sup>
<? } ?>
</div></td>
</tr>
<? } 
if(!$guest){
?>
<tr class="raceusertr raceactivetr">
<td><?=$user_data['user'];?></td>
<td><div><?=$arr_1;?><span class="racerub"> <i class="fa fa-rub" aria-hidden="true"></i></span></div></td>
</tr> 
<?
}
?>
</tbody>
</table>
</div>
</div>
</div>
</div>
</div>
<div class="col-lg-3 col-md-6 col-12">
<div class="panel2 panel-default">
<div class="panel-heading">
<h3 class="panel-title racetabletitle"><i class="fa fa-car"></i> Лидеры за 7 дней</h3>
</div>
<div class="panel-body">
<div class="row">
<div class="col-xs-12">
<table class="table table-striped">
<tbody>
<? 
$db->Query("SELECT user, sum, perc FROM db_race_leader2 WHERE block = 2");
while($bl = $db->FetchArray()){ ?>
<tr class="raceusertr">
<td><?=$bl['user'];?></td>
<td><div><?=$bl['sum'];?><span class="racerub"> <i class="fa fa-rub" aria-hidden="true"></i></span> 
<? if($bl['perc'] > 0){ ?>
<sup>+<?=$bl['perc'];?>%</sup>
<? } ?>
</div></td>
</tr>
<? } 
if(!$guest){
?>
<tr class="raceusertr raceactivetr">
<td><?=$user_data['user'];?></td>
<td><div><?=$arr_2;?><span class="racerub"> <i class="fa fa-rub" aria-hidden="true"></i></span></div></td>
</tr> 
<?
}
?></tbody>
</table>
</div>
</div>
</div>
</div>
</div>
<div class="col-lg-3 col-md-6 col-12">
<div class="panel2 panel-default">
<div class="panel-heading">
<h3 class="panel-title racetabletitle"><i class="fa fa-plane"></i> Лидеры за 30 дней</h3>
</div>
<div class="panel-body">
<div class="row">
<div class="col-xs-12">
<table class="table table-striped">
<tbody>
<? 
$db->Query("SELECT user, sum, perc FROM db_race_leader2 WHERE block = 3");
while($bl = $db->FetchArray()){ ?>
<tr class="raceusertr">
<td><?=$bl['user'];?></td>
<td><div><?=$bl['sum'];?><span class="racerub"> <i class="fa fa-rub" aria-hidden="true"></i></span> 
<? if($bl['perc'] > 0){ ?>
<sup>+<?=$bl['perc'];?>%</sup>
<? } ?>
</div></td>
</tr>
<? } 
if(!$guest){
?>
<tr class="raceusertr raceactivetr">
<td><?=$user_data['user'];?></td>
<td><div><?=$arr_3;?><span class="racerub"> <i class="fa fa-rub" aria-hidden="true"></i></span></div></td>
</tr> 
<?
}
?> </tbody>
</table>
</div>
</div>
</div>
</div>
</div>
<div class="col-lg-3 col-md-6 col-12">
<div class="panel2 panel-default">
<div class="panel-heading">
<h3 class="panel-title racetabletitle"><i class="fa fa-rocket"></i> Лидеры за 365 дней</h3>
</div>
<div class="panel-body">
<div class="row">
<div class="col-xs-12">
<table class="table table-striped">
<tbody>
<? 
$db->Query("SELECT user, sum, perc FROM db_race_leader2 WHERE block = 4");
while($bl = $db->FetchArray()){ ?>
<tr class="raceusertr">
<td><?=$bl['user'];?></td>
<td><div><?=$bl['sum'];?><span class="racerub"> <i class="fa fa-rub" aria-hidden="true"></i></span> 
<? if($bl['perc'] > 0){ ?>
<sup>+<?=$bl['perc'];?>%</sup>
<? } ?>
</div></td>
</tr>
<? } 
if(!$guest){
?>
<tr class="raceusertr raceactivetr">
<td><?=$user_data['user'];?></td>
<td><div><?=$arr_4;?><span class="racerub"> <i class="fa fa-rub" aria-hidden="true"></i></span></div></td>
</tr> 
<?
}
?> </tbody>
</table>
</div>
</div>
</div>
</div>
</div>
</div>

</div>