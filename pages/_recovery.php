<?PHP
$_OPTIMIZATION["title"] = "Восстановление пароля";
$_OPTIMIZATION["description"] = "Восстановление забытого пароля";
$_OPTIMIZATION["keywords"] = "Восстановление забытого пароля";

if (isset($_SESSION["user_id"])) {
    Header("Location: /account");
    return;
}

?>
<div class="s-bk-lf">
	<div class="acc-title">Восстановление пароля</div>
</div>
<div class="silver-bk">


<div class="car" style="opacity: 1;position: absolute;margin-right: 50px;width: 455px;margin-top: 20px;margin-left: 510px;padding: 10px;color: #484847;-webkit-box-shadow: 0 1px 5px rgba(0, 0, 0, 0.25);box-shadow: 0 1px 5px rgba(0, 0, 0, 0.25);padding-top: 8px;padding-bottom: 7px;">
<h1 class="sign-up-title" style="margin: -2px -10px 5px;padding: 15px 25px;line-height: 37px;font-size: 26px;font-weight: 500;color: #484847;text-align: center;background: #f7f7f7;font-family: 'Open Sans', sans-serif;">ВОПРОС-ОТВЕТ</h1>
<div class="layer" style="height: 351px;">
<b>Вопрос:</b> Как начать играть? <br>
    <b>Ответ:</b> После регистрации в проекте, переходите в раздел "Покупка персонажей", где покупаете одного из персонажей,
    далее получаете каждый час опыт, затем продаете опыт в торговой лавке за серебро и выводите серебро в реальные денежные средства!
    <br><br>

        <b>Вопрос:</b> У вас есть платежные баллы: PAY POINTS, CASH POINTS, и др? <br>
    <b>Ответ:</b> В отличии от других подобных проектов, на нашем проекте НЕТ платежных баллов и нет лимитов на выплаты. Всё что пользователь зарабатывает, он может выводить в полном объеме в любое время.
    <br><br>
    
    <b>Вопрос:</b> Если я не пополнил баланс, могу ли я начать играть?<br>
    <b>Ответ:</b> Да, можете! Ежедневный бонусы позволят Вам собрать серебра для покупок, но для того чтобы купить
    персонажей, нужно заходить каждый день и получать бонусы! Есть разные бонусы, в том числе "При регистрации Персонаж в подарок".<br><br>

    <b>Вопрос:</b> Как пополнить баланс?<br>
    <b>Ответ:</b> Баланс Вы можете пополнить в разделе "Пополнить баланс".<br><br>

    <b>Вопрос</b>: Как мне вывести деньги из проекта?<br>
    <b>Ответ</b>: Вывести заработанное Вы можете в разделе "Заказать выплату". Там присутствуют специальные формы для
    выплат.<br><br>

    <b>Вопрос</b>: Как скоро я смогу начинать выводить деньги?<br>
    <b>Ответ</b>: Сразу после пополнения и покупки персонажей, в разделе "Заказать выплату". Перед выводом средств Вам нужно
    собрать опыт и продать в "торговой лавке"!
    <br><br>


    <b>Вопрос</b>: Как можно еще зарабатывать серебро в проекте, кроме пополнения?<br>
    <b>Ответ</b>:
    Игровую валюту Вы можете заработать несколькими способами:<br>
    1. Получать ежедневный бонус.<br>
    2. Приглашать друзей в игру и получать свой процент от их пополнений.<br>
    3. Участвуя в конкурсе рефералов<br>
    4. Получать разнообразные бонусы в социальных сетях.<br>
    5. Привлекать новых пользователей - рефералов.<br>

    В каждом соответствующем разделе есть подробное описание
    <br><br>

    <b>Вопрос</b>: Сколько уровней в вашей реферальной системе?<br>
    <b>Ответ</b>:
    На нашем проекте действует реферальная система с глубиной до 2 уровней. Более подробную информацию вы можете узнать в разделе "<a href="/account/referals">Мои партнеры</a>".
    <br><br>

    <b>Вопрос</b>: Как мне привлечь рефералов?<br>
    <b>Ответ</b>:
    Вам нужно отправить свою реферальную ссылку, которую вы можете найти в разделе <a href="/account/referals">Ваши
        партнеры</a>, своим друзьям, коллегам, или использовать промо-площадки.
    <br><br>
 
    <b>Вопрос</b>: Какие преимущества у пользователей, которые привлекают много рефералов?<br>
    <b>Ответ</b>:
    Заработанные на Ваших рефералах средства поступают на отдельный счет и могут быть выведенны в любое время. Более подробную информацию можно найти в разделе "Заказать выплату" -&gt; "Экстренная выплата".
    <br><br>

    <b>Если у Вас еще остались вопросы, обратитесь в нашу техническую поддержку!</b>

    <br>
</div></div>
    <?PHP

    if (isset($_POST["email"])){

    if (isset($_SESSION["captcha"]) AND strtolower($_SESSION["captcha"]) == strtolower($_POST["captcha"])){

    unset($_SESSION["captcha"]);

    $email = $func->IsMail($_POST["email"]);
    $time = time();
    $tdel = $time + 60 * 15;

    if ($email !== false){

    $db->Query("DELETE FROM db_recovery WHERE date_del < '$time'");
    $db->Query("SELECT COUNT(*) FROM db_recovery WHERE ip = INET_ATON('" . $func->UserIP . "') OR email = '$email'");
    if ($db->FetchRow() == 0){

    $db->Query("SELECT id, user, email, pass FROM db_users_a WHERE email = '$email'");
    if ($db->NumRows() == 1){
    $db_q = $db->FetchArray();

    # Вносим запись в БД
    $db->Query("INSERT INTO db_recovery (email, ip, date_add, date_del) VALUES ('$email',INET_ATON('" . $func->UserIP . "'),'$time','$tdel')");

    # Отправляем пароль
    $sender = new isender;
    $sender->RecoveryPassword($db_q["email"], $db_q["pass"], $db_q["email"]);

    echo "<center><font color = 'green'><b>Данные для входа отправлены на Email</b></font></center>";
    ?>
</div>
    <div class="clr"></div>
<?PHP
return;

} else echo "<center><font color = 'red'><b>Пользователь с таким Email не зарегистрирован</b></font></center>";

} else echo "<center><font color = 'red'><b>На Ваш Email или IP уже был отправлен пароль за последние 15 минут</b></font></center>";

} else echo "<center><font color = 'red'><b>Email указан неверно</b></font></center>";

} else echo "<center><font color = 'red'><b>Символы с картинки введены неверно</b></font></center>";

}

?>
<form class="sign-up" action="" method="post" style="height: 438px;">
<h1 class="sign-up-title">Восстановление пароля</h1>

   <input name="email" class="sign-up-input" placeholder="Введите ваш e-mail" type="text" size="25" maxlength="50" value="<?= (isset($_POST["email"])) ? htmlspecialchars($_POST["email"]) : false; ?>"/>
	 <div class="silver-bkloxum">
    <center><div style="width: 160px;height: 60px;cursor: pointer;float: left;">
	 <a href="#" onclick="ResetCaptcha(this);"><img src="/captcha.php?rnd=<?= rand(1, 10000); ?>"
                                                                   border="0" style="margin:0;"/></a>
	</div></center>
	
	<input class="sign-up-input" placeholder="Цифры с картинки" style="width: 160px;margin-left: 0px;text-align:center;background-color: #fff;font-size: 14px;float: right;" name="captcha" type="text" size="25" maxlength="50" />
	</div>
    <input type="submit" value="Отправить" class="sign-up-button" style="margin-top: 10px;"></td>
</form>
</div>	</div>


