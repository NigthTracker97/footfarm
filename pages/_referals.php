<?PHP
$_OPTIMIZATION["title"] = "Партнерская программа";
$_OPTIMIZATION["description"] = "Партнерская программа";
$_OPTIMIZATION["keywords"] = "Партнерская программа";

# Блокировка сессии
if (!isset($_SESSION["user_id"])) {
    Header("Location: /");
    return;
}


if (isset($_GET["sel"])) {

    $smenu = strval($_GET["sel"]);
    switch ($smenu) {

        case "404": include("pages/_404.php"); break; // Страница ошибки
        case "myreferals": include("pages/referals/_referals.php"); break; // Мои рефералы
		case "refleader": include("pages/referals/_refleader.php"); break; // Мои рефералы
        case "refsale": include("pages/referals/_refsale.php"); break; // Продажа рефералов
        case "promo": include("pages/referals/_promo.php"); break; // Промо-материалы
        case "visits": include("pages/referals/_visits.php"); break; // Визиты
		case "invcompetition": include("pages/referals/_invcompetition.php"); break; // Визиты
        default: @include("pages/referals/_referals.php"); break;

    }

} else @include("pages/referals/_referals.php");

?>