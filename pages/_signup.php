<?PHP

$_OPTIMIZATION["title"] = "Регистрация";
$_OPTIMIZATION["description"] = "Регистрация пользователя в системе";
$_OPTIMIZATION["keywords"] = "Регистрация нового участника в системе";

if (isset($_SESSION["user_id"])) {
    Header("Location: /account");
    return;
}
?>
<div class="s-bk-lf">
	<div class="acc-title">Регистрация</div>
</div>
<?
if ($detect->isMobile()) {
?> 
	<section class="container-fluid">
<?
}else{
?>
	<section class="container">
		<div class="row">
   <?php
  }

// Регистрация
$db->Query("SELECT * FROM db_leader");   $leader = $db->FetchArray();

$referer_id = (isset($_COOKIE["i"]) AND intval($_COOKIE["i"]) > 0 AND intval($_COOKIE["i"]) < 1000000) ? intval($_COOKIE["i"]) : 1;
$referer_name = "";

if ($referer_id != 1) {
	$db->Query("SELECT user FROM db_users_a WHERE id = '$referer_id' LIMIT 1");
	if ($db->NumRows() > 0) {
		$referer_name = $db->FetchRow();
	}
	else {
		$referer_id = $leader["user_id"];
		$referer_name = $leader["user"]; }
	}
}
else {
	$referer_id = $leader["user_id"];
	$referer_name = $leader["user"]; }
}

if (isset($_POST["login"])) {
	if (isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response'])) {
		//your site secret key
        $secret = '6LdUG1cUAAAAAOKrHCM3KhDlc1Pi937HwCVIHqC9';
        //get verify response data
        $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
        $responseData = json_decode($verifyResponse);
        if($responseData->success == 1){
			print_r($responseData);
			$login = $func->IsLogin($_POST["login"]);
			$pass = $func->IsPassword($_POST["pass"]);
			$rules = isset($_POST["rules"]) ? true : false;
			$time = time();
			$ip = $func->UserIP;
			$ipregs = $db->Query("SELECT * FROM `db_users_a` WHERE INET_NTOA(db_users_a.ip) = '$ip' ");
			$ipregs = $db->NumRows();
			$ipregs = 0;
			$email = $func->IsMail($_POST["email"]);
			if ($email !== false) {
				$db->Query("SELECT id FROM db_users_a WHERE email = '{$email}' LIMIT 1");
				if ($db->NumRows() > 0) $email = false;
			}

			if ($rules) {
				if ($ipregs == 0 || isset($_SESSION['admin4ik'])) {
					if ($email !== false) {
						if ($login !== false) {
							if ($pass !== false) {
								if ($pass == $_POST["repass"]) {
									$db->Query("SELECT COUNT(*) FROM db_users_a WHERE user = '$login'");
									if ($db->FetchRow() == 0) {

										// Продажа рефералов

										$sr_referer_id = (isset($_COOKIE["rs"]) AND intval($_COOKIE["rs"]) > 0 AND intval($_COOKIE["rs"]) < 1000000) ? intval($_COOKIE["rs"]) : 0;
										$rs_referer_money = 0;
										$rs_referer_mtype = 0;
										if ($sr_referer_id > 0) {
											$user_pconfig = $func->config_site($db, $sr_referer_id);
											if ($user_pconfig['refsale_enable'] == 1) {
												$rs_referer_money = $user_pconfig['refsale_price'];
												$rs_referer_mtype = $user_pconfig['refsale_mtype'];
												//$referer_id = 1;
												//$referer_name = "system";

												// Зачисляем вознаграждение пользователю

												$paymeny_money_type = array(
													'0' => 'money_b',
													'1' => 'money_p',
													'2' => 'money_r',
													'3' => 'cash_points'
												);
												$db->Query("UPDATE db_users_b SET {$paymeny_money_type[$rs_referer_mtype]} = {$paymeny_money_type[$rs_referer_mtype]} + {$rs_referer_money}, money_r = money_r + {$rs_referer_money} WHERE id = {$sr_referer_id}");
											}
										}

										// Регаем пользователя

										$db->Query("INSERT INTO db_users_a (user, email, pass, referer, referer_id, date_reg, ip, come_url, rs_referer_id, rs_referer_money, rs_referer_mtype) 
										VALUES ('$login','{$email}','$pass','$referer_name','$referer_id','$time',INET_ATON('$ip'),'{$_SESSION['come_url']}', {$sr_referer_id}, {$rs_referer_money}, {$rs_referer_mtype})");
										
										$lid = $db->LastInsert();
										if (isset($_POST['email_send'])) {
											$db->Query("UPDATE db_users_a SET spam = 1 WHERE id = {$lid}");
										}

										$db->Query("INSERT INTO db_users_b (id, user, last_sbor) VALUES ('$lid','$login', '" . time() . "')");

										// Собираем информаци о рефералах до 6го уровня

										$db->Query("SELECT id, user, referer_id FROM db_users_a WHERE id = {$referer_id}");
										$r[1] = $db->FetchArray();
										for ($i = 1; $i <= 5; $i++) {
											if (!empty($r[$i]['id'])) {
												$db->Query("SELECT id, user, referer_id FROM db_users_a WHERE id = {$r[$i]['referer_id']}");
												if ($db->NumRows() > 0) $r[$i + 1] = $db->FetchArray();
											}
										}

										$sql_string = '';
										foreach($r as $level => $data) {
											if ($data['id'] > 0) $sql_string.= "referer{$level}_id = {$data['id']}, referer{$level} = '{$data['user']}', ";
											$db->query("UPDATE db_users_c SET count_ref{$level} = count_ref{$level} + 1 WHERE id = {$data['id']}");
										}

										$db->Query("INSERT INTO db_users_c SET {$sql_string} id = {$lid}");

										// Добавляем бонус при регистрации

										$conf = $func->config_site($db);
										$regbonus = $conf['regbonus'];
										if ($regbonus > 0) {
											$db->query("UPDATE db_users_b SET money_b = money_b + {$regbonus} WHERE id = {$lid}");
										}

										// Вставляем статистику

										$db->Query("UPDATE db_stats SET all_users = all_users +1 WHERE id = '1'");

										// Добавляем +1 к количеству рефералов у реферера

										$db->Query("UPDATE db_users_a SET referals = referals + 1 WHERE id = {$referer_id}");
										echo "<center><b><font color = 'green'>Вы успешно зарегистрировались.</font></b></center><BR />";
										if (true) {
											$db->Query("SELECT id, user, referer_id, pass, email FROM db_users_a WHERE id = {$lid}");
											$log_data = $db->FetchArray();
											if (!isset($_SESSION['refsale_enable'])) {
												$sonfig_site = $func->config_site($db, $log_data["id"]);
												$_SESSION['refsale_enable'] = $sonfig_site['refsale_enable'];
											}

											$_SESSION["user_id"] = $log_data["id"];
											$_SESSION["user"] = $log_data["user"];
											$_SESSION["referer_id"] = $log_data["referer_id"];
											$_SESSION["warning"] = "<b>Ваш логин:</b> {$log_data['email']}<br/><b>Ваш пароль:</b> {$log_data['pass']}";
											$db->Query("UPDATE db_users_a SET date_login = '" . time() . "', ip = INET_ATON('" . $func->UserIP . "') WHERE id = '" . $log_data["id"] . "'");
											echo '<meta http-equiv="refresh" content="1;URL=/account" />';
										}

										return;
									}
									else echo "<center><b><font color = 'red'>Указанный логин уже используется</font></b></center><BR />";
								}
								else echo "<center><b><font color = 'red'>Пароль и повтор пароля не совпадают</font></b></center><BR />";
							}
							else echo "<center><b><font color = 'red'>Пароль заполнен неверно</font></b></center><BR />";
						}
						else echo "<center><b><font color = 'red'>Логин заполнен неверно</font></b></center><BR />";
					}
					else echo "<center><font color = 'red'><b>Email имеет неверный формат или уже зарегистрирован в системе</b></font></center>";
				}
				else echo "<center><font color = 'red'><b>Регистрация с этого IP уже производилась</b></font></center>";
			}
			else echo "<center><b><font color = 'red'>Вы не подтвердили правила</font></b></center><BR />";
		}
		else echo "<center><font color = 'red'><b>Каптча введена неверно</b></font></center>";
	}
	else echo "<center><font color = 'red'><b>Каптча введена неверно</b></font></center>";
}


?>
<div class="col-12 col-sm-5 sign-up">
	<form action="" method="post">
		<h1 class="sign-up-title">РЕГИСТРАЦИЯ</h1>
		<input type="text" class="sign-up-input" placeholder="Придумайте логин" pattern="[0-9A-Za-z]{4,10}" required title="Псевдоним должно иметь от 4 до 10 символов (только латинские буквы и цифры)" name="login" size="25" maxlength="10" value="<?= (isset($_POST["login"])) ? $_POST["login"] : false; ?>" autofocus="">
		<input type="text" class="sign-up-input" placeholder="Email"  required title="Ваш Email" name="email" maxlength="40" value="<?= (isset($_POST["email"])) ? $_POST["email"] : false; ?>" autofocus="">
		<input type="password" class="sign-up-input" placeholder="Введите пароль" pattern="[0-9A-Za-z]{6,20}" required title="Пароль должно иметь от 6 до 20 символов (только латинские буквы и цифры)" name="pass" size="25" maxlength="20">
		<input type="password"  class="sign-up-input" placeholder="Повторите пароль" pattern="[0-9A-Za-z]{6,20}" required title="Пароль должно иметь от 6 до 20 символов (только латинские буквы и цифры)" size="25" maxlength="20" name="repass">
		<label>
		<input name="rules" style="cursor: pointer; width: 15px; height: 15px; vertical-align: -4px;" required type="checkbox">
		<font style="font-weight: 600;font-size: 11px;">
		С <a href="/rules" target="_blank" class="stn" style="border-bottom: none;">правилами</a> проекта ознакомлен(а) и принимаю: 
		</font>
		</label>
		<div class="silver-bkloxum">
			<center>
				<table>
					<tr>
						<td  style="padding:3px; max-width: 100%;">
							<div class="g-recaptcha" data-sitekey="6LdUG1cUAAAAAF7zxzpZN6WpOZpPF8F6Ls-cQboD" 
							<? echo $detect->isMobile() ? 'style="transform:scale(0.8);transform-origin:0;-webkit-transform:scale(0.8);transform:scale(0.8);-webkit-transform-origin:0 0;transform-origin:0 0;"' : ''; ?>
							></div>
						</td>
					</tr>
				</table>
			</center>
		</div>
		<input type="submit" value="ПРОДОЛЖИТЬ" class="sign-up-button" style="margin-top: 10px;">
	</form>
</div>
<div class="col-12 col-sm-5 offset-md-1 sign-up" >
	<h1 class="sign-up-title">ОБЩИЕ ПРАВИЛА</h1>
	<div class="layer" style="height: 463px;">
		<b>Общие положения.</b><br>
		<b>1.1.</b> Настоящее Пользовательское соглашение (далее «Соглашение») регламентирует порядок и условия предоставления услуг сайтом именуемой далее по тексту «Организатор», и адресовано физическому лицу, желающему получать услуги указанного сайта (далее «Участник».)
		<br>
		<b>1.2.</b> Для начала получения услуг участник явно, полно и безоговорочно принимает все условия настоящего Соглашения, и, если вы не согласны с каким-либо условием соглашения, Организатор предлагает вам отказаться от использования его услуг.
		<br>
		<b>1.3.</b> Организатор и участник признают порядок и форму заключения настоящего соглашения равнозначным по юридической силе соглашению, заключенному в письменной форме.
		<br>
		<b>2.1</b> Термины и Определения.
		<br>
		<b>Игра</b> — вид деятельности, направленный на удовлетворение потребностей человека в развлечении, удовольствии, снятию напряжения, а также на развитие определенных навыков и умений в форме свободного самовыражения человека, не связанных с достижением утилитарных целей и доставляющих радость сами по себе.
		<br>
		<b>Игровая площадка</b> — программно-аппаратный комплекс физических устройств и программного обеспечения, расположенный в глобальной сети Интернет, предназначенный для организации проведения досуга и отдыха.
		<br>
		<b>Экономическая онлайн-игра «Виртуальные онлайн деньги»</b> - обособленное и уникальное название игровой площадки, принадлежащей организатору и находящейся по адресам в сети интернет <b><font color="#3aaf44" style="font-weight:600;">https://</font>FootFarm.Com</b>, на которой Организатором предоставляются услуги участнику по организации его развлечения, досуга и отдыха в порядке и на условиях, изложенных в настоящем соглашении.
		<br>
		<b>Игровой инвентарь</b> — условная игровая единица для участия в игре, именуемая «Серебро», местом учета и хранения которой является игровой счет участника в электронном виде в формате учетной системы игровой площадки <b>«FootFarm.Com»</b>.
		<br>
		<b>Игровой счет</b> — виртуальный счет участника игры, предоставляемый организатором каждому участнику на игровой площадке для учета игрового инвентаря (Серебра).
		<br>
		<b>3.1.</b> Предметом настоящего Соглашения является предоставление организатором участнику услуг по организации досуга и отдыха в игре в соответствии с условиями настоящего Соглашения. Под такими услугами, в частности, понимаются следующие: услуги по покупке-продаже игрового инвентаря (Серебро), ведение учета значимой информации: движения по игровому счету, обеспечение мер по идентификации и безопасности участников, разработка программного обеспечения, интегрируемого в игровую площадку и внешние приложения, информационные и другие услуги, необходимые для организации игры и обслуживания участника в ее процессе на площадке организатора..
		<br>
		<b>3.2.</b> Игра в целом, а равно любой ее элемент или любое сопряженное внешнее игровое приложение, созданы исключительно для развлечений. Участник признает, что все виды деятельности в игре на игровой площадке являются для него развлечением. Участник соглашается с тем, что в зависимости от характеристик его аккаунта, степень его участия в игре будет доступна в различной мере.
		<br>
		<b>3.3.</b> Участник соглашается, что он несет персональную ответственность за все действия, произведенные с игровым инвентарем (Серебро): покупкой, продажей, вводом и выводом, а также за игровые действия на игровой площадке: создание, покупку-продажу, операции со всеми игровыми элементами и другими игровыми атрибутами и объектами, используемыми для игрового процесса.
		<br>
		<b>3.4.</b> Участник признает, что степень и возможность участия в развлечениях на сервере Игры являются главными качествами оказываемой ему услуги.
		<br><br>
		<b>Права и обязанности сторон.</b><br>
		<b>4.1</b> Права и обязанности участника.
		<br>
		<b>4.1.1.</b> Принимать участие в игре могут только лица, достигшие гражданской дееспособности по законодательству страны своей резиденции. Все последствия неисполнения данного условия возлагаются на участника.
		<br>
		<b>4.1.2.</b> Степень и способ участия в игре определяются самим участником, но не могут противоречить настоящему Соглашению и правилам игровой площадки.
		<br>
		<b>4.1.2.</b> Участник обязан:
		<br>
		<b>4.1.2.1.</b> Правдиво сообщать сведения о себе при регистрации и по первому требованию Организатора предоставить достоверные данные о своей личности, позволяющие идентифицировать его как владельца аккаунта в игре;
		<br>
		<b>4.1.2.2.</b> Не использовать игру для совершения каких-либо действий, противоречащих международному законодательству и законодательству страны — резиденции Участника;
		<br>
		<b>4.1.2.3.</b> Не использовать недокументированные особенности (баги) и ошибки программного обеспечения игры и незамедлительно сообщать Организатору о них, а так же о лицах, использующих эти ошибки;
		<br>
		<b>4.1.2.4.</b> Не использовать внешние программы любого рода, для получения преимуществ в игре;
		<br>
		<b>4.1.2.5.</b> Не использовать для рекламы своей партнерской ссылки, а равно ресурса, ее содержащего, почтовые рассылки и иного вида сообщения лицам, не выражавшим согласия их получать (спам);
		<br>
		<b>4.1.2.6.</b> Не вправе ограничивать доступ других участников или других лиц к Игре, обязан уважительно и корректно относиться к участникам игры, а так же к Организатору, его партнерам и сотрудникам, не создавать помехи в работе последних;
		<br>
		<b>4.1.2.7.</b> Не обманывать Организатора и участников игры;
		<br>
		<b>4.1.2.8.</b> Не использовать ненормативную лексику и оскорбления в любой форме;
		<br>
		<b>4.1.2.9.</b> Не порочить действия других игроков и Администрации;
		<br>
		<b>4.1.2.10.</b> Не угрожать насилием и физической расправой кому бы то ни было;
		<br>
		<b>4.1.2.11.</b> Не распространять материалы пропагандирующие неприятие или ненависть к любой расе, религии, культуре, нации, народу, языку, политике, государству, идеологии или общественному движению;
		<br>
		<b>4.1.2.12.</b> Не рекламировать порнографию, наркотики и ресурсы, содержащие подобную информацию;
		<br>
		<b>4.1.2.13.</b> Не использовать действия, терминологию или жаргон для завуалирования нарушения обязанностей участника;
		<br>
		<b>4.1.2.14.</b> Самостоятельно заботиться о необходимых мерах компьютерной и иной безопасности, хранить в секрете и не передавать другому лицу или другому участнику свои идентификационные данные: логин, пароль аккаунта и др., не допускать несанкционированного доступа к почтовому ящику, указанному в профиле аккаунта участника. Весь риск неблагоприятных последствий разглашения этих данных несет участник, так как участник согласен с тем, что система информационной безопасности игровой площадки исключает передачу логина, пароля и идентификационной информации аккаунта участника третьим лицам;
		<br>
		<b>4.1.2.15.</b> Самостоятельно нести персональную ответственность за ведение своих финансовых сделок и операций, Организатор не несет ответственности за совершаемые финансовые действия между игроками по передаче игрового инвентаря и игровой валюты, а равно иных игровых атрибутов.
		<br>
		<b>4.1.2.16.</b> О своих претензиях и жалобах первым уведомлять организатора в письменной форме через страницу «Контакты».
		<br>
		<b>4.1.2.17.</b> Регулярно самостоятельно знакомиться с новостями игры, а также с изменениями в настоящем Соглашении и в правилах игры на игровой площадке.
		<br>
		<b>4.2</b> Права и обязанности организатора.
		<br>
		<b>4.2.1.</b> Организатор обязан:
		<br>
		<b>4.2.1.1.</b> Обеспечить без взимания платы доступ участника на игровую площадку и к участию в игре. Участник самостоятельно за свой счет оплачивает доступ в сеть Интернет и несет иные расходы, связанные с данным действием.
		<br>
		<b>4.2.1.2.</b> Вести учет игрового инвентаря (Серебро) на игровом счете участника.
		<br>
		<b>4.2.1.3.</b> Регулярно совершенствовать аппаратно-программный комплекс, но не гарантирует, что программное обеспечение Игры не содержит ошибок, а аппаратная часть не выйдет из рабочих параметров и будет функционировать бесперебойно.
		<br>
		<b>4.2.1.4.</b> Соблюдать режим конфиденциальности в отношении персональных данных участника в порядке п. 6 настоящего соглашения.
		<br>
		<b>4.2.1.5.</b> Нести финансовые обязательства по обеспечению эквивалентной курсовой стоимости игрового инвентаря (Серебра) на игровом счете участника. Курсовая стоимость игрового инвентаря (Серебро) равняется: 100 серебра = 1 рубль. При выводе средств с проекта курс может быть изменен как в большую, так и в меньшую сторону. Информацию о курсе для вывода средств можно найти в разделе заказа выплаты.
		<br>
		<b>4.2.2.</b> Организатор имеет право:
		<br>
		<b>4.2.2.2.</b> Предоставлять участнику дополнительные платные услуги, перечень которых, а также порядок и условия пользования которыми определяются настоящим соглашением, правилами игровой площадки и иными объявлениями организатора. При этом организатор вправе в любое время изменить количество и объем предлагаемых платных услуг, их стоимость, название, вид и эффект от использования.
		<br>
		<b>4.2.2.3.</b> Приостановить действие настоящего соглашения и отключить участника от участия в игре на время проведения расследования по подозрению участника в нарушении настоящего Соглашения и правил игровой площадки.
		<br>
		<b>4.2.2.4.</b> Исключить участника из игры, если установит, что участник нарушил настоящее соглашение или правила, установленные на игровой площадке, в порядке 5.10 настоящего соглашения.
		<br>
		<b>4.2.2.5.</b> Частично или полностью прерывать предоставление услуг без предупреждения участника при проведении реконструкции, ремонта и профилактических работ на площадке.
		<br>
		<b>4.2.2.6.</b> Организатор не несет ответственности за неправильное функционирование программного обеспечения игры. Участник использует программное обеспечение по принципу «КАК ЕСТЬ» (“AS IS”). Если организатор установит, что при игре возник сбой (ошибка) в работе площадки, то результаты, которые состоялись во время некорректной работы программного обеспечения, могут быть аннулированы или скорректированы по усмотрению организатора. Участник согласен не апеллировать к организатору по поводу качества, количества, порядка и сроков предоставляемых ему игровых возможностей и услуг.
		<br>
		<b>4.3.</b><span> Организатор имеет право конфисковать весь игровой инвентарь, а так же ограничить доступ к аккаунту пользователя в случае&nbsp;</span><span>отсутствия</span><span>&nbsp;активности. Под</span><span>отсутствием</span><span>&nbsp;активности&nbsp;</span><span>подразумевается</span><span>&nbsp;полное прекращение&nbsp;</span><span>пользованием</span><span>&nbsp;аккаунтом на протяжении 45 календарных дней.</span>
		<br><br>
		<b>Гарантии и ответственность.</b><br>
		<b>5.1.</b> Организатор не гарантирует постоянный и непрерывный доступ к игровой площадке и его услугам в случае возникновения технических неполадок и/или непредвиденных обстоятельств, в числе которых: неполноценная работа или не функционирование интернет–провайдеров, серверов информации, банковских и платёжных систем, а также неправомерных действий третьих лиц. Организатор приложит все усилия по недопущению сбоев, но не несет ответственности за временные технические сбои и перерывы в работе Игры, вне зависимости от причин таких сбоев.
		<br>
		<b>5.2</b> Участник полностью согласен, что организатор не может нести ответственность за убытки участника, которые возникли в связи с противоправными действиями третьих лиц, направленными на нарушение системы безопасности электронного оборудования и баз данных игры, либо вследствие независящих от организатора перебоев, приостановления или прекращения работы каналов и сетей связи, используемых для взаимодействия с участником, а также неправомерных или необоснованных действий платежных систем, а так же третьих лиц.
		<br>
		<b>5.3.</b> Организатор не несет ответственности за убытки, понесенные в результате использования или не использования участником информации об Игре, игровых правил и самой Игры и не несет ответственности за убытки или иной вред, возникший у участника в связи с его неквалифицированными действиями и незнанием игровых правил или его ошибках в расчетах;
		<br>
		<b>5.4.</b> Участник согласен с тем, что использует игровую площадку по своей доброй воле и на свой собственный риск. Организатор не дает участнику никакой гарантии того, что он извлечет выгоду или пользу от участия в игре. Степень участия в Игре определяется самим участником.
		<br>
		<b>5.5.</b> Организатор не несет ответственности перед участником за действия других участников.
		<br>
		<b>5.6.</b> В случае возникновения споров и разногласий на игровой площадке, решение организатора является окончательным, и участник с ним полностью согласен. Все споры и разногласия, возникающие из настоящего Соглашения или в связи с ним, подлежат разрешению путем переговоров. В случае невозможности достижения согласия путем переговоров, споры, разногласия и требования, возникающие из настоящего Соглашения, подлежат разрешению в соответствии с действующим законодательством.
		<br>
		<b>5.7.</b> Организатор не несет налогового бремени за Участника. Участник обязуется самостоятельно включать возможные полученные доходы в налоговую декларацию в соответствии с законодательством страны своей резиденции.
		<br>
		<b>5.8.</b> Организатор может вносить изменения в настоящее Соглашение, правила игровой площадки и другие документы в одностороннем порядке. В случае внесения изменений в документы Организатор размещает последние версии документов на сайте игровой площадки. Все изменения вступают в силу с момента размещения. Участник имеет право расторгнуть настоящее Соглашение в течение 3 дней, если он не согласен с внесенными изменениями. В таком случае расторжение Соглашения производится согласно п. 5.9 настоящего Соглашения. На Участника возлагается обязанность регулярно посещать официальный сайт Игры с целью ознакомления с официальными документами и новостями.
		<br>
		<b>5.9.</b> Участник имеет право расторгнуть настоящее Соглашение в одностороннем порядке без сохранения игрового аккаунта. При этом все расходы, связанные с участием в игре, игровой инвентарь (Серебро), находящиеся на игровом счете участника,&nbsp;участнику не компенсируются и не возвращаются.
		<br>
		<b>5.10.</b> Организатор имеет право расторгнуть настоящее Соглашение в одностороннем порядке, а также совершать иные действия, ограничивающие возможности в Игре, в отношении участника или группы участников, являющихся соучастниками выявленных нарушений условий настоящего Соглашения. При этом все игровые атрибуты, игровой инвентарь (Серебро) находящиеся в аккаунте и на игровом счете участника или группы участников, а равно все расходы возврату не подлежат и не компенсируются.
		<br>
		<b>5.11.</b> Организатор и Участник несут ответственность за неисполнение или ненадлежащее исполнение своих обязательств по настоящему Соглашению.
		<br>
		<b>5.12.</b> Организатор и Участник освобождаются от ответственности в случае возникновения обстоятельств непреодолимой силы (форс-мажорных обстоятельств), к числу которых относятся, но перечнем не ограничиваются: стихийные бедствия, войны, огонь (пожары), наводнения, взрывы, терроризм, бунты, гражданские волнения, акты правительственной или регулирующей власти, хакерские атаки, отсутствия, нефункционирование или сбои работы энергоснабжения, поставщиков Интернет услуг, сетей связи или других систем, сетей и услуг. Сторона, у которой возникли такие обстоятельства, должна в разумные сроки и доступным способом оповестить о таких обстоятельствах другую сторону.
		<br><br>
		<b>Конфиденциальность.</b><br>
		<b>6.1.</b> Условие конфиденциальности распространяется на информацию, которую Организатор может получить об Участнике во время его пребывания на сайте Игры и которая может быть соотнесена с данным конкретным пользователем. Организатор автоматически получает и записывает в серверные логи техническую информацию из вашего браузера: IP адрес, адрес запрашиваемой страницы и т.д. Организатор может записывать «cookies» на компьютер пользователя и впоследствии использовать их. Организатор гарантирует, что данные, сообщенные участником при регистрации в Игре, будут использоваться Организатором только внутри Игры.
		<br>
		<b>6.2.</b> Организатор вправе передать персональную информацию об Участнике третьим лицам только в случаях, если:
		<br>
		<b>6.2.1.</b> Участник изъявил желание раскрыть эту информацию;
		<br>
		<b>6.2.2.</b> Без этого Участник не может воспользоваться желаемым продуктом или услугой, в частности - информация об именах (никах), игровых атрибутах - может быть доступна другим участникам;
		<br>
		<b>6.2.3.</b> Этого требует международное законодательство и/или органы власти с соблюдением законной процедуры;
		<br>
		<b>6.2.4.</b> Участник нарушает настоящее Cоглашение и правила игровой площадки.
		<br><br>
		<b>Иные положения.</b><br>
		<b>7.1.</b> Недействительность части или пункта (подпункта) настоящего соглашения не влечет недействительности всех остальных частей и пунктов (подпунктов).
		<br>
		<b>7.2.</b> Срок действия настоящего Соглашения устанавливается на весь период действия игровой площадки, то есть на неопределенный срок, и не предполагает срока окончания данного соглашения.
		<br>
		<b>7.3.</b> Регистрируясь и находясь на игровой площадке, участник признает, что он прочитал, понял и полностью принимает условия настоящего Соглашения, а также правила игры и иных официальных документов.
</div></div>





	</div>
</section>
</div>