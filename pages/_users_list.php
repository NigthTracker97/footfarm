<div class="s-bk-lf">
    <div class="acc-title">Список пользователей</div>
</div>
<div class="silver-bk">
    <?PHP
    $_OPTIMIZATION["title"] = "Аккаунт - Список пользователей";
    $num_p = (isset($_GET["page"]) AND intval($_GET["page"]) < 1000 AND intval($_GET["page"]) >= 1) ? (intval($_GET["page"]) - 1) : 0;
    $lim = $num_p * 100;

    $db->Query("SELECT * FROM db_users_a ORDER BY id DESC LIMIT {$lim}, 100");

    if ($db->NumRows() > 0) {

        ?>
        <table cellpadding="3" cellspacing="0" border="0" bordercolor="#336633" align="center" width="99%">
            <tr bgcolor="#efefef">
                <td style="border: 1px dashed #db8;" class="m-tb" align="center" width="25%">Пользователь</td>
                <td style="border: 1px dashed #db8;" class="m-tb" align="center" width="25%">Email</td>
                <td style="border: 1px dashed #db8;" class="m-tb" align="center" width="25%">Зарегистрирован</td>
                <td style="border: 1px dashed #db8;" class="m-tb" align="center" width="25%">Последний вход</td>
            </tr>


            <?PHP

            while ($data = $db->FetchArray()) {

                ?>
                <tr class="htt">
                    <td style="border: 1px dashed #db8;" align="center"><?=substr($data["user"],0,-2).'<font style="color:red">ХХ</font>'; ?></td>
                    <td style="border: 1px dashed #db8;" align="center"><?= str_replace(substr($data["email"], 2, 3), '<font color="red">***</font>', $data["email"]); ?></td>
                    <td style="border: 1px dashed #db8;" align="center"><?=$func->time2word($data["date_reg"])?></td>
                    <td style="border: 1px dashed #db8;" align="center"><?=$func->time2word($data["date_login"])?></td>
                </tr>
                <?PHP

            }

            ?>

        </table>
        <BR/>
        <?PHP

    } else echo "<center><b>На данной странице нет записей</b></center><BR />";

    $db->Query("SELECT COUNT(*) FROM db_users_a");
    $all_pages = $db->FetchRow();

    if ($all_pages > 100) {

        $sort_b = (isset($_GET["sort"])) ? intval($_GET["sort"]) : 0;

        $nav = new navigator;
        $page = (isset($_GET["page"]) AND intval($_GET["page"]) < 9999999999 AND intval($_GET["page"]) >= 1) ? (intval($_GET["page"])) : 1;

        echo "<BR /><center>" . $nav->Navigation(10, $page, ceil($all_pages / 100), "/users/"), "</center>";

    }
    ?>
    <div class="clr"></div>
</div>
