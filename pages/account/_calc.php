<?

$_OPTIMIZATION["title"] = "Калькулятор прибыли";
$usid = $_SESSION["user_id"];
$usname = $_SESSION["user"];

$db->Query("SELECT * FROM db_users_b WHERE id = '$usid' LIMIT 1");
$user_data = $db->FetchArray();

# Настройки
$sonfig_site = $func->config_site($db);

?>

<script>
	
var cars=[];
	cars[0]=<?=$sonfig_site["a_in_h"]; ?> / 100;
	cars[1]=<?=$sonfig_site["b_in_h"]; ?> / 100;
	cars[2]=<?=$sonfig_site["c_in_h"]; ?> / 100;
	cars[3]=<?=$sonfig_site["d_in_h"]; ?> / 100;
	cars[4]=<?=$sonfig_site["e_in_h"]; ?> / 100;
	cars[5]=<?=$sonfig_site["f_in_h"]; ?> / 100;
	cars[6]=<?=$sonfig_site["g_in_h"]; ?> / 100;
	cars[7]=<?=$sonfig_site["h_in_h"]; ?> / 100;
	cars[8]=<?=$sonfig_site["i_in_h"]; ?> / 100;
	cars[9]=<?=$sonfig_site["j_in_h"]; ?> / 100;
  
function number_format( number, decimals, dec_point, thousands_sep ) {	// Format a number with grouped thousands
	//
	// +   original by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
	// +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	// +	 bugfix by: Michael White (http://crestidg.com)

	var i, j, kw, kd, km;

	// input sanitation & defaults
	if( isNaN(decimals = Math.abs(decimals)) ){
		decimals = 2;
	}
	if( dec_point == undefined ){
		dec_point = ",";
	}
	if( thousands_sep == undefined ){
		thousands_sep = ".";
	}

	i = parseInt(number = (+number || 0).toFixed(decimals)) + "";

	if( (j = i.length) > 3 ){
		j = j % 3;
	} else{
		j = 0;
	}

	km = (j ? i.substr(0, j) + thousands_sep : "");
	kw = i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands_sep);
	//kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).slice(2) : "");
	kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).replace(/-/, 0).slice(2) : "");


	return km + kw + kd;
}


function calculate() {
  var h1=0;
  var h24=0;
  var d30=0;
  var d365=0;
  for(var i=0;i<10;i++) {
    h1+=cars[i]*parseInt($("#car"+i).val());
    h24+=cars[i]*parseInt($("#car"+i).val())*24;
    d30+=cars[i]*parseInt($("#car"+i).val())*24*30;
    d365+=cars[i]*parseInt($("#car"+i).val())*24*365;
  }
  $("#dhd1").html(number_format(h1, 2, '.', ' ')+" сер.");
  $("#dhd2").html(number_format(h24, 2, '.', ' ')+" сер.");
  $("#dhd3").html(number_format(d30, 2, '.', ' ')+" сер.");
  $("#dhd4").html(number_format(d365, 2, '.', ' ')+" сер.");
  
  $("#dhr1").html(number_format(h1 / 100, 2, '.', ' ')+" <i class='fa fa-rub'></i>");
  $("#dhr2").html(number_format(h24 / 100, 2, '.', ' ')+" <i class='fa fa-rub'></i>");
  $("#dhr3").html(number_format(d30 / 100, 2, '.', ' ')+" <i class='fa fa-rub'></i>");
  $("#dhr4").html(number_format(d365 / 100, 2, '.', ' ')+" <i class='fa fa-rub'></i>");
}
setTimeout(calculate, 500);

</script>

<div class="container">
<p class="raceinfotext">Калькулятор дохода служит для расчета Вашей прибыли от клубов в вашем владении. <b>Калькулятор не учитывает возможные бонусы, ускорения заработка от <a>Гонки лидеров</a> а так же реферальные вознаграждения.</b> Калькулятор считает только чистую прибыль за час, сутки, месяц и год согласно скорости заработка каждого из зданий.</p>
<hr><div class="ideas_coment text-center">Введите необходимое кол-во клубов для расчета:</div><hr class="m-b-10">
<div class="row">
<div class="col-md-4">
<div class="form-group">
<label class="control-label">ТОТТЕНХЕМ</label>
<div class="input-group bootstrap-touchspin"><span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span><input id="car0" class="vertical-spin form-control" type="number" value="<?=$user_data["a_t"]; ?>" name="vertical-spin" style="display: block;"><span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span><span class="input-group-btn-vertical"><button class="btn btn-default bootstrap-touchspin-up" type="button"><i class="ion-plus-round"></i></button><button class="btn btn-default bootstrap-touchspin-down" type="button"><i class="ion-minus-round"></i></button></span></div>
</div>
<div class="form-group">
<label class="control-label">БАВАРИЯ</label>
<div class="input-group bootstrap-touchspin"><span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span><input id="car1" class="vertical-spin form-control" type="number" value="<?=$user_data["b_t"]; ?>" name="vertical-spin" style="display: block;"><span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span><span class="input-group-btn-vertical"><button class="btn btn-default bootstrap-touchspin-up" type="button"><i class="ion-plus-round"></i></button><button class="btn btn-default bootstrap-touchspin-down" type="button"><i class="ion-minus-round"></i></button></span></div>
</div>
<div class="form-group">
<label class="control-label">ЛИВЕРПУЛЬ</label>
<div class="input-group bootstrap-touchspin"><span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span><input id="car2" class="vertical-spin form-control" type="number" value="<?=$user_data["c_t"]; ?>" name="vertical-spin" style="display: block;"><span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span><span class="input-group-btn-vertical"><button class="btn btn-default bootstrap-touchspin-up" type="button"><i class="ion-plus-round"></i></button><button class="btn btn-default bootstrap-touchspin-down" type="button"><i class="ion-minus-round"></i></button></span></div>
</div>
<div class="form-group">
<label class="control-label">ЧЕЛСИ</label>
<div class="input-group bootstrap-touchspin"><span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span><input id="car3" class="vertical-spin form-control" type="number" value="<?=$user_data["d_t"]; ?>" name="vertical-spin" style="display: block;"><span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span><span class="input-group-btn-vertical"><button class="btn btn-default bootstrap-touchspin-up" type="button"><i class="ion-plus-round"></i></button><button class="btn btn-default bootstrap-touchspin-down" type="button"><i class="ion-minus-round"></i></button></span></div>
</div>
</div>
<div class="col-md-4">
<div class="form-group">
<label class="control-label">ЦСКА</label>
<div class="input-group bootstrap-touchspin"><span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span><input id="car4" class="vertical-spin form-control" type="number" value="<?=$user_data["e_t"]; ?>" name="vertical-spin" style="display: block;"><span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span><span class="input-group-btn-vertical"><button class="btn btn-default bootstrap-touchspin-up" type="button"><i class="ion-plus-round"></i></button><button class="btn btn-default bootstrap-touchspin-down" type="button"><i class="ion-minus-round"></i></button></span></div>
</div>
<div class="form-group">
<label class="control-label">ЗЕНИТ</label>
<div class="input-group bootstrap-touchspin"><span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span><input id="car5" class="vertical-spin form-control" type="number" value="<?=$user_data["f_t"]; ?>" name="vertical-spin" style="display: block;"><span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span><span class="input-group-btn-vertical"><button class="btn btn-default bootstrap-touchspin-up" type="button"><i class="ion-plus-round"></i></button><button class="btn btn-default bootstrap-touchspin-down" type="button"><i class="ion-minus-round"></i></button></span></div>
</div>
<div class="form-group">
<label class="control-label">РЕАЛ МАДРИД</label>
<div class="input-group bootstrap-touchspin"><span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span><input id="car6" class="vertical-spin form-control" type="number" value="<?=$user_data["g_t"]; ?>" name="vertical-spin" style="display: block;"><span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span><span class="input-group-btn-vertical"><button class="btn btn-default bootstrap-touchspin-up" type="button"><i class="ion-plus-round"></i></button><button class="btn btn-default bootstrap-touchspin-down" type="button"><i class="ion-minus-round"></i></button></span></div>
</div>
<div class="form-group">
<label class="control-label">МАНЧЕСТЕР ЮНАЙТЕД</label>
<div class="input-group bootstrap-touchspin"><span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span><input id="car7" class="vertical-spin form-control" type="number" value="<?=$user_data["h_t"]; ?>" name="vertical-spin" style="display: block;"><span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span><span class="input-group-btn-vertical"><button class="btn btn-default bootstrap-touchspin-up" type="button"><i class="ion-plus-round"></i></button><button class="btn btn-default bootstrap-touchspin-down" type="button"><i class="ion-minus-round"></i></button></span></div>
</div>
</div>
<div class="col-md-4">

<div class="form-group">
<label class="control-label">СПАРТАК</label>
<div class="input-group bootstrap-touchspin"><span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span><input id="car8" class="vertical-spin form-control" type="number" value="<?=$user_data["i_t"]; ?>" name="vertical-spin" style="display: block;"><span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span><span class="input-group-btn-vertical"><button class="btn btn-default bootstrap-touchspin-up" type="button"><i class="ion-plus-round"></i></button><button class="btn btn-default bootstrap-touchspin-down" type="button"><i class="ion-minus-round"></i></button></span></div>
</div>
<div class="form-group">
<label class="control-label">БАРСЕЛОНА</label>
<div class="input-group bootstrap-touchspin"><span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span><input id="car9" class="vertical-spin form-control" type="number" value="<?=$user_data["j_t"]; ?>" name="vertical-spin" style="display: block;"><span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span><span class="input-group-btn-vertical"><button class="btn btn-default bootstrap-touchspin-up" type="button"><i class="ion-plus-round"></i></button><button class="btn btn-default bootstrap-touchspin-down" type="button"><i class="ion-minus-round"></i></button></span></div>
</div>

</div>
</div>
<div class="text-center m-t-10">
<button type="button" class="btn waves-effect btn-primary btn-block calc_btn" style="width:300px;" onclick="calculate();"><i class="fa fa-calculator"></i> Рассчитать доход</button>
</div>
<div class="row" style="margin-top: 20px;">
<div class="col-md-6 offset-3 p-t-10">
<div class="calc_resultline">Доход за 1 час: <div><span id="dhd1">0</span> / <span id="dhr1">0<i class="fa fa-rub"></i></span></div></div>
<div class="calc_resultline">Доход за 24 часа: <div><span id="dhd2">0</span> / <span id="dhr2">0<i class="fa fa-rub"></i></span></div></div>
<div class="calc_resultline">Доход за 30 дней: <div><span id="dhd3">0</span> / <span id="dhr3">0<i class="fa fa-rub"></i></span></div></div>
<div class="calc_resultline">Доход за 365 дней: <div><span id="dhd4">0</span> / <span id="dhr4">0<i class="fa fa-rub"></i></span></div></div>
</div>
</div>
</div>