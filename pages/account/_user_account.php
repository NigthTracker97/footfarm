<?php

$_OPTIMIZATION["title"] = "Мой профиль";

# Настройки
$sonfig_site = $func->config_site($db);

$db->Query("SELECT * FROM db_users_a, db_users_b, db_users_c WHERE db_users_a.id = db_users_b.id AND db_users_a.id = db_users_c.id AND db_users_a.id = {$_SESSION["user_id"]}");
$prof_data = $db->FetchArray();

$tw1 = $sonfig_site["a_in_h"] / 100;
$tw2 = $sonfig_site["b_in_h"] / 100;
$tw3 = $sonfig_site["c_in_h"] / 100;
$tw4 = $sonfig_site["d_in_h"] / 100;
$tw5 = $sonfig_site["e_in_h"] / 100;
$tw6 = $sonfig_site["f_in_h"] / 100;
$tw7 = $sonfig_site["g_in_h"] / 100;
$tw8 = $sonfig_site["h_in_h"] / 100;
$tw9 = $sonfig_site["i_in_h"] / 100;
$tw10 = $sonfig_site["j_in_h"] / 100;

$speed = $prof_data['a_t'] * $tw1 + $prof_data['b_t'] * $tw2 + $prof_data['c_t'] * $tw3 + $prof_data['d_t'] * $tw4 + $prof_data['e_t'] * $tw5 + $prof_data['f_t'] * $tw6
 + $prof_data['g_t'] * $tw7 + $prof_data['h_t'] * $tw8 + $prof_data['i_t'] * $tw9 + $prof_data['j_t'] * $tw10;

$paymeny_money_type = array(
    '0' => 'сер.',
    '1' => 'сер.',
    '2' => 'руб.',
    '3' => 'CP'
);

?>

<div class="container">
<div class="s-bk-lf" style="background: rgba(220, 53, 53, 0.47);">
<div class="acc-title"></div>
<center><h5>АКЦИЯ НА ПОПОЛНЕНИЕ БАЛАНСА:</h5>

<h6>Пополнение на любую сумму: +10% от суммы пополнения!</h6>
</center>
</div>
</div>
<div class="container-fluid">
	<div class="row">
		<div class="col-sm-6 col-lg-3">
			<div class="panel2 text-center">
				<div class="panel-heading">
					<h4 class="panel-title text-muted font-light profilemsz">Скорость заработка</h4>
				</div>
				<div class="panel-body p-t-10">
					<h2 class="m-t-0 m-b-15 profilemst"><i class="mdi mdi-speedometer text-danger m-r-10"></i><b><?=$speed / 100;?><i class="fa fa-rub" aria-hidden="true"></i> / час</b></h2>
				</div>
			</div>
		</div>
		<div class="col-sm-6 col-lg-3">
			<div class="panel2 text-center">
				<div class="panel-heading">
					<h4 class="panel-title text-muted font-light profilemsz">Количество рефералов</h4>
				</div>
				<div class="panel-body p-t-10">
					<h2 class="m-t-0 m-b-15 profilemst"><i class="mdi mdi-speedometer text-danger m-r-10"></i><b><?=$prof_data['referals'];?>  чел.</b></h2>
				</div>
			</div>
		</div>
		<div class="col-sm-6 col-lg-3">
			<div class="panel2 text-center">
				<div class="panel-heading">
					<h4 class="panel-title text-muted font-light profilemsz">Выплачено</h4>
				</div>
				<div class="panel-body p-t-10">
					<h2 class="m-t-0 m-b-15 profilemst"><i class="mdi mdi-speedometer text-danger m-r-10"></i><b><?= sprintf("%.2f", $prof_data["payment_sum"]); ?><i class="fa fa-rub" aria-hidden="true"></i></b></h2>
				</div>
			</div>
		</div>
		<div class="col-sm-6 col-lg-3">
			<div class="panel2 text-center">
				<div class="panel-heading">
					<h4 class="panel-title text-muted font-light profilemsz">Пополнено</h4>
				</div>
				<div class="panel-body p-t-10">
					<h2 class="m-t-0 m-b-15 profilemst"><i class="mdi mdi-speedometer text-danger m-r-10"></i><b><?= sprintf("%.2f", $prof_data["insert_sum"]); ?><i class="fa fa-rub" aria-hidden="true"></i></b></h2>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container">
<center style="margin-bottom: 10px;text-align: left;color: #484847;margin-top: 0px;font-size: 14px;text-transform: uppercase;font-weight: bold;">
<span id="blink"> Добро пожаловать в увлекательную игру с выводом средств «<?=$config->site_name?>»
<br></span></center>
<div style="background-color: #ffffff;color: #484847;padding: 0px 10px 10px 0px;width: 100%; margin-bottom: 10px;">
Покупайте различные футбольные клубы и зарабатывайте на них деньги. Чем дороже клуб, тем большее количество реальных денег он приносит ежедневно.
</div>

<div class="stat-acc">
	<div class="row">
		
			<div class="col-sm-12 col-xs-12">
				<table class="table table-hover" style="border: 1px solid #ddd;margin-top: 5px;margin-bottom: 10px;background: #f3f4f5;">
					<tbody>
						<tr>
						<td>Ваш псевдоним (логин):</td>
						<td align="right"><div class="label label-primary" style="background: #fff;float: right;font-size: 14px;line-height: 14px;color: #484847;border: 1px solid #e7e7e7;font-weight: bold;padding: 7px 15px 7px 15px;z-index: 10;font-family: 'Open Sans', sans-serif;border-radius: 25px;"><?= $prof_data["user"]; ?></div></td>
						</tr>
						<tr>
						<td>Дата регистрации на проекте:</td>
						<td align="right"><div class="label label-primary" style="background: #fff;float: right;font-size: 14px;line-height: 14px;color: #484847;border: 1px solid #e7e7e7;font-weight: bold;padding: 7px 15px 7px 15px;z-index: 10;font-family: 'Open Sans', sans-serif;border-radius: 25px;"><?= date("d.m.Y в H:i:s", $prof_data["date_reg"]); ?></div></td>
						</tr>
						<tr>
						<td>На проект вас пригласил:</td>
						<td align="right"><div class="label label-primary" style="background: #fff;float: right;font-size: 14px;line-height: 14px;color: #484847;border: 1px solid #e7e7e7;font-weight: bold;padding: 7px 15px 7px 15px;z-index: 10;font-family: 'Open Sans', sans-serif;border-radius: 25px;">
							<? if($prof_data["referer_id"] != 1){ ?>
								<?= $prof_data["referer"]; ?> / ID: <?= $prof_data["referer_id"]; ?>
							<? } else { ?>
								сам пришел
							<? } ?>
						</div></td>
						</tr>
						<tr>
						<td>На данный момент у вас серебра:</td>
						<td align="right"><div class="label label-primary" style="background: #fff;float: right;font-size: 14px;line-height: 14px;color: #484847;border: 1px solid #e7e7e7;font-weight: bold;padding: 7px 15px 7px 15px;z-index: 10;font-family: 'Open Sans', sans-serif;border-radius: 25px;"><?= sprintf("%.2f", $prof_data["money_b"]); ?></div></td>
						</tr>
						<tr>
						<td>Заработано с рефералов:</td>
						<td align="right"><div class="label label-primary" style="background: #fff;float: right;font-size: 14px;line-height: 14px;color: #484847;border: 1px solid #e7e7e7;font-weight: bold;padding: 7px 15px 7px 15px;z-index: 10;font-family: 'Open Sans', sans-serif;border-radius: 25px;"><?= sprintf("%.2f", $prof_data["from_referals"]); ?></div></td>
						</tr>
                    </tbody>
				</table>
			</div>
		</div>
		
	</div>
</div>


  <script type="text/javascript" src="/js/syronex-colorpicker.js"></script>
  	
	


		
	   
		
<div class="container">
 
	<div style="color: #484847;text-transform: uppercase;font-weight: bold;">
		<span id="blink"> Мои купленные клубы в работе</span>
	</div>

</div>
<?

$usid = $_SESSION["user_id"];
$usname = $_SESSION["user"];

$db->Query("SELECT * FROM db_users_b WHERE id = '$usid' LIMIT 1");
$user_data = $db->FetchArray();

# Настройки
$sonfig_site = $func->config_site($db);
?>
<div class="row">
	<div class="col-md-3">
		<div class="panel panel-product">
			<div class="panel-body garden_list_item">
				<div class="panel-image">
					<img src="images/v2/rueaca0887773.jpeg" class="garden_list_item_img">
				</div>
				<div class="panel-name">
					<span class="garden_list_title">ТОТТЕНХЕМ</span>
				</div>
				<div class="about-block"><span style="line-height: 80px;font-size: 13px;"><?=$user_data["a_t"]; ?> шт.</span></div>
			</div>
		</div>
	</div>

	<div class="col-md-3">
		<div class="panel panel-product">
			<div class="panel-body garden_list_item">
				<div class="panel-image">
					<img src="images/v2/rue4a417205ae.jpeg" class="garden_list_item_img">
				</div>
				<div class="panel-name">
					<span class="garden_list_title">БАВАРИЯ</span>
				</div>
				<div class="about-block"><span style="line-height: 80px;font-size: 13px;"><?=$user_data["b_t"]; ?> шт.</span></div>
			</div>
		</div>
	</div>

	<div class="col-md-3">
		<div class="panel panel-product">
			<div class="panel-body garden_list_item">
				<div class="panel-image">
					<img src="images/v2/rue2f5f4a4fd1.jpeg" class="garden_list_item_img">
				</div>
				<div class="panel-name">
					<span class="garden_list_title">ЛИВЕРПУЛЬ</span>
				</div>
				<div class="about-block"><span style="line-height: 80px;font-size: 13px;"><?=$user_data["c_t"]; ?> шт.</span></div>
			</div>
		</div>
	</div>

	<div class="col-md-3">
		<div class="panel panel-product">
			<div class="panel-body garden_list_item">
				<div class="panel-image">
					<img src="images/v2/rue5281d41089.jpeg" class="garden_list_item_img">
				</div>
				<div class="panel-name">
					<span class="garden_list_title">ЧЕЛСИ</span>
				</div>
				<div class="about-block"><span style="line-height: 80px;font-size: 13px;"><?=$user_data["d_t"]; ?> шт.</span></div>
			</div>
		</div>
	</div>

	<div class="col-md-3">
		<div class="panel panel-product">
			<div class="panel-body garden_list_item">
				<div class="panel-image">
					<img src="images/v2/rue80ff95a4b4.jpeg" class="garden_list_item_img">
				</div>
				<div class="panel-name">
					<span class="garden_list_title">ЦСКА</span>
				</div>
				<div class="about-block"><span style="line-height: 80px;font-size: 13px;"><?=$user_data["e_t"]; ?> шт.</span></div>
			</div>
		</div>
	</div>
	
	<div class="col-md-3">
		<div class="panel panel-product">
			<div class="panel-body garden_list_item">
				<div class="panel-image">
					<img src="images/v2/rueb389d6bfbe.jpg" class="garden_list_item_img">
				</div>
				<div class="panel-name">
					<span class="garden_list_title">ЗЕНИТ</span>
				</div>
				<div class="about-block"><span style="line-height: 80px;font-size: 13px;"><?=$user_data["f_t"]; ?> шт.</span></div>
			</div>
		</div>
	</div>
	
	<div class="col-md-3">
		<div class="panel panel-product">
			<div class="panel-body garden_list_item">
				<div class="panel-image">
					<img src="images/v2/rue00d7d50127.jpeg" class="garden_list_item_img">
				</div>
				<div class="panel-name">
					<span class="garden_list_title">РЕАЛ МАДРИД</span>
				</div>
				<div class="about-block"><span style="line-height: 80px;font-size: 13px;"><?=$user_data["g_t"]; ?> шт.</span></div>
			</div>
		</div>
	</div>
	
	<div class="col-md-3">
		<div class="panel panel-product">
			<div class="panel-body garden_list_item">
				<div class="panel-image">
					<img src="images/v2/rue6d982f73e3.jpeg" class="garden_list_item_img">
				</div>
				<div class="panel-name">
					<span class="garden_list_title">МАНЧЕСТЕР ЮНАЙТЕД</span>
				</div>
				<div class="about-block"><span style="line-height: 80px;font-size: 13px;"><?=$user_data["h_t"]; ?> шт.</span></div>
			</div>
		</div>
	</div>
	
	<div class="col-md-3">
		<div class="panel panel-product">
			<div class="panel-body garden_list_item">
				<div class="panel-image">
					<img src="images/v2/rue2997571681.jpeg" class="garden_list_item_img">
				</div>
				<div class="panel-name">
					<span class="garden_list_title">СПАРТАК</span>
				</div>
				<div class="about-block"><span style="line-height: 80px;font-size: 13px;"><?=$user_data["i_t"]; ?> шт.</span></div>
			</div>
		</div>
	</div>
	
	<div class="col-md-3">
		<div class="panel panel-product">
			<div class="panel-body garden_list_item">
				<div class="panel-image">
					<img src="images/v2/rue1f07b4ae80.jpeg" class="garden_list_item_img">
				</div>
				<div class="panel-name">
					<span class="garden_list_title">БАРСЕЛОНА</span>
				</div>
				<div class="about-block"><span style="line-height: 80px;font-size: 13px;"><?=$user_data["j_t"]; ?> шт.</span></div>
			</div>
		</div>
	</div>
</div>

		</div>	

	

</div>









