<div class="s-bk-lf">
    <div class="acc-title">Настройки</div>
</div>
<div class="silver-bk">
    <div class="clr"></div>
    <?PHP
    $db->Query("SELECT * FROM db_config WHERE id = '1'");
    $data_c = $db->FetchArray();

    $paymeny_limit_ar = array(
        '0' => 'Выплаты не ограничены',
        '1' => 'Платежные баллы',
        '2' => 'Динамический курс на вывод'
    );
    $paymeny_money_type = array(
        '0' => 'Счет для покупок (серебро)',
        '1' => 'Счет для вывода (серебро)',
        '2' => 'Реферальный счет (руб.)',
        '3' => 'Cash Points'
    );

    function ViewPurse($purse)
    {

        if (substr($purse, 0, 1) != "P") return false;
        if (
            ereg("^[0-9]{7}$", substr($purse, 1)) ||
            ereg("^[0-9]{8}$", substr($purse, 1)) ||
            ereg("^[0-9]{9}$", substr($purse, 1)) ||
            ereg("^[0-9]{10}$", substr($purse, 1)) ||
            ereg("^[0-9]{11}$", substr($purse, 1)) ||
            ereg("^[0-9]{12}$", substr($purse, 1)) ||
            ereg("^[0-9]{13}$", substr($purse, 1)) ||
            ereg("^[0-9]{14}$", substr($purse, 1)) ||
            ereg("^[0-9]{15}$", substr($purse, 1))
        ) return $purse;
        else return false;
    }

    if(isset($_GET['delete_admin_purse'])){
        $delete_admin_purse = intval($_GET['delete_admin_purse']);
        $db->Query("DELETE FROM db_admins WHERE id = {$delete_admin_purse}");
        header("Location: /?menu=admin4ik&sel=config");
    }

    # Обновление
    if (isset($_POST["admin"])) {

        $admin = $func->IsLogin($_POST["admin"]);
        $pass = $func->IsLogin($_POST["pass"]);


        $ser_per_wmr = intval($_POST["ser_per_wmr"]);
        $ser_per_wmz = intval($_POST["ser_per_wmz"]);
        $ser_per_wme = intval($_POST["ser_per_wme"]);
        $percent_swap = intval($_POST["percent_swap"]);
        $percent_sell = intval($_POST["percent_sell"]);
        $items_per_coin = intval($_POST["items_per_coin"]);
        $percent_back = intval($_POST["percent_back"]);

        $tomat_in_h = intval($_POST["a_in_h"]);
        $straw_in_h = intval($_POST["b_in_h"]);
        $pump_in_h = intval($_POST["c_in_h"]);
        $peas_in_h = intval($_POST["d_in_h"]);
        $pean_in_h = intval($_POST["e_in_h"]);

        $amount_tomat_t = intval($_POST["amount_a_t"]);
        $amount_straw_t = intval($_POST["amount_b_t"]);
        $amount_pump_t = intval($_POST["amount_c_t"]);
        $amount_peas_t = intval($_POST["amount_d_t"]);
        $amount_pean_t = intval($_POST["amount_e_t"]);

        $site_start = intval($_POST["site_start"]);

        $admin_persent = intval($_POST["admin_persent"]);
        $admin_purse = ViewPurse($_POST["admin_purse"]);

        $fake_insert = floatval($_POST["fake_insert"]);
        $fake_payment = floatval($_POST["fake_payment"]);

        $ref1_pers = floatval($_POST["ref1_pers"]);
        $ref2_pers = floatval($_POST["ref2_pers"]);
        $ref3_pers = floatval($_POST["ref3_pers"]);
        $ref4_pers = floatval($_POST["ref4_pers"]);
        $ref5_pers = floatval($_POST["ref5_pers"]);
        $ref6_pers = floatval($_POST["ref6_pers"]);

        $serf_price          = floatval($_POST["serf_price"]);
        $serf_price_timer    = floatval($_POST["serf_price_timer"]);
        $serf_price_move     = floatval($_POST["serf_price_move"]);
        $serf_price_high     = floatval($_POST["serf_price_high"]);
        $serf_price_target   = floatval($_POST["serf_price_target"]);

        $ref1_cp = floatval($_POST["ref1_cp"]);
        $ref2_cp = floatval($_POST["ref2_cp"]);
        $ref3_cp = floatval($_POST["ref3_cp"]);
        $ref4_cp = floatval($_POST["ref4_cp"]);
        $ref5_cp = floatval($_POST["ref5_cp"]);
        $ref6_cp = floatval($_POST["ref6_cp"]);

        $referals_level = intval($_POST["referals_level"]);

        $payment_limit = intval($_POST["payment_limit"]);

        $first_payment_pers = intval($_POST["first_payment_pers"]);

        $payment_unlock = floatval($_POST["payment_unlock"]);
        $payment_ext_unlock = floatval($_POST["payment_ext_unlock"]);
        $insert_min = floatval($_POST["insert_min"]);

        $payment_min = floatval($_POST["payment_min"]);

        $wmset_pers = floatval($_POST["wmset_pers"]);
        $wmset_mult = floatval($_POST["wmset_mult"]);

        $item_sale = intval($_POST["item_sale"]);
        $sale_insert_ok = floatval($_POST["sale_insert_ok"]);
        $sale_persent = floatval($_POST["sale_persent"]);

        $user_cash_points = floatval($_POST["user_cash_points"]);

        $null_curse = intval($_POST["null_curse"]);

        $fakecontrol_payment = intval($_POST["fakecontrol_payment"]);
        $fakecontrol_insert = intval($_POST["fakecontrol_insert"]);
        $fakecontrol_users = intval($_POST["fakecontrol_users"]);
        $fc_payment_count_min = intval($_POST["fc_payment_count_min"]);
        $fc_payment_count_max = intval($_POST["fc_payment_count_max"]);
        $fc_insert_count = intval($_POST["fc_insert_count"]);
        $fc_insert_max = floatval($_POST["fc_insert_max"]);
        $fc_users_count_min = intval($_POST["fc_users_count_min"]);
        $fc_users_count_max = intval($_POST["fc_users_count_max"]);
        $fc_loto = intval($_POST["fc_loto"]);
        $fc_wheel = intval($_POST["fc_wheel"]);
        $fc_fwheel1 = intval($_POST["fc_fwheel1"]);
        $fc_fwheel2 = intval($_POST["fc_fwheel2"]);

        $persent_cp2active = floatval($_POST["persent_cp2active"]);
        $time_cp2active = intval($_POST["time_cp2active"]);

        $captcha = intval($_POST["captcha"]);

        $refsale_enable = intval($_POST["refsale_enable"]);
        $refsale_price = floatval($_POST["refsale_price"]);
        $refsale_mtype = floatval($_POST["refsale_mtype"]);

        $serf_enable = intval($_POST["serf_enable"]);
        $serf_money_type = intval($_POST["serf_money_type"]);
        $serf_ref1_persent = floatval($_POST["serf_ref1_persent"]);
        $serf_bonus = floatval($_POST["serf_bonus"]);

        $regbonus = floatval($_POST["regbonus"]);

        $visits_money = floatval($_POST["visits_money"]);

        $ext_payment_komis = floatval($_POST["ext_payment_komis"]);

        $payeer_insert_bonus = floatval($_POST["payeer_insert_bonus"]);

        $wmset_sums = htmlspecialchars(strval($_POST["wmset_sums"]));

        $ref_mtype = intval($_POST["ref_mtype"]);

        $userpayment_type = intval($_POST["userpayment_type"]);
        $userpayment_maxpay = intval($_POST["userpayment_maxpay"]);

        $serf_unlim1d = intval($_POST["serf_unlim1d"]);
        $serf_unlim1w = intval($_POST["serf_unlim1w"]);
        $serf_unlim1m = intval($_POST["serf_unlim1m"]);
        
        $serf_price_vip     = floatval($_POST["serf_price_vip"]);
        $serf_price_oneuser = floatval($_POST["serf_price_oneuser"]);

        //echo '<pre>'.print_r($_POST,true).'</pre>';

        if(isset($_POST['admin_name']) && $_POST['admin_name'] != ''){
            $admin_name = strval($_POST['admin_name']);
            if(isset($_POST['admin_purse']) && $_POST['admin_purse'] != ''){
                $admin_purse = ViewPurse($_POST["admin_purse"]);
                if($admin_purse != false){
                    if(isset($_POST['admin_persent']) && $_POST['admin_persent'] != ''){
                        $admin_persent = floatval($_POST['admin_persent']);
                        $db->Query("INSERT INTO db_admins 
                                            SET 
                                            name = '{$admin_name}',
                                            purse = '{$admin_purse}',
                                            persent = {$admin_persent},
                                            date_add = ".time()."
                            ");
                    }else echo "<center><font color = 'red'><b>Не верный процент комисии администрации</b></font></center><BR />";
                }else echo "<center><font color = 'red'><b>Неверный формат кошелька</b></font></center><BR />";
            }else echo "<center><font color = 'red'><b>Неверный формат кошелька</b></font></center><BR />";
        }

        # Проверка на ошибки
        $errors = true;

        $last_sum = 0;
        for($i=0;$i<count($_POST['wheel_bonus_sum']);$i++){
            $sum = intval($_POST['wheel_bonus_sum'][$i]);
            $wheel = intval($_POST['wheel_bonus_count'][$i]);
            if($sum > $last_sum){
                if($wheel > 0){
                    $wheel_bonus .= "{$sum}:{$wheel};";
                    $last_sum = $sum;
                }else{
                    $errors = false;
                    echo "<center><font color = 'red'><b>Количество билетов на рулетку должно быть больше 0!</b></font></center><BR />";
                    break;
                }
            }else{
                $errors = false;
                echo "<center><font color = 'red'><b>Суммы за бонусные билеты на рулетку должны идти по возрастанию!</b></font></center><BR />";
                break;
            }

        }

        if ($payment_limit < 0 || $payment_limit > 2) {
            $errors = false;
            echo "<center><font color = 'red'><b>Недопустимые настройки ограничений системы</b></font></center><BR />";
        }

        if ($referals_level < 1 || $referals_level > 6) {
            $errors = false;
            echo "<center><font color = 'red'><b>Недопустимые настройки уровня реферальной системы</b></font></center><BR />";
        }

        if ($admin === false) {
            $errors = false;
            echo "<center><font color = 'red'><b>Логин администратора имеет неверный формат</b></font></center><BR />";
        }

        if ($pass === false) {
            $errors = false;
            echo "<center><font color = 'red'><b>Пароль администратора имеет неверный формат</b></font></center><BR />";
        }

        if ($percent_swap < 1 OR $percent_swap > 99) {
            $errors = false;
            echo "<center><font color = 'red'><b>Прибавляемый процент при обмене должен быть от 1 до 99</b></font></center><BR />";
        }

        if ($percent_sell < 1 OR $percent_sell > 99) {
            $errors = false;
            echo "<center><font color = 'red'><b>% серебра на вывод при продаже должен быть от 1 до 99</b></font></center><BR />";
        }

        if ($items_per_coin < 1 OR $items_per_coin > 50000) {
            $errors = false;
            echo "<center><font color = 'red'><b>Сколько фруктов = 1 серебра, должно быть от 1 до 50000</b></font></center><BR />";
        }

        if ($tomat_in_h < 6 OR $straw_in_h < 6 OR $pump_in_h < 6 OR $peas_in_h < 6 OR $pean_in_h < 6) {
            $errors = false;
            echo "<center><font color = 'red'><b>Неверная настройка урожайности деревьев в час! Минимум 6</b></font></center><BR />";
        }


        if ($amount_tomat_t < 1 OR $amount_straw_t < 1 OR $amount_pump_t < 1 OR $amount_peas_t < 1 OR $amount_pean_t < 1) {
            $errors = false;
            echo "<center><font color = 'red'><b>Минимальная стоимость дерева не должна быть менее 1го серебра</b></font></center><BR />";
        }

        if ($percent_back < 0 OR $percent_back > 100) {
            $errors = false;
            echo "<center><font color = 'red'><b>% серебра по акции 'Накопительный банк' должен быть от 0 до 100</b></font></center><BR />";
        }

        # Обновление
        if ($errors) {

            $db->Query("UPDATE db_config SET 
		
		admin = '$admin',
		pass = '$pass',
		ser_per_wmr = '$ser_per_wmr',
		ser_per_wmz = '$ser_per_wmz',
		ser_per_wme = '$ser_per_wme',
		percent_swap = '$percent_swap',
		percent_sell = '$percent_sell',
		items_per_coin = '$items_per_coin',
                percent_back = '$percent_back',
		a_in_h = '$tomat_in_h',
		b_in_h = '$straw_in_h',
		c_in_h = '$pump_in_h',
		d_in_h = '$peas_in_h',
		e_in_h = '$pean_in_h',
		amount_a_t = '$amount_tomat_t',
		amount_b_t = '$amount_straw_t',
		amount_c_t = '$amount_pump_t',
		amount_d_t = '$amount_peas_t',
		amount_e_t = '$amount_pean_t',
		
		fake_insert = {$fake_insert},
		fake_payment = {$fake_payment},
		
		ref1_pers = {$ref1_pers},
		ref2_pers = {$ref2_pers},
		ref3_pers = {$ref3_pers},
		ref4_pers = {$ref4_pers},
		ref5_pers = {$ref5_pers},
		ref6_pers = {$ref6_pers},
		
		ref1_cp = {$ref1_cp},
		ref2_cp = {$ref2_cp},
		ref3_cp = {$ref3_cp},
		ref4_cp = {$ref4_cp},
		ref5_cp = {$ref5_cp},
		ref6_cp = {$ref6_cp},
		
		referals_level = {$referals_level},
		
		site_start = {$site_start},
		
		payment_limit = {$payment_limit},
		payment_unlock = {$payment_unlock},
		payment_ext_unlock = {$payment_ext_unlock},
		
		insert_min = {$insert_min},
		payment_min = {$payment_min},
		
		user_cash_points = {$user_cash_points},
		
		wmset_pers = {$wmset_pers},
		wmset_mult = {$wmset_mult},
		
		wheel_bonus = '{$wheel_bonus}',
		
		item_sale = {$item_sale},
		sale_insert_ok = {$sale_insert_ok},
		sale_persent = {$sale_persent},
		
		first_payment_pers = {$first_payment_pers},
		
		null_curse = {$null_curse},
		
		fakecontrol_payment = {$fakecontrol_payment},
		fakecontrol_insert = {$fakecontrol_insert},
		fakecontrol_users = {$fakecontrol_users},
		fc_payment_count_min = {$fc_payment_count_min},
		fc_payment_count_max = {$fc_payment_count_max},
		fc_insert_count = {$fc_insert_count},
		fc_insert_max = {$fc_insert_max},
		fc_users_count_min = {$fc_users_count_min},
		fc_users_count_max = {$fc_users_count_max},
		fc_loto = {$fc_loto},
		fc_wheel = {$fc_wheel},
		fc_fwheel1 = {$fc_fwheel1},
		fc_fwheel2 = {$fc_fwheel2},
		
		wmset_sums = '{$wmset_sums}',
		
		captcha = {$captcha},
		
		persent_cp2active = {$persent_cp2active},
		time_cp2active = {$time_cp2active},
		
		refsale_enable = {$refsale_enable},
		refsale_price = {$refsale_price},
		refsale_mtype = {$refsale_mtype},
		
		regbonus = {$regbonus},
		
		visits_money = {$visits_money},
		
        serf_price        = {$serf_price},
		serf_price_timer  = {$serf_price_timer},
		serf_price_move   = {$serf_price_move},
		serf_price_high   = {$serf_price_high},
		serf_price_target = {$serf_price_target},		
		serf_enable       = {$serf_enable},		
		serf_money_type   = {$serf_money_type},		
		serf_ref1_persent = {$serf_ref1_persent},	
		serf_bonus        = {$serf_bonus},
		serf_unlim1d      = {$serf_unlim1d},
		serf_unlim1w      = {$serf_unlim1w},
		serf_unlim1m      = {$serf_unlim1m},
		
		ref_mtype         = {$ref_mtype},
		
		ext_payment_komis = {$ext_payment_komis},
		
		payeer_insert_bonus = {$payeer_insert_bonus},
		
		userpayment_type          = {$userpayment_type},
		userpayment_maxpay        = {$userpayment_maxpay},
			
		serf_price_vip            = {$serf_price_vip},
		serf_price_oneuser        = {$serf_price_oneuser}
		
		WHERE id = '1'");

            echo "<center><font color = 'green'><b>Сохранено</b></font></center><BR />";
            $db->Query("SELECT * FROM db_config WHERE id = '1'");
            $data_c = $db->FetchArray();
        }

    }

    ?>
    <form action="" method="post">
        <table width="100%" border="0">
            <tr>
                <td colspan="2" align="center">-------------------Доступ-------------------</td>
            </tr>
            <tr>
                <td><b>Логин администратора:</b></td>
                <td width="150" align="center"><input type="text" name="admin" value="<?= $data_c["admin"]; ?>"/></td>
            </tr>
            <tr>
                <td><b>Пароль администратора:</b></td>
                <td width="150" align="center"><input type="text" name="pass" value="<?= $data_c["pass"]; ?>"/></td>
            </tr>

            <tr>
                <td colspan="2" align="center">-------------------Авторизация-------------------</td>
            </tr>

            <tr>
                <td><b>ReCaptcha:</b></td>
                <td width="150" align="center">
                    <select name="captcha" style="width: 100%;">
                        <option value="0" <? if($data_c["captcha"] == 0) echo 'selected'; ?>>ВЫКЛ.</option>
                        <option value="1" <? if($data_c["captcha"] == 1) echo 'selected'; ?>>ВКЛ.</option>
                    </select>
                </td>
            </tr>

            <tr>
                <td colspan="2" align="center">-------------------Админские доходы-------------------</td>
            </tr>
            <tr>
                <td colspan="2">
                    <table style="background: #efefef;padding: 10px 10px;width: 100%;box-sizing: border-box;">
                        <tr>
                            <td>Имя</td>
                            <td>Кошелек PAYEER</td>
                            <td>Процент отчисления</td>
                            <td>Начисленно сегодня</td>
                            <td>Доход</td>
                            <td></td>
                        </tr>
                        <?
                        $db->Query("SELECT * FROM db_admins");
                        while($adm = $db->FetchArray()){ ?>
                            <tr>
                                <td><input style="width: 130px;" type="text" name="admin_name" value="<?=$adm['name']?>" disabled></td>
                                <td><input style="width: 130px;" type="text" name="admin_purse" value="<?=$adm['purse']?>" disabled></td>
                                <td><input style="width: 130px;" type="text" name="admin_persent" value="<?=$adm['persent']?>" disabled></td>
                                <td><input style="width: 130px;" type="text" value="<?=$adm['money']?>" disabled></td>
                                <td><input style="width: 130px;" type="text" value="<?=$adm['payment_sum']?>" disabled></td>
                                <td>
                                    <a href="/?menu=admin4ik&sel=config&delete_admin_purse=<?=$adm['id']?>"><input type="button" name="sale" value="Удалить"></a>
                                </td>
                            </tr>
                        <? } ?>
                        <tr>
                            <td><input style="width: 130px;" type="text" name="admin_name" placeholder="admin"></td>
                            <td><input style="width: 130px;" type="text" name="admin_purse" placeholder="P0000000"></td>
                            <td><input style="width: 130px;" type="text" name="admin_persent" placeholder="%"></td>
                            <td><input style="width: 130px;" type="text" value="0" disabled></td>
                            <td><input style="width: 130px;" type="text" value="0" disabled></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <center>
                                    <input type="submit" name="buy" value="Добавить"/>
                                </center>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr>
                <td colspan="2" align="center">-------------------Ограничения выплат-------------------</td>
            </tr>
            <tr>
                <td><b>Система ограничений выплат:</b></td>
                <td width="150" align="center">
                    <select name="payment_limit" style="width: 100%;">
                        <? foreach ($paymeny_limit_ar as $ls_n => $ls_name) { ?>
                            <option <? if ($data_c["payment_limit"] == $ls_n) echo 'selected' ?>
                                value="<?= $ls_n ?>"><?= $ls_name ?></option>
                        <? } ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td><b>Сумма для разблокировки обычных выплат (руб.):</b></td>
                <td width="150" align="center"><input type="text" name="payment_unlock"
                                                      value="<?= $data_c["payment_unlock"]; ?>"/></td>
            </tr>
            <tr>
                <td><b>Сумма для разблокировки экстренных выплат (реферал.руб.):</b></td>
                <td width="150" align="center"><input type="text" name="payment_ext_unlock"
                                                      value="<?= $data_c["payment_ext_unlock"]; ?>"/></td>
            </tr>
            <tr>
                <td><b>Мин. сумма пополнения баланса (руб.):</b></td>
                <td width="150" align="center"><input type="text" name="insert_min"
                                                      value="<?= $data_c["insert_min"]; ?>"/></td>
            </tr>
            <tr>
                <td><b>Мин. сумма для вывода средств (руб.):</b></td>
                <td width="150" align="center"><input type="text" name="payment_min"
                                                      value="<?= $data_c["payment_min"]; ?>"/></td>
            </tr>

            <tr>
                <td><b>Экстренные выплаты (коммиссия администрации):</b></td>
                <td width="150" align="center"><input type="text" name="ext_payment_komis"
                                                      value="<?= $data_c["ext_payment_komis"]; ?>"/></td>
            </tr>

            <?if($data_c["payment_limit"] == 2){?>

                <tr>
                    <td><b>Обнуление курса пользователя при пополнении:</b></td>
                    <td width="150" align="center">
                        <select name="null_curse" style="width: 100%;">
                            <option value="0" <? if($data_c["null_curse"] == 0) echo 'selected'; ?>>ВЫКЛ.</option>
                            <option value="1" <? if($data_c["null_curse"] == 1) echo 'selected'; ?>>до исходного</option>
                            <option value="2" <? if($data_c["null_curse"] == 2) echo 'selected'; ?>>1/2 от текущего курса</option>
                            <option value="3" <? if($data_c["null_curse"] == 3) echo 'selected'; ?>>1/4 от текущего курса</option>
                            <option value="4" <? if($data_c["null_curse"] == 4) echo 'selected'; ?>>1/8 от текущего курса</option>
                            <option value="5" <? if($data_c["null_curse"] == 5) echo 'selected'; ?>>Относительно пополнения</option>
                        </select>
                    </td>
                </tr>

            <? } ?>

            <tr>
                <td><b>Распределение реферальных между счетами (счет для покупок / реферальный счет):</b></td>
                <td width="150" align="center">
                    <select name="ref_mtype" style="width: 100%;">
                        <option <? if ($data_c["ref_mtype"] == 0) echo 'selected' ?> value="0">50% / 50%</option>
                        <option <? if ($data_c["ref_mtype"] == 1) echo 'selected' ?> value="1">100% счет для покупок</option>
                        <option <? if ($data_c["ref_mtype"] == 2) echo 'selected' ?> value="2">100% реферальный счет</option>
                    </select>
                </td>
            </tr>

            <tr>
                <td colspan="2" align="center">-------------------Выплаты безопасность-------------------</td>
            </tr>

            <tr>
                <td><b>Принцип работы системы выплат:</b></td>
                <td width="150" align="center">
                    <select name="userpayment_type" style="width: 100%;">
                        <option <? if ($data_c["userpayment_type"] == 0) echo 'selected' ?> value="0">Ручные</option>
                        <option <? if ($data_c["userpayment_type"] == 1) echo 'selected' ?> value="1">Полуавтоматические</option>
                        <option <? if ($data_c["userpayment_type"] == 2) echo 'selected' ?> value="2">Автоматические</option>
                    </select>
                </td>
            </tr>

            <tr>
                <td><b>Защита выплат [полуавтоматические выплаты]: Пропускать выплаты до (руб.)</b></td>
                <td width="150" align="center"><input type="text" name="userpayment_maxpay"
                                                      value="<?= $data_c["userpayment_maxpay"]; ?>"/></td>


            <tr>
                <td colspan="2" align="center">-------------------Накрутка-------------------</td>
            </tr>
            <tr>
                <td><b>Накрутка (выплачено):</b></td>
                <td width="150" align="center"><input type="text" name="fake_payment"
                                                      value="<?= $data_c["fake_payment"]; ?>"/></td>
            </tr>
            <tr>
                <td><b>Накрутка (пополнено):</b></td>
                <td width="150" align="center"><input type="text" name="fake_insert"
                                                      value="<?= $data_c["fake_insert"]; ?>"/></td>
            </tr>
            <tr>
                <td><b>Старт проекта (UNIX):</b></td>
                <td width="150" align="center"><input type="text" name="site_start"
                                                      value="<?= $data_c["site_start"]; ?>"/></td>
            </tr>

            <tr>
                <td colspan="2" align="center">-------------------Продажа рефералов системе-------------------</td>
            </tr>

            <tr>
                <td><b>Продажа рефералов:</b></td>
                <td width="150" align="center">
                    <select name="refsale_enable" style="width: 100%;">
                        <option value="0" <? if($data_c["refsale_enable"] == 0) echo 'selected'; ?>>ВЫКЛ.</option>
                        <option value="1" <? if($data_c["refsale_enable"] == 1) echo 'selected'; ?>>ВКЛ.</option>
                    </select>
                </td>
            </tr>

            <tr>
                <td><b>Счет для зачисления вознаграждения:</b></td>
                <td width="150" align="center">
                    <select name="refsale_mtype" style="width: 100%;">
                        <? foreach ($paymeny_money_type as $ls_n => $ls_name) { ?>
                            <option <? if ($data_c["refsale_mtype"] == $ls_n) echo 'selected' ?>
                                value="<?= $ls_n ?>"><?= $ls_name ?></option>
                        <? } ?>
                    </select>
                </td>
            </tr>

            <tr>
                <td><b>Цена за реферала:</b></td>
                <td width="150" align="center"><input type="text" name="refsale_price"
                                                      value="<?= $data_c["refsale_price"]; ?>"/></td>
            </tr>

            <tr>
                <td colspan="2" align="center">-------------------Поощрения реферальных визитов-------------------</td>
            </tr>

            <tr>
                <td><b>Стоимость одного визита (руб.):</b></td>
                <td width="150" align="center"><input type="text" name="visits_money"
                                                      value="<?= $data_c["visits_money"]; ?>"/></td>
            </tr>

            <tr>
                <td colspan="2" align="center">-------------------Реферальная система и Cash Points-------------------</td>
            </tr>

            <tr>
                <td><b>Тип реферальной системы:</b></td>
                <td width="150" align="center">
                    <select name="referals_level" style="width: 100%;">
                        <? for ($i = 1; $i <= 6; $i++) { ?>
                            <option <? if ($data_c["referals_level"] == $i) echo 'selected' ?>
                                value="<?= $i ?>">до <?= $i ?>-го уровня
                            </option>
                        <? } ?>
                    </select>
                </td>
            </tr>

            <?if($data_c["payment_limit"] == 1){?>
                <tr style="background: #ecdaff;">
                    <td><b>Процент Cash Points пользователю:</b></td>
                    <td width="150" align="center"><input type="text" name="user_cash_points"
                                                          value="<?= $data_c["user_cash_points"]; ?>"/></td>
                </tr>
                <tr style="background: #cb8851">
                    <td><b>Процент Cash Points активным пользователям:</b></td>
                    <td width="150" align="center"><input type="text" name="persent_cp2active"
                                                          value="<?= $data_c["persent_cp2active"]; ?>"/></td>
                </tr>
                <tr style="background: #cb8851">
                    <td><b>Срок активности пользователя (час):</b></td>
                    <td width="150" align="center">
                        <select name="time_cp2active" style="width: 100%;">
                            <? for ($i = 1; $i <= 100; $i++) { ?>
                                <option <? if ($data_c["time_cp2active"] == $i) echo 'selected' ?>
                                    value="<?= $i ?>"><?= $i ?>
                                </option>
                            <? } ?>
                        </select>
                    </td>
                </tr>
            <?}?>

            <?
            $color_array = array(
                1 => '#efefef',
                2 => '#bfbfbf',
                3 => '#efefef',
                4 => '#bfbfbf',
                5 => '#efefef',
                6 => '#bfbfbf'
            );
            ?>

            <? for ($i = 1; $i <= $data_c["referals_level"]; $i++) { ?>
                <tr style="background:<?=$color_array[$i]?>">
                    <td><b>Рефералы <?= $i ?>-го уровня (% серебро):</b></td>
                    <td width="150" align="center"><input type="text" name="ref<?= $i ?>_pers"
                                                          value="<?= $data_c["ref{$i}_pers"]; ?>"/></td>
                </tr>
                <?if($data_c["payment_limit"] == 1){?>
                    <tr style="background:<?=$color_array[$i]?>">
                        <td><b>Рефералы <?= $i ?>-го уровня (% Cash Point):</b></td>
                        <td width="150" align="center"><input type="text" name="ref<?= $i ?>_cp"
                                                              value="<?= $data_c["ref{$i}_cp"]; ?>"/></td>
                    </tr>
                <?}?>

            <? } ?>

            <tr>
                <td colspan="2" align="center">-------------------Бонусы-------------------</td>
            </tr>
            <tr>
                <td><b>Подарок при регистрации (серебро):</b></td>
                <td width="150" align="center"><input type="text" name="regbonus"
                                                      value="<?= $data_c["regbonus"]; ?>"/></td>
            </tr>

            <tr>
                <td><b>При первом пополнении (% от пополнения):</b></td>
                <td width="150" align="center"><input type="text" name="first_payment_pers"
                                                      value="<?= $data_c["first_payment_pers"]; ?>"/></td>
            </tr>

            <tr>
                <td><b>При пополнении через PAYEER (% от пополнения):</b></td>
                <td width="150" align="center"><input type="text" name="payeer_insert_bonus"
                                                      value="<?= $data_c["payeer_insert_bonus"]; ?>"/></td>
            </tr>
            <tr>
                <td><b>WMSET бонусы на сумму (% от суммы):</b></td>
                <td width="150" align="center"><input type="text" name="wmset_pers"
                                                      value="<?= $data_c["wmset_pers"]; ?>"/></td>
            </tr>
            <tr>
                <td><b>WMSET корректор увеличения (коэфициент за 1 руб.):</b></td>
                <td width="150" align="center"><input type="text" name="wmset_mult"
                                                      value="<?= $data_c["wmset_mult"]; ?>"/></td>
            </tr>
            <tr>
                <td><b>WMSET баннер (суммы пополнений для баннера в руб.):</b></td>
                <td width="150" align="center"><input type="text" name="wmset_sums"
                                                      value="<?= $data_c["wmset_sums"]; ?>"/></td>
            </tr>
            <tr>
                <td colspan="2">
                    <table width="100%" style="background: #e6e6e6;padding: 10px;">
                        <tr>
                            <td valign="top" width="50%" align="left"><b>Билеты на рулетку:</b></td>
                            <td valign="top" width="50%" align="right">
                                <table width="300" cellpadding="3" id="wheel_set">
                                    <tr>
                                        <td align="center">Сумма</td>
                                        <td align="center">Кол-во</td>
                                        <td align="center"></td>
                                    </tr>

                                    <?
                                    $ws = explode(';',$data_c["wheel_bonus"]);
                                    foreach($ws as $item){
                                        $data = explode(":",$item);
                                        if($data[0] <= 0 && $data[1] <= 0) break;
                                        ?>
                                        <tr style="background: #bfbfbf;">
                                            <td align="center"><input type="number" style="width: 80px;" class="money" name="wheel_bonus_sum[]" value="<?=$data[0]?>"></td>
                                            <td align="center">
                                                <input type="button" class="minus" value="-"/>
                                                <input type="number" style="width: 40px;" name="wheel_bonus_count[]" value="<?=$data[1]?>">
                                                <input type="button" class="plus" value="+"/>
                                            </td>
                                            <td align="center"><input type="button" onclick="$(this).parent().parent().remove()" class="delete" name="delete" value="X"/></td>
                                        </tr>
                                        <?
                                    }
                                    ?>

                                    <tr>
                                        <td colspan="3" align="center"><input type="button" name="add" value="Добавить"></td>
                                    </tr>
                                </table>
                                <script>

                                    var pole = '';

                                    pole += '<tr style="background: #bfbfbf;">';
                                    pole += '<td align="center"><input type="number" style="width: 80px;" class="money" name="wheel_bonus_sum[]" value="0"></td>';
                                    pole += '<td align="center">';
                                    pole += '<input type="button" class="minus" value="-"/>';
                                    pole += '<input type="number" style="width: 40px;" name="wheel_bonus_count[]" value="1">';
                                    pole += '<input type="button" class="plus" value="+"/>';
                                    pole += '</td>';
                                    pole += '<td align="center"><input type="button" onclick="$(this).parent().parent().remove()" class="delete" name="delete" value="X"/></td>';
                                    pole += '</tr>';

                                    $("#wheel_set input[name=add]").click(function () {
                                        var table=document.getElementById("wheel_set");
                                        var tr = $("#wheel_set tr").eq(table.rows.length - 2);
                                        tr.after(pole);
                                        plus2minus();
                                    });
                                </script>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr>
                <td colspan="2" align="center">-------------------Настройки курсов-------------------</td>
            </tr>
            <tr>
                <td><b>Стоимость 1 RUB (Серебром):</b></td>
                <td width="150" align="center"><input type="text" name="ser_per_wmr"
                                                      value="<?= $data_c["ser_per_wmr"]; ?>"/></td>
            </tr>

            <tr>
                <td><b>Стоимость 1 USD (Серебром):</b></td>
                <td width="150" align="center"><input type="text" name="ser_per_wmz"
                                                      value="<?= $data_c["ser_per_wmz"]; ?>"/></td>
            </tr>

            <tr>
                <td><b>Стоимость 1 EUR (Серебром):</b></td>
                <td width="150" align="center"><input type="text" name="ser_per_wme"
                                                      value="<?= $data_c["ser_per_wme"]; ?>"/></td>
            </tr>

            <tr>
                <td><b>Прибавлять % при обмене (От 1 до 99):</b></td>
                <td width="150" align="center"><input type="text" name="percent_swap"
                                                      value="<?= $data_c["percent_swap"]; ?>"/></td>
            </tr>

            <tr>
                <td><b>% серебра на вывод при продаже (от 1 до 99):</b><BR/></td>
                <td width="150" align="center"><input type="text" name="percent_sell"
                                                      value="<?= $data_c["percent_sell"]; ?>"/></td>
            </tr>

            <tr>
                <td><b>Сколько фруктов = 1 серебра:</b></td>
                <td width="150" align="center"><input type="text" name="items_per_coin"
                                                      value="<?= $data_c["items_per_coin"]; ?>"/></td>
            </tr>
            <tr>
                <td><b>Сколько Накопительный бонус:</b></td>
                <td width="150" align="center"><input type="text" name="percent_back"
                                                      value="<?= $data_c["percent_back"]; ?>"/></td>
            </tr>
            <tr>
                <td colspan="2" align="center">-------------------Плодовитость-------------------</td>
            </tr>
            <tr>
                <td><b>Плодородность в час (Частный дом) (мин 6):</b></td>
                <td width="150" align="center"><input type="text" name="a_in_h" value="<?= $data_c["a_in_h"]; ?>"/></td>
            </tr>

            <tr>
                <td><b>Плодородность в час (Жилой дом) (мин 6):</b></td>
                <td width="150" align="center"><input type="text" name="b_in_h" value="<?= $data_c["b_in_h"]; ?>"/></td>
            </tr>

            <tr>
                <td><b>Плодородность в час (Супермаркет) (мин 6):</b></td>
                <td width="150" align="center"><input type="text" name="c_in_h" value="<?= $data_c["c_in_h"]; ?>"/></td>
            </tr>

            <tr>
                <td><b>Плодородность в час (Завод) (мин 6):</b></td>
                <td width="150" align="center"><input type="text" name="d_in_h" value="<?= $data_c["d_in_h"]; ?>"/></td>
            </tr>

            <tr>
                <td><b>Плодородность в час (Банк) (мин 6):</b></td>
                <td width="150" align="center"><input type="text" name="e_in_h" value="<?= $data_c["e_in_h"]; ?>"/></td>
            </tr>
            <tr>
                <td colspan="2" align="center">-------------------Стоимость инвентаря-------------------</td>
            </tr>

            <tr>
                <td><b>Стоимость частного дома (серебро):</b></td>
                <td width="150" align="center"><input type="text" name="amount_a_t"
                                                      value="<?= $data_c["amount_a_t"]; ?>"/></td>
            </tr>

            <tr>
                <td><b>Стоимость жилого дома (серебро):</b></td>
                <td width="150" align="center"><input type="text" name="amount_b_t"
                                                      value="<?= $data_c["amount_b_t"]; ?>"/></td>
            </tr>

            <tr>
                <td><b>Стоимость супермаркета (серебро):</b></td>
                <td width="150" align="center"><input type="text" name="amount_c_t"
                                                      value="<?= $data_c["amount_c_t"]; ?>"/></td>
            </tr>

            <tr>
                <td><b>Стоимость завода (серебро):</b></td>
                <td width="150" align="center"><input type="text" name="amount_d_t"
                                                      value="<?= $data_c["amount_d_t"]; ?>"/></td>
            </tr>

            <tr>
                <td><b>Стоимость банка (серебро):</b></td>
                <td width="150" align="center"><input type="text" name="amount_e_t"
                                                      value="<?= $data_c["amount_e_t"]; ?>"/></td>
            </tr>

            <tr>
                <td colspan="2" align="center">-------------------Продажа инвентаря-------------------</td>
            </tr>

            <tr>
                <td><b>Продажа клубов:</b></td>
                <td width="150" align="center">
                    <select name="item_sale" style="width: 100%;">
                        <option value="0" <? if($data_c["item_sale"] == 0) echo 'selected'; ?>>ВЫКЛ.</option>
                        <option value="1" <? if($data_c["item_sale"] == 1) echo 'selected'; ?>>ВКЛ.</option>
                    </select>
                </td>
            </tr>

            <tr>
                <td><b>Продажа доступна при пополнении (руб.):</b></td>
                <td width="150" align="center"><input type="text" name="sale_insert_ok"
                                                      value="<?= $data_c["sale_insert_ok"]; ?>"/></td>
            </tr>

            <tr>
                <td><b>Комиссия (% от стоимости):</b></td>
                <td width="150" align="center"><input type="text" name="sale_persent"
                                                      value="<?= $data_c["sale_persent"]; ?>"/></td>
            </tr>

            <tr>
                <td colspan="2" align="center">-------------------Серфинг-------------------</td>
            </tr>

            <tr>
                <td><b>Серфинг:</b></td>
                <td width="150" align="center">
                    <select name="serf_enable" style="width: 100%;">
                        <option value="0" <? if($data_c["serf_enable"] == 0) echo 'selected'; ?>>ВЫКЛ.</option>
                        <option value="1" <? if($data_c["serf_enable"] == 1) echo 'selected'; ?>>ВКЛ.</option>
                    </select>
                </td>
            </tr>

            <tr>
                <td><b>Минимальная стоимость просмотра:</b></td>
                <td width="150" align="center"><input type="text" name="serf_price"
                                                      value="<?= $data_c["serf_price"]; ?>"/></td>
            </tr>

            <tr>
                <td><b>Стоимость таймера:</b></td>
                <td width="150" align="center"><input type="text" name="serf_price_timer"
                                                      value="<?= $data_c["serf_price_timer"]; ?>"/></td>
            </tr>

            <tr>
                <td><b>Стоимость последующего перехода на сайт:</b></td>
                <td width="150" align="center"><input type="text" name="serf_price_move"
                                                      value="<?= $data_c["serf_price_move"]; ?>"/></td>
            </tr>

            <tr>
                <td><b>Стоимость выделения ссылки:</b></td>
                <td width="150" align="center"><input type="text" name="serf_price_high"
                                                      value="<?= $data_c["serf_price_high"]; ?>"/></td>
            </tr>

            <tr>
                <td><b>Стоимость таргетинга:</b></td>
                <td width="150" align="center"><input type="text" name="serf_price_target"
                                                      value="<?= $data_c["serf_price_target"]; ?>"/></td>
            </tr>

            <tr>
                <td><b>Счет для зачисления вознаграждения:</b></td>
                <td width="150" align="center">
                    <select name="serf_money_type" style="width: 100%;">
                        <? foreach ($paymeny_money_type as $ls_n => $ls_name) { ?>
                            <option <? if ($data_c["serf_money_type"] == $ls_n) echo 'selected' ?>
                                value="<?= $ls_n ?>"><?= $ls_name ?></option>
                        <? } ?>
                    </select>
                </td>
            </tr>

            <tr>
                <td><b>Корректировка вознаграждения (+/- % от вознаграждения):</b></td>
                <td width="150" align="center"><input type="text" name="serf_bonus"
                                                      value="<?= $data_c["serf_bonus"]; ?>"/></td>
            </tr>

            <tr>
                <td><b>Реферальная программа за 1й уровень (% от дохода):</b></td>
                <td width="150" align="center"><input type="text" name="serf_ref1_persent"
                                                      value="<?= $data_c["serf_ref1_persent"]; ?>"/></td>
            </tr>

            <tr>
                <td><b>БЕЗЛИМИТ СУТКИ (кол-во кликов, 0 - отключено)</b></td>
                <td width="150" align="center"><input type="text" name="serf_unlim1d"
                                                      value="<?= $data_c["serf_unlim1d"]; ?>"/></td>
            </tr>

            <tr>
                <td><b>БЕЗЛИМИТ НЕДЕЛЯ (кол-во кликов, 0 - отключено):</b></td>
                <td width="150" align="center"><input type="text" name="serf_unlim1w"
                                                      value="<?= $data_c["serf_unlim1w"]; ?>"/></td>
            </tr>

            <tr>
                <td><b>БЕЗЛИМИТ МЕСЯЦ (кол-во кликов, 0 - отключено):</b></td>
                <td width="150" align="center"><input type="text" name="serf_unlim1m"
                                                      value="<?= $data_c["serf_unlim1m"]; ?>"/></td>
            </tr>

            <tr>
                <td><b>VIP:</b></td>
                <td width="150" align="center"><input type="text" name="serf_price_vip"
                                                      value="<?= $data_c["serf_price_vip"]; ?>"/></td>
            </tr>

            <tr>
                <td><b>Единоразовый просмотр:</b></td>
                <td width="150" align="center"><input type="text" name="serf_price_oneuser"
                                                      value="<?= $data_c["serf_price_oneuser"]; ?>"/></td>
            </tr>

            <tr>
                <td colspan="2" align="center">-------------------ФЭЙКИ-------------------</td>
            </tr>

            <tr style="background: #dff5ff;">
                <td><b>Накрутка пользователей:</b></td>
                <td width="150" align="center">
                    <select name="fakecontrol_users" style="width: 100%;">
                        <option value="0" <? if($data_c["fakecontrol_users"] == 0) echo 'selected'; ?>>ВЫКЛ.</option>
                        <option value="1" <? if($data_c["fakecontrol_users"] == 1) echo 'selected'; ?>>ВКЛ.</option>
                    </select>
                </td>
            </tr>

            <tr style="background: #dff5ff;" class="fakecontrol_users">
                <td><b>Мин. кол-во регистраций в минуту:</b></td>
                <td width="150" align="center"><input type="text" name="fc_users_count_min"
                                                      value="<?= $data_c["fc_users_count_min"]; ?>"/></td>
            </tr>

            <tr style="background: #dff5ff;" class="fakecontrol_users">
                <td><b>Макс. кол-во регистраций в минуту:</b></td>
                <td width="150" align="center"><input type="text" name="fc_users_count_max"
                                                      value="<?= $data_c["fc_users_count_max"]; ?>"/></td>
            </tr>

            <tr style="background: #b1efe9">
                <td><b>Накрутка выплат:</b></td>
                <td width="150" align="center">
                    <select name="fakecontrol_payment" style="width: 100%;">
                        <option value="0" <? if($data_c["fakecontrol_payment"] == 0) echo 'selected'; ?>>ВЫКЛ.</option>
                        <option value="1" <? if($data_c["fakecontrol_payment"] == 1) echo 'selected'; ?>>ВКЛ.</option>
                    </select>
                </td>
            </tr>

            <tr style="background: #b1efe9" class="fakecontrol_payment">
                <td><b>Мин. кол-во выплат в минуту:</b></td>
                <td width="150" align="center"><input type="text" name="fc_payment_count_min"
                                                      value="<?= $data_c["fc_payment_count_min"]; ?>"/></td>
            </tr>

            <tr style="background: #b1efe9" class="fakecontrol_payment">
                <td><b>Макс. кол-во выплат в минуту:</b></td>
                <td width="150" align="center"><input type="text" name="fc_payment_count_max"
                                                      value="<?= $data_c["fc_payment_count_max"]; ?>"/></td>
            </tr>

            <tr style="background: #b1efe9" class="fakecontrol_payment">
                <td><b>Накрутка лотереи (вероятность) 0 - 10:</b></td>
                <td width="150" align="center"><input type="text" name="fc_loto"
                                                      value="<?= $data_c["fc_loto"]; ?>"/></td>
            </tr>
            <tr style="background: #b1efe9" class="fakecontrol_payment">
                <td><b>Накрутка рулетки (вероятность) 0 - 10:</b></td>
                <td width="150" align="center"><input type="text" name="fc_wheel"
                                                      value="<?= $data_c["fc_wheel"]; ?>"/></td>
            </tr>
            <tr style="background: #b1efe9" class="fakecontrol_payment">
                <td><b>Накрутка колесо фортуны [покупки] (вероятность) 0 - 10:</b></td>
                <td width="150" align="center"><input type="text" name="fc_fwheel1"
                                                      value="<?= $data_c["fc_fwheel1"]; ?>"/></td>
            </tr>
            <tr style="background: #b1efe9" class="fakecontrol_payment">
                <td><b>Накрутка колесо фортуны [вывод] (вероятность) 0 - 10:</b></td>
                <td width="150" align="center"><input type="text" name="fc_fwheel2"
                                                      value="<?= $data_c["fc_fwheel2"]; ?>"/></td>
            </tr>

            <tr  style="background: #e9efb1">
                <td><b>Накрутка пополнений:</b></td>
                <td width="150" align="center">
                    <select name="fakecontrol_insert" style="width: 100%;">
                        <option value="0" <? if($data_c["fakecontrol_insert"] == 0) echo 'selected'; ?>>ВЫКЛ.</option>
                        <option value="1" <? if($data_c["fakecontrol_insert"] == 1) echo 'selected'; ?>>ВКЛ.</option>
                    </select>
                </td>
            </tr>

            <tr style="background: #e9efb1" class="fakecontrol_insert">
                <td><b>Кол-во пополнений в минуту:</b></td>
                <td width="150" align="center"><input type="text" name="fc_insert_count"
                                                      value="<?= $data_c["fc_insert_count"]; ?>"/></td>
            </tr>
            <tr style="background: #e9efb1" class="fakecontrol_insert">
                <td><b>Макс сумм пополнения (руб.):</b></td>
                <td width="150" align="center"><input type="text" name="fc_insert_max"
                                                      value="<?= $data_c["fc_insert_max"]; ?>"/></td>
            </tr>

            <tr>
                <td colspan="2" align="center"><br><br><input type="submit" name="buy" value="Сохранить"/></form></td>
    </tr>

    </table>

</div>
<div class="clr"></div>

<script>
    function fakecontrol(type){
        if($("select[name=fakecontrol_" + type + "]").val() == 1){
            $(".fakecontrol_" + type).fadeIn();
        }else{
            $(".fakecontrol_" + type).fadeOut();
        }
    }

    $("select").change(function(){
        fakecontrol('insert');
        fakecontrol('payment');
        fakecontrol('users');
    });

    fakecontrol('insert');
    fakecontrol('payment');
    fakecontrol('users');

</script>