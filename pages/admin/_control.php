<?PHP

$_OPTIMIZATION["title"] = "Ручные выплаты";
$_OPTIMIZATION["description"] = "Ручные выплаты";
$_OPTIMIZATION["keywords"] = "Ручные выплаты";

$strong_array = array(
	0 => 'Замечаний нет',
	1 => 'Вывод части серебра',
	2 => 'Быстрая окупаемость',
	3 => 'Сбор сырья больше 24ч назад',
	4 => 'Смена кошелька',
	5 => 'Частые выплаты'
);

include("./_geolocation/SxGeo.php");
$SxGeo = new SxGeo('./_geolocation/SxGeoCity.dat');

function geodata($ip){
	global $SxGeo;
	$geo_date = $SxGeo->getCityFull($ip);
	$country = $geo_date['country']['name_ru'];
	$city = $geo_date['city']['name_ru'];
	return $country.' / '.$city;
}

$db->Query("SELECT COUNT(id) FROM db_payments WHERE status = 0 AND manual = 1");
$mp1 = $db->FetchRow();
if($mp1 > 0) $mp1 = "({$mp1})"; else $mp1 = "";
$db->Query("SELECT COUNT(id) FROM db_payment_ext WHERE status = 0 AND manual = 1");
$mp2 = $db->FetchRow();
if($mp2 > 0) $mp2 = "({$mp2})"; else $mp2 = "";

?>
<div class="s-bk-lf">
	<div class="acc-title">Ручные выплаты</div>
</div>
<div class="silver-bk">

	<div>
		<center>
			<a href="/?menu=admin4ik&sel=control&pt=1">Обычные выплаты <?=$mp1;?></a> || <a href="/?menu=admin4ik&sel=control&pt=2">Экстренные выплаты <?=$mp2;?></a>
		</center>
	</div>
	<br>
	<?
	if($_GET['pt'] == 1 || !isset($_GET['pt'])){

		$db->Query("SELECT 
				*, 
				ps1.id AS id, 
				(SELECT GROUP_CONCAT(INET_NTOA(ip)) FROM db_login WHERE db_login.user_id = ps1.user_id) ips, 
				(SELECT GROUP_CONCAT(purse) FROM db_payments AS ps2 WHERE ps2.user_id = ps1.user_id AND ps2.status = 3) purses 
				FROM db_payments AS ps1 LEFT JOIN db_payment_system ON db_payment_system.pid = ps1.pay_sys_id LEFT JOIN db_users_b ON db_users_b.id = ps1.user_id WHERE manual = 1 AND status = 0");

		if($db->NumRows() > 0){
			?>
			<table cellpadding='3' cellspacing='0' border='0' bordercolor='#336633' align='center' width="99%">
				<tr bgcolor="#efefef">
					<td style="border: 1px dashed #db8;" align="center" class="m-tb"><a style="color: white" >ID</a></td>
					<td style="border: 1px dashed #db8;" align="center" class="m-tb"><a style="color: white" >Дата заказа</a></td>
					<td style="border: 1px dashed #db8;" align="center" class="m-tb"><a style="color: white" >Кошелек</a></td>
					<td style="border: 1px dashed #db8;" align="center" class="m-tb"><a style="color: white" >Сумма</a></td>
					<td style="border: 1px dashed #db8;" align="center" class="m-tb"><a style="color: white" >Юзер</a></td>
					<td style="border: 1px dashed #db8;" align="center" class="m-tb"><a style="color: white" >Юзеринфо</a></td>
					<td style="border: 1px dashed #db8;" align="center" class="m-tb"><a style="color: white" >Замечания</a></td>
					<td style="border: 1px dashed #db8;" align="center" class="m-tb">Параметры</td>
				</tr>
				<?PHP

				while ($data = $db->FetchArray()) {
					$user_ips = '';
					$n=0;
					$temp_array = explode(',',$data['ips']);
					for($i=count($temp_array)-1;$i>=0;$i--){
						$user_ips .= geodata($temp_array[$i])." == [{$temp_array[$i]}]\x0D\x0A";
						if($n<=10){
							$n++;
						}else{
							break;
						}
					}
					$n=0;
					$user_purses = '';
					$temp_array = explode(',',$data['purses']);
					for($i=count($temp_array);$i>=0;$i--){
						$user_purses .= "{$temp_array[$i]}\x0D\x0A";
						if($n<=10){
							$n++;
						}else{
							break;
						}
					}
					?>
					<tr class="htt">
						<td style="border: 1px dashed #db8;" align="center"><?=$data['id']?></td>
						<td style="border: 1px dashed #db8;" align="center"><?=$func->time2word($data['date_add'])?></td>
						<td style="border: 1px dashed #db8;" align="left" title="<?=$user_purses?>"><img class="psimg" src="<?= $data['img']; ?>"><?=$data["purse"]; ?></td>
						<td style="border: 1px dashed #db8;"	align="center"><?= sprintf("%.2f", $data["sum"]); ?> <?= $data["valuta"]; ?></td>
						<td style="border: 1px dashed #db8;" align="center" title="<?=$user_ips?>"><a target="_blank" href="/?menu=admin4ik&sel=users&edit=<?=$data['user_id']?>"><?=$data['user']?></a></td>
						<td style="border: 1px dashed #db8;" align="center" title="<?=$user_geo?>">
							<font color="green"><?=number_format($data['insert_sum'],2,'.',"'");?></font> / <font color="red"><?=number_format($data['payment_sum'],2,'.',"'");?></font>
						</td>
						<td style="border: 1px dashed #db8;" align="center"><?=$strong_array[$data['strong_id']]?></td>
						<td style="border: 1px dashed #db8;" align="center">
							<form class="moder_form" id="pm-<?=$data['id']?>" action="/payments_control_v1.php" method="POST" style="display: inline-block;margin: 0;padding: 0;">
								<input type="hidden" name="user_id" value="<?=$data['user_id']?>">
								<input type="hidden" name="pt" value="1">
								<input type="hidden" name="id" value="<?=$data['id']?>">
								<input type="hidden" name="hash" value="<?=md5($data['user_id'].$data['id']);?>">
								<input type="button" name="no" value="X"/>
								<input type="button" name="yes" value="V"/>
							</form>
						</td>
					</tr>
					<?PHP
				}

				?>
			</table>

			<?
		}else{
			echo '<br/><center><b>Нет выплат</b></center><br/>';
		}
	}else if($_GET['pt'] == 2){

		$db->Query("SELECT 
				*, 
				ps1.id AS id, 
				(SELECT GROUP_CONCAT(INET_NTOA(ip)) FROM db_login WHERE db_login.user_id = ps1.user_id) ips, 
				(SELECT GROUP_CONCAT(purse) FROM db_payment_ext AS ps2 WHERE ps2.user_id = ps1.user_id AND ps2.status = 3) purses 
				FROM db_payment_ext AS ps1 LEFT JOIN db_payment_system ON db_payment_system.pid = 1136053 LEFT JOIN db_users_b ON db_users_b.id = ps1.user_id WHERE manual = 1 AND status = 0");

		if($db->NumRows() > 0){
			?>
			<table cellpadding='3' cellspacing='0' border='0' bordercolor='#336633' align='center' width="99%">
				<tr bgcolor="#efefef">
					<td style="border: 1px dashed #db8;" align="center" class="m-tb"><a style="color: white" >ID</a></td>
					<td style="border: 1px dashed #db8;" align="center" class="m-tb"><a style="color: white" >Дата заказа</a></td>
					<td style="border: 1px dashed #db8;" align="center" class="m-tb"><a style="color: white" >Кошелек</a></td>
					<td style="border: 1px dashed #db8;" align="center" class="m-tb"><a style="color: white" >Сумма</a></td>
					<td style="border: 1px dashed #db8;" align="center" class="m-tb"><a style="color: white" >Юзер</a></td>
					<td style="border: 1px dashed #db8;" align="center" class="m-tb"><a style="color: white" >Юзеринфо</a></td>
					<td style="border: 1px dashed #db8;" align="center" class="m-tb"><a style="color: white" >Замечания</a></td>
					<td style="border: 1px dashed #db8;" align="center" class="m-tb">Параметры</td>
				</tr>
				<?PHP
				while ($data = $db->FetchArray()) {
					$user_ips = '';
					$n=0;
					$temp_array = explode(',',$data['ips']);
					for($i=count($temp_array)-1;$i>=0;$i--){
						$user_ips .= geodata($temp_array[$i])." == [{$temp_array[$i]}]\x0D\x0A";
						if($n<=10){
							$n++;
						}else{
							break;
						}
					}
					$n=0;
					$user_purses = '';
					$temp_array = explode(',',$data['purses']);
					for($i=count($temp_array);$i>=0;$i--){
						$user_purses .= "{$temp_array[$i]}\x0D\x0A";
						if($n<=10){
							$n++;
						}else{
							break;
						}
					}
					?>
					<tr class="htt">
						<td style="border: 1px dashed #db8;" align="center"><?=$data['id']?></td>
						<td style="border: 1px dashed #db8;" align="center"><?=$func->time2word($data['date_add'])?></td>
						<td style="border: 1px dashed #db8;" align="left" title="<?=$user_purses?>"><img class="psimg" src="<?= $data['img']; ?>"><?=$data["purse"]; ?></td>
						<td style="border: 1px dashed #db8;"	align="center"><?= sprintf("%.2f", $data["sum"]); ?> <?= $data["valuta"]; ?></td>
						<td style="border: 1px dashed #db8;" align="center" title="<?=$user_ips?>"><a target="_blank" href="/?menu=admin4ik&sel=users&edit=<?=$data['user_id']?>"><?=$data['user']?></a></td>
						<td style="border: 1px dashed #db8;" align="center" title="<?=$user_geo?>">
							<font color="green"><?=number_format($data['insert_sum'],2,'.',"'");?></font> / <font color="red"><?=number_format($data['payment_sum'],2,'.',"'");?></font>
						</td>
						<td style="border: 1px dashed #db8;" align="center"><?=$strong_array[$data['strong_id']]?></td>
						<td style="border: 1px dashed #db8;" align="center">
							<form class="moder_form" id="pm-<?=$data['id']?>" action="/payments_control_v1.php" method="POST" style="display: inline-block;margin: 0;padding: 0;">
								<input type="hidden" name="user_id" value="<?=$data['user_id']?>">
								<input type="hidden" name="pt" value="2">
								<input type="hidden" name="id" value="<?=$data['id']?>">
								<input type="hidden" name="hash" value="<?=md5($data['user_id'].$data['id']);?>">
								<input type="button" name="no" value="X"/>
								<input type="button" name="yes" value="V"/>
							</form>
						</td>
					</tr>
					<?PHP
				}

				?>
			</table>
			<?
		}else{
			echo '<br/><center><b>Нет выплат</b></center><br/>';
		}


	}
	?>

</div>
<script>

	$('.moder_form input[type=button]').click(function(){
		var $f = $(this).parent('form');
		var url = $f.attr('action');
		var id 	= $f.attr('id');
		var comm = $(this).attr('name');
		var data = $f.serialize() + '&' + comm + '=1';
		$f.empty();
		$f.append('<img width="25" src="/img/loading2.gif"/>');
		jQuery.ajax({
			url:     url,
			type:     "POST",
			dataType: "html",
			data: data,
			success: function(response) {
				$f.empty();
				if(response == 'OK'){
					if(comm == 'yes'){
						$f.append('<font color="green"><b>Одобрена!</b></font>');
					}else{
						$f.append('<font color="red"><b>Отменена</b></font>');
					}
				}else{
					$f.append('<font color="red"><b>Ошибка (' + response + ')</b></font>');
				}

			},
			error: function(response) {
				$f.empty();
				$f.append('<font color="red"><b>Ошибка отправки данных!</b></font>');
			}
		});
		return false;
	});

</script>