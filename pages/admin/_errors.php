<div class="s-bk-lf">
	<div class="acc-title">Ошибки</div>
</div>
<div class="silver-bk"><div class="clr"></div>	
<?PHP

$db->Query("SELECT * FROM db_errors ORDER BY id DESC");

if($db->NumRows() > 0){

?>
<table cellpadding='3' cellspacing='0' border='0' bordercolor='#336633' align='center' width="99%">
  <tr bgcolor="#efefef">
    <td style="border: 1px dashed #db8;" align="center" width="50" class="m-tb">ID</td>
    <td style="border: 1px dashed #db8;" align="center" class="m-tb">Дата</td>
    <td style="border: 1px dashed #db8;" align="center" class="m-tb">Пользователь</td>
    <td style="border: 1px dashed #db8;" align="center" width="75" class="m-tb">Страница</td>
	<td style="border: 1px dashed #db8;" align="center" width="75" class="m-tb">Текст ошибки</td>
  </tr>


<?PHP

	while($data = $db->FetchArray()){
	
	?>
	<tr class="htt">
    <td style="border: 1px dashed #db8;" align="center" width="50"><?=$data["id"]; ?></td>
    <td style="border: 1px dashed #db8;" align="center"><?=$func->time2word($data['date_add'])?></td>
    <td style="border: 1px dashed #db8;" align="center"><?=$data["user"]; ?></td>
    <td style="border: 1px dashed #db8;" align="center" width="75"><?=$data["page"]; ?></td>
	<td style="border: 1px dashed #db8;" align="center" width="75"><?=$data["error"]; ?></td>
  	</tr>
	<?PHP
	
	}

?>

</table>
<BR />
<?PHP

}else echo "<center><b>Записей нет</b></center><BR />";
?>
</div>
<div class="clr"></div>	