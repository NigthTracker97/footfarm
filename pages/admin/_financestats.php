<?php
$c_date = date("Ymd", time());
$c_date_begin = strtotime($c_date . " 00:00:00");
$online = time() - 60 * 60 * 1;

#ini_set('display_errors',1);
#ini_set('error_reporting',2047);

?>

<div class="s-bk-lf">
    <div class="acc-title">Финансовая статистика</div>
</div>
<div class="silver-bk">
    <div class="clr"></div>
    <?PHP

    $db->Query("SELECT ps FROM db_insert_money GROUP BY ps");
    $ps_array = array();
    while($d = $db->FetchAssoc()){
        $ps_array[] = $d['ps'];
    }

    $db->Query("SELECT * FROM db_insert_money ORDER BY id DESC");

    $days_money = array();
    $days_allmoney = array();

    if($db->NumRows() > 0){

        while($data = $db->FetchArray()){
            $index = date("d.m.Y", $data["date_add"]);

            foreach($ps_array as $ps){
                if($data['ps'] == $ps){
                    $days_money[$index][$ps] = (isset($days_money[$index][$ps])) ? $days_money[$index][$ps] + $data["money"] : $data["money"];
                    $days_allmoney[$index] = (isset($days_allmoney[$index])) ? $days_allmoney[$index] + $data["money"] : $data["money"];
                }
            }

        }

        $db->Query("SELECT * FROM db_payments  WHERE (status = 3 OR status = 1) ORDER BY id DESC");

        $days_money_payments = array();
        $days_payments = array();

        if ($db->NumRows() > 0) {

            while ($data = $db->FetchArray()) {
                $index = date("d.m.Y", $data["date_add"]);

                $days_money_payments[$index] = (isset($days_money_payments[$index])) ? $days_money_payments[$index] + $data["sum"] : $data["sum"];

            }
        }

        $db->Query("SELECT * FROM db_payment_ext  WHERE status = 3 ORDER BY id DESC");

        $days_money_payments2 = array();
        $days_payments2 = array();

        if ($db->NumRows() > 0) {

            while ($data = $db->FetchArray()) {
                $index = date("d.m.Y", $data["date_add"]);

                $days_money_payments2[$index] = (isset($days_money_payments2[$index])) ? $days_money_payments2[$index] + $data["sum"] : $data["sum"];

            }
        }

        $db->Query("SELECT * FROM db_payments_admin ORDER BY id DESC");

        $days_money_payments3 = array();
        $days_payments3 = array();

        if ($db->NumRows() > 0) {

            while ($data = $db->FetchArray()) {
                $index = date("d.m.Y", $data["date_add"]);

                $days_money_payments3[$index] = (isset($days_money_payments3[$index])) ? $days_money_payments3[$index] + $data["sum"] : $data["sum"];

            }
        }

        # Вывод
        if(count($days_money) > 0){

            ?>
            <table cellpadding='3' cellspacing='0' border='0' bordercolor='#336633' align='center' width="99%" style="font-size: 9pt;table-layout: fixed">
                <thead style="font-size: 8pt">
                <tr bgcolor="#efefef">
                    <td align="center" style="border: 1px dashed #db8;" class="m-tb" rowspan="2">Дата</td>
                    <td align="center" style="border: 1px dashed #db8;" class="m-tb" colspan="<?=count($ps_array)?>">Пополнения</td>
                    <td align="center" style="border: 1px dashed #db8;" class="m-tb" colspan="3">Выплаты</td>
                    <td align="center" style="border: 1px dashed #db8;" class="m-tb" colspan="3">Движение</td>
                </tr>
                <tr bgcolor="#efefef">
                    <? foreach ($ps_array as $ps_name){ ?>
                        <td align="center" style="border: 1px dashed #db8;" class="m-tb"><?=$ps_name;?></td>
                    <? } ?>
                    <td align="center" style="border: 1px dashed #db8;" class="m-tb">Обычные</td>
                    <td align="center" style="border: 1px dashed #db8;" class="m-tb">Экстренные</td>
                    <td align="center" style="border: 1px dashed #db8;" class="m-tb">Админские</td>
                    <td align="center" style="border: 1px dashed #db8;" class="m-tb">ПРИХОД</td>
                    <td align="center" style="border: 1px dashed #db8;" class="m-tb">РАСХОД</td>
                    <td align="center" style="border: 1px dashed #db8;" class="m-tb">ОСТАТОК</td>
                </tr>
                </thead>
                <?PHP

                $ps_summ = array();

                foreach($days_allmoney as $date => $insert){

                    $payments1 = $days_money_payments[$date];
                    if($payments1 == '') $payments1 = 0;

                    $payments2 = $days_money_payments2[$date];
                    if($payments2 == '') $payments2 = 0;

                    $payments3 = $days_money_payments3[$date];
                    if($payments3 == '') $payments3 = 0;

                    $insert = floatval($insert);

                    $ost = $insert - ($payments1 + $payments2 + $payments3);
                    if($ost > 0) $ost_color = "green"; else $ost_color = "red";

                    foreach ($ps_array as $ps_name){
                        if($days_money[$date][$ps_name] == '') $days_money[$date][$ps_name] = 0;
                        $ps_summ[$ps_name] += $days_money[$date][$ps_name];
                        $ins_ps[$ps_name] = $days_money[$date][$ps_name] > 0 ? '+'.number_format($days_money[$date][$ps_name], 2, ',', ' ') : '-';
                    }

                    $pay_sum_all = $payments1 + $payments2 + $payments3;

                    $ins_sum += $insert;
                    $pay_sum1 += $payments1;
                    $pay_sum2 += $payments2;
                    $pay_sum3 += $payments3;
                    $pay_sumAll += $pay_sum_all;
                    $ost_sum += $ost;

                    $insert1 = $insert > 0 ? '+'.number_format($insert, 2, ',', ' ') : '-';
                    $payments1 = $payments1 > 0 ? '-'.number_format($payments1, 2, ',', ' ') : '-';
                    $payments2 = $payments2 > 0 ? '-'.number_format($payments2, 2, ',', ' ') : '-';
                    $payments3 = $payments3 > 0 ? '-'.number_format($payments3, 2, ',', ' ') : '-';
                    $pay_sum_all = $pay_sum_all > 0 ? '-'.number_format($pay_sum_all, 2, ',', ' ') : '-';

                    ?>
                    <tr class="htt">
                        <td style="border: 1px dashed #db8;" align="center"><b><?=$date; ?></b></td>
                        <? foreach ($ps_array as $ps_name){ ?>
                            <td style="border: 1px dashed #db8; color: green;" align="right"><?=$ins_ps[$ps_name];?></td>
                        <? } ?>
                        <td style="border: 1px dashed #db8; color:red;" align="right"><?=$payments1;?></td>
                        <td style="border: 1px dashed #db8; color:red;" align="right"><?=$payments2;?></td>
                        <td style="border: 1px dashed #db8; color:red;" align="right"><?=$payments3;?></td>
                        <td style="border: 1px dashed #db8; color: green; font-weight: bold;" align="right"><?=$insert1;?></td>
                        <td style="border: 1px dashed #db8; color: red; font-weight: bold;" align="right"><?=$pay_sum_all;?></td>
                        <td style="border: 1px dashed #db8; color:<?=$ost_color;?>" align="right"><b><?=number_format($ost, 2, ',', ' ') ?></b></td>
                    </tr>
                    <?PHP

                }

                ?>
                <tr class="htt">
                    <td class="m-tb" style="border: 1px dashed #db8;" align="right"><b>ИТОГО:</b></td>
                    <? foreach ($ps_array as $ps_name){ ?>
                        <td class="m-tb" style="border: 1px dashed #db8;" align="right"><b>+<?=number_format($ps_summ[$ps_name], 2, ',', ' ') ?></b></td>
                    <? } ?>
                    <td class="m-tb" style="border: 1px dashed #db8;" align="right"><b>-<?=number_format($pay_sum1, 2, ',', ' ') ?></b></td>
                    <td class="m-tb" style="border: 1px dashed #db8;" align="right"><b>-<?=number_format($pay_sum2, 2, ',', ' ') ?></b></td>
                    <td class="m-tb" style="border: 1px dashed #db8;" align="right"><b>-<?=number_format($pay_sum3, 2, ',', ' ') ?></b></td>
                    <td class="m-tb" style="border: 1px dashed #db8;" align="right"><b>+<?=number_format($ins_sum, 2, ',', ' ') ?></b></td>
                    <td class="m-tb" style="border: 1px dashed #db8;" align="right"><b>-<?=number_format($pay_sumAll, 2, ',', ' ') ?></b></td>
                    <td class="m-tb" style="border: 1px dashed #db8;" align="right"><b><?=number_format($ost_sum, 2, ',', ' ') ?></b></td>
                </tr>
            </table>
            <?PHP

        }

    }else echo "<center><b>Записей нет</b></center><BR />";

    ?>

</div>
<div class="clr"></div>