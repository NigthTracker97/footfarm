<div class="s-bk-lf">
    <div class="acc-title">Платежные системы (PAYEER)</div>
</div>
<div class="silver-bk">
    <div class="clr"></div>

    <?
	
	$db->Query('SET CHARACTER SET utf8');
	$db->Query("set names utf8");

    if(isset($_GET['psinfo'])){

        ?><center>
            <br>
            <a href="/?menu=admin4ik&sel=payment_system">Список платежных систем</a>
            <br>
        </center><?

        $payeer = new rfs_payeer($config->AccountNumber, $config->apiId, $config->apiKey);
        echo '<pre>';
        print_r($payeer->PaySystemData($_GET['psinfo']));
        echo '</pre>';

        return;

    }

    if (!isset($_GET['id']) && !isset($_GET['new'])) {
        ?>

        <table cellpadding='3' cellspacing='0' border='0' bordercolor='#336633' align='center' width="99%">
            <tr bgcolor="#efefef" class="trstyle">
                <td align="center" class="m-tb">Название</td>
                <td align="center" class="m-tb">Мин. сумма</td>
                <td align="center" class="m-tb">Макс. сумма</td>
                <td align="center" class="m-tb">Комиссия</td>
                <td align="center" class="m-tb">Валюта (О/П)</td>
                <td align="center" class="m-tb">Статус</td>
                <td align="center" class="m-tb">PAYEER ст.</td>
            </tr>

            <?php
            $db->Query("SELECT * FROM db_payment_system");
            if ($db->NumRows() > 0) {

                $payeer = new rfs_payeer($config->AccountNumber, $config->apiId, $config->apiKey);
                $psdata = $payeer->getPaySystems();
                //echo '<pre>';
                //print_r($payeer->getPaySystems());
                //echo '</pre>';

                while ($data = $db->FetchArray()) {
                    if(!empty($psdata['list'][$data['pid']])){
                        $payeer_enable = 1;
                        unset($psdata['list'][$data['pid']]);
                    }else{
                        $payeer_enable = 0;
                    }
                    ?>

                    <tr class="htt trstyle" id="<?= $data['id'] ?>" style="cursor: pointer">
                        <td><img class="psimg" src="<?= $data['img']; ?>"><b><?= $data['name'] ?></b></td>
                        <td><?= $data['min_sum'] ?></td>
                        <td><?= $data['max_sum'] ?></td>
                        <td><?= $data['commis'] ?>%<? if ($data['fix_commis'] > 0) echo " [+{$data['fix_commis']} {$data['cur_in']}]"; ?></td>
                        <td><?= $data['cur_in'] ?> / <?= $data['cur_out'] ?></td>
                        <td><? if ($data['enable'] == 1) echo '<font style="color: darkgreen;">Включена</font>'; else echo '<font style="color: red;">Выключена</font>'; ?></td>
                        <td><? if ($payeer_enable == 1) echo '<font style="color: darkgreen;">Доступна</font>'; else echo '<font style="color: red;">Не доступна</font>'; ?></td>
                    </tr>

                    <?

                }
            }

            foreach($psdata['list'] as $id=>$dataaa){
                ?>

                <tr class="htt2 trstyle" id="<?=$id?>" style="cursor: pointer;    background: gainsboro;">
                    <td><b><?= $dataaa['name'] ?></b></td>
                    <td>-</td>
                    <td>-</td>
                    <td><?= $dataaa['gate_commission']['RUB'] ?>%<? if ($dataaa['gate_commission_min']['RUB'] > 0) echo " [+{$dataaa['gate_commission_min']['RUB']} RUB]"; ?></td>
                    <td>- / -</td>
                    <td><font style="gray">нет в списке</font></td>
                    <td><font style="color: darkgreen;">Доступна</font></td>
                </tr>

                <?
            }

            ?>

        </table>

        <br/>
        <center><a href="/?menu=admin4ik&sel=payment_system&new">Добавить платежную ситсему</a></center>
        <br/>
<!--        <center><a href="/?menu=admin4ik&sel=payment_system&pslist">Список доступных платежны систем PAYEER</a></center>-->
<!--        <br/>-->

        <script>
            $(".htt").click(function () {
                var id = $(this).attr('id');
                window.location.replace("/?menu=admin4ik&sel=payment_system&id=" + id);
            });
            $(".htt2").click(function () {
                var id = $(this).attr('id');
                window.location.replace("/?menu=admin4ik&sel=payment_system&psinfo=" + id);
            });
        </script>

        <?

        if(isset($_GET['pslist'])){
            $payeer = new rfs_payeer($config->AccountNumber, $config->apiId, $config->apiKey);
            echo '<pre>';
            print_r($payeer->getPaySystems());
            echo '</pre>';
        }

    } else {

        $id = intval($_GET['id']);

        if(!empty($_POST)){
            if(isset($_POST['delete'])){
                $db->Query("DELETE FROM db_payment_system WHERE id = {$id}");
                echo '<center><b style="color:green">Платежная система удалена!</b></center>';
                header("Location: /?menu=admin4ik&sel=payment_system");
            }else{
                $query = '';
                foreach ($_POST as $name=>$value){
                    $value = mysql_escape_string($value);
                    if($query != '') $query .= ', ';
                    $query .= "`{$name}`='{$value}'";
                }
                if(isset($_GET['new'])){
                    $db->Query("INSERT INTO db_payment_system SET {$query}");
                    header("Location: /?menu=admin4ik&sel=payment_system&id=".$db->LastInsert());
                }else if(isset($_GET['id'])){
                    $db->Query("UPDATE db_payment_system SET {$query} WHERE id = {$id}");
                    echo '<center><b style="color:green">Данные сохранены!</b></center>';
                }
            }

        }

        $db->Query("SELECT * FROM db_payment_system WHERE id = {$id} LIMIT 1");
        if ($db->NumRows() == 0 && !isset($_GET['new'])) {
            ?>
            <center>Нет данных</center><?
        } else {

            $data = $db->FetchArray();
            ?>

            <form action="" method="POST">

            <center>

                <br>
                <a href="/?menu=admin4ik&sel=payment_system">Список платежных систем</a>
                <br>
                <br>

                <table width="500" border="0">
                    <tr>
                        <td>Название платежной системы</td>
                        <td width="150"><input type="text" name="name" value="<?=$data['name']?>"></td>
                    </tr>
                    <tr>
                        <td>Название страницы выплат</td>
                        <td width="150"><input type="text" name="title" value="<?=$data['title']?>"></td>
                    </tr>
                    <tr>
                        <td>Картинка платежной системы</td>
                        <td width="150"><input type="text" name="img" value="<?=$data['img']?>"></td>
                    </tr>
                    <tr>
                        <td>ID платежной системы</td>
                        <td width="150"><input type="text" name="pid" value="<?=$data['pid']?>"></td>
                    </tr>
                    <tr>
                        <td>Проверка кошелька(регулярка)</td>
                        <td width="150"><input type="text" name="check" value="<?=$data['check']?>"></td>
                    </tr>
                    <tr>
                        <td>Пример кошелька</td>
                        <td width="150"><input type="text" name="example" value="<?=$data['example']?>"></td>
                    </tr>
                    <tr>
                        <td>Проверка имени (регулярка)</td>
                        <td width="150"><input type="text" name="check_name" value="<?=$data['check_name']?>"></td>
                    </tr>
                    <tr>
                        <td>Пример имени</td>
                        <td width="150"><input type="text" name="example_name" value="<?=$data['example_name']?>"></td>
                    </tr>
                    <tr>
                        <td>Описание платежной системы (html)</td>
                        <td width="150"><textarea name="about" id="" cols="30" rows="10"><?=$data['about']?></textarea></td>
                    </tr>
                    <tr>
                        <td>Мин. сумма выплаты (руб.)</td>
                        <td width="150"><input type="text" name="min_sum" value="<?=$data['min_sum']?>"></td>
                    </tr>
                    <tr>
                        <td>Макс. сумма выплаты (руб.)</td>
                        <td width="150"><input type="text" name="max_sum" value="<?=$data['max_sum']?>"></td>
                    </tr>
                    <tr>
                        <td>Комиссия (админ)</td>
                        <td width="150"><input type="text" name="commis" value="<?=$data['commis']?>"></td>
                    </tr>
                    <tr>
                        <td>Комиссия фиксированная (руб.)</td>
                        <td width="150"><input type="text" name="fix_commis" value="<?=$data['fix_commis']?>"></td>
                    </tr>
                    <tr>
                        <td>Валюта - отправитель (RUB)</td>
                        <td width="150"><input type="text" name="cur_in" value="<?=$data['cur_in']?>"></td>
                    </tr>
                    <tr>
                        <td>Валюта - получатель (RUB)</td>
                        <td width="150"><input type="text" name="cur_out" value="<?=$data['cur_out']?>"></td>
                    </tr>
                    <tr>
                        <td>ВКЛ / ВЫКЛ</td>
                        <td width="150">
                            <select name="enable" style="width: 100%; padding: 1px 0px;">
                                <option value="0" <? if ($data["enable"] == 0) echo 'selected'; ?>>ВЫКЛ.</option>
                                <option value="1" <? if ($data["enable"] == 1) echo 'selected'; ?>>ВКЛ.</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center"><? if(!isset($_GET['new'])){ ?><!--<input type="submit" name="delete" value="Удалить">--><? } ?> <input type="submit" value="<? if(isset($_GET['new'])) echo 'Добавить систему'; else echo 'Сохранить изменения'; ?>"></td>
                    </tr>
                </table>
            </center>
            </form>
            <div>
                <?

                if(isset($_GET['id'])){

                    $payeer = new rfs_payeer($config->AccountNumber, $config->apiId, $config->apiKey);
                    echo '<pre>';
                    print_r($payeer->PaySystemData($data['pid']));
                    echo '</pre>';

                }

                ?>
            </div>
            <?
        }
    }

    ?>

</div>
<div class="clr"></div>	