<div class="s-bk-lf">
    <div class="acc-title">Список выплат</div>
</div>
<div class="silver-bk">
    <div class="clr"></div>
    <center><a href="/?menu=admin4ik&sel=payments">Выплачено</a> || <a href="/?menu=admin4ik&sel=payments&balance">Баланс
            на Payeer</a> ||
        <a href="/?menu=admin4ik&sel=payments&list_day">По дням</a> || <a href="/?menu=admin4ik&sel=payments&last_31">График
            за 30 дней</a></center>
    <BR/>
    <?PHP
    # График
    if (isset($_GET["last_31"])){

    $dlim = time() - 60 * 60 * 24 * 30;
    $db->Query("SELECT * FROM db_payments WHERE date_add > $dlim ORDER BY id DESC");

    $days_money = array();
    $days_insert = array();

    if ($db->NumRows() > 0) {

        while ($data = $db->FetchArray()) {
            $index = date("d.m.Y", $data["date_add"]);

            $days_money[$index] = (isset($days_money[$index])) ? $days_money[$index] + $data["sum"] : $data["sum"];
            $days_insert[$index] = (isset($days_insert[$index])) ? $days_insert[$index] + 1 : 1;

        }

        # Вывод
        if (count($days_money) > 0) {

            $array_for_chart = array();
            $array_for_chart2 = array();
            $array_for_chart3 = array();

            foreach ($days_money as $date => $sum) {

                $array_for_chart[] = "['" . $date . "', " . round($sum) . "]";
                $array_for_chart2[] = "['" . $date . "', " . $days_insert[$date] . "]";
                $array_for_chart3[] = "['" . $date . "', " . round($sum / $days_insert[$date], 2) . "]";

            }

            $retd = implode(", ", array_reverse($array_for_chart));
            $retd2 = implode(", ", array_reverse($array_for_chart2));
            $retd3 = implode(", ", array_reverse($array_for_chart3));

            ?>

            <script type="text/javascript" src="https://www.google.com/jsapi"></script>
            <script type="text/javascript">
                google.load("visualization", "1", {packages: ["corechart"]});
                google.setOnLoadCallback(drawChart);
                function drawChart() {
                    var data = google.visualization.arrayToDataTable([
                        ['День', 'Сумма'],
                        <?=$retd; ?>
                    ]);

                    var options = {
                        title: 'История Выплат (Сумма)',
                        hAxis: {title: 'Last 30 Days', titleTextStyle: {color: 'green'}}
                    };

                    var chart = new google.visualization.SteppedAreaChart(document.getElementById('chart_div'));
                    chart.draw(data, options);
                }
            </script>
            <div id="chart_div" style="width: 100%; height: 500px;"></div>

            <script type="text/javascript">
                google.load("visualization", "1", {packages: ["corechart"]});
                google.setOnLoadCallback(drawChart2);
                function drawChart2() {
                    var data2 = google.visualization.arrayToDataTable([
                        ['День', 'Кол-во'],
                        <?=$retd2; ?>
                    ]);

                    var options2 = {
                        title: 'История Выплат (Кол-во)',
                        hAxis: {title: 'Last 30 Days', titleTextStyle: {color: 'green'}}
                    };

                    var chart2 = new google.visualization.SteppedAreaChart(document.getElementById('chart_div2'));
                    chart2.draw(data2, options2);
                }
            </script>
            <div id="chart_div2" style="width: 100%; height: 500px;"></div>
            <script type="text/javascript">
                google.load("visualization", "1", {packages: ["corechart"]});
                google.setOnLoadCallback(drawChart3);
                function drawChart3() {
                    var data3 = google.visualization.arrayToDataTable([
                        ['День', 'Сумма'],
                        <?=$retd3; ?>
                    ]);

                    var options3 = {
                        title: 'AVG (Сумма / Кол-во)',
                        hAxis: {title: 'Last 30 Days', titleTextStyle: {color: 'green'}}
                    };

                    var chart3 = new google.visualization.SteppedAreaChart(document.getElementById('chart_div3'));
                    chart3.draw(data3, options3);
                }
            </script>
            <div id="chart_div3" style="width: 100%; height: 500px;"></div>


            <?PHP

        }

    } else echo "<center><b>Записей нет</b></center><BR />";


    ?></div>
<div class="clr"></div>    <?PHP
return;
}


# Вывод статистики по дням
if (isset($_GET["list_day"])){

$db->Query("SELECT * FROM db_payments ORDER BY id DESC");

$days_money = array();
$days_insert = array();

if ($db->NumRows() > 0){

while ($data = $db->FetchArray()) {
    $index = date("d.m.Y", $data["date_add"]);

    $days_money[$index] = (isset($days_money[$index])) ? $days_money[$index] + $data["sum"] : $data["sum"];
    $days_insert[$index] = (isset($days_insert[$index])) ? $days_insert[$index] + 1 : 1;

}

# Вывод
if (count($days_money) > 0){

?>
    <table cellpadding='3' cellspacing='0' border='0' bordercolor='#336633' align='center' width="99%">
        <tr bgcolor="#efefef">
            <td style="border: 1px dashed #db8;" align="center" class="m-tb">Дата</td>
            <td style="border: 1px dashed #db8;" align="center" class="m-tb">Выплат</td>
            <td style="border: 1px dashed #db8;" align="center" class="m-tb">На сумму</td>
            <td style="border: 1px dashed #db8;" align="center" class="m-tb">AVG</td>
        </tr>
        <?PHP

        $array_for_chart = array();

        foreach ($days_money as $date => $sum) {

            ?>
            <tr class="htt">
                <td style="border: 1px dashed #db8;" align="center"><b><?= $date; ?></b></td>
                <td style="border: 1px dashed #db8;" align="center"><?= $days_insert[$date]; ?> шт.</td>
                <td style="border: 1px dashed #db8;" align="center"><?= $sum; ?> <?= $config->VAL; ?></td>
                <td style="border: 1px dashed #db8;" align="center"><?= round($sum / $days_insert[$date], 2); ?> <?= $config->VAL; ?></td>
            </tr>
            <?PHP

        }

        ?>
    </table>
<?PHP

}

} else echo "<center><b>Записей нет</b></center><BR />";


?></div>
    <div class="clr"></div>    <?PHP
return;
}

# Проверка баланса Payeer
if (isset($_GET["balance"])){

$payeer = new rfs_payeer($config->AccountNumber, $config->apiId, $config->apiKey);
if ($payeer->isAuth()) {

    $arBalance = $payeer->getBalance();
    echo "<pre>" . print_r($arBalance, true) . "</pre>";

}

?></div>
    <div class='clr'></div><?PHP

return;
}

# Проверка операции выплаты
if (isset($_GET["operation_id"])){

$payeer = new rfs_payeer($config->AccountNumber, $config->apiId, $config->apiKey);
if ($payeer->isAuth()) {

    $operation_data = $payeer->getHistoryInfo(intval($_GET["operation_id"]));
    echo "<pre>" . print_r($operation_data, true) . "</pre>";

}

?></div>
    <div class='clr'></div><?PHP

return;
}


$num_p = (isset($_GET["page"]) AND intval($_GET["page"]) < 1000 AND intval($_GET["page"]) >= 1) ? (intval($_GET["page"]) - 1) : 0;
$lim = $num_p * 100;

$db->Query("SELECT * FROM db_payments LEFT JOIN db_payment_system ON db_payment_system.pid = db_payments.pay_sys_id ORDER BY db_payments.id DESC LIMIT {$lim}, 100");

function colorSum($sum)
{

    if ($sum >= 100) return "red";
    return "#000000";
}

if ($db->NumRows() > 0) {
    $status_array = array(0 => "<font color='#ff7f50'>Проверяется</font>", 1 => "<font color='#daa520'>Выплачивается</font>", 2 => "<font color='#ff7f50'>Отменена</font>", 3 => "<font color='#006400'>Выплачено</font>", 4 => "<font color='#dc143c'>Ошибка</font>");

    ?>
    <table cellpadding='3' cellspacing='0' border='0' bordercolor='#336633' align='center' width="99%">
        <tr bgcolor="#efefef">
            <td style="border: 1px dashed #db8;" align="center" width="50" class="m-tb">Пользователь</td>

            <td style="border: 1px dashed #db8;" align="center" width="50" class="m-tb">Кошелек</td>

            <td style="border: 1px dashed #db8;" align="center" width="50" class="m-tb">Серебро</td>

            <td style="border: 1px dashed #db8;" align="center" width="50" class="m-tb">Сумма</td>

            <td style="border: 1px dashed #db8;" align="center" width="50" class="m-tb">ID операции</td>

            <td style="border: 1px dashed #db8;" align="center" width="50" class="m-tb">Дата операции</td>

            <td style="border: 1px dashed #db8;" align="center" width="50" class="m-tb">Статус</td>
        </tr>


        <?PHP

        while ($data = $db->FetchArray()) {
            ?>
            <tr class="htt">
                <td style="border: 1px dashed #db8;" align="center"><a href="/?menu=admin4ik&sel=users&edit=<?= $data["user_id"]; ?>"
                                      class="stn"><?= $data["user"]; ?></a></td>
                <td style="border: 1px dashed #db8;" align="left"><img class="psimg" src="<?= $data['img']; ?>"><?= $data["purse"]; ?>
                </td>
                <td style="border: 1px dashed #db8;" align="center"><?= $data["serebro"]; ?></td>
                <td style="border: 1px dashed #db8;" align="center"><font
                        color="<?= colorSum($data["sum"]); ?>"><?= sprintf("%.2f", $data["sum"]); ?> <?= $data["valuta"]; ?></font>
                </td>
                <td style="border: 1px dashed #db8;" align="center"><a
                        href="/?menu=admin4ik&sel=payments&operation_id=<?= $data["payment_id"]; ?>"><?= $data["payment_id"]; ?></a>
                </td>
                <td style="border: 1px dashed #db8;" align="center"><?= $func->time2word($data["date_add"]); ?></td>
                <td style="border: 1px dashed #db8;" align="center"><?= $status_array[$data["status"]]; ?></td>
            </tr>
            <?PHP

        }

        ?>

    </table>
    <BR/>
    <?PHP


} else echo "<center><b>На данной странице нет записей</b></center><BR />";


$db->Query("SELECT COUNT(*) FROM db_payments");
$all_pages = $db->FetchRow();

if ($all_pages > 100) {

    $sort_b = (isset($_GET["sort"])) ? intval($_GET["sort"]) : 0;

    $nav = new navigator;
    $page = (isset($_GET["page"]) AND intval($_GET["page"]) < 1000 AND intval($_GET["page"]) >= 1) ? (intval($_GET["page"])) : 1;

    echo "<BR /><center>" . $nav->Navigation(10, $page, ceil($all_pages / 100), "/?menu=admin4ik&sel=payments&page="), "</center>";

}
?>
</div>
<div class='clr'></div>
