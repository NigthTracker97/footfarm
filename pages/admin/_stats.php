<?php
$c_date = date("Ymd", time());
$c_date_begin = strtotime($c_date . " 00:00:00");
$online = time() - 60 * 60 * 1;
?>

<div class="s-bk-lf">
    <div class="acc-title">Статистика проекта</div>
</div>
<div class="silver-bk">
    <div class="clr"></div>
    <?PHP

    $db->Query("SELECT 
	COUNT(id) all_users, 
	(SELECT COUNT(id) FROM db_users_a WHERE fake = 0) real_users,
	SUM(money_b) money_b, 
	SUM(money_p) money_p, 
	
	SUM(a_t) a_t, 
	SUM(b_t) b_t, 
	SUM(c_t) c_t, 
	SUM(d_t) d_t, 
	SUM(e_t) e_t, 
	
	SUM(payment_sum) payment_sum, 
	SUM(insert_sum) insert_sum,
	
	(SELECT SUM(money_r) FROM db_users_b WHERE id != 1) money_r,
	(SELECT SUM(cash_points) FROM db_users_b WHERE id != 1) cash_points,
	
	(SELECT COUNT(*) FROM db_users_a WHERE date_reg > {$c_date_begin} AND fake = 0) today_users,
	(SELECT COUNT(*) FROM db_users_a WHERE date_reg > {$c_date_begin} AND fake = 0 AND rs_referer_id > 0) today_users_rs,
	(SELECT COUNT(*) FROM db_users_a WHERE date_login > {$online} AND fake = 0) online_users,
	(SELECT COUNT(*) FROM db_users_a WHERE date_login >= {$c_date_begin} AND fake = 0) today_users_login,
	(SELECT SUM(money) FROM db_insert_money WHERE date_add > {$c_date_begin}) insert_today,
	(SELECT SUM(`sum`) FROM db_payments WHERE date_add > {$c_date_begin}) payments_today_basic,
	(SELECT SUM(`sum`) FROM db_payment_ext WHERE date_add > {$c_date_begin}) payments_today_ext,
	(SELECT COUNT(id) FROM db_insert_money WHERE date_add > {$c_date_begin}) insert_count_today,
	((SELECT COUNT(id) FROM db_payments WHERE date_add > {$c_date_begin}) + (SELECT COUNT(id) FROM db_payment_ext WHERE date_add > {$c_date_begin})) payment_count_today
	FROM db_users_b");
    $data_stats = $db->FetchArray();

    $db->Query("SELECT * FROM db_stats WHERE id = '1' LIMIT 1");
    $admin_money = $db->FetchArray();

    # Баланс пайера
    $payeer = new rfs_payeer($config->AccountNumber, $config->apiId, $config->apiKey);
    if ($payeer->isAuth()) {
        $arBalance = $payeer->getBalance();
        $payeer_balance_rub = $arBalance['balance']['RUB']['DOSTUPNO'];
        $payeer_balance_eur = $arBalance['balance']['EUR']['DOSTUPNO'];
        $payeer_balance_usd = $arBalance['balance']['USD']['DOSTUPNO'];
        $payeer_balance_btc = $arBalance['balance']['BTC']['DOSTUPNO'];
    }

    # Получаем баланс фрикассы
    $fk = new rfs_fkassa($config->fk_merchant_id,$config->fk_merchant_sword2);
    $fk_money = $fk->getBalance();

    $fkw = new rfs_fkwallet($config->fw_apiId,$config->fw_apiKey);
    $fkw_money = $fkw->getBalance();
    ?>

    <table width="450" border="0" align="center">
        <tr class="htt">
            <td><b>Зарегистрировано пользователей:</b></td>
            <td width="100" align="right"><?= $data_stats["all_users"]; ?> чел.</td>
        </tr>

        <tr class="htt">
            <td><b>Реальные пользователи:</b></td>
            <td width="100" align="right"><?= $data_stats["real_users"]; ?> чел.</td>
        </tr> 

        <tr>
            <td colspan="2" align="center"><b>- - - - -</b></td>
        </tr>

        <tr class="htt">
            <td><b>Зарегистрировались сегодня (реф.программа) :</b></td>
            <td width="100" align="right"><?= $data_stats["today_users"] - $data_stats["today_users_rs"]; ?> чел.</td>
        </tr>

        <tr class="htt">
            <td><b>Зарегистрировались сегодня (продажа рефералов):</b></td>
            <td width="100" align="right"><?= $data_stats["today_users_rs"]; ?> чел.</td>
        </tr>

        <tr class="htt">
            <td><b>Зарегистрировались сегодня (ВСЕГО):</b></td>
            <td width="100" align="right"><?= $data_stats["today_users"]; ?> чел.</td>
        </tr>

        <tr>
            <td colspan="2" align="center"><b>- - - - -</b></td>
        </tr>

        <tr class="htt">
            <td><b>Входы за последнии 60 минут:</b></td>
            <td width="100" align="right"><?= $data_stats["online_users"]; ?> чел.</td>
        </tr>

        <tr class="htt">
            <td><b>Входы в аккаунт сегодня:</b></td>
            <td width="100" align="right"><?= $data_stats["today_users_login"]; ?> чел.</td>
        </tr>

        <tr>
            <td colspan="2" align="center"><b>- - - - -</b></td>
        </tr>

        <tr class="htt">
            <td><b>Количество пополнений сегодня:</b></td>
            <td width="100" align="right"><?= $data_stats["insert_count_today"]; ?> шт.</td>
        </tr>

        <tr class="htt">
            <td><b>Количество выплат сегодня:</b></td>
            <td width="100" align="right"><?= $data_stats["payment_count_today"]; ?> шт.</td>
        </tr>

        <tr>
            <td colspan="2" align="center"><b>- - - - -</b></td>
        </tr>

        <tr class="htt">
            <td><b>Пополнено сегодня:</b></td>
            <td width="100" align="right"><?= sprintf("%.2f", $data_stats["insert_today"]); ?> руб.</td>
        </tr>

        <tr class="htt">
            <td><b>Выплачено сегодня (основные):</b></td>
            <td width="100" align="right"><?= sprintf("%.2f", $data_stats["payments_today_basic"]); ?> руб.</td>
        </tr>

        <tr class="htt">
            <td><b>Выплачено сегодня (экстренные):</b></td>
            <td width="100" align="right"><?= sprintf("%.2f", $data_stats["payments_today_ext"]); ?> руб.</td>
        </tr>

        <tr class="htt">
            <td><b>Выплачено ВСЕГО:</b></td>
            <td width="100" align="right"><?= sprintf("%.2f", $data_stats["payments_today_basic"] + $data_stats["payments_today_ext"]); ?> руб.</td>
        </tr>

        <tr>
            <td colspan="2" align="center"><b>- - - - -</b></td>
        </tr>

        <tr class="htt">
            <td><b>Серебра на счетах (Для покупок):</b></td>
            <td width="100" align="right"><?= sprintf("%.0f", $data_stats["money_b"]); ?></td>
        </tr>

        <tr class="htt">
            <td><b>Серебра на счетах (На вывод):</b></td>
            <td width="100" align="right"><?= sprintf("%.0f", $data_stats["money_p"]); ?></td>
        </tr>

        <tr class="htt">
            <td><b>Реферальные:</b></td>
            <td width="100" align="right"><?= sprintf("%.2f", $data_stats["money_r"]); ?></td>
        </tr>

        <tr class="htt">
            <td><b>Cash Points:</b></td>
            <td width="100" align="right"><?= sprintf("%.2f", $data_stats["cash_points"]); ?></td>
        </tr>

        <tr>
            <td colspan="2" align="center"><b>- - - - -</b></td>
        </tr>

        <tr class="htt">
            <td><b>Куплено зеленых:</b></td>
            <td width="100" align="right"><?= intval($data_stats["a_t"]); ?> шт.</td>
        </tr>

        <tr class="htt">
            <td><b>Куплено желтых:</b></td>
            <td width="100" align="right"><?= intval($data_stats["b_t"]); ?> шт.</td>
        </tr>

        <tr class="htt">
            <td><b>Куплено коричневых:</b></td>
            <td width="100" align="right"><?= intval($data_stats["c_t"]); ?> шт.</td>
        </tr>

        <tr class="htt">
            <td><b>Куплено синих:</b></td>
            <td width="100" align="right"><?= intval($data_stats["d_t"]); ?> шт.</td>
        </tr>

        <tr class="htt">
            <td><b>Куплено красных:</b></td>
            <td width="100" align="right"><?= intval($data_stats["e_t"]); ?> шт.</td>
        </tr>

        <tr>
            <td colspan="2" align="center"><b>- - - - -</b></td>
        </tr>

        <tr class="htt">
            <td><b>Введено пользователями:</b></td>
            <td width="100" align="right"><?= sprintf("%.2f", $data_stats["insert_sum"]); ?> <?= $config->VAL; ?></td>
        </tr>

        <tr class="htt">
            <td><b>Комиссия администрации:</b></td>
            <td width="100"
                align="right"><?= sprintf("%.2f", $admin_money['admin_money']); ?> <?= $config->VAL; ?></td>
        </tr>

        <tr class="htt">
            <td><b>Выплачено пользователям:</b></td>
            <td width="100" align="right"><?= sprintf("%.2f", $data_stats["payment_sum"]); ?> <?= $config->VAL; ?></td>
        </tr>

        <tr class="htt">
            <td><b>Доход администрации:</b></td>
            <td width="100" align="right"><?= sprintf("%.2f", $admin_money['all_admin']); ?> <?= $config->VAL; ?></td>
        </tr>

        <tr>
            <td colspan="2" align="center"><b>- - - - -</b></td>
        </tr>

        <tr class="htt">
            <td><b>Состояние кошелька PAYEER:</b></td>
            <td width="100" align="right">
                <?= $payeer_balance_rub; ?> RUR
                <br>
                <?= $payeer_balance_usd; ?> USD
                <br>
                <?= $payeer_balance_eur; ?> EUR
                <br>
                <?= $payeer_balance_btc; ?> BTC
            </td>
        </tr>

        <tr class="htt">
            <td><b>Состояние кошелька FREE-KASSA:</b></td>
            <td width="100" align="right"><?= $fk_money; ?> RUB</td>
        </tr>

        <tr class="htt">
            <td><b>Состояние кошелька FK WALLET:</b></td>
            <td width="100" align="right">
                <? print_r($fkw_money['data']['RUR']); ?> RUR
                <br>
                <? print_r($fkw_money['data']['USD']); ?> USD
                <br>
                <? print_r($fkw_money['data']['EUR']); ?> EUR
            </td>
        </tr>

    </table>

</div>
<div class="clr"></div>