<div class="s-bk-lf">
	<div class="acc-title">История обменов</div>
</div>
<div class="silver-bk"><div class="clr"></div>	
<?PHP

$num_p = (isset($_GET["page"]) AND intval($_GET["page"]) < 1000 AND intval($_GET["page"]) >= 1) ? (intval($_GET["page"]) -1) : 0;
$lim = $num_p * 100;

$db->Query("SELECT * FROM db_swap_ser ORDER BY id DESC LIMIT {$lim}, 100");

if($db->NumRows() > 0){

?>
<table cellpadding='3' cellspacing='0' border='0' bordercolor='#336633' align='center' width="99%">
  <tr bgcolor="#efefef" class="m-tb">
    <td style="border: 1px dashed #db8;" align="center" width="50" class="m-tb">ID</td>
    <td style="border: 1px dashed #db8;" align="center" class="m-tb">Пользователь</td>
    <td style="border: 1px dashed #db8;" align="center" width="75" class="m-tb">Отдал</td>
	<td style="border: 1px dashed #db8;" align="center" width="75" class="m-tb">Получил</td>
	<td style="border: 1px dashed #db8;" align="center" width="150" class="m-tb">Дата операции</td>
  </tr>


<?PHP

	while($data = $db->FetchArray()){
	
	?>
	<tr class="htt">
    <td style="border: 1px dashed #db8;" align="center" width="50"><?=$data["id"]; ?></td>
    <td style="border: 1px dashed #db8;" align="center"><?=$data["user"]; ?></td>
    <td style="border: 1px dashed #db8;" align="center" width="75"><?=$data["amount_p"]; ?></td>
	<td style="border: 1px dashed #db8;" align="center" width="75"><?=$data["amount_b"]; ?></td>
	<td style="border: 1px dashed #db8;" align="center" width="150"><?=date("d.m.Y в H:i:s",$data["date_add"]); ?></td>
  	</tr>
	<?PHP
	
	}

?>

</table>
<BR />
	<?PHP

	$db->Query("SELECT COUNT(*) FROM db_swap_ser");
	$all_pages = $db->FetchRow();

	if($all_pages > 100){

		$sort_b = (isset($_GET["sort"])) ? intval($_GET["sort"]) : 0;

		$nav = new navigator;
		$page = (isset($_GET["page"]) AND intval($_GET["page"]) < 1000 AND intval($_GET["page"]) >= 1) ? (intval($_GET["page"])) : 1;

		echo "<BR /><center>".$nav->Navigation(10, $page, ceil($all_pages / 100), "/?menu=admin4ik&sel=story_swap&page="), "</center>";

	}

}else echo "<center><b>Записей нет</b></center><BR />";

?>
</div>
<div class="clr"></div>	