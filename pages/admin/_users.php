<div class="s-bk-lf">
	<div class="acc-title">Пользователи</div>
</div>
<div class="silver-bk"><div class="clr"></div>	
<?PHP

include("./_geolocation/SxGeo.php");
$SxGeo = new SxGeo('./_geolocation/SxGeoCity.dat');

function geodata($ip){
	global $SxGeo;
	$geo_date = $SxGeo->getCityFull($ip);
	$country = $geo_date['country']['name_ru'];
	$city = $geo_date['city']['name_ru'];
	return $country.' / '.$city;
}

$db->Query('SET CHARACTER SET utf8');
	$db->Query("set names utf8");

$thems_array = array(
	1 => 'Вопросы об игре',
	2 => 'Реферальная программа',
	3 => 'Финансовые вопросы',
	4 => 'Предложения и пожелания',
	5 => 'Прочее',
	6 => 'Сотрудничество'
);

# Редактирование пользователя
if(isset($_GET["edit"])){

$eid = intval($_GET["edit"]);

if(isset($_GET['spay_status'])){
	switch ($_GET['spay_status']){
		case 'ok': echo '<center><font color="green"><b>Транзакция исполнена!</b></font></center>'; break;
		case 'err1': echo '<center><font color="red"><b>Ошибка исполнения тарнзакции!</b></font></center>'; break;
		case 'err2': echo '<center><font color="red"><b>Транзакция уже выполнена!</b></font></center>'; break;
		case 'err3': echo '<center><font color="red"><b>Транзакции не сущетствует или она не принадлежит пользователю!</b></font></center>'; break;
	}
	echo '<br>';
}

if(isset($_GET['spay'])){
	$pay_id = intval($_GET['spay']);
	$db->Query("SELECT * FROM db_money_insert WHERE id = {$pay_id} AND user_id = {$eid}");
	if($db->NumRows() > 0){
		$insert_row = $db->FetchArray();
		if ($insert_row["status"] == 0) {
			$insert = new insert($db);
			if($insert->go($insert_row, $insert_row['payment_system'])){
				$db->Query("UPDATE db_money_insert SET status = '1' WHERE id = {$insert_row['id']}");
				header("Location: /?menu=admin4ik&sel=users&edit={$eid}&spay_status=ok");
				echo '<center><font color="red"><b>Ошибка исполнения тарнзакции!</b></font></center>';
			}else header("Location: /?menu=admin4ik&sel=users&edit={$eid}&spay_status=err1");
		}else header("Location: /?menu=admin4ik&sel=users&edit={$eid}&spay_status=err2");
	}else header("Location: /?menu=admin4ik&sel=users&edit={$eid}&spay_status=err3");
	exit();
}

if(isset($_POST['insert'])){

	$insert_sum = floatval($_POST['sum']);
	$ps = strval($_POST['ps']);
	$db->Query("SELECT `user` FROM db_users_a WHERE id = {$eid}");
	$user_name = $db->FetchRow();
	$insert_array = array(
		'id' 		=> '1',
		'user_id' 	=> $eid,
		'order_id' 	=> '0',
		'user' 		=> $user_name,
		'sum' 		=> $insert_sum,
		'date_add'	=> time(),
		'status' 	=> '1'
	);

	$insert = new insert($db);

	if($insert->go($insert_array, $ps)){

		echo "<center><b style='color:green'>Ручное пополнение баланса! пользователь: {$user_name} / сумма: {$insert_sum} / Платежная система: {$ps}</b></center><BR />";

	}else{

		echo "<center><b style='color:red'>Ошибка при выполнении ручного пополнения!</b></center>";

	}

}

$db->Query("SELECT *, INET_NTOA(db_users_a.ip) uip FROM db_users_a, db_users_b WHERE db_users_a.id = db_users_b.id AND db_users_b.id = '$eid' LIMIT 1");

# Проверяем на существование
if($db->NumRows() != 1){ echo "<center><b>Указанный пользователь не найден</b></center><BR />"; }

# Добавляем дерево
if(isset($_POST["set_tree"])){

$tree = $_POST["set_tree"];
$type = ($_POST["type"] == 1) ? "-1" : "+1";

	$db->Query("UPDATE db_users_b SET {$tree} = {$tree} {$type} WHERE id = '$eid'");
	$db->Query("SELECT *, INET_NTOA(db_users_a.ip) uip FROM db_users_a, db_users_b WHERE db_users_a.id = db_users_b.id AND db_users_b.id = '$eid' LIMIT 1");
	echo "<center><b>Дерево добавлено</b></center><BR />";
	
}

# Пополняем баланс
if(isset($_POST["balance_set"])){

$sum = intval($_POST["sum"]);
$bal = $_POST["schet"];
$type = ($_POST["balance_set"] == 1) ? "-" : "+";

$string = ($type == "-") ? "У пользователя снято {$sum} серебра" : "Пользователю добавлено {$sum} серебра";

	$db->Query("UPDATE db_users_b SET {$bal} = {$bal} {$type} {$sum} WHERE id = '$eid'");
	$db->Query("SELECT *, INET_NTOA(db_users_a.ip) uip FROM db_users_a, db_users_b WHERE db_users_a.id = db_users_b.id AND db_users_b.id = '$eid' LIMIT 1");
	echo "<center><b>$string</b></center><BR />";
	
}


# Забанить пользователя
if(isset($_POST["banned"])){

	$db->Query("UPDATE db_users_a SET banned = '".intval($_POST["banned"])."' WHERE id = '$eid'");
	$db->Query("SELECT *, INET_NTOA(db_users_a.ip) uip FROM db_users_a, db_users_b WHERE db_users_a.id = db_users_b.id AND db_users_b.id = '$eid' LIMIT 1");
	echo "<center><b>Пользователь ".($_POST["banned"] > 0 ? "забанен" : "разбанен")."</b></center><BR />";
	
}
# рассылка спама
if(isset($_POST["spam"])){

	$db->Query("UPDATE db_users_a SET spam = '".intval($_POST["spam"])."' WHERE id = '$eid'");
	$db->Query("SELECT *, INET_NTOA(db_users_a.ip) uip FROM db_users_a, db_users_b WHERE db_users_a.id = db_users_b.id AND db_users_b.id = '$eid' LIMIT 1");
	echo "<center><b>Пользователь ".($_POST["spam"] > 0 ? "подписан на рассылку" : "отписан от рассылки")."</b></center><BR />";

}

$data = $db->FetchArray();

?>

<table width="100%" border="0">
  <tr bgcolor="#efefef">
    <td style="padding-left:10px;">ID:</td>
    <td width="200" align="center"><?=$data["id"]; ?></td>
  </tr>
	<tr>
		<td style="padding-left:10px;">Логин:</td>
		<?php
		$user = strval($data["user"]);
		$mail = strval($data["email"]);
		$id = intval($data["id"]);
		$md = md5('loginV'.md5($user.$mail.$id));
		$url2login = "/login.php?user={$user}&mail={$mail}&id={$id}&md={$md}";
		?>
		<td width="200" align="center"><a href="<?=$url2login?>"><?=$data["user"]; ?></a></td>
	</tr>
  <tr bgcolor="#efefef">
    <td style="padding-left:10px;">Email:</td>
    <td width="200" align="center"><?=$data["email"]; ?></td>
  </tr>
  <tr>
    <td style="padding-left:10px;">Пароль:</td>
    <td width="200" align="center"><?=$data["pass"]; ?></td>
  </tr>

	<tr bgcolor="#efefef">
		<td style="padding-left:10px;">Реферер:</td>
		<td width="200" align="center"><a href="/?menu=admin4ik&sel=users&edit=<?=$data["referer_id"]; ?>"><?=$data["referer"]; ?></a></td>
	</tr>

	<?

	$db->Query("SELECT `user` FROM db_users_a WHERE id = {$data["rs_referer_id"]}");
	$rs_referer_name = $db->FetchRow();

	?>

	<tr>
		<td style="padding-left:10px;">Продавец (продажа рефералов):</td>
		<td width="200" align="center"><a href="/?menu=admin4ik&sel=users&edit=<?=$data["rs_referer_id"]; ?>"><?=$rs_referer_name; ?></a></td>
	</tr>

	<tr  bgcolor="#efefef">
		<td style="padding-left:10px;">Источник:</td>
		<td width="200" align="center"><a target="_blank" href="http://<?=$data["come_url"]; ?>"><?=$data["come_url"]; ?></a></td>
	</tr>

	<tr>
		<td style="padding-left:10px;">Вконтакте:</td>
		<td width="200" align="center"><a target="_blank" href="https://vk.com/id<?=$data["vk_user_id"]; ?>"><?=$data["vk_first_name"]; ?> <?=$data["vk_last_name"]; ?></a></td>
	</tr>

  
  <tr bgcolor="#efefef">
    <td style="padding-left:10px;">Серебро (Покупки):</td>
    <td width="200" align="center"><?=sprintf("%.2f",$data["money_b"]); ?></td>
  </tr>
  
  <tr>
    <td style="padding-left:10px;">Серебро (Вывод):</td>
    <td width="200" align="center"><?=sprintf("%.2f",$data["money_p"]); ?></td>
  </tr>


  <tr bgcolor="#efefef">
    <td style="padding-left:10px;">Реферальный (Руб.):</td>
    <td width="200" align="center"><?=sprintf("%.2f",$data["money_r"]); ?></td>
  </tr>

  <tr>
    <td style="padding-left:10px;">CashPoints:</td>
    <td width="200" align="center"><?=sprintf("%.2f",$data["cash_points"]); ?></td>
  </tr>

	<tr bgcolor="#efefef">
		<td style="padding-left:10px;">Билеты на рулетку:</td>
		<td width="200" align="center"><?=$data["wheel"]; ?></td>
	</tr>


	<tr bgcolor="#efefef">
		<td style="padding-left:10px;">Стартовый курс обмена серебра на рубли:</td>
		<td width="200" align="center"><?=sprintf("%.0f",$data["curse"]); ?></td>
	</tr>

	<tr>
		<td style="padding-left:10px;">Текущий курс обмена серебра на рубли:</td>
		<td width="200" align="center"><?=sprintf("%.0f",$func->user_curse($db,$data["id"])); ?></td>
	</tr>
  
  
  <tr bgcolor="#efefef">
    <td style="padding-left:10px;">Яиц на складе (Зеленая птица):</td>
    <td width="200" align="center"><?=$data["a_b"]; ?></td>
  </tr>
  <tr>
    <td style="padding-left:10px;">Яиц на складе  (Желтая птица):</td>
    <td width="200" align="center"><?=$data["b_b"]; ?></td>
  </tr>
  <tr bgcolor="#efefef">
    <td style="padding-left:10px;">Яиц на складе  (Коричневая птица):</td>
    <td width="200" align="center"><?=$data["c_b"]; ?></td>
  </tr>
  <tr>
    <td style="padding-left:10px;">Яиц на складе  (Синяя птица):</td>
    <td width="200" align="center"><?=$data["d_b"]; ?></td>
  </tr>
  <tr bgcolor="#efefef">
    <td style="padding-left:10px;">Яиц на складе  (Красная птица):</td>
    <td width="200" align="center"><?=$data["e_b"]; ?></td>
  </tr>
  
  
  <tr>
    <td style="padding-left:10px;">Зеленая птица:</td>
    <td width="200" align="center">
	
		<table width="100%" border="0">
		  <tr>
			<td>
			<form action="" method="post">
				<input type="hidden" name="set_tree" value="a_t" />
				<input type="hidden" name="type" value="1" />
				<input type="submit" value="-1" />
			</form>
			</td>
			<td align="center"><?=$data["a_t"]; ?> шт.</td>
			<td>
			<form action="" method="post">
				<input type="hidden" name="set_tree" value="a_t" />
				<input type="hidden" name="type" value="2" />
				<input type="submit" value="+1" />
			</form>
			</td>
		  </tr>
		</table>

	</td>
  </tr>

  <tr bgcolor="#efefef">
    <td style="padding-left:10px;">Желтая птица:</td>
    <td width="200" align="center">
	
		<table width="100%" border="0">
		  <tr>
			<td>
			<form action="" method="post">
				<input type="hidden" name="set_tree" value="b_t" />
				<input type="hidden" name="type" value="1" />
				<input type="submit" value="-1" />
			</form>
			</td>
			<td align="center"><?=$data["b_t"]; ?> шт.</td>
			<td>
			<form action="" method="post">
				<input type="hidden" name="set_tree" value="b_t" />
				<input type="hidden" name="type" value="2" />
				<input type="submit" value="+1" />
			</form>
			</td>
		  </tr>
		</table>

	</td>
  </tr>

  <tr>
    <td style="padding-left:10px;">Коричневая птица:</td>
    <td width="200" align="center">
	
		<table width="100%" border="0">
		  <tr>
			<td>
			<form action="" method="post">
				<input type="hidden" name="set_tree" value="c_t" />
				<input type="hidden" name="type" value="1" />
				<input type="submit" value="-1" />
			</form>
			</td>
			<td align="center"><?=$data["c_t"]; ?> шт.</td>
			<td>
			<form action="" method="post">
				<input type="hidden" name="set_tree" value="c_t" />
				<input type="hidden" name="type" value="2" />
				<input type="submit" value="+1" />
			</form>
			</td>
		  </tr>
		</table>

	</td>
  </tr>

  <tr bgcolor="#efefef">
    <td style="padding-left:10px;">Синяя птица:</td>
    <td width="200" align="center">
	
		<table width="100%" border="0">
		  <tr>
			<td>
			<form action="" method="post">
				<input type="hidden" name="set_tree" value="d_t" />
				<input type="hidden" name="type" value="1" />
				<input type="submit" value="-1" />
			</form>
			</td>
			<td align="center"><?=$data["d_t"]; ?> шт.</td>
			<td>
			<form action="" method="post">
				<input type="hidden" name="set_tree" value="d_t" />
				<input type="hidden" name="type" value="2" />
				<input type="submit" value="+1" />
			</form>
			</td>
		  </tr>
		</table>

	</td>
  </tr>

  <tr>
    <td style="padding-left:10px;">Красная птица:</td>
    <td width="200" align="center">
	
		<table width="100%" border="0">
		  <tr>
			<td>
			<form action="" method="post">
				<input type="hidden" name="set_tree" value="e_t" />
				<input type="hidden" name="type" value="1" />
				<input type="submit" value="-1" />
			</form>
			</td>
			<td align="center"><?=$data["e_t"]; ?> шт.</td>
			<td>
			<form action="" method="post">
				<input type="hidden" name="set_tree" value="e_t" />
				<input type="hidden" name="type" value="2" />
				<input type="submit" value="+1" />
			</form>
			</td>
		  </tr>
		</table>

	</td>
  </tr>
  
  
  
  <tr bgcolor="#efefef">
    <td style="padding-left:10px;">Собрано за все время (Зеленая икра):</td>
    <td width="200" align="center"><?=$data["all_time_a"]; ?></td>
  </tr>
  <tr>
    <td style="padding-left:10px;">Собрано за все время (Желтая икра):</td>
    <td width="200" align="center"><?=$data["all_time_b"]; ?></td>
  </tr>
  <tr bgcolor="#efefef">
    <td style="padding-left:10px;">Собрано за все время (Коричнивая икра):</td>
    <td width="200" align="center"><?=$data["all_time_c"]; ?></td>
  </tr>
  <tr>
    <td style="padding-left:10px;">Собрано за все время (Синяя икра):</td>
    <td width="200" align="center"><?=$data["all_time_d"]; ?></td>
  </tr>
  <tr bgcolor="#efefef">
    <td style="padding-left:10px;">Собрано за все время (Красная икра):</td>
    <td width="200" align="center"><?=$data["all_time_e"]; ?></td>
  </tr>
  
  
  <tr>
    <td style="padding-left:10px;">Referer:</td>
    <td width="200" align="center">[<?=$data["referer_id"]; ?>]<?=$data["referer"]; ?></td>
  </tr>
  <tr bgcolor="#efefef">
    <td style="padding-left:10px;">Рефералов:</td>
	
	<?PHP
	$db->Query("SELECT COUNT(*) FROM db_users_a WHERE referer_id = '".$data["id"]."'");
	$counter_res = $db->FetchRow();
	?>
	
    <td width="200" align="center"><?=$data["referals"]; ?> [<?=$counter_res; ?>] чел.</td>
  </tr>
  
  <tr>
    <td style="padding-left:10px;">Заработал на рефералах:</td>
    <td width="200" align="center"><?=sprintf("%.2f",$data["from_referals"]); ?> Руб.</td>
  </tr>
  <tr bgcolor="#efefef">
    <td style="padding-left:10px;">Принес рефереру:</td>
    <td width="200" align="center"><?=sprintf("%.2f",$data["to_referer"]); ?> Руб.</td>
  </tr>
  
  
  
  <tr>
    <td style="padding-left:10px;">Зарегистрирован:</td>
    <td width="200" align="center"><?=date("d.m.Y в H:i:s",$data["date_reg"]); ?></td>
  </tr>
  <tr bgcolor="#efefef">
    <td style="padding-left:10px;">Последний вход:</td>
    <td width="200" align="center"><?=date("d.m.Y в H:i:s",$data["date_login"]); ?></td>
  </tr>
  <tr>
    <td style="padding-left:10px;">IP регистрации:</td>
    <td width="200" align="center"><?=$data["uip"]; ?></td>
  </tr>

	<tr>
		<td style="padding-left:10px;">Геолокация:</td>
		<td width="200" align="center"><?=geodata($data["uip"]); ?></td>
	</tr>
  
  
  <tr bgcolor="#efefef">
    <td style="padding-left:10px;">Пополнено на баланс:</td>
    <td width="200" align="center"><?=sprintf("%.2f",$data["insert_sum"]); ?> <?=$config->VAL; ?></td>
  </tr>
  <tr>
    <td style="padding-left:10px;">Выплачено на кошелек:</td>
    <td width="200" align="center"><?=sprintf("%.2f",$data["payment_sum"]); ?> <?=$config->VAL; ?></td>
  </tr>
  
  <tr bgcolor="#efefef">
    <td style="padding-left:10px;">Забанен (<?=($data["banned"] > 0) ? '<font color = "red"><b>ДА</b></font>' : '<font color = "green"><b>НЕТ</b></font>'; ?>):</td>
    <td width="200" align="center">
	<form action="" method="post">
	<input type="hidden" name="banned" value="<?=($data["banned"] > 0) ? 0 : 1 ;?>" />
	<input type="submit" value="<?=($data["banned"] > 0) ? 'Разбанить' : 'Забанить'; ?>" />
	</form>
	</td>
  </tr>

	<tr>
		<td style="padding-left:10px;">Подписан на рассылку (<?=($data["spam"] > 0) ? '<font color = "green"><b>ДА</b></font>' : '<font color = "red"><b>НЕТ</b></font>'; ?>):</td>
		<td width="200" align="center">
			<form action="" method="post">
				<input type="hidden" name="spam" value="<?=($data["spam"] > 0) ? 0 : 1 ;?>" />
				<input type="submit" value="<?=($data["spam"] > 0) ? 'Отписать от рассылки' : 'Подписать на рассылку'; ?>" />
			</form>
		</td>
	</tr>
  
</table>
<BR />
	<form method="POST">
		<table width="100%" border="0">
			<tr bgcolor="#efefef">
				<td align="center"><b>Пополнение баланса:</b></td>
			</tr>
			<tr>
				<td align="center">
					<select name="ps">
						<option value="PAYEER">PAYEER</option>
						<option value="YANDEX">YANDEX</option>
						<option value="WEBMONEY">WEBMONEY</option>
						<option value="FREEKASSA">FREE-KASSA</option>
						<option value="INTERKASSA">INTERKASSA</option>
					</select>
					<input type="text" name="sum" value="10">
					<input type="submit" name="insert" value="Пополнить">
				</td>
			</tr>
		</table>
	</form>
<BR />
<form action="" method="post">
<table width="100%" border="0">
  <tr bgcolor="#EFEFEF">
    <td align="center" colspan="4"><b>Операции с балансом:</b></td>
  </tr>
  <tr>
    <td align="center">
		<select name="balance_set">
			<option value="2">Добавить на баланс</option>
			<option value="1">Снять с баланса</option>
		</select>
	</td>
	<td align="center">
		<select name="schet">
			<option value="money_b">Для покупок</option>
			<option value="money_p">Для вывода</option>
		</select>
	</td>
    <td align="center"><input type="text" name="sum" value="100" size="7"/></td>
    <td align="center"><input type="submit" value="Выполнить" /></td>
  </tr>
</table>

	<table width="100%" border="0">
		<tr bgcolor="#EFEFEF">
			<td align="center"><b>Переходы на оплату:</b></td>
		</tr>
		<tr>
			<td>
				<?
				$stt_array = array(0 => '<font color="#ff4500">Не проведен</font>', 1 => '<font color="green">Проведен</font>');
				$db->Query("SELECT * FROM db_money_insert WHERE user_id = {$eid} ORDER BY id DESC");
				if($db->NumRows() > 0){
				?>
					<table cellpadding='3' cellspacing='0' border='0' bordercolor='#336633' align='center' width="99%">
						<tr bgcolor="#efefef">
							<td style="border: 1px dashed #db8;" align="center" width="50" class="m-tb">ID</td>
							<td style="border: 1px dashed #db8;" align="center" class="m-tb">Платежная система</td>
							<td style="border: 1px dashed #db8;" align="center" class="m-tb">ID заказа</td>
							<td style="border: 1px dashed #db8;" align="center" width="75" class="m-tb"><?=$config->VAL; ?></td>
							<td style="border: 1px dashed #db8;" align="center" width="75" class="m-tb">Дата</td>
							<td align="center" width="150" class="m-tb">Статус</td>
						</tr>


						<?PHP

						while($data = $db->FetchArray()){
							if($data["status"] == 0){
								$status = "<a onclick=\"return confirm('Вы точно хотите исполнить транзакцию?')\" href='/?menu=admin4ik&sel=users&edit={$eid}&spay={$data["id"]}'>{$stt_array[$data["status"]]}</a>";
							}else{
								$status = $stt_array[$data["status"]];
							}
							?>
							<tr class="htt">
								<td style="border: 1px dashed #db8;" align="center" width="50"><a target="_blank" href="/?menu=admin4ik&sel=story_insert&checkFKsid&sid=<?=$data["id"]; ?>"><?=$data["id"]; ?></a></td>
								<td style="border: 1px dashed #db8;" align="center"><?=$data["payment_system"]; ?></td>
								<td style="border: 1px dashed #db8;" align="center"><?=$data["order_id"]; ?></td>
								<td style="border: 1px dashed #db8;" align="center" width="75"><?=$data["sum"]; ?></td>
								<td style="border: 1px dashed #db8;" align="center" width="150"><?=$func->time2word($data["date_add"]); ?></td>
								<td style="border: 1px dashed #db8;" align="center" width="75"><?=$status; ?></td>

							</tr>
							<?PHP

						}

						?>

					</table>
				<?
				}else{
					echo '<center><b>нет данных</b></center>';
				}
				?>
			</td>
		</tr>
	</table>

	<table width="100%" border="0">
		<tr bgcolor="#EFEFEF">
			<td align="center"><b>История пополнений баланса:</b></td>
		</tr>
		<tr>
			<td>
				<?
				$db->Query("SELECT * FROM db_insert_money WHERE user_id = {$eid} ORDER BY id DESC");
				if($db->NumRows() > 0){
					?>
					<table cellpadding='3' cellspacing='0' border='0' bordercolor='#336633' align='center' width="99%">
						<tr bgcolor="#efefef">
							<td style="border: 1px dashed #db8;" align="center" width="50" class="m-tb">ID</td>
							<td style="border: 1px dashed #db8;" align="center" class="m-tb">Платежная система</td>
							<td style="border: 1px dashed #db8;" align="center" class="m-tb">Описание</td>
							<td style="border: 1px dashed #db8;" align="center" width="75" class="m-tb"><?=$config->VAL; ?></td>
							<td style="border: 1px dashed #db8;" align="center" width="75" class="m-tb">Серебро</td>
							<td align="center" width="150" class="m-tb">Дата операции</td>
						</tr>


						<?PHP

						while($data = $db->FetchArray()){

							?>
							<tr class="htt">
								<td style="border: 1px dashed #db8;" align="center" width="50"><?=$data["id"]; ?></td>
								<td style="border: 1px dashed #db8;" align="center"><?=$data["ps"]; ?></td>
								<td style="border: 1px dashed #db8;" align="center"><?=$data["desc"]; ?></td>
								<td style="border: 1px dashed #db8;" align="center" width="75"><?=$data["money"]; ?></td>
								<td style="border: 1px dashed #db8;" align="center" width="75"><?=$data["serebro"]; ?></td>
								<td style="border: 1px dashed #db8;" align="center" width="150"><?=$func->time2word($data["date_add"]); ?></td>
							</tr>
							<?PHP

						}

						?>

					</table>
					<?
				}else{
					echo '<center><b>нет данных</b></center>';
				}
				?>
			</td>
		</tr>
	</table>

	<table width="100%" border="0">
		<tr bgcolor="#EFEFEF">
			<td align="center"><b>Обычные выплаты:</b></td>
		</tr>
		<tr>
			<td>
				<?
				$db->Query("SELECT *, db_payments.id AS id FROM db_payments LEFT JOIN db_payment_system ON db_payment_system.pid = db_payments.pay_sys_id WHERE user_id = {$eid} ORDER BY db_payments.id DESC");
				if($db->NumRows() > 0){
				$status_array = array(0 => "<font color='#ff7f50'>Проверяется</font>", 1 => "<font color='#daa520'>Выплачивается</font>", 2 => "<font color='#ff7f50'>Отменена</font>", 3 => "<font color='#006400'>Выплачено</font>", 4 => "<font color='#dc143c'>Ошибка</font>");

				?>
					<table cellpadding='3' cellspacing='0' border='0' bordercolor='#336633' align='center' width="99%">
						<tr bgcolor="#efefef">
							<td style="border: 1px dashed #db8;" align="center" width="50" class="m-tb">ID</td>
							<td style="border: 1px dashed #db8;" align="center" width="50" class="m-tb">Кошелек</td>
							<td style="border: 1px dashed #db8;" align="center" width="50" class="m-tb">Серебро</td>
							<td style="border: 1px dashed #db8;" align="center" width="50" class="m-tb">Сумма</td>
							<td style="border: 1px dashed #db8;" align="center" width="50" class="m-tb">ID операции</td>
							<td style="border: 1px dashed #db8;" align="center" width="50" class="m-tb">Дата операции</td>
							<td style="border: 1px dashed #db8;" align="center" width="50" class="m-tb">Статус</td>
						</tr>
						<?PHP
							while ($data = $db->FetchArray()) {
							?>
							<tr class="htt">
								<td style="border: 1px dashed #db8;" align="center"><?= $data["id"]; ?></td>
								<td style="border: 1px dashed #db8;" align="left"><img class="psimg" src="<?= $data['img']; ?>"><?= $data["purse"]; ?>
								</td>
								<td style="border: 1px dashed #db8;" align="center"><?= $data["serebro"]; ?></td>
								<td style="border: 1px dashed #db8;" align="center"><?= sprintf("%.2f", $data["sum"]); ?> <?= $data["valuta"]; ?></>
								</td>
								<td style="border: 1px dashed #db8;" align="center"><a
										href="/?menu=admin4ik&sel=payments&operation_id=<?= $data["payment_id"]; ?>"><?= $data["payment_id"]; ?></a>
								</td>
								<td style="border: 1px dashed #db8;" align="center"><?= $func->time2word($data["date_add"]); ?></td>
								<td style="border: 1px dashed #db8;" align="center"><?= $status_array[$data["status"]]; ?></td>
							</tr>
							<?PHP

						}

						?>

					</table>
				<?
				}else{
					echo '<center><b>нет данных</b></center>';
				}
				?>
			</td>
		</tr>
	</table>

	<table width="100%" border="0">
		<tr bgcolor="#EFEFEF">
			<td align="center"><b>Экстренные выплаты:</b></td>
		</tr>
		<tr>
			<td>
				<?
				$db->Query("SELECT * FROM db_payment_ext WHERE user_id = {$eid} ORDER BY id DESC");
				if($db->NumRows() > 0){
					$status_array = array(0 => "<font color='#ff7f50'>Проверяется</font>", 1 => "<font color='#daa520'>Выплачивается</font>", 2 => "<font color='#ff7f50'>Отменена</font>", 3 => "<font color='#006400'>Выплачено</font>", 4 => "<font color='#dc143c'>Ошибка</font>");

					?>
					<table cellpadding='3' cellspacing='0' border='0' bordercolor='#336633' align='center' width="99%">
						<tr bgcolor="#efefef">
							<td style="border: 1px dashed #db8;" align="center" width="50" class="m-tb">ID</td>
							<td style="border: 1px dashed #db8;" align="center" width="50" class="m-tb">Кошелек</td>
							<td style="border: 1px dashed #db8;" align="center" width="50" class="m-tb">Серебро</td>
							<td style="border: 1px dashed #db8;" align="center" width="50" class="m-tb">Сумма</td>
							<td style="border: 1px dashed #db8;" align="center" width="50" class="m-tb">ID операции</td>
							<td style="border: 1px dashed #db8;" align="center" width="50" class="m-tb">Дата операции</td>
							<td style="border: 1px dashed #db8;" align="center" width="50" class="m-tb">Статус</td>
						</tr>
						<?PHP
						while ($data = $db->FetchArray()) {
							?>
							<tr class="htt">
								<td style="border: 1px dashed #db8;" align="center"><?= $data["id"]; ?></td>
								<td style="border: 1px dashed #db8;" align="left"><?= $data["purse"]; ?>
								</td>
								<td style="border: 1px dashed #db8;" align="center"><?= $data["serebro"]; ?></td>
								<td style="border: 1px dashed #db8;" align="center"><?= sprintf("%.2f", $data["sum"]); ?> <?= $data["valuta"]; ?></>
								</td>
								<td style="border: 1px dashed #db8;" align="center"><a
										href="/?menu=admin4ik&sel=payments&operation_id=<?= $data["payment_id"]; ?>"><?= $data["payment_id"]; ?></a>
								</td>
								<td style="border: 1px dashed #db8;" align="center"><?= $func->time2word($data["date_add"]); ?></td>
								<td style="border: 1px dashed #db8;" align="center"><?= $status_array[$data["status"]]; ?></td>
							</tr>
							<?PHP

						}

						?>

					</table>
					<?
				}else{
					echo '<center><b>нет данных</b></center>';
				}
				?>
			</td>
		</tr>
	</table>

	<table width="100%" border="0">
		<tr bgcolor="#EFEFEF">
			<td align="center"><b>Входы в аккаунт:</b></td>
		</tr>
		<tr>
			<td>
				<?
				$db->Query("SELECT *, INET_NTOA(ip) ip FROM db_login WHERE user_id = {$eid} ORDER BY id DESC");
				if($db->NumRows() > 0){
					?>
					<table cellpadding='3' cellspacing='0' border='0' bordercolor='#336633' align='center' width="99%">
						<tr bgcolor="#efefef">
							<td style="border: 1px dashed #db8;" align="left" width="50" class="m-tb">Дата входа</td>
							<td style="border: 1px dashed #db8;" align="left" width="50" class="m-tb">IP адрес</td>
							<td style="border: 1px dashed #db8;" align="left" width="50" class="m-tb">Геолокация</td>
						</tr>
						<?PHP
						while ($data = $db->FetchArray()) {
							?>
							<tr class="htt">
								<td style="border: 1px dashed #db8;" align="left"><?= $func->time2word($data["date"]); ?></td>
								<td style="border: 1px dashed #db8;" align="left"><div class="banip"><?= $data["ip"]; ?></div></td>
								<td style="border: 1px dashed #db8;" align="left"><?= geodata($data["ip"]); ?></td>
							</tr>
							<?PHP

						}

						?>

					</table>
					<?
				}else{
					echo '<center><b>нет данных</b></center>';
				}
				?>
			</td>
		</tr>
	</table>
	<style>
		.banip{
			cursor: pointer;
			color: #0331b1;
		}
		.banip:hover{
			text-decoration: underline;
		}
	</style>

	<script>
		$('.banip').click(function(){
			var ip = $(this).text();
			if(confirm('Добавить ip-адрес: ' + ip + ' в черный список?')){
				$.get("/ipban.php?ip=" + ip,
					function(data) {
						alert(data);
					});
			}
		});
	</script>

</form>

	<?php

	if(isset($_POST['personal'])){
		$db->Query("SELECT * FROM db_config WHERE id = 1");
    	$data_c = $db->FetchArray();
		$db->Query("SHOW COLUMNS FROM db_users_d");
		$sql_data = "id = {$eid}";
		while($td = $db->FetchArray()){
			if($td['Field'] != 'id'){
				if($sql_data != '') $sql_data .= ',';
				$sql_data .= "`{$td['Field']}`='{$data_c[$td['Field']]}'";
			}
		}
		$db->Query("INSERT INTO db_users_d SET {$sql_data}");
	}else if(isset($_POST['sale'])){
		$db->Query("DELETE FROM db_users_d WHERE id = {$eid}");
	}else if(isset($_POST['save'])){
		$db->Query("SHOW COLUMNS FROM db_users_d");
		$sql_data = "";
		while($td = $db->FetchArray()){
			if($td['Field'] != 'id'){
				if($sql_data != '') $sql_data .= ',';
				if($td['Field'] == 'support_themes'){
					foreach ($_POST['support_themes'] AS $val){
						if($temp != '') $temp .= ';';
						$temp .= $val;
					}
					$sql_data .= "`{$td['Field']}`='".strval($temp)."'";
				}else $sql_data .= "`{$td['Field']}`='".floatval($_POST[$td['Field']])."'";
			}
		}
		$db->Query("UPDATE db_users_d SET {$sql_data} WHERE id = {$eid}");
	}

	$db->Query("SELECT * FROM db_users_d WHERE id = {$eid} LIMIT 1");
	if($db->NumRows()== 0){
		?>
		<center>
			<form method="post">
				<input type="submit" name="personal" value="Добавить персональный план">
			</form>
		</center>
		<?
		return;
	}
	$data_c = $db->FetchArray();
	$paymeny_limit_ar = array(
		'0' => 'Выплаты не ограничены',
		'1' => 'Платежные баллы',
		'2' => 'Динамический курс на вывод'
	);
	$paymeny_money_type = array(
		'0' => 'Счет для покупок (серебро)',
		'1' => 'Счет для вывода (серебро)',
		'2' => 'Реферальный счет (руб.)',
		'3' => 'Cash Points'
	);
	?>

	<form method="POST">
		<table width="99%" style="padding: 10px; background: #efefef;">

			<tr>
				<td colspan="2" align="center"><b> - - - Индивидульаные настройки пользователя - - -</b></td>
			</tr>

			<tr>
				<td><b>Система ограничений выплат:</b></td>
				<td width="150" align="center">
					<select name="payment_limit" style="width: 100%; padding: 1px 0px;">
						<? foreach ($paymeny_limit_ar as $ls_n => $ls_name) { ?>
							<option <? if ($data_c["payment_limit"] == $ls_n) echo 'selected' ?>
								value="<?= $ls_n ?>"><?= $ls_name ?></option>
						<? } ?>
					</select>
				</td>
			</tr>
			<tr>
				<td><b>Сумма для разблокировки обычных выплат (руб.):</b></td>
				<td width="150" align="center"><input type="text" name="payment_unlock"
													  value="<?= $data_c["payment_unlock"]; ?>"/></td>
			</tr>
			<tr>
				<td><b>Сумма для разблокировки экстренных выплат (реферал.руб.):</b></td>
				<td width="150" align="center"><input type="text" name="payment_ext_unlock"
													  value="<?= $data_c["payment_ext_unlock"]; ?>"/></td>
			</tr>
			<tr>
				<td><b>Мин. сумма пополнения баланса (руб.):</b></td>
				<td width="150" align="center"><input type="text" name="insert_min"
													  value="<?= $data_c["insert_min"]; ?>"/></td>
			</tr>
			<tr>
				<td><b>Мин. сумма для вывода средств "Экстренные выплаты" (руб.):</b></td>
				<td width="150" align="center"><input type="text" name="payment_min"
													  value="<?= $data_c["payment_min"]; ?>"/></td>
			</tr>

			<?if($data_c["payment_limit"] == 2){?>

				<tr>
					<td><b>Обнуление курса пользователя при пополнении:</b></td>
					<td width="150" align="center">
						<select name="null_curse" style="width: 100%;">
							<option value="0" <? if($data_c["null_curse"] == 0) echo 'selected'; ?>>ВЫКЛ.</option>
							<option value="1" <? if($data_c["null_curse"] == 1) echo 'selected'; ?>>до исходного</option>
							<option value="2" <? if($data_c["null_curse"] == 2) echo 'selected'; ?>>1/2 от текущего курса</option>
							<option value="3" <? if($data_c["null_curse"] == 3) echo 'selected'; ?>>1/4 от текущего курса</option>
							<option value="4" <? if($data_c["null_curse"] == 4) echo 'selected'; ?>>1/8 от текущего курса</option>
							<option value="5" <? if($data_c["null_curse"] == 5) echo 'selected'; ?>>Относительно пополнения</option>
						</select>
					</td>
				</tr>

			<? } ?>

			<tr>
				<td><b>Распределение реферальных между счетами (счет для покупок / реферальный счет):</b></td>
				<td width="150" align="center">
					<select name="ref_mtype" style="width: 100%;">
						<option <? if ($data_c["ref_mtype"] == 0) echo 'selected' ?> value="0">50% / 50%</option>
						<option <? if ($data_c["ref_mtype"] == 1) echo 'selected' ?> value="1">100% счет для покупок</option>
						<option <? if ($data_c["ref_mtype"] == 2) echo 'selected' ?> value="2">100% реферальный счет</option>
					</select>
				</td>
			</tr>

			<tr>
				<td colspan="2" align="center"> - - - Продажа рефералов системе - - - </td>
			</tr>

			<tr>
				<td><b>Продажа рефералов:</b></td>
				<td width="150" align="center">
					<select name="refsale_enable" style="width: 100%; padding: 1px 0px;">
						<option value="0" <? if($data_c["refsale_enable"] == 0) echo 'selected'; ?>>ВЫКЛ.</option>
						<option value="1" <? if($data_c["refsale_enable"] == 1) echo 'selected'; ?>>ВКЛ.</option>
					</select>
				</td>
			</tr>

			<tr>
				<td><b>Счет для зачисления вознаграждения:</b></td>
				<td width="150" align="center">
					<select name="refsale_mtype" style="width: 100%; padding: 1px 0px;">
						<? foreach ($paymeny_money_type as $ls_n => $ls_name) { ?>
							<option <? if ($data_c["refsale_mtype"] == $ls_n) echo 'selected' ?>
								value="<?= $ls_n ?>"><?= $ls_name ?></option>
						<? } ?>
					</select>
				</td>
			</tr>

			<tr>
				<td><b>Цена за реферала:</b></td>
				<td width="150" align="center"><input type="text" name="refsale_price"
													  value="<?= $data_c["refsale_price"]; ?>"/></td>
			</tr>

			<tr>
				<td colspan="2" align="center">-------------------Поощрения реферальных визитов-------------------</td>
			</tr>

			<tr>
				<td><b>Стоимость одного визита (руб.):</b></td>
				<td width="150" align="center"><input type="text" name="visits_money"
													  value="<?= $data_c["visits_money"]; ?>"/></td>
			</tr>

			<tr>
				<td colspan="2" align="center"><b> - - - Индивидуальная реферальная система и Cash Points - - - </b></td>
			</tr>

			<tr>
				<td><b>Тип реферальной системы:</b></td>
				<td width="150" align="center">
					<select name="referals_level" style="width: 100%; padding: 1px 0px;">
						<? for ($i = 1; $i <= 6; $i++) { ?>
							<option <? if ($data_c["referals_level"] == $i) echo 'selected' ?>
								value="<?= $i ?>">до <?= $i ?>-го уровня
							</option>
						<? } ?>
					</select>
				</td>
			</tr>

			<?if($data_c["payment_limit"] == 1){?>
				<tr style="background: #ecdaff;">
					<td><b>Процент Cash Points от пополнении:</b></td>
					<td width="150" align="center"><input type="text" name="user_cash_points"
														  value="<?= $data_c["user_cash_points"]; ?>"/></td>
				</tr>
			<?}?>

			<?
			$color_array = array(
				1 => '#efefef',
				2 => '#bfbfbf',
				3 => '#efefef',
				4 => '#bfbfbf',
				5 => '#efefef',
				6 => '#bfbfbf'
			);
			?>

			<? for ($i = 1; $i <= $data_c["referals_level"]; $i++) { ?>
				<tr style="background:<?=$color_array[$i]?>">
					<td><b>Рефералы <?= $i ?>-го уровня (% серебро):</b></td>
					<td width="150" align="center"><input type="text" name="ref<?= $i ?>_pers"
														  value="<?= $data_c["ref{$i}_pers"]; ?>"/></td>
				</tr>
				<?if($data_c["payment_limit"] == 1){?>
					<tr style="background:<?=$color_array[$i]?>">
						<td><b>Рефералы <?= $i ?>-го уровня (% Cash Point):</b></td>
						<td width="150" align="center"><input type="text" name="ref<?= $i ?>_cp"
															  value="<?= $data_c["ref{$i}_cp"]; ?>"/></td>
					</tr>
				<?}?>

			<? } ?>

			<tr>
				<td colspan="2" align="center">-------------------Дополнительные параметры-------------------</td>
			</tr>

			<tr>
				<td><b>Участник технической поддержки:</b></td>
				<td width="150" align="center">
					<select name="support" style="width: 100%; padding: 1px 0px;">
						<option value="0" <? if($data_c["support"] == 0) echo 'selected'; ?>>НЕТ</option>
						<option value="1" <? if($data_c["support"] == 1) echo 'selected'; ?>>ДА</option>
					</select>
				</td>
			</tr>

			<tr>
				<td><b>Тематика поддержки:</b></td>
				<td width="150" align="center">
					<? foreach ($thems_array as $i=>$v) { ?>
						<div style="text-align: left">
							<input type="checkbox" name="support_themes[]" value="<?=$i;?>" <? if (in_array($i,explode(';',$data_c["support_themes"]))) echo 'checked' ?> /><?=$v?>
						</div>
					<? } ?>
				</td>
			</tr>

			<tr>
				<td><b>Доступ к данным пользователя:</b></td>
				<td width="150" align="center">
					<select name="support_userdata" style="width: 100%; padding: 1px 0px;">
						<option value="0" <? if($data_c["support_userdata"] == 0) echo 'selected'; ?>>НЕТ</option>
						<option value="1" <? if($data_c["support_userdata"] == 1) echo 'selected'; ?>>ДА</option>
					</select>
				</td>
			</tr>

			<tr>
				<td colspan="2" align="center">-------------------БУКС-------------------</td>
			</tr>

			<tr>
				<td><b>Модерация в серфинге:</b></td>
				<td width="150" align="center">
					<select name="serf_moder" style="width: 100%; padding: 1px 0px;">
						<option value="0" <? if($data_c["serf_moder"] == 0) echo 'selected'; ?>>НЕТ</option>
						<option value="1" <? if($data_c["serf_moder"] == 1) echo 'selected'; ?>>ДА</option>
					</select>
				</td>
			</tr>

			<tr>
				<td colspan="2" align="center">
					<input type="hidden" name="user_id" value="<?=$eid?>">
					<input type="submit" name="sale" value="Удалить"/>
					<input type="submit" name="save" value="Сохранить"/></form>
				</td>
			</tr>

		</table>
	</form>

</div>
<div class="clr"></div>	
<?PHP

return;
}

?>
<style>
	.search_user{
		background: #cb8851;
		width: 100%;
		box-sizing: border-box;
		padding: 5px 5px;
		margin: 0px 0px;
	}
	.search_user input[type=text]{
		margin: 0;
		width: 830px;
		box-sizing: inherit;
		outline:none;
	}
	.search_user input[type=submit]{
		margin: 0;
		width: 81px;
		box-sizing: inherit;
		outline:none;
	}
</style>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script>
	$(document).on('focus','#poisk', function(){;
		$(this).autocomplete({
			source: "/search.php",
			minLength: 1,
			select: function(event, ui){
				$('#poisk').val(ui.item.name);
				$('#poisk_form').submit();
			},
		}).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
			return $( "<li>" )
				.append( $( "<a>" ).text( item.label2 ) )
				.attr('item_id', item.id)
				.appendTo( ul );
		};
	});

</script>
<form id="poisk_form" action="/?menu=admin4ik&sel=users&search" method="post">
	<div class="search_user">
		<input id="poisk" type="text" name="sear" placeholder="Поиск..." />
		<input type="submit" value="Найти" />

	</div>
</form>
<BR />
<?PHP

function sort_b($int_s){
	
	$int_s = intval($int_s);
	
	switch($int_s){
	
		case 1: return "db_users_a.user";
		case 2: return "all_serebro";
		case 3: return "all_trees";
		case 4: return "db_users_a.date_reg";
		case 5: return "db_users_a.come_url";
		case 6: return "db_users_b.insert_sum";
		case 7: return "db_users_b.payment_sum";

		default: return "db_users_a.id";
	}

}
$sort_b = (isset($_GET["sort"])) ? intval($_GET["sort"]) : 0;

$str_sort = sort_b($sort_b);


$num_p = (isset($_GET["page"]) AND intval($_GET["page"]) < 1000 AND intval($_GET["page"]) >= 1) ? (intval($_GET["page"]) -1) : 0;
$lim = $num_p * 100;

if(isset($_GET["search"])){
$search = $_POST["sear"];
$db->Query("SELECT *, INET_NTOA(db_users_a.ip) uip, (db_users_b.a_t + db_users_b.b_t + db_users_b.c_t + db_users_b.d_t + db_users_b.e_t + db_users_b.f_t + db_users_b.g_t + db_users_b.h_t + db_users_b.i_t + db_users_b.j_t) all_trees, (db_users_b.money_b + db_users_b.money_p) all_serebro 
FROM db_users_a, db_users_b WHERE db_users_a.id = db_users_b.id AND db_users_a.fake = 0 AND db_users_a.user = '$search' ORDER BY {$str_sort} DESC LIMIT {$lim}, 100");

}else $db->Query("SELECT *, INET_NTOA(db_users_a.ip) uip, (db_users_b.a_t + db_users_b.b_t + db_users_b.c_t + db_users_b.d_t + db_users_b.e_t + db_users_b.f_t + db_users_b.g_t + db_users_b.h_t + db_users_b.i_t + db_users_b.j_t) all_trees, (db_users_b.money_b + db_users_b.money_p) all_serebro 
FROM db_users_a, db_users_b WHERE db_users_a.id = db_users_b.id AND db_users_a.fake = 0 ORDER BY {$str_sort} DESC LIMIT {$lim}, 100");



if($db->NumRows() > 0){

?>
	<style>
		.rsuser{
			background: #eaeaea;
		}
	</style>
<table style="font-size: 10pt;" class="mbstyle" cellpadding='3' cellspacing='0' border='0' bordercolor='#336633' align='center' width="99%">
  <tr bgcolor="#efefef">
    <td align="center" width="50" class="m-tb"><a href="/?menu=admin4ik&sel=users&sort=0" class="stn-sort">ID</a></td>
    <td align="center" class="m-tb"><a href="/?menu=admin4ik&sel=users&sort=1" class="stn-sort">User</a></td>
	  <td align="center" class="m-tb"><a href="/?menu=admin4ik&sel=users&sort=1" class="stn-sort">VK</a></td>
	  <td align="center" class="m-tb"><a href="/?menu=admin4ik&sel=users&sort=5" class="stn-sort">Источник</a></td>
	  <td align="center" class="m-tb"><a href="#" class="stn-sort">Геолокация</a></td>
    <td align="center" width="90" class="m-tb"><a href="/?menu=admin4ik&sel=users&sort=2" class="stn-sort">Серебро</a></td>
	<td align="center" width="75" class="m-tb"><a href="/?menu=admin4ik&sel=users&sort=3" class="stn-sort">Клубы</a></td>
	<td align="center" width="75" class="m-tb"><a href="/?menu=admin4ik&sel=users&sort=6" class="stn-sort">Приход</a></td>
	<td align="center" width="75" class="m-tb"><a href="/?menu=admin4ik&sel=users&sort=7" class="stn-sort">Расход</a></td>
	<td align="center" width="100" class="m-tb"><a href="/?menu=admin4ik&sel=users&sort=4" class="stn-sort">Зарегистрирован</a></td>
  </tr>


<?PHP

	while($data = $db->FetchArray()){
	
	?>
	<tr class="htt <? if($data['rs_referer_id'] > 0){ ?>rsuser<?} ?>">
    <td align="center"><?=$data["id"]; ?></td>
    <td align="left"><a <? if($data['banned']) echo 'style="color:red"'; ?> href="/?menu=admin4ik&sel=users&edit=<?=$data["id"]; ?>" class="stn"><?=$data["user"]; ?></a></td>
    <td align="left"><?if($data['vk_user_id'] > 0){?><a href="https://vk.com/id<?=$data['vk_user_id']?>" target="_blank" class="stn"><?=$data["vk_first_name"]; ?> <?=$data["vk_last_name"]; ?></a><?}else{?><center>-</center><?}?></td>
    <td align="center"><?if($data["come_url"] != '-'){?><a href="http://<?=$data["come_url"]?>" target="_blank"><?=$data["come_url"]; ?></a><?}?></td>
    <td align="center"><?=geodata($data["uip"]); ?></td>
    <td align="center"><?=sprintf("%.2f",$data["all_serebro"]); ?></td>
	<td align="center"><?=$data["all_trees"]; ?></td>
	<td align="center" <? if($data["insert_sum"] > 0){ echo 'style="font-weight: bold;color: green;"';} ?>><?=$data["insert_sum"]; ?></td>
	<td align="center" <? if($data["payment_sum"] > 0){ echo 'style="font-weight: bold;color: red;"';} ?>><?=$data["payment_sum"]; ?></td>
	<td align="center"><?=$func->time2word($data["date_reg"]); ?></td>
  	</tr>
	<?PHP
	
	}

?>

</table>
<BR />
<?PHP


}else echo "<center><b>На данной странице нет записей</b></center><BR />";

	if(isset($_GET["search"])){
	
	?>
	</div>
	<div class="clr"></div>	
	<?PHP
	
		return;
	
	}
	
$db->Query("SELECT COUNT(*) FROM db_users_a WHERE db_users_a.fake = 0");
$all_pages = $db->FetchRow();

	if($all_pages > 100){
	
	$sort_b = (isset($_GET["sort"])) ? intval($_GET["sort"]) : 0;
	
	$nav = new navigator;
	$page = (isset($_GET["page"]) AND intval($_GET["page"]) < 1000 AND intval($_GET["page"]) >= 1) ? (intval($_GET["page"])) : 1;
	
	echo "<BR /><center>".$nav->Navigation(10, $page, ceil($all_pages / 100), "/?menu=admin4ik&sel=users&sort={$sort_b}&page="), "</center>";
	
	}
?>
</div>
<div class="clr"></div>

