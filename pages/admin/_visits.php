
<?
include("./_geolocation/SxGeo.php");
$SxGeo = new SxGeo('./_geolocation/SxGeoCity.dat');

function geodata($ip){
    global $SxGeo;
    $geo_date = $SxGeo->getCityFull($ip);
    $country = $geo_date['country']['name_ru'];
    $city = $geo_date['city']['name_ru'];
    return $country.' / '.$city;
}

if(!isset($_GET['show'])) $_GET['show'] = 1;

?>

<div class="s-bk-lf">
    <div class="acc-title">Статистика проекта</div>
</div>
<div class="silver-bk">
    <div class="clr"></div>

    <style>
        #selected_admmenu{
            font-weight: bold;
        }
    </style>

    <center>
        <a <?if($_GET['show'] == 1) echo 'id="selected_admmenu"';?> href="/?menu=admin4ik&sel=visits&show=1">Переходы</a> || <a <?if($_GET['show'] == 2) echo 'id="selected_admmenu"';?> href="/?menu=admin4ik&sel=visits&show=2">Источники</a> || <a <?if($_GET['show'] == 3) echo 'id="selected_admmenu"';?> href="/?menu=admin4ik&sel=visits&show=3">Пользователи</a>
    </center>

    <br>

    <? if($_GET['show'] == 1){

        $num_p = (isset($_GET["page"]) AND intval($_GET["page"]) < 1000 AND intval($_GET["page"]) >= 1) ? (intval($_GET["page"]) -1) : 0;
        $lim = $num_p * 100;

        if(isset($_GET['ist'])){
            $ist = htmlentities($_GET['ist']);
            $dpget = "&ist={$ist}";
            $dpsql = "WHERE url = '{$ist}'";
        }

        if(isset($_GET['ref'])){
            $ref_id = intval($_GET['ref']);
            $dpget .= "&ref={$ref_id}";
            if($dpsql != ''){
                $dpsql .= " AND ref_id = {$ref_id}";
            }else{
                $dpsql = "WHERE ref_id = {$ref_id}";
            }
        }

        $href = "/?menu=admin4ik&sel=visits&show={$_GET['show']}{$dpget}&page=".($num_p + 1)."&sort=";

        if(isset($_GET['sort'])){
            $dpget .= "&sort={$_GET['sort']}";
            switch ($_GET['sort']){
                case 'visites': $order = 'cnt'; break;
                case 'page': $order = 'page'; break;
                case 'date': $order = 'date'; break;
                case 'ist': $order = 'url'; break;
                case 'id': $order = 'id'; break;
                case 'ip': $order = 'ip'; break;
                case 'ref_name': $order = 'user'; break;
                default: $order = 'id'; break;
            }
        }else{
            $order = "id";
        }

    ?>

    <table cellpadding='3' cellspacing='0' border='0' bordercolor='#336633' align='center' width="99%">
        <tr bgcolor="#efefef">
            <td style="border: 1px dashed #db8;" align="center" class="m-tb"><a style="color: white" href="<?=$href;?>id">№</a></td>
            <td style="border: 1px dashed #db8;" align="center" class="m-tb"><a style="color: white" href="<?=$href;?>date">Дата</a></td>
            <td style="border: 1px dashed #db8;" align="center" class="m-tb"><a style="color: white" href="<?=$href;?>ist">Источник</a></td>
            <td style="border: 1px dashed #db8;" align="center" class="m-tb"><a style="color: white" href="<?=$href;?>page">Страница</a></td>
            <td style="border: 1px dashed #db8;" align="center" class="m-tb"><a style="color: white" href="<?=$href;?>visites">Визиты</a></td>
            <td style="border: 1px dashed #db8;" align="center" class="m-tb"><a style="color: white" href="<?=$href;?>ip">IP</a></td>
            <td style="border: 1px dashed #db8;" align="center" class="m-tb"><a style="color: white" href="<?=$href;?>ip">Геолокация</a></td>
            <td style="border: 1px dashed #db8;" align="center" class="m-tb"><a style="color: white" href="<?=$href;?>ref_name">Реферер</a></td>
        </tr>
        <?PHP

        $db->Query("SELECT db_visits.id, page, reg, ref_id, INET_NTOA(db_visits.ip) AS ip, cnt, url, date, user FROM db_visits LEFT JOIN db_users_a ON db_users_a.id = db_visits.ref_id {$dpsql} ORDER BY {$order} DESC LIMIT {$lim}, 100");

        while ($data = $db->FetchArray()) {

            ?>
            <tr class="htt" <?if($data['reg'] > 0){?>style="background: #e0ffeb"<?}?>>
                <td style="border: 1px dashed #db8;" align="center"><?=$data['id']?></td>
                <td style="border: 1px dashed #db8;" align="center"><?=date("H:i:s",$data['date'])?></td>
                <td style="border: 1px dashed #db8;" class="clip" align="center"><a href="http://<?=$data['url']?>" target="_blank"><?=$data['url']?></a></td>
                <td style="border: 1px dashed #db8;" class="clip" align="center"><?=$data['page']?></td>
                <td style="border: 1px dashed #db8;" align="center"><?=$data['cnt']?></td>
                <td style="border: 1px dashed #db8;" align="center"><div class="banip"><?=$data['ip']?></div></td>
                <td style="border: 1px dashed #db8;" align="center"><?=geodata($data["ip"]); ?></td>
                <td style="border: 1px dashed #db8;" align="center"><a href="/?menu=admin4ik&sel=users&edit=<?=$data["ref_id"]; ?>" class="stn"><?=$data['user']?></a></td>
            </tr>
            <?PHP

        }

        ?>
    </table>

    <?php
    $db->Query("SELECT COUNT(*) FROM db_visits {$dpsql}");
    $all_pages = $db->FetchRow();

    if($all_pages > 100){

        $sort_b = (isset($_GET["sort"])) ? intval($_GET["sort"]) : 0;

        $nav = new navigator;
        $page = (isset($_GET["page"]) AND intval($_GET["page"]) < 1000 AND intval($_GET["page"]) >= 1) ? (intval($_GET["page"])) : 1;

        echo "<BR /><center>".$nav->Navigation(10, $page, ceil($all_pages / 100), "/?menu=admin4ik&sel=visits&show={$_GET['show']}{$dpget}&page="), "</center>";

    }
    ?>

    <? }else ?>

    <? if($_GET['show'] == 2){

        $num_p = (isset($_GET["page"]) AND intval($_GET["page"]) < 1000 AND intval($_GET["page"]) >= 1) ? (intval($_GET["page"]) -1) : 0;
        $lim = $num_p * 100;

        if(isset($_GET['ref'])){
            $ref_id = intval($_GET['ref']);
            $dpget = "&ref={$ref_id}";
            $dpsql = "WHERE ref_id = '{$ref_id}'";
        }

        $href = "/?menu=admin4ik&sel=visits&show={$_GET['show']}{$dpget}&page=".($num_p + 1)."&sort=";

        if(isset($_GET['sort'])){
            $dpget .= "&sort={$_GET['sort']}";
            switch ($_GET['sort']){
                case 'pn': $order = 'visites'; break;
                case 'ist': $order = 'url'; break;
                case 'visites': $order = 'visites'; break;
                case 'refers': $order = 'users'; break;
                case 'logins': $order = 'auth'; break;
                default: $order = 'visites'; break;
            }
        }else{
            $order = "visites";
        }

    ?>

        <table cellpadding='3' cellspacing='0' border='0' bordercolor='#336633' align='center' width="99%">
            <tr bgcolor="#efefef">
                <td style="border: 1px dashed #db8;" align="center" class="m-tb"><a style="color: white" href="<?=$href;?>pn">п.н.</a></td>
                <td style="border: 1px dashed #db8;" align="center" class="m-tb"><a style="color: white" href="<?=$href;?>ist">Источник</a></td>
                <td style="border: 1px dashed #db8;" align="center" class="m-tb"><a style="color: white" href="<?=$href;?>link">Переходы</a></td>
                <td style="border: 1px dashed #db8;" align="center" class="m-tb"><a style="color: white" href="<?=$href;?>refers">Рефереры</a></td>
                <td style="border: 1px dashed #db8;" align="center" class="m-tb"><a style="color: white" href="<?=$href;?>logins">Авторизации</a></td>
                <td style="border: 1px dashed #db8;" align="center" class="m-tb">Параметры</td>
            </tr>
            <?PHP

            $db->Query("SELECT COUNT(DISTINCT url) FROM db_visits {$dpsql}");
            $max_num = $db->FetchRow();

            $pn = $max_num - $lim;

            $db->Query("SELECT COUNT(db_visits.id) visites, COUNT(DISTINCT ref_id) users, ABS(COUNT(DISTINCT db_visits.reg)-1) auth ,db_visits.id, page, reg, ref_id, INET_NTOA(db_visits.ip) AS ip, cnt, url, date, user FROM db_visits LEFT JOIN db_users_a ON db_users_a.id = db_visits.ref_id {$dpsql} GROUP BY url ORDER BY {$order} DESC LIMIT {$lim}, 100");

            while ($data = $db->FetchArray()) {

                ?>
                <tr class="htt">
                    <td style="border: 1px dashed #db8;" align="center"><?=$pn--?></td>
                    <td style="border: 1px dashed #db8;" class="clip" align="center"><a href="http://<?=$data['url']?>" target="_blank"><?=$data['url']?></a></td>
                    <td style="border: 1px dashed #db8;" align="center"><?=$data['visites']?></td>
                    <td style="border: 1px dashed #db8;" align="center"><?=$data['users']?></td>
                    <td style="border: 1px dashed #db8;" align="center"><?=$data['auth']?></td>
                    <td style="border: 1px dashed #db8;" align="center"><a href="/?menu=admin4ik&sel=visits&show=1&ist=<?=$data['url'].$dpget;?>">подробнее</a></td>
                </tr>
                <?PHP

            }

            ?>
        </table>

        <?php
        $db->Query("SELECT COUNT(DISTINCT url) FROM db_visits {$dpsql}");
        $all_pages = $db->FetchRow();

        if($all_pages > 100){

            $sort_b = (isset($_GET["sort"])) ? intval($_GET["sort"]) : 0;

            $nav = new navigator;
            $page = (isset($_GET["page"]) AND intval($_GET["page"]) < 1000 AND intval($_GET["page"]) >= 1) ? (intval($_GET["page"])) : 1;

            echo "<BR /><center>".$nav->Navigation(10, $page, ceil($all_pages / 100), "/?menu=admin4ik&sel=visits&show={$_GET['show']}{$dpget}&page="), "</center>";

        }
        ?>

    <? }else ?>

    <? if($_GET['show'] == 3){

        $num_p = (isset($_GET["page"]) AND intval($_GET["page"]) < 1000 AND intval($_GET["page"]) >= 1) ? (intval($_GET["page"]) -1) : 0;
        $lim = $num_p * 100;

        $href = "/?menu=admin4ik&sel=visits&show={$_GET['show']}{$dpget}&page=".($num_p + 1)."&sort=";

        if(isset($_GET['sort'])){
            $dpget .= "&sort={$_GET['sort']}";
            switch ($_GET['sort']){
                case 'pn': $order = 'visites'; break;
                case 'refer': $order = 'user'; break;
                case 'ist': $order = 'ists'; break;
                case 'logins': $order = 'auth'; break;
                case 'links': $order = 'visites'; break;
                case 'visitesc': $order = 'cnts'; break;
                default: $order = 'visites'; break;
            }
        }else{
            $order = "visites";
        }

    ?>

        <table cellpadding='3' cellspacing='0' border='0' bordercolor='#336633' align='center' width="99%">
            <tr bgcolor="#efefef">
                <td style="border: 1px dashed #db8;" align="center" class="m-tb"><a style="color: white" href="<?=$href;?>pn">п.н.</a></td>
                <td style="border: 1px dashed #db8;" align="center" class="m-tb"><a style="color: white" href="<?=$href;?>refer">Реферер</a></td>
                <td style="border: 1px dashed #db8;" align="center" class="m-tb"><a style="color: white" href="<?=$href;?>ist">Источники</a></td>
                <td style="border: 1px dashed #db8;" align="center" class="m-tb"><a style="color: white" href="<?=$href;?>logins">Авторизации</a></td>
                <td style="border: 1px dashed #db8;" align="center" class="m-tb"><a style="color: white" href="<?=$href;?>links">Переходы</a></td>
                <td style="border: 1px dashed #db8;" align="center" class="m-tb"><a style="color: white" href="<?=$href;?>visites">Визиты</a></td>
            </tr>
            <?PHP

            $db->Query("SELECT COUNT(DISTINCT ref_id) FROM db_visits");
            $max_num2 = $db->FetchRow();

            $pn2 = $max_num2 - $lim;

            $db->Query("SELECT SUM(db_visits.cnt) cnts, COUNT(db_visits.id) visites, ABS(COUNT(DISTINCT db_visits.reg)-1) auth, COUNT(DISTINCT url) ists ,db_visits.id, page, reg, ref_id, INET_NTOA(db_visits.ip) AS ip, cnt, url, date, user FROM db_visits LEFT JOIN db_users_a ON db_users_a.id = db_visits.ref_id GROUP BY ref_id ORDER BY {$order} DESC LIMIT {$lim}, 100");

            while ($data = $db->FetchArray()) {

                ?>
                <tr class="htt">
                    <td style="border: 1px dashed #db8;" align="center"><?=$pn2--;?></td>
                    <td style="border: 1px dashed #db8;" align="center"><a href="/?menu=admin4ik&sel=users&edit=<?=$data["ref_id"]; ?>" class="stn"><?=$data['user']?></a></td>
                    <td style="border: 1px dashed #db8;" align="center"><a href="/?menu=admin4ik&sel=visits&show=2&ref=<?=$data['ref_id']?>"><?=$data['ists']?></a></td>
                    <td style="border: 1px dashed #db8;" align="center"><?=$data['auth']?></td>
                    <td style="border: 1px dashed #db8;" align="center"><?=$data['visites']?></td>
                    <td style="border: 1px dashed #db8;" align="center"><?=$data['cnts']?></td>
                </tr>
                <?PHP
                $pn++;
            }

            ?>
        </table>

        <?php
        $db->Query("SELECT COUNT(DISTINCT ref_id) FROM db_visits");
        $all_pages = $db->FetchRow();

        if($all_pages > 100){

            $sort_b = (isset($_GET["sort"])) ? intval($_GET["sort"]) : 0;

            $nav = new navigator;
            $page = (isset($_GET["page"]) AND intval($_GET["page"]) < 1000 AND intval($_GET["page"]) >= 1) ? (intval($_GET["page"])) : 1;

            echo "<BR /><center>".$nav->Navigation(10, $page, ceil($all_pages / 100), "/?menu=admin4ik&sel=visits&show={$_GET['show']}&page="), "</center>";

        }
        ?>

    <? } ?>
    
</div>
<div class="clr"></div>

<style>
    .banip{
        cursor: pointer;
        color: #0331b1;
    }
    .banip:hover{
        text-decoration: underline;
    }
</style>

<script>
    $('.banip').click(function(){
        var ip = $(this).text();
        if(confirm('Добавить ip-адрес: ' + ip + ' в черный список?')){
            $.get("/ipban.php?ip=" + ip,
                function(data) {
                    alert(data);
                });
        }
    });
</script>

