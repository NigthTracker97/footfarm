<?php

$db->Query('SET CHARACTER SET utf8');
$db->Query("set names utf8");

$_OPTIMIZATION["title"] = "Пополнение рекламного баланса";
$order_id = 0;
?>
<div class="container">

<div style="background-color: #ffffff;color: #484847;padding: 0px 10px 0px 0px;width: 100%;">
Ввод средств позволяет автоматически приобрести рекламный баланс с помощью различных платежных систем: Yandex Деньги, банковских карт, SMS, терминалов, денежных переводов и т.д.
Оплата и зачисление на баланс производится в автоматическом режиме. Введите сумму в РУБЛЯХ, которую Вы хотите пополнить на баланс. 
</div>

<?PHP

# Настройки
$sonfig_site = $func->config_site($db);

    if (isset($_POST['payment_system']) && $_POST['payment_system'] == 'webmoney_info') {
        ?>
        <div class="insert_table">
            <h3 style="margin-top: 0px;">ИНСТРУКЦИЯ: пополнение игрового баланса через WebMoney</h3>
            <div>
                <p><b>1-й шаг.</b> Перейти на сайт <a href="https://www.bestchange.ru/?p=65791">www.bestchange.ru</a>.
                </p>
                <p><b>2-й шаг.</b> Выберите в левом блоке WebMoney валюту, а в правом любой имеющийся у Вас кошелек
                    друой платежной системы (PAYEER, QIWI, Яндекс.Деньги и др.).</p>
                <p><b>3-й шаг.</b> Выберите подходящий Вам обменник с выгодным курсом обмена.</p>
                <p><b>4-й шаг.</b> Перейдите на сайт обменника и введите данные для совершения операции.</p>
                <p><b>5-й шаг.</b> После выполнения операции произведите пополнение с того кошелька, на который обменяли
                    свои WebMoney деньги.
            </div>
            <center><a href="https://www.bestchange.ru/?p=65791"><input type="button"
                                                                        value="Перейти на www.bestchange.ru"></a>
            </center>
        </div>
        <?
        return;
    }

    /// db_payeer_insert
if (isset($_POST["sum"])){

    if (floatval($_POST['sum']) >= $sonfig_site['insert_min'] || $order_id > 0){

    if ($order_id > 0) {
        $_POST["sum"] = $order_data['money'];
        $desc = htmlspecialchars($order_data['desc']);
    } else {
        $desc= "Пополнение рекламного баланса на проекте " . $_SERVER["HTTP_HOST"] . " - USER " . $_SESSION["user"];
    }

    $time = time();

    $sum = round(floatval($_POST["sum"]), 2);

    $ps_array = explode(":",$_POST['payment_system']);
    $_POST['payment_system'] = $ps_array[0];

    if(isset($ps_array[1])) $_POST['payment_system_id'] = $ps_array[1]; else $_POST['payment_system_id'] = '';

    if ($order_id > 0 && $_POST['payment_system'] == 'payser') {

        $db->Query("SELECT money_p, insert_sum FROM db_users_b WHERE id = {$_SESSION['user_id']}");
        $user_data = $db->FetchAssoc();
        $user_ser = $user_data['money_p'];
        $user_insert = $user_data['insert_sum'];
        $need_ser = $user_curse * $order_data['money'];
        if ($user_insert >= $sonfig_site['payment_unlock']) {
            if ($user_ser >= $need_ser) {
                $insert = new insert($db);
                if ($insert->payment_order_ser($order_id, $_SESSION['user_id'])) {
                    $db->Query("UPDATE db_users_b SET money_p = money_p - {$need_ser} WHERE id = {$_SESSION['user_id']}");
                    echo "<center><b style='color:green'>Операция оплаты счета выполнена успешно!</b></center>";
                }else {
                    echo "<center><b style='color:red'>Ошибка выполнения операции!</b></center>";
                    echo "<br/><br/>";
                    echo "<center><a href='/finance/insert/order/{$order_id}'>Выбрать другой способ оплаты</a></center>";
                }
            }else{
                echo "<center><b style='color:red'>Не достаточно серебра для оплаты данного счета!</b></center>";
                echo "<br/><br/>";
                echo "<center><a href='/finance/insert/order/{$order_id}'>Выбрать другой способ оплаты</a></center>";
            }
        }else{
            echo "<center><b style='color:red'>Пополнения с счета для вывода доступно только для пользователей, которые пополняли свой баланс на общую сумму более {$sonfig_site['payment_unlock']} руб.!</b></center>";
            echo "<br/><br/>";
            echo "<center><a href='/finance/insert/order/{$order_id}'>Выбрать другой способ оплаты</a></center>";
        }
        echo "<br/><br/>";
        
    }else if ($_POST['payment_system'] == 'freekassa') {

        $curr_id = $_REQUEST['payment_system_id'];

        # Заносим в БД
        $db->Query("INSERT INTO db_money_insert SET `user_id` = {$_SESSION["user_id"]}, `user` = '{$_SESSION["user"]}', `adv` = '1', `payment_system` = 'FREEKASSA', `sum` = {$sum}, date_add = {$time}, order_id = {$order_id}");
        $opid = $db->LastInsert();
        $sum = number_format($sum, 2, ".", "");
        $id = $db->LastInsert();

        $sign = md5("{$config->fk_merchant_id}:{$sum}:{$config->fk_merchant_sword}:{$id}");

        $get_array = array(
            "m" => $config->fk_merchant_id,
            "oa" => $sum,
            "o" => $id,
            "s" => $sign,
            "lang" => 'ru',
            "us_login" => $_SESSION["user"],
			"i" => $curr_id
        );
        $db->Query("SELECT email FROM db_users_a WHERE id = {$_SESSION['user_id']}");
        $user_email = $db->FetchRow();
        $get_array['em'] = $user_email;

        header("Location: //www.free-kassa.ru/merchant/cash.php?" . http_build_query($get_array));

    } else if ($_POST['payment_system'] == 'interkassa') {

        $db->Query("INSERT INTO db_money_insert SET `user_id` = {$_SESSION["user_id"]}, `user` = '{$_SESSION["user"]}', `adv` = '1', `payment_system` = 'INTERKASSA', `sum` = {$sum}, `date_add` = {$time}, `order_id` = {$order_id}");
        $opid = $db->LastInsert();

        ?>
        <center>
        <form id="wmoplata" name="payment" method="post" action="https://sci.interkassa.com/" accept-charset="UTF-8">
            <input type="hidden" name="ik_co_id" value="<?=$config->ik_shopId;?>" />
            <input type="hidden" name="ik_pm_no" value="<?=$opid?>" />
            <? if($_POST['payment_system_id'] != ''){ ?>
                <input type="hidden" name="ik_pw_via" value="<?=$_POST['payment_system_id']?>" />
            <? } ?>
            <input type="hidden" name="ik_am" value="<?=$sum;?>" />
            <input type="hidden" name="ik_desc" value="<?=htmlspecialchars($desc); ?>" />
            <input type="submit" value="Перейти к оплате">
        </form>
        </center>
        <?

    } else if ($_POST['payment_system'] == 'webmoney') {

    # Заносим в БД
    $db->Query("INSERT INTO db_money_insert SET `user_id` = {$_SESSION["user_id"]}, `user` = '{$_SESSION["user"]}', `adv` = '1', `payment_system` = 'WEBMONEY', `sum` = {$sum}, date_add = {$time}, order_id = {$order_id}");
    $opid = $db->LastInsert();
    $db->Query("SELECT email FROM db_users_a WHERE id = {$_SESSION['user_id']}");
    $user_email = $db->FetchRow();
    ?>
        <center>

            <p>Переход на сайт WebMoney произойдет автоматически, если этого не случилось, нажмите на кнопку "Перейти к
                оплате".</p>

            <form id="wmoplata" method="POST" action="https://merchant.webmoney.ru/lmi/payment.asp?at=authtype_8"
                  accept-charset="windows-1251">
                <input type="hidden" name="LMI_PAYMENT_AMOUNT" value="<?= number_format($sum, 2, ".", ""); ?>">
                <input type="hidden" name="LMI_PAYMENT_DESC_BASE64" value="<?= base64_encode($desc); ?>">
                <input type="hidden" name="LMI_PAYMENT_NO" value="<?= $opid; ?>">
                <input type="hidden" name="LMI_PAYEE_PURSE" value="<?= $config->wm_purse_wmr; ?>">
                <input type="hidden" name="LMI_PAYMER_EMAIL" value="<?= $user_email; ?>">
                <input type="submit" value="Перейти к оплате">
            </form>

        </center>
        <?

    } else if ($_POST['payment_system'] == 'yandex') {

        # Заносим в БД
        $db->Query("INSERT INTO db_money_insert SET `user_id` = {$_SESSION["user_id"]}, `user` = '{$_SESSION["user"]}', `adv` = '1', `payment_system` = 'YANDEX', `sum` = {$sum}, date_add = {$time}, order_id = {$order_id}");
        $opid = $db->LastInsert();
        $sum = $sum + ($sum * 0.005);
        ?>
        <center>
        <form id="wmoplata" method="POST" action="https://money.yandex.ru/quickpay/confirm.xml">
            <input type="hidden" name="receiver" value="<?=$config->ya_purse;?>">
            <input type="hidden" name="formcomment" value="<?= $desc; ?>">
            <input type="hidden" name="short-dest" value="<?= $desc; ?>">
            <input type="hidden" name="label" value="<?=$opid;?>">
            <input name="quickpay-form" type="hidden" value="shop">
            <input type="hidden" name="targets" value="транзакция <?=$opid;?>">
            <input type="hidden" name="sum" value="<?= number_format($sum, 2, ".", ""); ?>" data-type="number">
            <input type="hidden" name="comment" value="проект <?=$config->site_name;?>">
            <input type="hidden" name="comment-needed" value="false">
            <input type="hidden" name="need-fio" value="false">
            <input type="hidden" name="need-email" value="false">
            <input type="hidden" name="need-phone" value="false">
            <input type="hidden" name="need-address" value="false">
            <input type="hidden" name="writable-targets" value="false">
            <input type="hidden" name="writable-sum" value="false">
            <input type="hidden" name="paymentType" value="PC">
<!--            <input type="hidden" name="paymentType" value="AC">-->
            <input type="submit" value="Перейти к оплате">
        </form>
        </center>

        <?
    } else if ($_POST['payment_system'] == 'megakassa'){
		$db->Query("INSERT INTO db_money_insert SET `user_id` = {$_SESSION["user_id"]}, `user` = '{$_SESSION["user"]}', `adv` = '1', `payment_system` = 'MEGAKASSA', `sum` = {$sum}, date_add = {$time}, order_id = {$order_id}");
        $opid = $db->LastInsert();
		$shop_id		= '2827';
		$amount			= number_format($sum, 2, '.', ''); // -> "100.50"
		$currency		= 'RUB'; // или "USD", "EUR"
		$description	= $desc;
		$order_id		= $opid;
		$method_id		= '';
		$client_email	= '';
		$debug			= ''; // или "1"
		$secret_key		= '199433146a562ea9'; // из настроек сайта в личном кабинете
		$signature		= md5($secret_key.md5(join(':', array($shop_id, $amount, $currency, $description, $order_id, $method_id, $client_email, $debug, $secret_key))));
		$language		= 'ru'; // или 'en'
		
		$get_array = array(
            "shop_id" => $shop_id,
            "amount" => $sum,
            "currency" => $currency,
            "order_id" => $opid,
            "description" => $desc,
            "method_id" => $method_id,
			"client_email" => $client_email,
			"debug" => $debug,
			"signature" => $signature,
			"language" => $language
        );
		
		header("Location: https://megakassa.ru/merchant/?" . http_build_query($get_array));
		
    } else if ($_POST['payment_system'] == 'anypay'){
		$shop_id = '2342';
		$secret_key = '81b20eda412';
		$currency = 'RUB';
		$db->Query("INSERT INTO db_money_insert SET `user_id` = {$_SESSION["user_id"]}, `user` = '{$_SESSION["user"]}', `adv` = '1', `payment_system` = 'ANYPAY', `sum` = {$sum}, date_add = {$time}, order_id = {$order_id}");
        $opid = $db->LastInsert();
		$sign = md5($currency.':'.$sum.':'.$secret_key.':'.$shop_id.':'.$opid);
		$curr_id = $_REQUEST['payment_system_id'];
		
		$get_array = array(
            "merchant_id" => $shop_id,
            "amount" => $sum,
            "currency" => $currency,
            "pay_id" => $opid,
            "desc" => $desc,
            "method" => $curr_id,
			"sign" => $sign
        );
		
		header("Location: https://any-pay.org/merchant?" . http_build_query($get_array));
		
	} else {

        # Заносим в БД
        $db->Query("INSERT INTO db_money_insert SET `user_id` = {$_SESSION["user_id"]}, `user` = '{$_SESSION["user"]}', `adv` = '1', `payment_system` = 'PAYEER', `sum` = {$sum}, date_add = {$time}, order_id = {$order_id}");
        $opid = $db->LastInsert();
        $m_shop = $config->shopID;
        $m_orderid = $opid;
        $m_amount = number_format($sum, 2, ".", "");
        $m_curr = "RUB";
        $m_desc = base64_encode($desc);
        $m_key = $config->secretW;

        $arHash = array(
            $m_shop,
            $m_orderid,
            $m_amount,
            $m_curr,
            $m_desc,
            $m_key
        );
        $sign = strtoupper(hash('sha256', implode(":", $arHash)));
        $get_array = array(
            "m_shop" => $config->shopID,
            "m_orderid" => $m_orderid,
            "m_amount" => number_format($sum, 2, ".", ""),
            "m_curr" => "RUB",
            "m_desc" => $m_desc,
            "m_sign" => $sign,
            "m_process" => "Оплатить и получить серебро"
        );

        header("Location: //payeer.com/api/merchant/m.php?" . http_build_query($get_array));

    }

    ?>
    <script>
        $(document).ready(function () {
            $("#wmoplata").submit();
        });
    </script>
    <div class="clr"></div>
</div>
<?PHP

return;
} else echo "<center><b style='color:red'>Ошибка! Минимальная сумма пополнения: {$sonfig_site['insert_min']} руб.</b></center><br/>";
}
?>

<div id="error3"></div>
<? if ($order_id == 0) { ?>
    <center>
        <form method="POST" action="">
			<span style="margin-bottom: 10px;text-align: left;color: #484847;margin-top: 0px;font-size: 14px;text-transform: uppercase;font-weight: bold;"> Введите сумму [Руб.]:  </span>
			<input style="color: #1f3642;font-size: 14px;border: 2px solid #CDCDCD;padding: 7px 18px 7px;outline: 0;font-weight: 600;line-height: 16px;width: 100%;margin-bottom: 0px;border-radius: 4px; font-family: 'Open Sans', sans-serif;" type="text" value="100" name="sum" size="7" id="psevdo" onchange="calculate(this)" onkeyup="calculate(this)" onfocusout="calculate(this)" onactivate="calculate(this)" ondeactivate="calculate(this)"> 
			<input type="hidden" id="payment_system" name="payment_system" value="payeer">
			<input type="hidden" id="payment_system_id" name="payment_system_id" value="0">
			<div class="form-group" style="margin-top:10px; width: 150px;">
				<input type="submit" value="Перейти к оплате" class="buy_farm-btn">
			</div>
			
			<span class="divider-2 index-s-divider"></span>
			
			<ul class="pay active group-0">
				<li data-id="114" data-group_id="2" class="active"><img src="/images/fc/114.png" alt=""></li>
				<li data-id="133" data-group_id="4" style="background: black;border-color: black;"><img src="/images/fc/mk.png" alt=""></li>
				<li data-id="133" data-group_id="1"><img src="/images/fc/133.png" alt=""></li>
				<li data-id="155" data-group_id="1"><img src="/images/fc/155.png" alt=""></li>
				<li data-id="161" data-group_id="1"><img src="/images/fc/161.png" alt=""></li>
				<li data-id="123" data-group_id="1"><img src="/images/fc/123.png" alt=""></li>
				<li data-id="45" data-group_id="1"><img src="/images/fc/45.png" alt=""></li>
				<li data-id="162" data-group_id="1"><img src="/images/fc/162.png" alt=""></li>
				<li data-id="94" data-group_id="1"><img src="/images/fc/94.png" alt=""></li>
				<li data-id="146" data-group_id="1"><img src="/images/fc/146.png" alt=""></li>
				<li data-id="174" data-group_id="1"><img src="/images/fc/174.png" alt=""></li>
				<li data-id="147" data-group_id="1"><img src="/images/fc/147.png" alt=""></li>
				<li data-id="166" data-group_id="1"><img src="/images/fc/166.png" alt=""></li>
				<li data-id="163" data-group_id="1"><img src="/images/fc/163.png" alt=""></li>
				<li data-id="167" data-group_id="1"><img src="/images/fc/167.png" alt=""></li>
				<li data-id="168" data-group_id="1"><img src="/images/fc/168.png" alt=""></li>
				<li data-id="169" data-group_id="1"><img src="/images/fc/169.png" alt=""></li>
				<li data-id="170" data-group_id="1"><img src="/images/fc/170.png" alt=""></li>
				<li data-id="171" data-group_id="1"><img src="/images/fc/171.png" alt=""></li>
				<li data-id="165" data-group_id="1"><img src="/images/fc/165.png" alt=""></li>
				<li data-id="164" data-group_id="1"><img src="/images/fc/164.png" alt=""></li>
				<li data-id="116" data-group_id="1"><img src="/images/fc/116.png" alt=""></li>
				<li data-id="154" data-group_id="1"><img src="/images/fc/154.png" alt=""></li>
				<li data-id="64" data-group_id="1"><img src="/images/fc/64.png" alt=""></li>
				<li data-id="82" data-group_id="1"><img src="/images/fc/82.png" alt=""></li>
				<li data-id="84" data-group_id="1"><img src="/images/fc/84.png" alt=""></li>
				<li data-id="132" data-group_id="1"><img src="/images/fc/132.png" alt=""></li>
				<li data-id="83" data-group_id="1"><img src="/images/fc/83.png" alt=""></li>
				<li data-id="150" data-group_id="1"><img src="/images/fc/150.png" alt=""></li>
				<!-- Anypay -->
				<li data-id="ab" data-group_id="3"><img src="/images/ap/ab.png" alt=""></li>
				<li data-id="psb" data-group_id="3"><img src="/images/ap/psb.png" alt=""></li>
				<li data-id="pv" data-group_id="3"><img src="/images/ap/pv.png?1" alt=""></li>
				<li data-id="rs" data-group_id="3"><img src="/images/ap/rs.png" alt=""></li>
				<li data-id="sb" data-group_id="3"><img src="/images/ap/sb.png" alt=""></li>
				<li data-id="sz" data-group_id="3"><img src="/images/ap/sz.png" alt=""></li>
				<li data-id="es" data-group_id="3"><img src="/images/ap/es.png" alt=""></li>
				<li data-id="unistream" data-group_id="3"><img src="/images/ap/unistream.png" alt=""></li>
				<li data-id="w1" data-group_id="3"><img src="/images/ap/w1.png?1" alt=""></li>
				<li data-id="term" data-group_id="3"><img src="/images/ap/term.png" alt=""></li>
				<li data-id="bank" data-group_id="3"><img src="/images/ap/bank.png" alt=""></li>
			</ul>
		<script>
			$(".pay li").click(function(){
				var self = this;
				$(".pay li").removeClass('active');
				$(this).addClass('active');
				switch($(this).data('group_id')){
					case 1:
						$("#payment_system").val('freekassa');
						$("#payment_system_id").val($(self).data('id'));
					break;
					case 2:
						$("#payment_system").val('payeer');
						$("#payment_system_id").val('0');
					break;
					case 3:
						$("#payment_system").val('anypay');
						$("#payment_system_id").val($(self).data('id'));
					break;
					case 4:
						$("#payment_system").val('megakassa');
						$("#payment_system_id").val($(self).data('id'));
					break;
				}
			});
		</script>
		
		<style>
		.pay.active {
			margin-top: 20px;
			display: block;
		}
		
		.pay li {
			float: left;
			width: 164px;
			height: 94px;
			line-height: 94px;
			text-align: center;
			box-sizing: border-box;
			background: #fff;
			border-bottom: 4px solid #f1f1f1;
			border-radius: 5px;
			box-shadow: 0 0 20px rgba(0,0,0,0.35);
			position: relative;
			cursor: pointer;
			transition: .5s;
			margin: 10px auto;
			margin-left: 10px;
		}
		
		.pay img {
			vertical-align: middle;
			max-width: 169px;
			max-height: 94px;
		}
		
		.pay li:after {
			background: url(/images/fc/act.png) no-repeat 0 0;
			width: 31px;
			height: 31px;
			position: absolute;
			top: 0;
			left: 0;
			content: '';
			opacity: 0;
			transition: 0.5s;
		}
		
		.pay li:hover {
			box-shadow: 0 0 25px rgba(0,0,0,0.75);
		}
		
		.pay .active:after {
			opacity: 1;
		}	
        </form>
    </center>
<? } else { ?>
    <center>
        <center><?= $order_data['desc'] ?></center>
        <br>
        <form method="POST" action="">
            <table width="450" class="insert_table">
                <tr>
                    <td>Счет:</td>
                    <td align="right"><b>№<?= $order_data['id']; ?> от <?= date("d.m.Y г.", $order_data['date']); ?></b>
                    </td>
                </tr>
                <tr style="display: none" class="payrub">
                    <td>К оплате:</td>
                    <td align="right"><b><?= $order_data['money']; ?> <?= $config->VAL ?></b></td>
                </tr>
                <tr style="display: none" class="payser">
                    <td>К оплате:</td>
                    <td align="right"><b><?= $order_data['money'] * $user_curse; ?> серебра</b></td>
                </tr>
                <tr>
				
				
				
				<center>  <br><select name="payment_system" class="psSelect" style="color: #1f3642;font-weight: 600;font-size: 14px;border: 2px solid #CDCDCD;padding: 5px 18px 5px;outline: 0;line-height: 16px;width: 100%;margin-bottom: 10px;border-radius: 4px; font-family: 'Open Sans', sans-serif;">
                            <option value="payeer">PAYEER кошелек</option>
                          <!--                            <option value="interkassa:qiwi_ipaycard_merchant_rub">QIWI кошелек</option>
         <option value="freekassa:63">QIWI кошелек</option>
                     <option value="freekassa:94">VISA/MASTERCARD RUB</option>
                            <option value="freekassa:45">Яндекс.Деньги</option>
                            <option value="yandex">Яндекс.Деньги</option>
                            <option value="webmoney">WebMoney кошелек</option>-->
                            <option value="freekassa">Другие варианты</option>
                        </select>
				
				
               
                    <td align="center">
                        <input type="hidden" name="sum" value="<?= $order_data['money'] ?>">
                        <input style="width: 153px;" type="submit" id="submit" value="Оплатить">
                    </td>
                </tr>
                <tr class="payser">
                    <td colspan="2">Оплата с счета для вывода производится в серебре по текущему курсу пользователя для
                        обмена серебра на рубли, используемого для вывода средств и равного: <?= $user_curse; ?> серебра
                        = 1 <?= $config->VAL ?></td>
                </tr>
            </table>
            <BR/><BR/> 
            <script>
                function update_tr() {
                    var ps = $("select[name=payment_system]").val();
                    if (ps == 'payser') {
                        $(".payser").show();
                        $(".payrub").hide();
                    } else {
                        $(".payser").hide();
                        $(".payrub").show();
                    }
                }
                $(window).load(function () {
                    $("select[name=payment_system]").change(function () {
                        update_tr()
                    });
                });
                update_tr();
            </script>
        </form>
    </center>
<? } ?>
<script type="text/javascript">
    calculate(100);
</script>
<center>

    <BR/>

</center>
<BR/><BR/>

<style>
	  #tbonus_tbl {
  
 
    box-sizing: border-box;
    font-size: 10pt;
    background: rgba(226,226,226,1);
    text-transform: uppercase;
 
    -moz-box-shadow: 0px 0px 26px -6px rgba(94,182,79,0.85);
    box-shadow: 0px 0px 26px -6px #484847;
    background: -moz-linear-gradient(45deg, rgba(226,226,226,1) 0%, rgba(219,219,219,1) 50%, rgba(209,209,209,1) 51%, rgba(254,254,254,1) 100%);
    background: -webkit-gradient(left bottom, right top, color-stop(0%, rgba(226,226,226,1)), color-stop(50%, rgba(219,219,219,1)), color-stop(51%, rgba(209,209,209,1)), color-stop(100%, rgba(254,254,254,1)));
    background: -webkit-linear-gradient(45deg, rgba(226,226,226,1) 0%, rgba(219,219,219,1) 50%, rgba(209,209,209,1) 51%, rgba(254,254,254,1) 100%);
    background: -o-linear-gradient(45deg, rgba(226,226,226,1) 0%, rgba(219,219,219,1) 50%, rgba(209,209,209,1) 51%, rgba(254,254,254,1) 100%);
    background: -ms-linear-gradient(45deg, rgba(226,226,226,1) 0%, rgba(219,219,219,1) 50%, rgba(209,209,209,1) 51%, rgba(254,254,254,1) 100%);
    background: linear-gradient(45deg, rgba(226,226,226,1) 0%, rgba(219,219,219,1) 50%, rgba(209,209,209,1) 51%, rgba(254,254,254,1) 100%);
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#e2e2e2', endColorstr='#fefefe', GradientType=1 );
    margin: 6px auto;
    padding: 10px;
    position: relative;
    text-align: center;
    }
	</style></center>
<BR /><BR />
</div>


