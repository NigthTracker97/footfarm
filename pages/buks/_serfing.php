<?php
# Настройки
$db->Query('SET CHARACTER SET utf8');
	$db->Query("set names utf8");
$sonfig_site = $func->config_site($db);
$_OPTIMIZATION["title"] = "Серфинг";
if($sonfig_site['serf_enable'] == 0 && !$admin){ @include("pages/_404.php"); return; }
if(empty($_SESSION["user_id"])){
	@include("pages/_404.php"); return;
}
$paymeny_money_type = array(
    '0' => 'сер.',
    '1' => 'сер.',
    '2' => 'руб.',
    '3' => 'CP'
);
$paymeny_money_name = array(
    '0' => 'на счет для покупок',
    '1' => 'на счет для вывода',
    '2' => 'на реферальный счет',
    '3' => 'в CP'
);

define('TIME', time());
define('VALUTA', $paymeny_money_type[$sonfig_site['serf_money_type']]);

if (isset($_GET['delete'])) {
    $id = intval($_GET['delete']);
    if ($admin) {
        $db->query("DELETE FROM db_serfing WHERE id = '" . $id . "'");
        $db->query("DELETE FROM db_serfing_view WHERE ident = '" . $id . "'");
    }
}
?>
<script>

    function getHTTPRequest() {
        var req = false;
        try {
            req = new XMLHttpRequest();
        } catch (err) {
            try {
                req = new ActiveXObject("MsXML2.XMLHTTP");
            } catch (err) {
                try {
                    req = new ActiveXObject("Microsoft.XMLHTTP");
                } catch (err) {
                    req = false;
                }
            }
        }
        return req;
    }

    jQuery(document).ready(function () {
        $(".normalm").click(function (e) {
            var oLeft = 0, oTop = 0;
            element = this;
            if (element.className == 'normalm') {
                do {
                    oLeft += element.offsetLeft;
                    oTop += element.offsetTop;
                } while (element = element.offsetParent);
                var sx = e.pageX - oLeft;
                var sy = e.pageY - oTop;
                var elid = $(this).attr("id");
                fixed(elid, sx, sy);
            }
        });
    })

    function goserf(obj) {
		$(obj).closest( ".panel" ).addClass('surfblockopen');
		
        //return false;
    }

    function fixed(p1, p2, p3) {
        var myReq = getHTTPRequest();
        var params = "p1=" + p1 + "&p2=" + p2 + "&p3=" + p3;

        function setstate() {
            if ((myReq.readyState == 4) && (myReq.status == 200)) {
                var resvalue = myReq.responseText;
                if (resvalue != '') {
                    if (resvalue.length > 12) {
                        if (elem = document.getElementById(p1)) {
                            elem.style.backgroundImage = 'none';
                            elem.className = 'goadvsite';
                            elem.innerHTML = '<div><a target="_blank" href="/' + resvalue + '" onclick="javascript:goserf(this);">Просмотреть сайт рекламодателя</a></div>';
                        }
                    } else {
                        if (elem = document.getElementById(resvalue)) {
                            $(elem).fadeOut('low', function () {
                                elem.innerHTML = "<td colspan='3'></td>";
                            });
                        }
                    }
                }
            }
        }

        myReq.open("POST", "/ajax/us-fixedserf.php", true);
        myReq.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        myReq.setRequestHeader("Content-lenght", params.length);
        myReq.setRequestHeader("Connection", "close");
        myReq.onreadystatechange = setstate;
        myReq.send(params);
        return false;
    }
</script>
<link rel="stylesheet" href="/style/main.css" type="text/css"/>

<div class="container">
<div class="s-bk-lf" style="background: rgba(220, 53, 53, 0.47);">
<div class="acc-title"></div>
<center><h5>АКЦИИ НА ПОПОЛНЕНИЕ БАЛАНСА:</h5>

<h6>Пополнение на любую сумму: +10% от суммы пополнения!</h6>
</center>
</div>
</div>

<div class="bright">
   <div class="tn-box tn-box-color-1"> 
<p style="padding: 10px 0px 10px 60px;font-size: 16px;margin: 0;line-height: 20px;font-weight: 400;color: #ffffff;text-align: left;">
Вознаграждения, получаемые за выполнение заданий в серфинге зачисляются на счет для вывода и приравниваются к <b>пополнению баланса</b> на проекте!
</p>
</div>

    <?=$serf_menu;?>
    
    <table class="work-serf">

        <?php

        $paymeny_money_type = array(
            '0' => 'сер.',
            '1' => 'сер.',
            '2' => 'руб.',
            '3' => 'CP'
        );

        $time = time();

        if($admin){
            $db->Query("SELECT *, (SELECT COUNT(id) FROM db_serfing_view WHERE user_id = {$_SESSION['user_id']} LIMIT 1) onevisit FROM db_serfing WHERE (money >= price OR unlimend > {$time}) and status = '2' ORDER BY high DESC, vip DESC, time_add DESC");
        }else{
            $db->Query("SELECT ident FROM db_serfing_view WHERE user_id = {$_SESSION['user_id']} and time_add + INTERVAL 24*60*60 SECOND > NOW()");
            $ids = '';
            while ($row_view = $db->FetchAssoc()) {
                if($ids != '') $ids .= ",";
                $ids .= $row_view['ident'];
            }

            if($ids != ''){
                $db->Query("SELECT *, (SELECT COUNT(id) FROM db_serfing_view WHERE user_id = {$_SESSION['user_id']} LIMIT 1) onevisit FROM db_serfing WHERE (money >= price OR unlimend > {$time}) and status = '2' and id NOT IN({$ids}) ORDER BY high DESC, vip DESC, time_add DESC");
            }else{
                $db->Query("SELECT *, (SELECT COUNT(id) FROM db_serfing_view WHERE user_id = {$_SESSION['user_id']} LIMIT 1) onevisit FROM db_serfing WHERE (money >= price OR unlimend > {$time}) and status = '2' and id ORDER BY  high DESC, vip DESC, time_add DESC");
            }
        }

        if ($db->NumRows()) {
            while ($row = $db->FetchAssoc()) {
                if (!$admin) {
                    if ($row['oneuser'] == 1 && $row['onevisit'] == 1 ) continue;
                    if ($row['speed'] > 1) {
                        if (mt_rand(1, $row['speed']) != 1) continue;
                    }
                    if ($row['country']) {
                        $country = explode('|', $row['country']);
                        if ($row['crev']) {
                            if (in_array($_SESSION['country'], $country)) continue; //показывать всем кроме указаных
                        } else {
                            if (!in_array($_SESSION['country'], $country)) continue; //показывать только указаным
                        }
                    }
                    if ($row['rating']) {
                        if ($row['rating'] == 1 && $users_info['lavel'] < 2) {
                            continue;
                        }
                        if ($row['rating'] == 2 && $users_info['lavel'] < 5) {
                            continue;
                        }
                        if ($row['rating'] == 3 && $users_info['lavel'] < 10) {
                            continue;
                        }
                        if ($row['rating'] == 4 && $users_info['lavel'] < 15) {
                            continue;
                        }
                        if ($row['rating'] == 5 && $users_info['lavel'] < 20) {
                            continue;
                        }
                    }
                }
                $high = ($row['high']) ? 'serfimghigh' : 'serfimg';
                $row['price'] = $row['price'] + ($row['price'] * ($sonfig_site['serf_bonus'] / 100));
                switch ($sonfig_site['serf_money_type']){
                    case 0: $pay_user = number_format($row['price'] * $sonfig_site['ser_per_wmr'], 0); break;
                    case 1: $pay_user = number_format($row['price'] * $sonfig_site['ser_per_wmr'], 0); ;break;
                    case 2: $pay_user = number_format($row['price'], 3); break;
                    case 3: $pay_user = number_format($row['price'], 3); break;
                }
                ?>
				<div class="col-lg-12" style="padding: 0;">
					<div class="panel panel-default <? echo $row['high'] ? 'surfblock3' : 'surfblock1'; ?>">
						<div class="panel-body">
							<div class="surflink">
								<img src="https://www.google.com/s2/favicons?domain=<?php echo $row['url'];?>">
								<h4><a href="http://FootFarm.com/buks/serfing/view/<?php echo $row['id']; ?>" target="_blank" onclick="javascript:goserf(this);" class="surflinkgoto waves-effect" title="Начать просмотр сайта"><small>«</small><?php echo $row['title']; ?><small>»</small> </a><?if($_SESSION['admin']){?> Реклама: <?php echo $row['id']; ?>, Рекламодатель: <?php echo $row['user_name']; ?> | <?php echo $row['url']; }?><a class="surfabuselink" href="http://online.us.drweb.com/result/?url=<?php echo $row['url'];?>" title="Проверить ссылку на вирусы" target="_blank"><i class="fa fa-bug"></i></a></h4>
								<h6><?php if ($admin) { ?><a href="/buks/serfing/delete/<?php echo $row['id']; ?>" title="Удалить ссылку"></a><? } ?><span class="surftimer"><i class="fa fa-clock-o"></i> Время просмотра: <?php echo $row['timer'];?> сек.</span><span class="surfprice"><i class="fa fa-eercast"></i> Оплата: <?php echo $pay_user; ?>&nbsp;<?= VALUTA ?></span><span class="surfviewleft">Осталось <? if($time > $row['unlimend']){ ?><?php echo intval($row['money'] / $row['price']); }else{ echo '&#8734;';} ?> просмотров</span></h6>
							</div>
						</div>
					</div>
				</div>
				
                <?php
            }
        } else {

        }
        ?>

    </table>
</div></div></div>

