<?php
# Настройки
$sonfig_site = $func->config_site($db);
if($sonfig_site['serf_enable'] == 0 && !$admin){ @include("pages/_404.php"); return; }
if(empty($_SESSION["user_id"])){
	@include("pages/_404.php"); return;
}
$_OPTIMIZATION["title"] = "Серфинг - Личный Кабинет";

$db->Query('SET CHARACTER SET utf8');
$db->Query("set names utf8");

function anti_sql2($val)
{
	$check = html_entity_decode( urldecode( $val ) );
	//$check = str_replace( "", "/", $check );

	$check2 = trim($check);
	$check = array("AND","UNION","SELECT","WHERE","INSERT","UPDATE","DELETE","OUTFILE","FROM","OR","SHUTDOWN","CHANGE","MODIFY","RENAME","RELOAD","ALTER","GRANT","DROP","CONCAT","cmd","exec", "script");
	$check = str_replace($check,"",$check2);
	return $val;
}

if (isset($_POST['title'])) {
    if (isset($_POST['rule']) || $advedit || !$_POST['rule']) {
        //Заголовок ссылки
		$err = false;
		$title = htmlspecialchars($_POST['title']);
		/*$check2 = array("AND","UNION","SELECT","WHERE","INSERT","UPDATE","DELETE","OUTFILE","FROM","OR","SHUTDOWN","CHANGE","MODIFY","RENAME","RELOAD","ALTER","GRANT","DROP","CONCAT","cmd","exec", "script");
		$title = str_replace($check2,"",$title);*/
		$title = addslashes(filter_var(mb_substr($title, 0, 55), FILTER_SANITIZE_STRING));
        $desc = 'suc';
		//URL сайта
		$url = anti_sql2($_POST['url']);
        $url = isset($url) ? $url : '';
		
        if (strlen($url) < 3 or strlen($url) > 50) {
            echo '<span class="msgbox-error">Неверный адрес сайта</span>';
            $err = true;
        }
		if(empty($_POST['type'])){
			echo '<span class="msgbox-error">Не выбран пакет услуг</span>';
            $err = true;
		}
		
		if(!$err){
			$types = $_POST['type'];
			switch($types){
				case '1':
					$timer = 20;
					$move = 1;
					$high = 0;
					$speed = 1;
					$vip = 0;
					$oneuser = 0;
					$rating = 0;
					$crev = 0;
					$price = 0.025;
					$tarif = 1;
				break;
				case '2':
					$timer = 30;
					$move = 1;
					$high = 1;
					$speed = 1;
					$vip = 0;
					$oneuser = 0;
					$rating = 0;
					$crev = 0;
					$price = 0.045;
					$tarif = 2;
				break;
				case '3':
					$timer = 40;
					$move = 1;
					$high = 1;
					$speed = 1;
					$vip = 0;
					$oneuser = 0;
					$rating = 0;
					$crev = 0;
					$price = 0.065;
					$tarif = 3;
				break;
			}
			
			$status = '3';
			
			$db->query("INSERT INTO db_serfing
			(
				`user_name`,
				`time_add`,
				`title`,
				`desc`,
				`url`,
				`timer`,
				`move`,
				`high`,
				`speed`,
				`vip`,
				`oneuser`,
				`country`,
				`rating`,
				`crev`,
				`price`,
				`status`,
				`tarif`
			)
			VALUES
			(
				'" . $_SESSION['user'] . "',
				'" . TIME . "',
				'" . $title . "',
				'" . $desc . "',
				'" . $url . "',
				'" . $timer . "',
				'" . $move . "',
				'" . $high . "',
				'" . $speed . "',
				'" . $vip . "',
				'" . $oneuser . "',
				'',
				'" . $rating . "',
				'" . $crev . "',
				'" . $price . "',
				'" . $status . "',
				'" . $tarif . "'
			)");
			header('Location: /buks/serfing/cabinet');
			exit();
		}
        
    } else {
        $error = '<center><b style="color:red">Вы не подтвердили согласие с условиями размещения рекламы!</b></center><br/>';
    }
}
?>

<div class="bright">
    <?=$serf_menu;?>
    <?= $error ?>
	
	<div class="row" style="margin-left:5px; margin-right: 5px;">
		<div class="col-md-4 col-12">
			<div class="panel2 panel-default">
				<div class="panel-body text-center">
					<h3 class="m-t-0 m-b-10 profilemst addsurf_topzag"><b>Тариф "Эконом"</b></h3>
					<hr>
					<h5 class="addsurf_h5 m-t-15"><i class="fa fa-cube"></i> Переход после просмотра: <div class="text-primary">ДА</div></h5>
					<h5 class="addsurf_h5"><i class="fa fa-window-maximize"></i> Просмотр в активном окне: <div class="text-danger">НЕТ</div></h5>
					<h5 class="addsurf_h5"><i class="fa fa-bug"></i> Защита от автокликеров: <div class="text-primary">ДА</div></h5>
					<h5 class="addsurf_h5"><i class="fa fa-clock-o"></i> Время просмотра сайта: <div>20 секунд</div></h5>
					<h5 class="addsurf_h5 m-b-15"><i class="fa fa-eercast"></i> Выделение в списке: <div class="text-danger">НЕТ</div></h5>
					<hr>
					<h5 class="m-b-0 m-t-15 addsurf_price">Цена за 1000 просмотров: 40 руб.</h5>
				</div>
			</div>
		</div>
		
		<div class="col-md-4 col-12">
			<div class="panel2 panel-default">
				<div class="panel-body text-center">
					<h3 class="m-t-0 m-b-10 profilemst addsurf_topzag"><b>Тариф "Обычный"</b></h3>
					<hr>
					<h5 class="addsurf_h5 m-t-15"><i class="fa fa-cube"></i> Переход после просмотра: <div class="text-primary">ДА</div></h5>
					<h5 class="addsurf_h5"><i class="fa fa-window-maximize"></i> Просмотр в активном окне: <div class="text-danger">НЕТ</div></h5>
					<h5 class="addsurf_h5"><i class="fa fa-bug"></i> Защита от автокликеров: <div class="text-primary">ДА</div></h5>
					<h5 class="addsurf_h5"><i class="fa fa-clock-o"></i> Время просмотра сайта: <div>30 секунд</div></h5>
					<h5 class="addsurf_h5 m-b-15"><i class="fa fa-eercast"></i> Выделение в списке: <div class="text-primary">ДА</div></h5>
					<hr>
					<h5 class="m-b-0 m-t-15 addsurf_price">Цена за 1000 просмотров: 60 руб.</h5>
				</div>
			</div>
		</div>
		
		<div class="col-md-4 col-12">
			<div class="panel2 panel-default">
				<div class="panel-body text-center">
					<h3 class="m-t-0 m-b-10 profilemst addsurf_topzag"><b>Тариф "Премиум"</b></h3>
					<hr>
					<h5 class="addsurf_h5 m-t-15"><i class="fa fa-cube"></i> Переход после просмотра: <div class="text-primary">ДА</div></h5>
					<h5 class="addsurf_h5"><i class="fa fa-window-maximize"></i> Просмотр в активном окне: <div class="text-primary">ДА</div></h5>
					<h5 class="addsurf_h5"><i class="fa fa-bug"></i> Защита от автокликеров: <div class="text-primary">ДА</div></h5>
					<h5 class="addsurf_h5"><i class="fa fa-clock-o"></i> Время просмотра сайта: <div>40 секунд</div></h5>
					<h5 class="addsurf_h5 m-b-15"><i class="fa fa-eercast"></i> Выделение в списке: <div class="text-primary">ДА</div></h5>
					<hr>
					<h5 class="m-b-0 m-t-15 addsurf_price">Цена за 1000 просмотров: 80 руб.</h5>
				</div>
			</div>
		</div>
	</div>
	<div class="row buks" style="margin-left:5px; margin-right: 5px; margin-top: 20px;">
		<div class="col-lg-4">
			<div class="panel3 panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Добавление сайта в сёрфинг</h3>
				</div>
				<div class="panel-body">
					<form method="post">
						<div class="form-group">
							<label>Заголовок рекламного блока:</label>
							<div><input name="title" type="text" maxlength="70" class="form-control" placeholder="Например: Отличный сайт, смотреть всем!" required=""></div>
						</div>
						<div class="form-group">
							<label>URL сайта:</label>
							<div><input name="url" type="url" class="form-control" placeholder="Например: http://yandex.ru" value="http://" required=""></div>
						</div>
						<div class="form-group">
							<label>Выберите тариф для показа:</label>
							<select name="type" class="form-control" id="tarif">
								<option value="1">Тариф "Эконом"</option>
								<option value="2" selected="">Тариф "Обычный"</option>
								<option value="3">Тариф "Премиум"</option>
							</select>
						</div>
						<div class="checkbox checkbox-primary"><input name="rule" value="1" id="checkbox222" type="checkbox" required=""> <label for="checkbox222"> Я согласен с <a class="href-checkbox" data-toggle="modal" data-target="#addsurfmodalrules">правилами размещения</a> рекламы на сайте. </label></div>
						<div class="addsurf_fbtn">
							<button type="submit" class="btn btn-primary waves-effect waves-light">Добавить сайт в сёрфинг</button>
							<div class="addsurf_priceform">Цена за 1000 просмотров: <span id="price">60</span> руб.</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="col-lg-8">
			<div class="panel3 panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Мои сайты в сёрфинге</h3>
				</div>
				<div class="panel-body">
					<div class="col-lg-12">
						<?
							$ttime = time();
							$db->Query("SELECT * FROM db_serfing WHERE user_name = '" . $_SESSION['user'] . "' ORDER BY time_add DESC");
							
							$ratif = array(
								1 => 'Эконом',
								2 => 'Обычный',
								3 => 'Премиум'
							);
							
							if ($db->NumRows()) {
								while ($row = $db->FetchAssoc()) {
									$ost = floor($row['money'] / ($row['price'] + 0.015));
						?>
									<div class="panel2 panel-default addsurf_sitepan" data-id="<?php echo $row['id']; ?>">
										<div class="panel-body">
											<div class="addsurf_sitebox">
												<h4><a href="<?php echo $row['url']; ?>" target="_blank" class="surflinkgoto waves-effect">«<?php echo $row['title']; ?>»</a></h4>
												<h6><span class="surftimer"><i class="fa fa-mouse-pointer"></i> Просмотрено: <?php echo $row['view']; ?> раз.</span><span class="surfprice"><i class="fa fa-diamond"></i> Тариф: "<span class="tarifName"><?=$ratif[$row['tarif']];?></span>"</span><span class="surfviewleft">Осталось <?
												if($row['money'] > $row['price']){
													echo $ost;
												}else{
													echo '0';
												}?> просмотров</span></h6>
												<hr class="m-t-15 m-b-15">
												<div>
													<div class="addsurf_balance"><i class="mdi mdi-square-inc-cash"></i> Баланс сайта: <span id="b_<?=$row['id'];?>"><?php echo $row['money']; ?></span> <a onclick="insBalance(<?=$row['id'];?>);" style="cursor:pointer;" data-toggle="modal" data-target=".addsurfmodalbalance">[пополнить баланс]</a></div>
													<div class="addsurf_status"><i class="mdi mdi-apple-safari"></i> Статус: 
													<? if($row['status'] == 3){ ?>
														<span class="text-warning">остановлен</span>
													<? }elseif($row['status'] == 2){ ?>
														<span class="text-primary">запущено</span>
													<? } ?>
													</div>
												</div>
												<hr class="m-t-15 m-b-15">
												<div class="btn-group addsurf_btngroup">
													<?
														if($row['status'] == 3){
													?>
														<button type="button" onclick="action(<?php echo $row['id']; ?>, 'play', this);" class="btn btn-default waves-effect addsurf_btn"><i class="fa fa-play"></i> Возобновить показ</button>
													<?
														}elseif($row['status'] == 2){
													?>
														<button type="button" onclick="action(<?php echo $row['id']; ?>, 'pause', this);" class="btn btn-default waves-effect addsurf_btn"><i class="fa fa-pause"></i> Остановить показ</button>
													<?
														}
													?>
													<button type="button" class="btn btn-default waves-effect addsurf_btn" data-toggle="modal" data-target="#addsurfmodaledit" onclick="setAdv(<?=$row['id'];?>);"><i class="mdi mdi-settings"></i> Редактировать сайт</button>
													<button type="button" class="btn btn-default waves-effect addsurf_btn" data-toggle="tooltip" data-placement="top" data-original-title="Баланс сайта вернется вам на рекламный счет" onclick="javascript:del(<?php echo $row['id']; ?>, this);"><i class="mdi mdi-delete-forever"></i> Удалить сайт</button>
												</div>
											</div>
										</div>
									</div>
							
						<?
								}
							}else{
						?>
							<h4 class="text-center surfnoneh4" style="color:#a94442;">Вы не добавили ни одного сайта в сёрфинг :(</h4>
						<?
							}
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="addsurfmodalrules" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="addsurf_modal1" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title" id="addsurf_modal1">Правила размещения рекламы</h4>
			</div>
			<div class="modal-body">
				<h4>Запрещена реклама сайтов следующих категорий:</h4>
				<p>- Фишинг, вирусы, секс-шопы, знакомства "на одну ночь";</p>
				<p>- Сайты которые разрушают фрейм, сайты с редиректом;</p>
				<p>- Сайты содержащие порнографию, обилие эротических материалов;</p>
				<p>- Политические и религиозные ресурсы, любые виды насилия;</p>
				<p>- Ресурсы с элементами магии, спиритизма, оккультизма;</p>
				<p>- Ресурсы, с явно выраженным обманом;</p>
				<p>- Сайты набитые множеством партнёрок, всплывающих pop-up и т.д.</p>
				<p>- Ресурсы, требующие отправку платных СМС-сообщений</p>
				<hr>
				<h6>В случае выявления нарушений к аккаунту пользователя могут быть применены штрафные санкции, вплоть блокировки аккаунта, в зависимости от строгости нарушения.</h6>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Закрыть</button>
			</div>
		</div>
	</div>
</div>
<div id="addsurfmodaledit" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="addsurf_modal2" aria-hidden="true">
	<div class="modal-dialog balancep_modalwidth">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title" id="addsurf_modal2">Редактирование сайта #<span id="bid"></span></h4>
			</div>
			
				<input type="hidden" name="id" id="bdata">
				<div class="modal-body">
					<div class="form-group m-t-5">
						<label>Заголовок рекламного блока:</label>
						<div><input name="title" type="text" id="btitle" maxlength="70" class="form-control" value="" placeholder="Например: Отличный сайт, смотреть всем!" required=""></div>
					</div>
					<div class="form-group">
						<label>URL сайта:</label>
						<div><input name="url" type="url" id="burl" class="form-control" placeholder="Например: http://yandex.ru" value="" required=""></div>
					</div>
					<div class="form-group">
						<label>Выберите тариф для показа:</label>
						<select name="type" class="form-control" id="btype">
						<option value="1">Тариф "Эконом"</option>
						<option value="2" selected="">Тариф "Обычный"</option>
						<option value="3">Тариф "Премиум"</option>
						</select>
					</div>
				</div>
				<div class="modal-footer">
					<button onclick="editAdv();" class="btn btn-primary waves-effect waves-light">Обновить настройки</button>
					<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Отмена</button>
				</div>
			
		</div>
	</div>
</div>
<div class="modal fade addsurfmodalbalance" tabindex="-1" role="dialog" aria-labelledby="addsurf_modal3" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title" id="addsurf_modal3">Баланс ссылки #<span id="iid"></span></h4>
			</div>
			<div class="modal-body text-center">
				<div class="addsurf_balance"><i class="mdi mdi-square-inc-cash"></i> Актуальный баланс: <span id="inow"></span></div>
				<hr class="m-t-15 m-b-15">
				
				<div class="form-group">
					<label style="letter-spacing:1px;">Пополнить баланс сайта:</label>
					<div><input name="sum" autocomplete="new-password" id="isum" onkeyup="calClicks();" type="text" maxlength="9" class="form-control" placeholder="Введите сумму... (руб.)" required=""></div>
				</div>
				<div class="form-group">
					<input type="text" class="form-control balancei_input" id="chclc" value="Хватит на 0 просмотров" disabled="">
				</div>
				<button id="refillBalance" class="btn btn-primary waves-effect waves-light btn-block">Пополнить баланс</button>
				
			</div>
		</div>
	</div>
</div>
<script src="https://unpkg.com/sweetalert2@7.15.1/dist/sweetalert2.all.js"></script>
<script>
	$("#tarif").change(function(){
		var val2 = $(this).val();
		switch(val2){
			case '1':
				$("#price").html('40');
			break;
			case '2':
				$("#price").html('60');
			break;
			case '3':
				$("#price").html('80');
			break;
		}
	});
	
	function del(id, block){
		var cnt = '<?php echo $_SESSION['cnt']; ?>';
		swal({
		  title: "Вы уверены?",
		  text: "Площадка №"+id+" будет удалена, баланс блока вернется вам на рекламный счет.",
		  type: "warning",
		  showCancelButton: true,
		  cancelButtonText: "Отмена",
		  confirmButtonText: "Да, удалить!",
		  closeOnConfirm: false
		}).then((result) => {
		  if (result.value) {
			$.post('/ajax/surf.php', {id: id, method: 'del', cnt: cnt}, function(data){
				if(data == 3){
					swal(
					  'Успех!',
					  'Ваша рекламная ссылка успешно удалена',
					  'success'
					);
					$(".addsurf_sitepan[data-id="+id+"]").remove();
				}
			});
		  } else if (result.dismiss === swal.DismissReason.cancel) {
			return false;
		  }
		})
	}
	
	function insBalance(gid) {
	  var cnt = '<?php echo $_SESSION['cnt']; ?>';
	  $("#iid").html("Загрузка");
	  $("#inow").html("");
	  $("#idata").val("");
	  $.post("/ajax/surf.php", { id: gid, method: 'getInfo', cnt: cnt })
	  .done(function(data) {
		if(data!='no2') {
		  var fd=data.split("<br/>");
		  $("#iid").html(fd[0]);
		  $("#inow").html(fd[1]);
		  $("#idata").val(fd[0]);
		  nowcost=fd[2];
		}
		else
		{
		  swal({
			  type: "warning",
			  title: "Ошибка!",
			  text: "Данного блока не существует",
			  timer: 5000,
			  showConfirmButton: true
		  });
		}
	  });
	}
	
	function calClicks() {
	  var seens;
	  seens=Math.floor($("#isum").val()/nowcost);
	  if(isNaN(seens)) {
		seens=0;
	  }
	  $("#chclc").val("Хватит на "+seens+" просмотров");
	}
	
	function action(id, method, block){
		var cnt = '<?php echo $_SESSION['cnt']; ?>';
		$.post('/ajax/surf.php', {id: id, method: method, cnt: cnt}, function(data){
			if(data == 1){
				swal(
				  'Успех!',
				  'Вы успешно возобновили ваше рекламное объявление #'+id,
				  'success'
				);
				$(block).remove();
				$(".addsurf_btngroup").prepend('<button type="button" onclick="show('+id+', \'pause\', this);" class="btn btn-default waves-effect addsurf_btn"><i class="fa fa-pause"></i> Остановить показ</button>');
			}else if(data == 2){
				swal(
				  'Успех!',
				  'Вы успешно приостановили ваше рекламное объявление #'+id,
				  'success'
				);
				$(block).remove();
				$(".addsurf_btngroup").prepend('<button type="button" onclick="show('+id+', \'play\', this);" class="btn btn-default waves-effect addsurf_btn"><i class="fa fa-play"></i> Возобновить показ</button>');
			}else if(data == 3){
				swal(
				  'Успех!',
				  'Ваша рекламная ссылка успешно удалена',
				  'success'
				);
				$(".addsurf_sitepan[data-id="+id+"]").remove();
			}else if(data == 'nomoney1'){
				swal(
				  'Ошибка!',
				  'Недостаточно средств для возобновления рекламного объявления #'+id,
				  'error'
				);
			}
		});
	}
	
	function setAdv(gid) {
		var cnt = '<?php echo $_SESSION['cnt']; ?>';
	  $("#bid").html("Загрузка");
	  $("#bdata").val("");
	  $("#btitle").val("");
	  $("#burl").val("");
	  $.post("/ajax/surf.php", { id: gid, method:'load', cnt: cnt })
	  .done(function(data) {
		if(data!='fail') {
		  var fd=data.split("<br/>");
		  $("#bid").html(fd[0]);
		  $("#bdata").val(fd[0]);
		  $("#btitle").val(fd[1]);
		  $("#burl").val(fd[2]);
		  $("#btype").val(fd[3]);
		}
		else
		{
		  swal({
			  type: "warning",
			  title: "Ошибка!",
			  text: "Данного блока не существует",
			  timer: 5000,
			  showConfirmButton: true
		  });
		}
	  });
	}
	
	function editAdv(){
		var cnt = '<?php echo $_SESSION['cnt']; ?>';
		var id = $("#bdata").val();
		var title = $("#btitle").val();
		var url = $("#burl").val();
		var type = $("#btype").val();
		$.post('/ajax/surf.php', {id: id, title: title, url: url, type: type, cnt: cnt, method: 'edit'}, function(data){
			if(data == 1){
				if(type == 1) var tarif = 'Эконом';
				if(type == 2) var tarif = 'Обычный';
				if(type == 3) var tarif = 'Премиум';
				$(".addsurf_sitepan[data-id="+id+"]").find('.surflinkgoto').attr('href', url);
				$(".addsurf_sitepan[data-id="+id+"]").find('.surflinkgoto').html(title);
				$(".addsurf_sitepan[data-id="+id+"]").find('.tarifName').html(tarif);
				swal(
				  'Успех!',
				  'Реклманое объявление #'+id+' успешно отредактировано',
				  'success'
				);
				$('#addsurfmodaledit').modal('hide')
			}else{
				swal(
				  'Ошибка!',
				  'Реклманое объявление не найдено',
				  'error'
				);
			}
		});
	}
	
	$("#refillBalance").click(function(){
		var sum = parseFloat($("#isum").val());
		if(sum < 10){
			swal(
			  'Ошибка!',
			  'Минимальная сумма для пополнения - 10 рублей',
			  'error'
			);
		}else{
			var id = $("#iid").html();
			var cnt = '<?php echo $_SESSION['cnt']; ?>';
			$.post('/ajax/surf.php', {id: id, method:'refill', cnt:cnt, sum:sum}, function(data){
				if(data == 1){
					swal(
					  'Успех!',
					  'Баланс реклманого объявления #'+id+' успешно пополнен',
					  'success'
					);
					var nowBalance = parseFloat($("#b_"+id).html());
					var newBalance = nowBalance + sum;
					$("#b_"+id).html(newBalance);
				}else{
					swal(
					  'Ошибка!',
					  'Недостаточно денег на рекламном балансе',
					  'error'
					);
				}
			});
		}
	});
</script>