<?
	$_OPTIMIZATION["title"] = "Обменник";
	$usid = $_SESSION["user_id"];
	$usname = $_SESSION["user"];

	$db->Query("SELECT * FROM db_users_b WHERE id = '$usid' LIMIT 1");
	$user_data = $db->FetchArray();

	$db->Query("SELECT * FROM db_config WHERE id = '1' LIMIT 1");
	$sonfig_site = $db->FetchArray();
?>
		<div class="row" style="margin: 20px;">
			<div class="col-sm-6 col-lg-6 col-12">
				<div class="panel3 panel-warning">
					<div class="panel-heading">
						<h3 class="panel-title racetabletitle"><i class="fa fa-refresh"></i> Обмен с ВЫВОДА на ПОКУПКИ</h3>
					</div>
					<div class="panel-body">
						<?
							if(isset($_POST['type']) and $_POST['type'] == '1'){
								$sum = intval($_POST["sum"]);

								if($sum >= 100){
								
									if($user_data["money_p"] >= $sum){
									
										//$add_sum = ($sonfig_site["percent_swap"] > 0) ? ( ($sonfig_site["percent_swap"] / 100) * $sum) + $sum : $sum;
										$add_sum = $sum + ($sum * 0.1);
										
										$ta = time();
										$td = $ta + 60*60*24*15;
										
										$db->Query("UPDATE db_users_b SET money_b = money_b + $add_sum, money_p = money_p - $sum WHERE id = '$usid'");
										$db->Query("INSERT INTO db_swap_ser (user_id, user, amount_b, amount_p, date_add, date_del) VALUES ('$usid','$usname','$add_sum','$sum','$ta','$td')");
										
										echo "<center><font color = 'green'><b>Обмен произведен</b></font></center><BR />";
									
									}else echo "<center><font color = 'red'><b>Недостаточно серебра для обмена</b></font></center><BR />";
								
								}else echo "<center><font color = 'red'><b>Минимальная сумма для обмена 100 серебра</b></font></center><BR />";
							}
						?>
						<center><img class="exchange_img" src="/images/exchange2.png"></center>
						<p class="exchange_desctext" style="margin-top: 10px;">Односторонний обмен средств с Вашего баланса для вывода, на Ваш баланс для покупок. Мин. сумма: 100 сер.</p>
						<form method="post">
							<div class="form-group exchange_formelem">
								<input type="hidden" name="type" value="1">
								<input name="sum" type="text" onkeyup="GetSum2(this);" maxlength="7" class="form-control" placeholder="Введите сумму обмена... (сер.)" required=""><br>
								<input type="text" id="suminser2" maxlength="7" class="form-control" placeholder="Получите (сер.)" disabled>
								<button type="submit" class="btn waves-effect btn-default btn-block m-t-10"> <i class="mdi mdi-call-split"></i> Произвести обмен средств</button>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="col-sm-6 col-lg-6 col-12">
				<div class="panel3 panel-warning">
					<div class="panel-heading">
						<h3 class="panel-title racetabletitle"><i class="fa fa-refresh"></i> Обмен с ВЫВОДА на РЕКЛАМУ</h3>
					</div>
					<div class="panel-body">
						<?
							if(isset($_POST['type']) and $_POST['type'] == '2'){
								$sum = intval($_POST["sum"]);

								if($sum >= 100){
								
									if($user_data["money_p"] >= $sum){
									
										$add_sum = $sum / 100;
										$add_sum = $add_sum + ($add_sum * 0.1);
										
										$ta = time();
										$td = $ta + 60*60*24*15;
										
										$db->Query("UPDATE db_users_b SET money_r = money_r + $add_sum, money_p = money_p - $sum WHERE id = '$usid'");
										$db->Query("INSERT INTO db_swap_ser (user_id, user, amount_b, amount_p, date_add, date_del) VALUES ('$usid','$usname','$add_sum','$sum','$ta','$td')");
										
										echo "<center><font color = 'green'><b>Обмен произведен</b></font></center><BR />";
									
									}else echo "<center><font color = 'red'><b>Недостаточно серебра для обмена</b></font></center><BR />";
								
								}else echo "<center><font color = 'red'><b>Минимальная сумма для обмена 100 серебра</b></font></center><BR />";
							}
						?>
						<center><img class="exchange_img" src="/images/exchange.png"></center>
						<p class="exchange_desctext" style="margin-top: 10px;">Односторонний обмен средств с Вашего баланса для вывода, на Ваш баланс для рекламы. Мин. сумма: 100 сер.</p>
						<form method="post">
							<div class="form-group exchange_formelem">
								<input type="hidden" name="type" value="2">
								<input name="sum" type="text" maxlength="7" class="form-control" onkeyup="GetSum(this);" placeholder="Введите сумму обмена... (сер.)" required=""><br>
								<input type="text" id="suminser" maxlength="7" class="form-control" placeholder="Получите (руб.)" disabled>
								<button type="submit" class="btn waves-effect btn-default btn-block m-t-10"> <i class="mdi mdi-call-split"></i> Произвести обмен средств</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>

<script>
	function GetSum(input){
		var sum = $(input).val();
		if (sum !== '') {
			if(isInteger(parseInt(sum))){
				var endsum = (parseInt(sum) / 100)
				var bonus = endsum * 0.1;
				endsum = endsum + bonus;
				$("#suminser").val(endsum + ' рублей');
			}else{
				alert('Введено некорректное число');
			}
		}else{
			$("#suminser").val('0 рублей');
		}
	}
	
	function GetSum2(input){
		var sum = $(input).val();
		if (sum !== '') {
			if(isInteger(parseInt(sum))){
				var bonus = parseInt(sum) * 0.1;
				var endsum = parseInt(sum) + bonus;
				$("#suminser2").val(endsum + ' серебра');
			}else{
				alert('Введено некорректное число');
			}
		}else{
			$("#suminser2").val('0 рублей');
		}
	}
	
	function isInteger(num) {
	  return (num ^ 0) === num;
	}
</script>