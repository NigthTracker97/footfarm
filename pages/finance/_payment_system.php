<?
$_OPTIMIZATION["title"] = "Заказ выплаты";
?>

<style>
.selectPS{
	float: left;
    width: 169px;
    height: 94px;
	line-height: 94px;
	text-align: center;
    box-sizing: border-box;
    background: #fff;
    border-bottom: 4px solid #f1f1f1;
    border-radius: 5px;
    box-shadow: 0 0 20px rgba(0,0,0,0.35);
    margin: 0 0 26px 20px;
    position: relative;
    cursor: pointer;
    transition: .5s;
}
.selectPS:hover {
    box-shadow: 0 0 25px rgba(0,0,0,0.75);
}
.selectPS img {
    vertical-align: middle;
    max-width: 194px;
    max-height: 94px;
}

</style>
<div class="container">
<div class="s-bk-lf" style="background: rgba(220, 53, 53, 0.47);">
<div class="acc-title"></div>
<center><h5>АКЦИЯ НА ПОПОЛНЕНИЕ БАЛАНСА:</h5>

<h6>Пополнение на любую сумму: +10% от суммы пополнения!</h6>
</center>
</div>
</div>
<div class="container">
<center style="margin-bottom: 10px; color: #484847;margin-top: 0px;font-size: 14px;text-transform: uppercase;font-weight: bold;">
<span> Выплаты осуществляются в  автоматическом режиме!
<br></span></center>
    
    <div style="margin-top:20px;">

        <?php
			 $db->Query('SET CHARACTER SET utf8');
			$db->Query("set names utf8");
            $db->Query("SELECT * FROM db_payment_system WHERE enable = 1");
            if($db->NumRows() == 0){
                echo '<center><b>Нет доступных платежных систем!</b></center>';
            }else{
                while ($data = $db->FetchArray()){
                    ?>
                    <div class="selectPS">
                        <a href="/finance/payments/mps_<?=$data['name']?>" style="text-decoration: none;">
                            <img title="<?=$data['title']?>" src="<?=$data['img']?>">
                        </a>
                    </div>
                    <?
                }
            }
        ?>
        
    </div>
	
	<div style="clear: both"></div>
	
	<div>
		<h1 style="text-align: center;">Экстренные выплаты</h1>
	
        <div class="selectPS" style="float: none; margin: 20px auto;">
            <a href="/finance/fast_payments" style="text-decoration: none;">
				<img title="Выплаты Payeer ЭКСТРЕННАЯ" src="/images/ps/payeer.png">
            </a>
        </div>

	</div>


    Каждая платежная система по своему удобна и уникальна. В какой-то платежной системе маленькая комиссия, а в какой-то больше направлений для вывода.

    <div class="clr"></div>
</div></div>