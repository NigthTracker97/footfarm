
<?
 $_OPTIMIZATION["title"] = "Статистика переходов";
include("./_geolocation/SxGeo.php");
$SxGeo = new SxGeo('./_geolocation/SxGeoCity.dat');

function geodata($ip){
    global $SxGeo;
    $geo_date = $SxGeo->getCityFull($ip);
    $country = $geo_date['country']['name_ru'];
    $city = $geo_date['city']['name_ru'];
    return $country.' / '.$city;
}

if(!isset($_GET['show'])) $_GET['show'] = 1;

?>
<div class="container">
<div class="s-bk-lf" style="background: rgba(220, 53, 53, 0.47);">
<div class="acc-title"></div>
<center><h5>АКЦИИ НА ПОПОЛНЕНИЕ БАЛАНСА:</h5>

<h6>Пополнение на любую сумму: +10% от суммы пополнения!</h6>
</center>
</div>
</div>
<div class="container">
    <div class="clr"></div>

    <style>
        #selected_admmenu{
            font-weight: bold;
        }
    </style>

    <center>
        <a <?if($_GET['show'] == 1) echo 'id="selected_admmenu"';?> href="/referals/visits">Переходы</a> | <a <?if($_GET['show'] == 2) echo 'id="selected_admmenu"';?> href="/referals/visits/ist">Источники</a>
    </center>

    <br>

    <? if($_GET['show'] == 1){

        $num_p = (isset($_GET["page"]) AND intval($_GET["page"]) < 1000 AND intval($_GET["page"]) >= 1) ? (intval($_GET["page"]) -1) : 0;
        $lim = $num_p * 100;

        if(isset($_GET['ist'])){
            $ist = htmlspecialchars($_GET['ist'], ENT_QUOTES);
            $dpget = "url/{$ist}/";
            $dpsql = "WHERE url = '{$ist}' AND ref_id = {$_SESSION['user_id']}";
        }else{
            $dpsql = "WHERE ref_id = {$_SESSION['user_id']}";
        }

        if(isset($_GET['ref'])){
            $ref_id = htmlspecialchars(intval($_GET['ref']));
            $dpget .= "&ref={$ref_id}";
            if($dpsql != ''){
                $dpsql .= " AND ref_id = {$ref_id}";
            }else{
                $dpsql = "WHERE ref_id = {$ref_id}";
            }
        }

        if(isset($_GET['sort'])){
            $gsort = htmlspecialchars($_GET['sort']);
            $dpget .= "&sort={$gsort}";
            switch ($_GET['sort']){
                case 'visites': $order = 'cnt'; break;
                case 'page': $order = 'page'; break;
                case 'date': $order = 'date'; break;
                case 'ist': $order = 'url'; break;
                case 'id': $order = 'id'; break;
                case 'ip': $order = 'ip'; break;
                default: $order = 'id'; break;
            }
        }else{
            $order = "id";
        }

        ?>

        <table cellpadding='3' cellspacing='0' border='0' bordercolor='#336633' align='center' width="99%">
            <tr>
                <td style="border: 1px dashed #db8;" align="center" class="m-tb"><a style="color: black" >№</a></td>
                <td style="border: 1px dashed #db8;" align="center" class="m-tb"><a style="color: black" >Дата</a></td>
                <td style="border: 1px dashed #db8;" align="center" class="m-tb"><a style="color: black" >Источник</a></td>
                <td style="border: 1px dashed #db8;" align="center" class="m-tb"><a style="color: black" >Визиты</a></td>
                <td style="border: 1px dashed #db8;" align="center" class="m-tb"><a style="color: black" >Геолокация</a></td>
            </tr>
            <?PHP

            $db->Query("SELECT COUNT(*) FROM db_visits {$dpsql}");
            $max_num = $db->FetchRow();

            $pn = $max_num - $lim;

            $db->Query("SELECT db_visits.id, page, reg, ref_id, INET_NTOA(db_visits.ip) AS ip, cnt, url, date, user FROM db_visits LEFT JOIN db_users_a ON db_users_a.id = db_visits.ref_id {$dpsql} ORDER BY {$order} DESC LIMIT {$lim}, 100");
            $num = $lim + 1;
            while ($data = $db->FetchArray()) {

                ?>
                <tr class="htt" <?if($data['reg'] > 0){?>style="background: #e0ffeb"<?}?>>
                    <td style="border: 1px dashed #db8;" align="center"><?=$pn--?></td>
                    <td style="border: 1px dashed #db8;" align="center"><?=date("H:i:s",$data['date'])?></td>
                    <td style="border: 1px dashed #db8;" class="clip" align="center"><a href="http://<?=$data['url']?>" target="_blank"><?=$data['url']?></a></td>
                    <td style="border: 1px dashed #db8;" align="center"><?=$data['cnt']?></td>
                    <td style="border: 1px dashed #db8;" align="center"><?=geodata($data["ip"]); ?></td>
                </tr>
                <?PHP

            }

            ?>
        </table>

        <?php
        $db->Query("SELECT COUNT(*) FROM db_visits {$dpsql}");
        $all_pages = $db->FetchRow();

        if($all_pages > 100){

            $sort_b = (isset($_GET["sort"])) ? intval($_GET["sort"]) : 0;

            $nav = new navigator;
            $page = (isset($_GET["page"]) AND intval($_GET["page"]) < 1000 AND intval($_GET["page"]) >= 1) ? (intval($_GET["page"])) : 1;

            echo "<BR /><center>".$nav->Navigation(10, $page, ceil($all_pages / 100), "/referals/visits/{$dpget}page/"), "</center>";

        }
        ?>

    <? }else ?>

    <? if($_GET['show'] == 2){

        $num_p = (isset($_GET["page"]) AND intval($_GET["page"]) < 1000 AND intval($_GET["page"]) >= 1) ? (intval($_GET["page"]) -1) : 0;
        $lim = $num_p * 100;

        $num = $lim + 1;

        $dpsql = "WHERE ref_id = {$_SESSION['user_id']}";


        if(isset($_GET['sort'])){
            $dpget .= "&sort={$_GET['sort']}";
            switch ($_GET['sort']){
                case 'pn': $order = 'visites'; break;
                case 'ist': $order = 'url'; break;
                case 'visites': $order = 'visites'; break;
                case 'logins': $order = 'auth'; break;
                default: $order = 'visites'; break;
            }
        }else{
            $order = "visites";
        }

        ?>

        <table cellpadding='3' cellspacing='0' border='0' bordercolor='#336633' align='center' width="99%">
            <tr>
                <td style="border: 1px dashed #db8;" align="center" class="m-tb"><a style="color: black" >п.н.</a></td>
                <td style="border: 1px dashed #db8;" align="center" class="m-tb"><a style="color: black" >Источник</a></td>
                <td style="border: 1px dashed #db8;" align="center" class="m-tb"><a style="color: black" >Переходы</a></td>
                <td style="border: 1px dashed #db8;" align="center" class="m-tb"><a style="color: black" >Авторизации</a></td>
                <td style="border: 1px dashed #db8;" align="center" class="m-tb">Параметры</td>
            </tr>
            <?PHP

            $db->Query("SELECT COUNT(DISTINCT url) FROM db_visits {$dpsql}");
            $max_num2 = $db->FetchRow();

            $pn2 = $max_num2 - $lim;

            $db->Query("SELECT COUNT(db_visits.id) visites, COUNT(DISTINCT ref_id) users, ABS(COUNT(DISTINCT db_visits.reg)-1) auth ,db_visits.id, page, reg, ref_id, INET_NTOA(db_visits.ip) AS ip, cnt, url, date, user FROM db_visits LEFT JOIN db_users_a ON db_users_a.id = db_visits.ref_id {$dpsql} GROUP BY url ORDER BY {$order} DESC LIMIT {$lim}, 100");

            while ($data = $db->FetchArray()) {

                ?>
                <tr class="htt">
                    <td style="border: 1px dashed #db8;" align="center"><?=$pn2--;?></td>
                    <td style="border: 1px dashed #db8;" class="clip" align="center"><a href="http://<?=$data['url']?>" target="_blank"><?=$data['url']?></a></td>
                    <td style="border: 1px dashed #db8;" align="center"><?=$data['visites']?></td>
                    <td style="border: 1px dashed #db8;" align="center"><?=$data['auth']?></td>
                    <td style="border: 1px dashed #db8;" align="center"><a href="/referals/visits/url/<?=$data['url'].$dpget;?>">подробнее</a></td>
                </tr>
                <?PHP
                $pn++;
            }

            ?>
        </table>

        <?php
        $db->Query("SELECT COUNT(DISTINCT url) FROM db_visits {$dpsql}");
        $all_pages = $db->FetchRow();

        if($all_pages > 100){

            $sort_b = (isset($_GET["sort"])) ? intval($_GET["sort"]) : 0;

            $nav = new navigator;
            $page = (isset($_GET["page"]) AND intval($_GET["page"]) < 1000 AND intval($_GET["page"]) >= 1) ? (intval($_GET["page"])) : 1;

            echo "<BR /><center>".$nav->Navigation(10, $page, ceil($all_pages / 100), "/?menu=admin4ik&sel=visits&show={$_GET['show']}{$dpget}&page="), "</center>";

        }
        ?>

    <? }else ?>

</div>
<div class="clr"></div>