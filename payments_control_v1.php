<?php
header('Content-Type: application/json');

#error_reporting(E_ALL);
#ini_set('display_errors', 1);

# Старт сессии
session_start();

$out = array();

if(!empty($_POST)){

    if(!isset($_SESSION['admin'])) exit('NO SUCCESS');
    if(!isset($_SESSION['user_id'])) exit('NO AUTH');

    # Автоподгрузка классов
    function __autoload($name){ include("classes/_class233r." . $name . ".php"); }

    # Класс конфига
    $config = new config;

    # Функции
    $func = new func;

    # База данных
    $db = new db($config->HostDB, $config->UserDB, $config->PassDB, $config->BaseDB);

    # Настройки
    $sonfig_site = $func->config_site($db);

    $out_array = array();

    $db->Query("SET CHARACTER SET utf8");

    #echo '<pre>'.print_r($_POST,true).'</pre><br/>';

    $payment_type = intval($_POST['pt']);
    $user_id = intval($_POST['user_id']);
    $payment_id = intval($_POST['id']);
    if(isset($_POST['yes'])) $set = 1; else $set = 0;
    $md = strval($_POST['hash']);
    $nmd = md5($user_id.$payment_id);

    if($md == $nmd){

        if($payment_type == 1){

            $db->Query("SELECT * FROM db_payments WHERE id = {$payment_id} AND status = 0 LIMIT 1");
            if($db->NumRows() == 0) exit('ERROR:paymentID');
            $payment_data = $db->FetchAssoc();

            if($set == 1){

                $db->Query("SELECT * FROM db_payment_system WHERE pid = {$payment_data['pay_sys_id']} AND enable = 1 LIMIT 1");
                if($db->NumRows() == 0) exit('ERROR:psID');
                $ps_data = $db->FetchArray();

                $payeer = new rfs_payeer($config->AccountNumber, $config->apiId, $config->apiKey);
                if (!$payeer->isAuth()) exit('ERROR:payeerAuth');
                #$arBalance = $payeer->getBalance();
                #if ($arBalance["auth_error"] != 0) exit('ERROR:payeerBalance');
                #$balance = $arBalance["balance"]["RUB"]["DOSTUPNO"];
                #if ($balance < $payment_data['sum']) exit('ERROR:NoMoney');

                $pppdata = array(
                    'ps' => $ps_data['pid'],
                    'curIn' => $ps_data['cur_in'],
                    'sumOut' => $payment_data['sum'],
                    'curOut' => $ps_data['cur_out'],
                    'param_ACCOUNT_NUMBER' => $payment_data['purse'],
                    'comment' => iconv('windows-1251', 'utf-8', "Выплата пользователю {$payment_data['user']} с проекта {$config->site_name}")
                );
                if($payment_data['person'] != ''){
                    $pppdata['param_CONTACT_PERSON'] = $payment_data['person'];
                }
                $initOutput = $payeer->initOutput($pppdata);
                if (!$initOutput) exit('ERROR:'.print_r($payeer->getErrors(),true));
                $historyId = $payeer->output();
                if ($historyId == 0) exit('ERROR:nopid');
                $time = time();
                $db->Query("UPDATE db_users_b SET payment_sum = payment_sum + {$payment_data['sum']}, curse = {$payment_data['curse']} WHERE id = {$payment_data['user_id']}");
                $db->Query("UPDATE db_stats SET all_payments = all_payments + {$payment_data['sum']} WHERE id = 1");
                $db->Query("UPDATE db_payments SET status = 1, payment_id = {$historyId}, manual_date = {$time}, manual_admin = {$_SESSION['user_id']} WHERE id = {$payment_id}");
                exit('OK');
            
            }else if($set == 0){

                if ($sonfig_site["payment_limit"] == 1){
                    $db->Query("UPDATE db_users_b SET money_p = money_p + {$payment_data['serebro']}, cash_points = cash_points + {$payment_data['real_sum']} WHERE id = {$payment_data['user_id']}");
                }else{
                    $db->Query("UPDATE db_users_b SET money_p = money_p + {$payment_data['serebro']} WHERE id = {$payment_data['user_id']}");
                }
                $time = time();
                $db->Query("UPDATE db_payments SET status = 2, manual_date = {$time}, manual_admin = {$_SESSION['user_id']} WHERE id = {$payment_data['id']}");
                exit('OK');

            }

        }else if($payment_type == 2){

            $db->Query("SELECT * FROM db_payment_ext WHERE id = {$payment_id} AND status = 0 LIMIT 1");
            if($db->NumRows() == 0) exit('ERROR:paymentID');
            $payment_data = $db->FetchAssoc();

            if($set == 1){

                $payeer_id = 1136053;

                $db->Query("SELECT * FROM db_payment_system WHERE pid = {$payeer_id} AND enable = 1 LIMIT 1");
                if($db->NumRows() == 0) exit('ERROR:psID');
                $ps_data = $db->FetchArray();

                $payeer = new rfs_payeer($config->AccountNumber, $config->apiId, $config->apiKey);
                if (!$payeer->isAuth()) exit('ERROR:payeerAuth');
                #$arBalance = $payeer->getBalance();
                #if ($arBalance["auth_error"] != 0) exit('ERROR:payeerBalance');
                #$balance = $arBalance["balance"]["RUB"]["DOSTUPNO"];
                #if ($balance < $payment_data['sum']) exit('ERROR:NoMoney');

                $pppdata = array(
                    'ps' => $ps_data['pid'],
                    'curIn' => $ps_data['cur_in'],
                    'sumOut' => $payment_data['sum'],
                    'curOut' => $ps_data['cur_out'],
                    'param_ACCOUNT_NUMBER' => $payment_data['purse'],
                    'comment' => iconv('windows-1251', 'utf-8', "Выплата пользователю {$payment_data['user']} с проекта {$config->site_name}")
                );
                if($payment_data['person'] != ''){
                    $pppdata['param_CONTACT_PERSON'] = $payment_data['person'];
                }
                $initOutput = $payeer->initOutput($pppdata);
                if (!$initOutput) exit('ERROR:'.print_r($payeer->getErrors(),true));
                $historyId = $payeer->output();
                if ($historyId == 0) exit('ERROR:nopid');
                $time = time();
                $db->Query("UPDATE db_users_b SET payment_sum = payment_sum + {$payment_data['sum']}, curse = {$payment_data['curse']} WHERE id = {$payment_data['user_id']}");
                $db->Query("UPDATE db_stats SET all_payments = all_payments + {$payment_data['sum']} WHERE id = 1");
                $db->Query("UPDATE db_payment_ext SET status = 1, payment_id = {$historyId}, manual_date = {$time}, manual_admin = {$_SESSION['user_id']} WHERE id = {$payment_id}");
                exit('OK');

            }else if($set == 0){

                if ($sonfig_site["payment_limit"] == 1){
                    $db->Query("UPDATE db_users_b SET money_r = cash_points = cash_points + {$payment_data['real_sum']} WHERE id = {$payment_data['user_id']}");
                }else{
                    $db->Query("UPDATE db_users_b SET money_r = money_r + {$payment_data['real_sum']} WHERE id = {$payment_data['user_id']}");
                }
                $time = time();
                $db->Query("UPDATE db_payment_ext SET status = 2, manual_date = {$time}, manual_admin = {$_SESSION['user_id']} WHERE id = {$payment_data['id']}");
                exit('OK');

            }

        }

    }else exit('HASH ERROR '."{$md} == {$nmd}");

}else{
    exit('NO PARAM');
}

?>