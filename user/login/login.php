<!DOCTYPE html>
<html lang="en">
<head>
	<title>FootFarm | Авторизация</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/user/login/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/user/login/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/user/login/fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/user/login/vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="/user/login/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/user/login/vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/user/login/vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="/user/login/vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/user/login/css/util.css">
	<link rel="stylesheet" type="text/css" href="/user/login/css/main.css?10">
<!--===============================================================================================-->
</head>
<body>
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<form action="" method="post" class="login100-form validate-form">

					<span class="login100-form-title p-b-34 p-t-27">
						Авторизация
					</span>
					
					<?
						ini_set( 'session.cookie_domain', '.footfarm.com' );
						session_set_cookie_params (0, '/', '.footfarm.com');
						setcookie('PHPSESSID', session_id(), 0, '/', '.footfarm.com');
						if (isset($_POST["log_email"])) {
			
							$lmail = $func->IsMail($_POST["log_email"]);

							if ($lmail !== false) {

								$db->Query("SELECT id, user, pass, referer_id, banned FROM db_users_a WHERE email = '$lmail'");
								if ($db->NumRows() == 1) {

									$log_data = $db->FetchArray();

									if (strtolower($log_data["pass"]) == strtolower($_POST["pass"])) {

										if ($log_data["banned"] == 0) {

											# Настройки
											$sonfig_site = $func->config_site($db, $log_data["id"]);

											$sqlquery = "SELECT (SELECT COUNT(id) FROM db_users_c WHERE referer1_id = {$log_data["id"]}) level_1";
											for($i=1;$i<=$sonfig_site["referals_level"];$i++){
												$sqlquery .= ", (SELECT COUNT(id) FROM db_users_c WHERE referer{$i}_id = {$log_data["id"]}) level_{$i}";
											}

											$db->Query($sqlquery);
											$refs = $db->FetchArray();

											for($i=1;$i<=$sonfig_site["referals_level"];$i++){
												$all_referals += $refs['level_'.$i];
											}
											$user_ip = $func->UserIP;
											$time = time();
											$session_id = session_id();
											$db->Query("UPDATE db_users_a SET date_login = '{$time}', referals = {$all_referals}, session_id = '{$session_id}' WHERE id = {$log_data["id"]}");

											if(!isset($_SESSION['refsale_enable'])){
												$sonfig_site = $func->config_site($db,$log_data["id"]);
												$_SESSION['refsale_enable'] = $sonfig_site['refsale_enable'];
											}

											# Записываем историю входов в аккаунт
											$db->Query("INSERT INTO db_login SET user_id = {$log_data["id"]}, ip = INET_ATON('{$user_ip}'), date = {$time}");
											# Количество строк, которое нужно оставить
											$num_row = 10;
											# Удаляем все кроме $num_row строк из таблицы
											$db->Query("DELETE FROM db_login WHERE user_id = {$log_data["id"]} AND id NOT IN(SELECT id FROM (SELECT id FROM db_login WHERE user_id = {$log_data["id"]} ORDER BY id DESC LIMIT {$num_row}) AS t)");

											$_SESSION['user_ip'] = $user_ip;
											$_SESSION['country'] = geodataISO($user_ip);
											$_SESSION["user_id"] = $log_data["id"];
											$_SESSION["user"] = $log_data["user"];
											$_SESSION["referer_id"] = $log_data["referer_id"];
											//$_COOKIE['user_id'] = $log_data['id'];
											setcookie("user_id", $log_data['id'], time()+(60*60*24*30), "/", "footfarm.com", false, false);
											Header("Location: /account");
											//header( "Location: http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] );

										} else echo "<div class='login100-form-error p-b-34'>Аккаунт заблокирован</div>";

									} else echo "<div class='login100-form-error p-b-34'>Email и/или Пароль указан неверно</div>";

								} else echo "<div class='login100-form-error p-b-34'>Указанный Email не зарегистрирован в системе</div>";

							} else echo "<div class='login100-form-error p-b-34'>Email указан неверно</div>";
						}
						?>

					<div class="wrap-input100 validate-input" data-validate = "Введите ваш Email">
						<input class="input100" type="email" name="log_email" placeholder="Email">
						<span class="focus-input100" data-placeholder="&#xf207;"></span>
					</div>

					<div class="wrap-input100 validate-input" data-validate="Введите ваш пароль">
						<input class="input100" type="password" name="pass" placeholder="Пароль">
						<span class="focus-input100" data-placeholder="&#xf191;"></span>
					</div>

					<div class="container-login100-form-btn">
						<button class="login100-form-btn">
							Войти
						</button>
					</div>

					<div class="text-center p-t-15">
						<a class="txt1" href="/recovery">
							Восстановить пароль
						</a>
						/
						<a class="txt1" href="/signup">
							Зарегистрироваться
						</a>
					</div>
				</form>
			</div>
		</div>
	</div>
	

	<div id="dropDownSelect1"></div>
	
<!--===============================================================================================-->
	<script src="/user/login/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="/user/login/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="/user/login/vendor/bootstrap/js/popper.js"></script>
	<script src="/user/login/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="/user/login/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="/user/login/vendor/daterangepicker/moment.min.js"></script>
	<script src="/user/login/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="/user/login/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="/user/login/js/main.js"></script>

</body>
</html>