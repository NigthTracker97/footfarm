<?

if (isset($_SESSION["user_id"])) {
    Header("Location: /account");
    return;
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>FootFarm | Авторизация</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/user/login/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/user/login/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/user/login/fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/user/login/vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="/user/login/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/user/login/vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/user/login/vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="/user/login/vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/user/login/css/util.css">
	<link rel="stylesheet" type="text/css" href="/user/login/css/main.css?10">
<!--===============================================================================================-->
<script src='https://www.google.com/recaptcha/api.js'></script>
</head>
<body>
	<div class="limiter">
		<div class="container-login100" >
			<div class="wrap-login100">
				<form action="" method="post" class="login100-form validate-form">

					<span class="login100-form-title p-b-34 p-t-27">
						Восстановление пароля
					</span>
					
					<?
						 if (isset($_POST["email"])){

							if (isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response'])) {
								//your site secret key
								$secret = '6LdUG1cUAAAAAOKrHCM3KhDlc1Pi937HwCVIHqC9';
								//get verify response data
								$verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
								$responseData = json_decode($verifyResponse);
								if($responseData->success == 1){

									$email = $func->IsMail($_POST["email"]);
									$time = time();
									$tdel = $time + 60 * 15;

									if ($email !== false){

										$db->Query("DELETE FROM db_recovery WHERE date_del < '$time'");
										$db->Query("SELECT COUNT(*) FROM db_recovery WHERE ip = INET_ATON('" . $func->UserIP . "') OR email = '$email'");
										if ($db->FetchRow() == 0){

											$db->Query("SELECT id, user, email, pass FROM db_users_a WHERE email = '$email'");
											if ($db->NumRows() == 1){
												$db_q = $db->FetchArray();

												# Вносим запись в БД
												$db->Query("INSERT INTO db_recovery (email, ip, date_add, date_del) VALUES ('$email',INET_ATON('" . $func->UserIP . "'),'$time','$tdel')");

												# Отправляем пароль
												$sender = new isender;
												$sender->RecoveryPassword($db_q["email"], $db_q["pass"], $db_q["email"]);

												echo "<div class='login100-form-error p-b-34'>Данные для входа отправлены на Email</div>";

											} else echo "<div class='login100-form-error p-b-34'>Пользователь с таким Email не зарегистрирован</div>";

										} else echo "<div class='login100-form-error p-b-34'>На Ваш Email или IP уже был отправлен пароль за последние 15 минут</div>";

									} else echo "<div class='login100-form-error p-b-34'>Email указан неверно</div>";
									
								} else echo "<div class='login100-form-error p-b-34'>Символы с картинки введены неверно</div>";

							} else echo "<div class='login100-form-error p-b-34'>Символы с картинки введены неверно</div>";

						}
					?>

					<div class="wrap-input100 validate-input" data-validate = "Введите ваш Email">
						<input class="input100" type="email" name="email" placeholder="Email">
						<span class="focus-input100" data-placeholder="&#xf15a;"></span>
					</div>
					
					<div class="g-recaptcha" data-sitekey="6Lc0a0YUAAAAAGXG8PrcQlBh-3pi-v2ueYwvY9oE"></div>

					<div class="container-login100-form-btn p-t-15">
						<button class="login100-form-btn">
							Восстановить пароль
						</button>
					</div>
					
					<div class="text-center p-t-15">
						<a class="txt1" href="/login">
							Авторизоваться
						</a>
						/
						<a class="txt1" href="/signup">
							Зарегистрироваться
						</a>
					</div>
				</form>
			</div>
		</div>
	</div>
	

	<div id="dropDownSelect1"></div>
	
<!--===============================================================================================-->
	<script src="/user/login/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="/user/login/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="/user/login/vendor/bootstrap/js/popper.js"></script>
	<script src="/user/login/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="/user/login/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="/user/login/vendor/daterangepicker/moment.min.js"></script>
	<script src="/user/login/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="/user/login/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="/user/login/js/main.js"></script>

</body>
</html>