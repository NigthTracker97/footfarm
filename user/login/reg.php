<?
if (isset($_SESSION["user_id"])) {
    Header("Location: /account");
    return;
}

// Регистрация
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>FootFarm | Регистрация</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/user/login/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/user/login/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/user/login/fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/user/login/vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="/user/login/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/user/login/vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/user/login/vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="/user/login/vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/user/login/css/util.css">
	<link rel="stylesheet" type="text/css" href="/user/login/css/main.css?9">
<!--===============================================================================================-->

<script src='https://www.google.com/recaptcha/api.js'></script>
</head>
<body>
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<form action="" method="post" class="login100-form validate-form">

					<span class="login100-form-title p-b-34 p-t-27">
						Регистрация
					</span>
					
					<?
						if (isset($_POST["login"])) {
							if (isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response'])) {
								//your site secret key
								$secret = '6LdUG1cUAAAAAOKrHCM3KhDlc1Pi937HwCVIHqC9';
								//get verify response data
								$verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
								$responseData = json_decode($verifyResponse);
								if($responseData->success == 1){
								    $leader = $db->FetchArray();
									$db->query("SELECT * FROM db_leader");
									$referer_id = (isset($_COOKIE["i"]) AND intval($_COOKIE["i"]) > 0 AND intval($_COOKIE["i"]) < 1000000) ? intval($_COOKIE["i"]) : 1;
									$referer_name = "";

									if ($referer_id != 1) {
										$db->Query("SELECT user FROM db_users_a WHERE id = '$referer_id' LIMIT 1");
										if ($db->NumRows() > 0) {
											$referer_name = $db->FetchRow();
										}
										else {
											$referer_id = $leader["user_id"];
											$referer_name = $leader["user"];
										}
									}
									else {
										$referer_id = $leader["user_id"];
										$referer_name = $leader["user"];
									}
									$login = $func->IsLogin($_POST["login"]);
									$pass = $func->IsPassword($_POST["pass"]);
									$rules = isset($_POST["rules"]) ? true : false;
									$time = time();
									$ip = $func->UserIP;
									$ipregs = $db->Query("SELECT * FROM `db_users_a` WHERE INET_NTOA(db_users_a.ip) = '$ip' ");
									$ipregs = $db->NumRows();
									$ipregs = 0;
									$email = $func->IsMail($_POST["email"]);
									if ($email !== false) {
										$db->Query("SELECT id FROM db_users_a WHERE email = '{$email}' LIMIT 1");
										if ($db->NumRows() > 0) $email = false;
									}

									if ($rules) {
										if ($ipregs == 0 || isset($_SESSION['admin4ik'])) {
											if ($email !== false) {
												if ($login !== false) {
													if ($pass !== false) {
														if ($pass == $_POST["repass"]) {
															$db->Query("SELECT COUNT(*) FROM db_users_a WHERE user = '$login'");
															if ($db->FetchRow() == 0) {

																// Продажа рефералов
																
																$db->Query("SELECT * FROM db_leader");   $leader = $db->FetchArray();
																$sr_referer_id = (isset($_COOKIE["rs"]) AND intval($_COOKIE["rs"]) > 0 AND intval($_COOKIE["rs"]) < 1000000) ? intval($_COOKIE["rs"]) : 0;
																$rs_referer_money = 0;
																$rs_referer_mtype = 0;
																/*if ($sr_referer_id > 0) {
																	$user_pconfig = $func->config_site($db, $sr_referer_id);
																	if ($user_pconfig['refsale_enable'] == 1) {
																		$rs_referer_money = $user_pconfig['refsale_price'];
																		$rs_referer_mtype = $user_pconfig['refsale_mtype'];
																		$referer_id = $leader["user_id"];
																		$referer_name = $leader["user"];

																		// Зачисляем вознаграждение пользователю

																		$paymeny_money_type = array(
																			'0' => 'money_b',
																			'1' => 'money_p',
																			'2' => 'money_r',
																			'3' => 'cash_points'
																		);
																		$db->Query("UPDATE db_users_b SET {$paymeny_money_type[$rs_referer_mtype]} = {$paymeny_money_type[$rs_referer_mtype]} + {$rs_referer_money} WHERE id = {$sr_referer_id}");
																	}
																}*/

																// Регаем пользователя

																$db->Query("INSERT INTO db_users_a (user, email, pass, referer, referer_id, date_reg, ip, come_url, rs_referer_id, rs_referer_money, rs_referer_mtype) 
																VALUES ('$login','{$email}','$pass','$referer_name','$referer_id','$time',INET_ATON('$ip'),'{$_SESSION['come_url']}', {$sr_referer_id}, {$rs_referer_money}, {$rs_referer_mtype})");
																
																$lid = $db->LastInsert();
																if (isset($_POST['email_send'])) {
																	$db->Query("UPDATE db_users_a SET spam = 1 WHERE id = {$lid}");
																}

																$db->Query("INSERT INTO db_users_b (id, user, last_sbor) VALUES ('$lid','$login', '" . time() . "')");

																// Собираем информаци о рефералах до 6го уровня

																$db->Query("SELECT id, user, referer_id FROM db_users_a WHERE id = {$referer_id}");
																$r[1] = $db->FetchArray();
																for ($i = 1; $i <= 5; $i++) {
																	if (!empty($r[$i]['id'])) {
																		$db->Query("SELECT id, user, referer_id FROM db_users_a WHERE id = {$r[$i]['referer_id']}");
																		if ($db->NumRows() > 0) $r[$i + 1] = $db->FetchArray();
																	}
																}

																$sql_string = '';
																foreach($r as $level => $data) {
																	if ($data['id'] > 0) $sql_string.= "referer{$level}_id = {$data['id']}, referer{$level} = '{$data['user']}', ";
																	$db->query("UPDATE db_users_c SET count_ref{$level} = count_ref{$level} + 1 WHERE id = {$data['id']}");
																}

																$db->Query("INSERT INTO db_users_c SET {$sql_string} id = {$lid}");

																// Добавляем бонус при регистрации

																$conf = $func->config_site($db);
																$regbonus = $conf['regbonus'];
																if ($regbonus > 0) {
																	$db->query("UPDATE db_users_b SET money_b = money_b + {$regbonus} WHERE id = {$lid}");
																}

																// Вставляем статистику

																$db->Query("UPDATE db_stats SET all_users = all_users +1, today_users = today_users + 1 WHERE id = '1'");

																// Добавляем +1 к количеству рефералов у реферера

																$db->Query("UPDATE db_users_a SET referals = referals + 1 WHERE id = {$referer_id}");
																echo "<center><b><font color = 'green'>Вы успешно зарегистрировались.</div>";
																if (true) {
																	$db->Query("SELECT id, user, referer_id, pass, email FROM db_users_a WHERE id = {$lid}");
																	$log_data = $db->FetchArray();
																	if (!isset($_SESSION['refsale_enable'])) {
																		$sonfig_site = $func->config_site($db, $log_data["id"]);
																		$_SESSION['refsale_enable'] = $sonfig_site['refsale_enable'];
																	}

																	$_SESSION["user_id"] = $log_data["id"];
																	$_SESSION["user"] = $log_data["user"];
																	$_SESSION["referer_id"] = $log_data["referer_id"];
																	$_SESSION["warning"] = "<b>Ваш логин:</b> {$log_data['email']}<br/><b>Ваш пароль:</b> {$log_data['pass']}";
																	$db->Query("UPDATE db_users_a SET date_login = '" . time() . "', ip = INET_ATON('" . $func->UserIP . "') WHERE id = '" . $log_data["id"] . "'");
																	echo '<meta http-equiv="refresh" content="1;URL=/account" />';
																}

																return;
															}
															else echo "<div class='login100-form-error p-b-34'>Указанный логин уже используется</div>";
														}
														else echo "<div class='login100-form-error p-b-34'>Пароль и повтор пароля не совпадают</div>";
													}
													else echo "<div class='login100-form-error p-b-34'>Пароль заполнен неверно</div>";
												}
												else echo "<div class='login100-form-error p-b-34'>Логин заполнен неверно</div>";
											}
											else echo "<div class='login100-form-error p-b-34'>Email имеет неверный формат или уже зарегистрирован в системе</div>";
										}
										else echo "<div class='login100-form-error p-b-34'>Регистрация с этого IP уже производилась</div>";
									}
									else echo "<div class='login100-form-error p-b-34'>Вы не подтвердили правила</div>";
								}
								else echo "<div class='login100-form-error p-b-34'>Каптча введена неверно</div>";
							}
							else echo "<div class='login100-form-error p-b-34'>Каптча введена неверно</div>";
						}
						?>

					<div class="wrap-input100 validate-input" data-validate = "Придумайте логин">
						<input class="input100" type="text" name="login" placeholder="Логин">
						<span class="focus-input100" data-placeholder="&#xf207;"></span>
					</div>
					
					<div class="wrap-input100 validate-input" data-validate = "Введите ваш Email">
						<input class="input100" type="email" name="email" placeholder="Email">
						<span class="focus-input100" data-placeholder="&#xf15a;"></span>
					</div>

					<div class="wrap-input100 validate-input" data-validate="Введите ваш пароль">
						<input class="input100" type="password" name="pass" placeholder="Пароль">
						<span class="focus-input100" data-placeholder="&#xf191;"></span>
					</div>

					<div class="wrap-input100 validate-input" data-validate="Введите ваш пароль">
						<input class="input100" type="password" name="repass" placeholder="Повторите пароль">
						<span class="focus-input100" data-placeholder="&#xf191;"></span>
					</div>
					
					<div class="contact100-form-checkbox">
						<input class="input-checkbox100" id="ckb1" type="checkbox" name="rules">
						<label class="label-checkbox100" for="ckb1">
							С <a href="/rules" target="_blank" style="border-bottom: none;">правилами</a> проекта ознакомлен(а) и принимаю:
						</label>
					</div>
					
					<div class="g-recaptcha" data-sitekey="6LdUG1cUAAAAAF7zxzpZN6WpOZpPF8F6Ls-cQboD"></div>
					
					<div class="container-login100-form-btn p-t-15">
						<button class="login100-form-btn">
							Зарегистрироваться
						</button>
					</div>

					<div class="text-center p-t-15">
						<a class="txt1" href="/login">
							Авторизоваться
						</a>
					</div>
				</form>
			</div>
		</div>
	</div>
	

	<div id="dropDownSelect1"></div>
	
<!--===============================================================================================-->
	<script src="/user/login/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="/user/login/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="/user/login/vendor/bootstrap/js/popper.js"></script>
	<script src="/user/login/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="/user/login/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="/user/login/vendor/daterangepicker/moment.min.js"></script>
	<script src="/user/login/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="/user/login/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="/user/login/js/main.js"></script>

</body>
</html>