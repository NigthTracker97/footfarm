<?

#ini_set("display_errors",1);
#error_reporting(E_ALL);

# Старт сессии
@session_start();

if (!isset($_SESSION['user_id'])) exit('not auth');

# Константа для Include
define("CONST_RUFUS", true);
# Автоподгрузка классов
function __autoload($name)
{
    include("classes/_class233r." . $name . ".php");
}

# Класс конфига
$config = new config;
# Подключение к базе данных
$db = new db($config->HostDB, $config->UserDB, $config->PassDB, $config->BaseDB);
# Функции
$func = new func;

# настройки АПИ вконтакте
$response_type = 'code';

# ВК АПИ
$vk = new vk($config->vk_id, $config->vk_key, $config->vk_redirect_uri);

$db->Query('SET CHARACTER SET utf8');
$db->Query("set names utf8");

if (isset($_GET['code'])) {
    if ($vk->access_token_get($_GET['code'])) {
        $user_id = $_SESSION['user_id'];
        $vk_user_id = intval($vk->user_id);
        # проверяем есть ли запись об этом пользователе в базе
        $state = intval($_GET['state']);
        $db->Query("SELECT vk_user_id FROM db_users_a WHERE id = {$user_id}");
        if ($db->FetchRow() == 0) {
            $db->Query("SELECT id FROM db_users_a WHERE vk_user_id = {$vk_user_id}");
            if($db->NumRows() == 0){
                if ($vk->user_in_groupT($vk_user_id, $config->vk_group_id)) {
                    if ($state == 0) {
                        $vk_user_data = $vk->user_data();
                        $vk_first_name = $vk_user_data->response[0]->first_name;
                        $vk_last_name = $vk_user_data->response[0]->last_name;
                        $vk_bdate = $vk_user_data->response[0]->bdate;
                        $date_reg = time();
                        $db->Query("UPDATE db_users_a SET 
							vk_user_id 	    = '{$vk_user_id}',
							vk_first_name 	= '{$vk_first_name}', 
							vk_last_name 	= '{$vk_last_name}', 
							vk_bdate 	    = '{$vk_bdate}',
							vk_dateadd 	    = '{$date_reg}'
							WHERE id = {$user_id}
						");
                        # зачисляем собственно сам бонус и отправляем пользователю уведомление
                        $db->Query("UPDATE db_users_b SET money_b = money_b + 250 WHERE id = {$user_id}");

                        header("Location: /farm/vkbonus/ok");
                    } else header("Location: /farm/vkbonus/error1");
                } else header("Location: /farm/vkbonus/error2");
            } else header("Location: /farm/vkbonus/error3");
        } else header("Location: /farm/vkbonus/error4");

        /*
        if($res = $vk->user_in_repost($vk_user_id,3573)){
            echo "repost:yes";
        }else{
            echo "repost:no";
        }
        */
        #header("Location: /?vk_res=error_bonus1");

    } else {
        header("Location: /farm/vkbonus/error4");
        #header("Location: /?vk_res=error_auth&error_text={$vk->error_text}");
    }
} else {
    $dp = '';
    if (isset($_GET['bonus'])) {
        $dp = 'state=' . $_GET['bonus'];
    } else {
        $dp = 'state=0';
    }
    $vk->get_auth($dp);
}


?>